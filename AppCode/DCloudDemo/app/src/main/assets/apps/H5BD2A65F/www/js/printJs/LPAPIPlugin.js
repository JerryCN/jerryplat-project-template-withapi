/**
 * Created by HuDianxing on 2019-03-25.
 *
 * 德佟电子标签打印机 DCloud异步接口。
 *
 * Copyright(C) 2011~2019, 上海道臻信息技术有限公司
 *
 */
document.addEventListener("plusready", function () {
	var service = 'LPAPI';
	var B = window.plus.bridge;

	window.plus.LPAPI = {
		exec: function (action, data, success, fail) {
			success = typeof success === "function" ? success : null;
			fail = typeof fail === "function" ? fail : null;
			if (typeof action !== "string" || typeof data != "object") {
				if (fail)
					fail('参数错误');
				return false;
			}

			return B.exec(service, action, [B.callbackId(success, fail), JSON.stringify(data)])
		},
		/**
		 * 以字符串的形式返回已经安装过的所有打印机名称，不同打印机名称间以英文","分隔。
		 * @param {object} data 输入参数，制定需要获取打印机的型号,可以不指定，表示获取所有打印机；
		 *		{
		 *			name,	// 获取制定型号的打印机；
		 *		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 * @return {String} name 表示打印机型号，如果指定了型号，只会获取制定型号的打印机，多个型号可以用';'分号进行分割；
		 */
		getPrinters: function (data, success, fail) {
			// 用户可以不指定data，直接指定回调；
			if (typeof data === "function") {
				fail = success;
				success = data;
				data = '';
			}

			data = typeof data === "object" ? data : { name: data };
			this.exec('getAllPrinters', data, (value) => {
				var items = value;
				if (plus.os.name == 'iOS') {
					items = [];
					value = value.split(',');
					if (value) {
						for (var i = 0; i < value.lenght; i++) {
							items.push({ shownName: value[i] });
						}
					}
				}
				if (typeof success === "function") {
					success(items);
				}
			}, fail);
		},
        /**
         * 获取检测到的第一台打印机；
		 * @param {object} data 输入参数，制定需要获取打印机的型号,可以不指定，表示获取所有打印机；
		 *		{
		 *			name,	// 获取制定型号的打印机；
		 *		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 * @return {String} name 表示打印机型号，如果指定了型号，只会获取制定型号的打印机，多个型号可以用';'分号进行分割；
         */
		getFirstPrinter: function (data, success, fail) {
			// 用户可以不指定data，直接指定回调；
			if (typeof data === "function") {
				fail = success;
				success = data;
				data = '';
			}

			data = typeof data === "object" ? data : { name: data };
			this.exec('getFirstPrinter', data, success, fail);
		},
        /**
         * 打开指定名称的打印机(异步调用)。
         * @param {object} data 需要连接的目标打印机名称，如果不指定则连接检测到的第一台打印机，
         *		{
         *			name,	// 指定的打印机名称、型号、MAC地址等；
         *            		// 打印机名称。打印机名称类型：
         *            		// 1、空字符串：打开当前客户端系统上的第一个支持的打印机；
         *            		// 2、打印机型号：例如：DT20S；
         *            		// 3、打印机名称：例如：DT20S-60901687;
         *            		// 4、MAC地址：打开指定地址的打印机，例如：00:18:E4:0C:68:CA。
         *		}
         * @param {function} success 接口调用成功回调函数；
         * @param {function} fail 接口调用失败回调函数；
         */
		openPrinter: function (data, success, fail) {
			// 用户可以不指定data，直接指定回调；
			if (typeof data === "function") {
				fail = success;
				success = data;
				data = '';
			}

			data = typeof data === "object" ? data : { name: (data || '') };
			this.exec('openPrinter', data, success, fail);
		},
        /**
         * 得到当前使用的打印机名称。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         * 		如果已连接打印机，则返回当前使用的打印机名称，否则返回空字符串。
         */
		getPrinterName: function (success, fail) {
			this.exec('getPrinterName', {}, success, fail);
		},
		/**
		 * 获取当前已连接的打印机的MAC地址；
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 */
		getPrinterAddress: function (success, fail) {
			return this.exec('getPrinterAddress', {}, success, fail);
		},
		/**
		 * 获取已打开打印机的详细信心。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 * 注意：该方法只有在打印机已连接的情况下有效；
		 */
		getPrinterInfo: function (success, fail) {
			return this.exec('getPrinterInfo', {}, success, fail);
		},
		/**
		 * 获取当前打印机的连接状态；
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 */
		getPrinterState: function (success, fail) {
			return this.exec('getPrinterState', {}, success, fail);
		},
        /**
         * 判断当前打印机是否打开（连接成功）？
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		isPrinterOpened: function (success, fail) {
			return this.exec('isPrinterOpened', {}, success, fail);
		},
        /**
         * 取消当前的打印操作，用于在提交打印任务后执行取消操作。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		cancel: function (success, fail) {
			return this.exec('cancel', {}, success, fail);
		},
        /**
         * 断开当前打印机的连接。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		closePrinter: function (success, fail) {
			return this.exec('closePrinter', {}, success, fail);
		},
        /**
         * 重新连接上次连接的打印机（异步调用）。
         *
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         * @return 异步连接操作是否成功提交？
         * 		注意：返回成功仅仅表示操作被提交成功，并不代表着连接成功了，具体的连接结果会通过回调函数给出通知。
         */
		reopenPrinter: function (success, fail) {
			this.exec('reopenPrinter', {}, success, fail);
		},
		// /**
		//  * 退出打印相关的所有操作。
		//  * @param {function} success 接口调用成功回调函数；
		//  * @param {function} fail 接口调用失败回调函数；
		//  */
		// quit : function (success, fail) {
		// 		return this.exec('quit', {}, success, fail);
		// },
		/**
		 * 打印图片资源；
		 * @param {object} data, 打印参数如下所示：
		 * 		{
		 * 			image,				// 需要打印的内容，可以为url路径，或者base64字符串；
		 * 			printDirection,		// 打印方向，值可为 0，90，180，270，也可以不指定，默认为0；
		 * 			printCopies,		// 打印份数，值可为任意正整数，也可以不指定，默认为1；
		 * 			printDarkness,		// 打印浓度，值可为0-14，2表示正常打印浓度，可以不指定，默认为0， 表示随打印机设置；
		 * 			printSpeed,			// 打印速度，值为0-4之间，2表示正常打印浓度，可以不指定，默认为0，表示随打印机设置；
		 * 			gapType				// 纸张类型，值为0-3之间，可以不指定，默认为0，表示随打印机设置，具体可参考属性 gapType；
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 */
		printImage: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				image: (data || '')
			};
			return this.exec('printImage', data, success, fail);
		},
		/**
		 * 常用打印参数名称；
		 */
		printParamName: {
			printDirection: "PRINT_DIRECTION",		// 表示打印方向，value值为 0， 90，180，270
			printCopies: "PRINT_COPIES",			// 表示打印份数，value值可以为任意正整数；
			printDarkness: "PRINT_DENSITY",		// 打印浓度，value值为：0-14，5表示正常打印浓度；
			printSpeed: "PRINT_SPEED",				// 打印速度，value值为：0-4，2表示正常打印速度；
			gapType: "GAP_TYPE"					// 纸张类型，value值为：0-3，具体可参考 gapType属性；
		},
		/**
		 * 打印机纸张类型；
		 */
		gapType: {
			GAP_NONE: 0,
			GAP_HOLE: 1,
			GAP_GAP: 2,
			GAP_BLACK: 3
		},
        /***************************************************************************
         * 打印任务的开始，分页，结束等操作。
         **************************************************************************/

        /**
         * 以指定的参数，开始一个打印任务。
         * 
         * @param {object} data 参数属性如下所示：
		 * 		{
		 * 			width,			// 标签宽度（单位mm）。
		 * 			height,			// 标签高度（单位mm）。
		 * 			orientation		// 标签打印方向。0：不旋转；90：顺时针旋转90度；180：旋转180度；270：逆时针旋转90度。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		startJob: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				width: (data || 48),
				height: (data || 48)
			}
			return this.exec('startJob', data, success, fail);
		},
        /**
         * 取消绘图任务，用于取消任务提交前的所有绘制操作。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		abortJob: function (success, fail) {
			return this.exec('abortJob', {}, success, fail);
		},
        /**
         * 结束绘图任务。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		endJob: function (success, fail) {
			return this.exec('endJob', {}, success, fail);
		},
        /**
         * 提交打印数据，进行真正的打印。
		 * @param {object} data 打印任务的相关打印参数，详细属性如下所示：
		 * 		{
		 *			PRINT_DENSITY,				// 打印浓度，value值为：0-14，5表示正常打印浓度；
		 *			PRINT_SPEED,				// 打印速度，value值为：0-4，2表示正常打印速度；
		 *			GAP_TYPE					// 纸张类型，value值为：0-3，具体可参考 gapType属性；
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		commitJob: function (data, success, fail) {
			data = typeof data === "object" ? data : {};
			return this.exec('commitJob', data, success, fail);
		},
        /**
         * 开始一个打印页面。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		startPage: function (success, fail) {
			return this.exec('startPage', {}, success, fail);
		},
        /**
         * 结束一个打印页面。
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		endPage: function (success, fail) {
			return this.exec('endPage', {}, success, fail);
		},
		/**
		 * 获取一个打印任务中的所有打印页面；
		 * 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 */
		getJobPages: function (success, fail) {
			this.exec('getJobPages', {}, success, fail);
		},
        /***************************************************************************
         * 打印参数设置。
         **************************************************************************/

        /**
         * 设置绘制相关的参数值。
         * @param {object} data 参数详细属性如下所示：
		 * 		{
		 * 			name,		// 参数名称，在 drawParamName 中被定义。
		 * 			value		// 参数值，具体含义参考不同的参数名称，具体可参考属性 drawParamName。
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		setDrawParam: function (data, success, fail) {
			this.exec('setDrawParam', data, success, fail);
		},
		drawParamName: {
			/**
			 * 字体文件名称，值为 String。存放在工程 assets 目录中的字体文件名称，如：FONT.ttf。没有指定后缀的情况下，会自动加上 .ttf 后缀。
			 */
			fontName: "FONT_NAME",
			/**
			 * QRCode 的编码版本号（1～40），值为 Integer。未指定时表示根据编码内容自动采用适合的编码版本号。
			 */
			qrcodeVersion: "QRCODE_VERSION",
			/**
			 * QRCode 的纠错级别（0～3），值为 Integer。默认为 ErrorCorrectionLevel.L。
			 */
			errorCorrection: "ERROR_CORRECTION",
			/**
			 * QRCode 的字符串编码类型，值为 String。默认为 UTF-8。
			 */
			characterSet: "CHARACTER_SET",
			/**
			 * 条码的留白，值为 Integer。对于 QRCode，规范中定义的留白值为 0/2/4。默认是 0，也即不留白。
			 */
			margin: "MARGIN"
		},
        /**
         * 得到当前打印动作的顺时针旋转角度。
         *
		 * @param {function} success 接口调用成功回调函数；
		 * 				回调函数格式如下：
		 * 				function (orientation){
		 * 				}
		 * 				orientation 表示当前打印动作的顺时针旋转角度（0，90，180，270）。
		 * @param {function} fail 接口调用失败回调函数；
         */
		getItemOrientation: function (success, fail) {
			this.exec('getItemOrientation', {}, success, fail);
		},
        /**
         * 设置打印动作的旋转角度。
         * 
         * @param {object} data 旋转角度参数，具体属性如下所示：
		 * 		{
		 * 			orientation		// 旋转角度。参数描述如下：
		 * 							// 0：不旋转；
		 * 							// 90：顺时针旋转90度；
		 * 							// 180：旋转180度；
		 * 							// 270：逆时针旋转90度。
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		setItemOrientation: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				orientation: (data || 0)
			};
			this.exec('setItemOrientation', data, success, fail);
		},
        /**
         * 得到当前打印动作的水平对齐方式。
         *
		 * @param {function} success 接口调用成功回调函数；
		 * 				回调函数格式如下：
		 * 				function (alignment){
		 * 				}
		 * 				alignment 后续打印动作的水平对齐方式。水平对齐方式值如下：
         *            		0：水平居左；
         *            		1：水平居中；
         *            		2：水平居右；
		 * @param {function} fail 接口调用失败回调函数；
         */
		getItemHorizontalAlignment: function (success, fail) {
			return this.exec('getItemHorizontalAlignment', {}, success, fail);
		},
        /**
         * 设置打印动作的水平对齐方式。
         *
         * @param {object} data 对齐方式属性如下所示：
		 * 		{
		 * 			alignment 		// 水平对齐方式。参数描述如下：
         *            				// 0：水平居左（默认方式）；
         *            				// 1：水平居中；
         *            				// 2：水平居右。
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		setItemHorizontalAlignment: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				alignment: (data || 0)
			};
			return this.exec('setItemHorizontalAlignment', data, success, fail);
		},
        /**
         * 得到当前打印动作的垂直对齐方式。
         *
		 * @param {function} success 接口调用成功回调函数；
		 * 				回调函数格式如下：
		 * 				function (alignment){
		 * 					
		 * 				}
		 * 				alignment 垂直对齐方式，参数描述如下：
         *            		0：垂直居上（默认方式）；
         *            		1：垂直居中；
         *            		2：垂直居下。
		 * @param {function} fail 接口调用失败回调函数；
         */
		getItemVerticalAlignment: function (success, fail) {
			return this.exec('getItemVerticalAlignment', {}, success, fail);
		},
        /**
         * 设置打印动作的垂直对齐方式。
         * @param {object} data 对齐方式属性如下所示：
		 * 		{
		 * 			alignment 		//垂直对齐方式，参数描述如下：
         *            				// 0：垂直居上（默认方式）；
         *            				// 1：垂直居中；
         *            				// 2：垂直居下。
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		setItemVerticalAlignment: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				alignment: (data || 0)
			};
			return this.exec('setItemVerticalAlignment', data, success, fail);
		},
        /**
         * 得到线条画笔对齐方式。
		 * @param {function} success 接口调用成功回调函数；
		 * 				回调函数格式如下：
		 * 				function (alignment){
		 * 					
		 * 				}
		 * 				alignment线条画笔对齐方式({@link PenAlignment})
		 * @param {function} fail 接口调用失败回调函数；
         */
		getItemPenAlignment: function (success, fail) {
			return this.exec('getItemPenAlignment', {}, success, fail);
		},
        /**
         * 设置线条画笔对齐方式。
         *
         * @param {object} data 设置参数，参数属性如下所示：
		 * 		{
		 * 			alignment		// 线条画笔对齐方式（{@link PenAlignment}）
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		setItemPenAlignment: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				alignment: (data || 0)
			};
			this.exec('setItemPenAlignment', data, success, fail);
		},
		/**
		 * 画笔对齐方式；
		 */
		PenAlignment: {
			CENTER: 0,		// 绘制的线以指定的位置为中央；
			INSET: 1		// 绘制的线在指定的位置内侧。
		},
		/**
		 * 设置当前打印任务的背景色；
		 * 一般在直接打印的时候，建议用白色作为底色打印效果会更好；
		 * 如果想获取一个透明的色的图片，则需要将背景色设置为透明底色；
		 * 其他颜色不建议使用；
		 * @param {object} data 背景色相关参数，具体参数属性如下所示：
		 * 		{
		 * 			color		// 背景色；
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
		 */
		setBackground: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				color: (data || 0xffffff)
			};
			this.exec('setBackground', data, success, fail);
		},
        /***************************************************************************
         * 打印对象的绘制操作。
         **************************************************************************/

        /**
         * 打印文本。
         *
         * @param {object} data 圆角矩形绘制相关参数，具体参数属性如下所示：
		 * 		{
		 *			text,			// 打印内容。
		 * 			x,				// 打印对象的位置(单位mm)。
		 * 			y, 				// 打印对象的位置(单位mm)。
		 * 			width, 			// 打印对象的宽度(单位mm)。
		 * 			height,			// 打印对象的高度(单位mm)。
		 * 			fontHeight,		// 文本的字体高度(单位mm)。
		 * 			fontStyle,		// 文本的字体风格（可按位组合），可以不指定，默认为0（正常）。0：正常；1：粗体；2：斜体；3：粗斜体 ；4：下划线；8：删除线。
		 * 			linespace		// 行间距，默认为0，1表示1倍行间距；
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawText: function (data, success, fail) {
			data = typeof data === "object" ? data : {
				text: (data || '')
			};
			this.exec('drawText', data, success, fail);
		},
        /**
         * 以指定的线宽，打印矩形框。
         *
         * @param {object} data 圆角矩形绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制矩形的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制矩形的左上角垂直位置（单位mm）。
		 * 			width, 			// 绘制矩形的水平宽度（单位mm）。
		 * 			height,			// 绘制矩形的垂直高度（单位mm）。
		 * 			lineWidth		// 矩形框的线宽（单位mm）。矩形框的线宽是向矩形框内部延伸的。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawRectangle: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawRectangle', data, success, fail);
		},
        /**
         * 打印填充的矩形框。
         *
         * @param {object} data 填充矩形绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的填充矩形的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的圆角矩形的左上角垂直位置（单位mm）。
		 * 			width, 			// 绘制的圆角矩形的水平宽度（单位mm）。
		 * 			height			// 绘制的圆角矩形的垂直高度（单位mm）。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		fillRectangle: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('fillRectangle', data, success, fail);
		},
        /**
         * 以指定的线宽，打印圆角矩形框。
         *
         * @param {object} data 圆角矩形绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的圆角矩形的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的圆角矩形的左上角垂直位置（单位mm）。
		 * 			width, 			// 绘制的圆角矩形的水平宽度（单位mm）。
		 * 			height,			// 绘制的圆角矩形的垂直高度（单位mm）。
		 * 			cornerWidth,	// 圆角宽度（单位mm）。
		 * 			cornerHeight,	// 圆角高度（单位mm）。
		 * 			lineWidth		// 圆角矩形框的线宽（单位mm）。圆角矩形框的线宽是向圆角矩形框内部延伸的。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawRoundRectangle: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawRoundRectangle', data, success, fail);
		},
        /**
         * 打印填充的圆角矩形框。
         *
         * @param {object} data 填充的圆角矩形绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的填充的圆角矩形的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的填充的圆角矩形的左上角垂直位置（单位mm）。
		 * 			width, 			// 绘制的填充的圆角矩形的水平宽度（单位mm）。
		 * 			height,			// 绘制的填充的圆角矩形的垂直高度（单位mm）。
		 * 			cornerWidth,	// 圆角宽度（单位mm）。
		 * 			cornerHeight	// 圆角高度（单位mm）。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		fillRoundRectangle: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('fillRoundRectangle', data, success, fail);
		},
        /**
         * 以指定的线宽，打印椭圆/圆。
         *
         * @param {object} data 椭圆绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的椭圆的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的椭圆的左上角垂直位置（单位mm）。
		 * 			width, 			// 绘制的椭圆的水平宽度（单位mm）。
		 * 			height,			// 绘制的椭圆的垂直高度（单位mm）。
		 * 			lineWidth		// 椭圆的线宽（单位mm）。椭圆的线宽是向椭圆内部延伸的。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawEllipse: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawEllipse', data, success, fail);
		},
        /**
         * 打印填充的椭圆/圆。
         *
         * @param {object} data 填充的椭圆绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的填充椭圆的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的填充椭圆的左上角垂直位置（单位mm）。
		 * 			width, 			// 绘制的填充椭圆的水平宽度（单位mm）。
		 * 			height			// 绘制的填充椭圆的垂直高度（单位mm）。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		fillEllipse: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('fillEllipse', data, success, fail);
		},
        /**
         * 以指定的线宽，打印圆。
         *
         * @param {object} data 圆绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的圆的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的圆的左上角垂直位置（单位mm）。
		 * 			radius, 		// 绘制的填圆的半径（单位mm）。
		 * 			lineWidth		// 圆的线宽（单位mm）。圆的线宽是向圆内部延伸的。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawCircle: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawCircle', data, success, fail);
		},
        /**
         * 打印填充的圆。
         *
         * @param {object} data 填充圆绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x,				// 绘制的填充圆的左上角水平位置（单位mm）。
		 * 			y, 				// 绘制的填充圆的左上角垂直位置（单位mm）。
		 * 			radius 			// 绘制的填充圆的半径（单位mm）。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		fillCircle: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			return this.exec('fillCircle', data, success, fail);
		},
        /**
         * 打印线（直线/斜线）。
         *
         * @param {object} data 直线/斜线绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x1,				// 线的起点的水平位置（单位mm）。
		 * 			y1, 			// 线的起点的垂直位置（单位mm）。
		 * 			x2, 			// 线的终点的水平位置（单位mm）。
		 * 			y2,				// 线的终点的垂直位置（单位mm）。
		 * 			lineWidth		// 线宽（单位mm）。线宽是向线的下方延伸的。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawLine: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawLine', data, success, fail);
		},
        /**
         * 打印点划线。
         *
         * @param {object} data 点划线绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			x1,				// 线的起点的水平位置（单位mm）。
		 * 			y1, 			// 线的起点的垂直位置（单位mm）。
		 * 			x2, 			// 线的终点的水平位置（单位mm）。
		 * 			y2,				// 线的终点的垂直位置（单位mm）。
		 * 			lineWidth,		// 线宽（单位mm）。线宽是向线的下方延伸的。
		 * 			dashLen			// 点划线线段长度的数组（单位mm）。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawDashLine: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawDashLine', data, success, fail);
		},
        /**
         * 打印一维条码。
         *
         * @param {object} data 一维码绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			text,		// 需要绘制的一维码的内容。
		 * 			x, 			// 绘制的一维条码的左上角水平位置（单位mm）。
		 * 			y, 			// 绘制的一维条码的左上角垂直位置（单位mm）。
		 * 			width,		// 一维条码的整体显示宽度。
		 * 			height,		// 一维条码的显示高度（包括供人识读文本）。
		 * 			textHeight	// 供人识读文本的高度（单位mm），建议为3毫米。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		draw1DBarcode: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('draw1DBarcode', data, success, fail);
		},
        /**
         * 打印 QRCode 二维码。
         *
         * @param {object} data 二维码绘制相关参数，具体参数属性如下所示：
		 * 		{
		 * 			text,		// 需要绘制的QRCode二维码的内容。
		 * 			x, 			// 绘制的QRCode二维码的左上角水平位置（单位mm）。
		 * 			y, 			// 绘制的QRCode二维码的左上角垂直位置（单位mm）。
		 * 			width		// 绘制的QRCode二维码的水平宽度（单位mm）。
		 * 		} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		draw2DQRCode: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('draw2DQRCode', data, success, fail);
		},
        /**
         * 打印 Pdf417 二维码。
         * 
         * @param {object} data PDF417打印相关参数，参数属性如下：
		 * 			{
		 * 				text,		// 需要绘制的Pdf417二维码的内容。
		 * 				x,			// 绘制的Pdf417二维码的左上角水平位置（单位mm）。
		 * 				y,			// 绘制的Pdf417二维码的左上角垂直位置（单位mm）。
		 * 				width,		// 绘制的Pdf417二维码的水平宽度（单位mm）。
		 * 				height		// 绘制的Pdf417二维码的垂直高度（单位mm）。
		 * 			} 
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		draw2DPdf417: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('draw2DPdf417', data, success, fail);
		},
        /**
         * 打印图片。
         * 
		 * @param {object} data 相关绘制参数，参数属性如下：
		 * 		{
         * 			image,		// 如果iamge为字符串，则表示图片路径，或者base64字符串；
         * 			x,			// 打印对象在水平方向上的位置(单位mm)。
         * 			y, 			// 打印对象在垂直方向上的位置(单位mm)。
         * 			width, 		// 打印对象的宽度(单位mm)。
         * 			height,		// 打印对象的高度(单位mm)。
         * 			threshold	// 绘制位图的灰度阀值。
         *            			//		256 表示绘制灰度图片；
         *            			//		257 表示绘制原色图片；
         *            			//		0～255表示绘制黑白图片，原图颜色>=灰度阀值的点会被认为是白色，而原图颜色<灰度阀值的点会被认为是黑色。默认值为192。
		 * 			
		 * 		}
		 * @param {function} success 接口调用成功回调函数；
		 * @param {function} fail 接口调用失败回调函数；
         */
		drawImage: function (data, success, fail) {
			data = typeof data === "object" ? data : null;
			this.exec('drawImage', data, success, fail);
		}
	};
}, true);
