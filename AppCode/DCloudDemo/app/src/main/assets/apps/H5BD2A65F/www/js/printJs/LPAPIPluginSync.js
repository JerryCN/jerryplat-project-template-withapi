/**
 * Created by HuDianxing on 2019-03-25.
 *
 * 德佟电子标签打印机 DCloud同步接口。
 *
 * Copyright(C) 2011~2019, 上海道臻信息技术有限公司
 *
 */
document.addEventListener("plusready", function () {
	var service = 'LPAPISync';
	var B = window.plus.bridge;

	window.plus.LPAPISync = {
		defaultLineWidth: 0.3,
		defaultRadius: 1.5,
		/**
		 * 以字符串的形式返回已经安装过的所有打印机名称，不同打印机名称间以英文","分隔。
		 * @param {String} name 表示打印机型号，如果指定了型号，只会获取制定型号的打印机，多个型号可以用';'分号进行分割；
		 * @returns {Array} 返回值是一个数组，数组中每一项的属性如下所示：
		 *  	{
		 * 			shownName,		// 打印机名称；
		 * 			macAddress,		// 打印机MAC地址；
		 * 			addressType		// 打印机地址类型；
		 * 		}
		 */
		getPrinters: function (name) {
			var data = typeof name === "object" ? name : { name: name};
			return B.execSync(service, "getAllPrinters", [JSON.stringify(data)]);
		},
        /**
         * 获取检测到的第一台打印机；
		 * @param {String} name 表示打印机型号，如果指定了型号，只会获取制定型号的打印机，多个型号可以用';'分号进行分割；
         */
		getFirstPrinter: function (name) {
			var data = typeof name === "object" ? name : { name: arguments[0] || '' };
			return B.execSync(service, "getFirstPrinter", [JSON.stringify(data)]);
		},
        /**
         * 打开指定名称的打印机(异步调用)。
         * @param {String} name 制定的打印机名称、型号、MAC地址等；
         *            打印机名称。打印机名称类型：
         *            1、空字符串：打开当前客户端系统上的第一个支持的打印机；
         *            2、打印机型号：例如：DT20S；
         *            3、打印机名称：例如：DT20S-60901687;
         *            4、MAC地址：打开指定地址的打印机，例如：00:18:E4:0C:68:CA。
         */
		openPrinter: function (name) {
			var data = typeof name === "object" ? name : { name: (arguments[0] || '') };
			return B.execSync(service, 'openPrinter', [JSON.stringify(data)]);
		},
        /**
         * 得到当前使用的打印机名称。
         *
         * @return 如果已连接打印机，则返回当前使用的打印机名称，否则返回空字符串。
         */
		getPrinterName: function () {
			return B.execSync(service, 'getPrinterName', []);
		},
		/**
		 * 获取当前已连接的打印机的MAC地址；
		 */
		getPrinterAddress: function () {
			return B.execSync(service, 'getPrinterAddress', []);
		},
		/**
		 * 获取已打开打印机的详细信心。
		 * 注意：该方法只有在打印机已连接的情况下有效；
		 * 返回值详细信息如下：
		 *  	{ 			
		 *			deviceName, 		// 设备名称；
		 *			deviceAddress,		// 设备地址；
		 *			deviceWidth,		// 打印头有效宽度，单位像素，如需毫米宽度，可根据dpi进行转换；
		 *			deviceType,			// 设备类型；
		 *			deviceDPI,			// 打印头分辨率；
		 *			deviceAddrType,		// 设备地址类型；
		 * 		}
		 */
		getPrinterInfo: function () {
			return B.execSync(service, 'getPrinterInfo', []);
		},
		/**
		 * 获取当前打印机的连接状态；
		 */
		getPrinterState: function () {
			return B.execSync(service, 'getPrinterState', []);
		},
        /**
         * 判断当前打印机是否打开（连接成功）？
         */
		isPrinterOpened: function () {
			return B.execSync(service, 'isPrinterOpened', []);
		},
        /**
         * 取消当前的打印操作，用于在提交打印任务后执行取消操作。
         */
		cancel: function () {
			B.execSync(service, 'cancel', []);
		},
        /**
         * 断开当前打印机的连接。
         */
		closePrinter: function () {
			B.execSync(service, 'closePrinter', []);
		},
        /**
         * 重新连接上次连接的打印机（异步调用）。
         *
         * @return 异步连接操作是否成功提交？
         * 		注意：返回成功仅仅表示操作被提交成功，并不代表着连接成功了，具体的连接结果会通过回调函数给出通知。
         */
		reopenPrinter: function () {
			return B.execSync(service, 'reopenPrinter', []);
		},
		/**
		 * 打印图片资源；
		 * @param {String} image 需要打印的内容，可以为url路径，或者base64字符串；
		 * @param {number} printDirection 打印方向，值可为 0，90，180，270，也可以不指定，默认为0；
		 * @param {number} printCopies 打印份数，值可为任意正整数，也可以不指定，默认为1；
		 * @param {number} printDarkness 打印浓度，值可为0-14，2表示正常打印浓度，可以不指定，默认为0， 表示随打印机设置；
		 * @param {number} printSpeed 打印书读，值为0-4之间，2表示正常打印浓度，可以不指定，默认为0，表示随打印机设置；
		 * @param {number} gapType 纸张类型，值为0-3之间，可以不指定，默认为0，表示随打印机设置，具体可参考属性 gapType；
		 */
		printImage: function (image, printDirection, printCopies, printDarkness, printSpeed, gapType) {
			var data = typeof image === "object" ? image : {
				image: (image || ''),
				PRINT_DIRECTION: (printDirection || 0),
				PRINT_COPIES: (printCopies || 1),
				PRINT_DENSITY: (printDarkness || 0),
				PRINT_SPEED: (printSpeed || 0),
				GAP_TYPE: (gapType || 0)
			};
			return B.execSync(service, 'printImage', [JSON.stringify(data)]);
		},
		/**
		 * 常用打印参数名称；
		 */
		printParamName: {
			printDirection: "PRINT_DIRECTION",		// 表示打印方向，value值为 0， 90，180，270
			printCopies: "PRINT_COPIES",			// 表示打印份数，value值可以为任意正整数；
			printDarkness: "PRINT_DENSITY",		// 打印浓度，value值为：0-14，5表示正常打印浓度；
			printSpeed: "PRINT_SPEED",				// 打印速度，value值为：0-4，2表示正常打印速度；
			gapType: "GAP_TYPE"					// 纸张类型，value值为：0-3，具体可参考 gapType属性；
		},
		/**
		 * 打印机纸张类型；
		 */
		gapType: {
			GAP_NONE: 0,
			GAP_HOLE: 1,
			GAP_GAP: 2,
			GAP_BLACK: 3
		},
        /***************************************************************************
         * 打印任务的开始，分页，结束等操作。
         **************************************************************************/

        /**
         * 以指定的参数，开始一个打印任务。
         * 
         * @param width
         *            标签宽度（单位mm）。
         * @param height
         *            标签高度（单位mm）。
         * @param orientation
         *            标签打印方向。0：不旋转；90：顺时针旋转90度；180：旋转180度；270：逆时针旋转90度。
         * @return 成功与否？
         */
		startJob: function (width, height, orientation) {
			var data = typeof width === "object" ? width : {
				width: (width || 48),
				height: (height || 48),
				orientation: (orientation || 0)
			}
			return B.execSync(service, 'startJob', [JSON.stringify(data)]);
		},
        /**
         * 取消绘图任务，用于取消任务提交前的所有绘制操作。
         */
		abortJob: function () {
			return B.execSync(service, 'abortJob', []);
		},
        /**
         * 结束绘图任务。
         */
		endJob: function () {
			return B.execSync(service, 'endJob', []);
		},
        /**
         * 提交打印数据，进行真正的打印。
         */
		commitJob: function () {
			return B.execSync(service, 'commitJob', []);
		},
        /**
         * 开始一个打印页面。
         */
		startPage: function () {
			return B.execSync(service, 'startPage', []);
		},
        /**
         * 结束一个打印页面。
         */
		endPage: function () {
			return B.execSync(service, 'endPage', []);
		},
		/**
		 * 获取一个打印任务中的所有打印页面；
		 */
		getJobPages: function () {
			return B.execSync(service, 'getJobPages', []);
		},
        /***************************************************************************
         * 打印参数设置。
         **************************************************************************/

        /**
         * 设置绘制相关的参数值。
         *
         * @param name
         *            参数名称，在 drawParamName 中被定义。
         * @param value
         *            参数值，具体含义参考不同的参数名称，具体可参考属性 drawParamName。
         */
		setDrawParam: function (name, value) {
			var data = typeof name === "object" ? name : {
				name: (name || ''),
				value: (value || 0)
			}
			return B.execSync(service, 'setDrawParam', [JSON.stringify(data)]);
		},
		drawParamName: {
			/**
			 * 字体文件名称，值为 String。存放在工程 assets 目录中的字体文件名称，如：FONT.ttf。没有指定后缀的情况下，会自动加上 .ttf 后缀。
			 */
			fontName: "FONT_NAME",
			/**
			 * QRCode 的编码版本号（1～40），值为 Integer。未指定时表示根据编码内容自动采用适合的编码版本号。
			 */
			qrcodeVersion: "QRCODE_VERSION",
			/**
			 * QRCode 的纠错级别（0～3），值为 Integer。默认为 ErrorCorrectionLevel.L。
			 */
			errorCorrection: "ERROR_CORRECTION",
			/**
			 * QRCode 的字符串编码类型，值为 String。默认为 UTF-8。
			 */
			characterSet: "CHARACTER_SET",
			/**
			 * 条码的留白，值为 Integer。对于 QRCode，规范中定义的留白值为 0/2/4。默认是 0，也即不留白。
			 */
			margin: "MARGIN"
		},
        /**
         * 得到当前打印动作的顺时针旋转角度。
         *
         * @return 当前打印动作的顺时针旋转角度（0，90，180，270）。
         */
		getItemOrientation: function () {
			return B.execSync(service, 'getItemOrientation', []);
		},
        /**
         * 设置打印动作的旋转角度。
         *
         * @param orientation
         *            orientation: 旋转角度。参数描述如下：
         *            0：不旋转；
         *            90：顺时针旋转90度；
         *            180：旋转180度；
         *            270：逆时针旋转90度。
         */
		setItemOrientation: function (orientation) {
			var data = typeof orientation === "object" ? orientation : {
				orientation: (orientation || 0)
			};
			B.execSync(service, 'setItemOrientation', [JSON.stringify(data)]);
		},
        /**
         * 得到当前打印动作的水平对齐方式。
         *
         * @return 后续打印动作的水平对齐方式。水平对齐方式值如下：
         *         0：水平居左；
         *         1：水平居中；
         *         2：水平居右；
         */
		getItemHorizontalAlignment: function () {
			return B.execSync(service, 'getItemHorizontalAlignment', []);
		},
        /**
         * 设置打印动作的水平对齐方式。
         *
         * @param alignment
         *            水平对齐方式。参数描述如下：
         *            0：水平居左（默认方式）；
         *            1：水平居中；
         *            2：水平居右。
         */
		setItemHorizontalAlignment: function (alignment) {
			var data = typeof alignment === "object" ? data : {
				alignment: (alignment || 0)
			};
			B.execSync(service, 'setItemHorizontalAlignment', [JSON.stringify(data)]);
		},
        /**
         * 得到当前打印动作的垂直对齐方式。
         *
         * @return 后续打印动作的垂直对齐方式。返回结果描述如下：
         *         0：垂直居上；
         *         1：垂直居中；
         *         2：垂直居下；
         */
		getItemVerticalAlignment: function () {
			return B.execSync(service, 'getItemVerticalAlignment', []);
		},
        /**
         * 设置打印动作的垂直对齐方式。
         *
         * @param alignment
         *            垂直对齐方式，参数描述如下：
         *            0：垂直居上（默认方式）；
         *            1：垂直居中；
         *            2：垂直居下。
         */
		setItemVerticalAlignment: function (alignment) {
			var data = typeof alignment === "object" ? data : {
				alignment: (alignment || 0)
			};
			B.execSync(service, 'setItemVerticalAlignment', [JSON.stringify(data)]);
		},
        /**
         * 得到线条画笔对齐方式。
         *
         * @return 线条画笔对齐方式（{@link PenAlignment}），<br>
         *         数值为以下两者之一：<br>
         *         PenAlignment.CENTER：绘制的线以指定的位置为中央；<br>
         *         PenAlignment.INSET：绘制的线在指定的位置内侧。<br>
         */
		getItemPenAlignment: function () {
			return B.execSync(service, 'getItemPenAlignment', []);
		},
        /**
         * 设置线条画笔对齐方式。
         *
         * @param alignment
         *            线条画笔对齐方式（{@link PenAlignment}），<br>
         *            数值为以下两者之一：<br>
         *            PenAlignment.CENTER：绘制的线以指定的位置为中央； <br>
         *            PenAlignment.INSET：绘制的线在指定的位置内侧。
         */
		setItemPenAlignment: function (alignment) {
			var data = typeof alignment === "object" ? data : {
				alignment: (alignment || 0)
			};
			B.execSync(service, 'setItemPenAlignment', [JSON.stringify(data)]);
		},
		/**
		 * 设置当前打印任务的背景色；
		 * 一般在直接打印的时候，建议用白色作为底色打印效果会更好；
		 * 如果想获取一个透明的色的图片，则需要将背景色设置为透明底色；
		 * 其他颜色不建议使用；
		 */
		setBackground: function (color) {
			var data = typeof color === "object" ? color : {
				color: (color || 0xffffff)
			};
			B.execSync(service, 'setBackground', [JSON.stringify(data)]);
		},
        /***************************************************************************
         * 打印对象的绘制操作。
         **************************************************************************/

        /**
         * 打印文本。
         *
         * @param text
         *            文本内容。
         * @param x
         *            打印对象的位置(单位mm)。
         * @param y
         *            打印对象的位置(单位mm)。
         * @param width
         *            打印对象的宽度(单位mm)。
         * @param height
         *            打印对象的高度(单位mm)。
         *            height 为 0 时，真正的打印文本高度会根据内容来扩展；否则当指定的高度不足以打印指定的文本时，会自动缩小字体来适应指定的高度进行文本打印。
         * @param fontHeight
         *            文本的字体高度(单位mm)。
         * @param fontStyle
         *            文本的字体风格（可按位组合），可以不指定，默认为0（正常）。0：正常；1：粗体；2：斜体；3：粗斜体 ；4：下划线；8：删除线。
         * @return 打印成功与否？
         */
		drawText: function (text, x, y, width, height, fontHeight, fontStyle, linespace) {
			var data = typeof text === "object" ? text : {
				text: (text || ''),
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				fontHeight: (fontHeight || 0),
				fontStyle: (fontStyle || 0),
				linespace: (linespace || 0)
			};
			return B.execSync(service, 'drawText', JSON.stringify(data));
		},
        /**
         * 以指定的线宽，打印矩形框。
         *
         * @param x
         *            绘制的矩形框的左上角水平位置（单位mm）。
         * @param y
         *            绘制的矩形框的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的矩形框的水平宽度（单位mm）。
         * @param height
         *            绘制的矩形框的垂直高度（单位mm）。
         * @param lineWidth
         *            矩形框的线宽（单位mm）。矩形框的线宽是向矩形框内部延伸的。
         * @return 打印成功与否？
         */
		drawRectangle: function (x, y, width, height, lineWidth) {
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				lineWidth: (lineWidth || this.defaultLineWidth)
			};
			return B.execSync(service, 'drawRectangle', JSON.stringify(data));
		},
        /**
         * 打印填充的矩形框。
         *
         * @param x
         *            绘制的填充矩形框的左上角水平位置（单位mm）。
         * @param y
         *            绘制的填充矩形框的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的填充矩形框的水平宽度（单位mm）。
         * @param height
         *            绘制的填充矩形框的垂直高度（单位mm）。
         * @return 打印成功与否？
         */
		fillRectangle: function (x, y, width, height) {
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0)
			}
			return B.execSync(service, 'fillRectangle', JSON.stringify(data));
		},
        /**
         * 以指定的线宽，打印圆角矩形框。
         *
         * @param x
         *            绘制的圆角矩形框的左上角水平位置（单位mm）。
         * @param y
         *            绘制的圆角矩形框的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的圆角矩形框的水平宽度（单位mm）。
         * @param height
         *            绘制的圆角矩形框的垂直高度（单位mm）。
         * @param cornerWidth
         *            圆角宽度（单位mm）。
         * @param cornerHeight
         *            圆角高度（单位mm）。
         * @param lineWidth
         *            圆角矩形框的线宽（单位mm）。圆角矩形框的线宽是向圆角矩形框内部延伸的。
         * @return 打印成功与否？
         */
		drawRoundRectangle: function (x, y, width, height, cornerWidth, cornerHeight, lineWidth) {
			radius = cornerWidth || cornerHeight || this.defaultRadius;
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				cornerWidth: (cornerWidth || radius),
				cornerHeight: (cornerHeight || radius),
				lineWidth: (lineWidth || this.defaultLineWidth)
			};
			return B.execSync(service, 'drawRoundRectangle', [JSON.stringify(data)]);
		},
        /**
         * 打印填充的圆角矩形框。
         *
         * @param x
         *            绘制的填充圆角矩形框的左上角水平位置（单位mm）。
         * @param y
         *            绘制的填充圆角矩形框的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的填充圆角矩形框的水平宽度（单位mm）。
         * @param height
         *            绘制的填充圆角矩形框的垂直高度（单位mm）。
         * @param cornerWidth
         *            圆角宽度（单位mm）。
         * @param cornerHeight
         *            圆角高度（单位mm）。
         * @return 打印成功与否？
         */
		fillRoundRectangle: function (x, y, width, height, cornerWidth, cornerHeight) {
			radius = cornerWidth || cornerHeight || this.defaultRadius;
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				cornerWidth: (cornerWidth || radius),
				cornerHeight: (cornerHeight || radius)
			};
			return B.execSync(service, 'fillRoundRectangle', [JSON.stringify(data)]);
		},
        /**
         * 以指定的线宽，打印椭圆/圆。
         *
         * @param x
         *            绘制的椭圆的左上角水平位置（单位mm）。
         * @param y
         *            绘制的椭圆的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的椭圆的水平宽度（单位mm）。
         * @param height
         *            绘制的椭圆的垂直高度（单位mm）。
         * @param lineWidth
         *            椭圆的线宽（单位mm）。椭圆的线宽是向椭圆内部延伸的。
         * @return 打印成功与否？
         */
		drawEllipse: function (x, y, width, height, lineWidth) {
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				lineWidth: (lineWidth || this.defaultLineWidth)
			};
			return B.execSync(service, 'drawEllipse', [JSON.stringify(data)]);
		},
        /**
         * 打印填充的椭圆/圆。
         *
         * @param x
         *            绘制的填充椭圆的左上角水平位置（单位mm）。
         * @param y
         *            绘制的填充椭圆的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的填充椭圆的水平宽度（单位mm）。
         * @param height
         *            绘制的填充椭圆的垂直高度（单位mm）。
         * @return 打印成功与否？
         */
		fillEllipse: function (x, y, width, height) {
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0)
			};
			return B.execSync(service, 'fillEllipse', [JSON.stringify(data)]);
		},
        /**
         * 以指定的线宽，打印圆。
         *
         * @param x
         *            绘制的填充椭圆的左上角水平位置（单位mm）。
         * @param y
         *            绘制的填充椭圆的左上角垂直位置（单位mm）。
         * @param radius
         *            绘制圆的半径（单位mm）。
         * @param lineWidth
         *            圆的线宽（单位mm）。圆的线宽是向圆内部延伸的。
         * @return 打印成功与否？
         */
		drawCircle: function (x, y, radius, lineWidth) {
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				radius: (radius || 0),
				lineWidth: (lineWidth || this.defaultLineWidth)
			};
			return B.execSync(service, 'drawCircle', [JSON.stringify(data)]);
		},
        /**
         * 打印填充的圆。
         *
         * @param x
         *            绘制的填充椭圆的左上角水平位置（单位mm）。
         * @param y
         *            绘制的填充椭圆的左上角垂直位置（单位mm）。
         * @param radius
         *            绘制的填圆的半径（单位mm）。
         * @return 打印成功与否？
         */
		fillCircle: function (x, y, radius) {
			var data = typeof x === "object" ? x : {
				x: (x || 0),
				y: (y || 0),
				radius: (radius || 0),
			};
			return B.execSync(service, 'fillCircle', [JSON.stringify(data)]);
		},
        /**
         * 打印线（直线/斜线）。
         *
         * @param x1
         *            线的起点的水平位置（单位mm）。
         * @param y1
         *            线的起点的垂直位置（单位mm）。
         * @param x2
         *            线的终点的水平位置（单位mm）。
         * @param y2
         *            线的终点的垂直位置（单位mm）。
         * @param lineWidth
         *            线宽（单位mm）。线宽是向线的下方延伸的。
         * @return 打印成功与否？
         */
		drawLine: function (x1, y1, x2, y2, lineWidth) {
			var data = typeof x1 === "object" ? x1 : {
				x1: (x1 || 0),
				y1: (y1 || 0),
				x2: (x2 || 0),
				y2: (y2 || 0),
				lineWidth: (lineWidth || this.defaultLineWidth)
			};
			return B.execSync(service, 'drawLine', JSON.stringify(data));
		},
        /**
         * 打印点划线。
         *
         * @param x1
         *            线的起点的水平位置（单位mm）。
         * @param y1
         *            线的起点的垂直位置（单位mm）。
         * @param x2
         *            线的终点的水平位置（单位mm）。
         * @param y2
         *            线的终点的垂直位置（单位mm）。
         * @param lineWidth
         *            线宽（单位mm）。线宽是向线的下方延伸的。
         * @param dashLen
         *            点划线线段长度的数组（单位mm）。
         * @return 打印成功与否。
         */
		drawDashLine: function (x1, y1, x2, y2, lineWidth, dashLen) {
			var data = typeof x1 === "object" ? x1 : {
				x1: (x1 || 0),
				y1: (y1 || 0),
				x2: (x2 || 0),
				y2: (y2 || 0),
				lineWidth: (lineWidth || this.defaultLineWidth),
				dashLen: (dashLen || [0.3, 0.3])
			};
			return B.execSync(service, 'drawDashLine', JSON.stringify(data));
		},
        /**
         * 打印一维条码。
         *
         * @param text
         *            需要绘制的一维条码的内容。
         * @param type
         *            一维条码的编码类型参考文档。
         * @param x
         *            绘制的一维条码的左上角水平位置（单位mm）。
         * @param y
         *            绘制的一维条码的左上角垂直位置（单位mm）。
         * @param width
         *            一维条码的整体显示宽度。
         * @param height
         *            一维条码的显示高度（包括供人识读文本）。
         * @param textHeight
         *            供人识读文本的高度（单位mm），建议为3毫米。
         * @return 打印成功与否？
         */
		draw1DBarcode: function (text, x, y, width, height, textHeight, type) {
			var data = typeof text === "object" ? text : {
				text: (text || ''),
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				textHeight: (textHeight || 0),
				type: (type || 0)
			}
			return B.execSync(service, 'draw1DBarcode', JSON.stringify(data));
		},
        /**
         * 打印 QRCode 二维码。
         *
         * @param text
         *            需要绘制的QRCode二维码的内容。
         * @param x
         *            绘制的QRCode二维码的左上角水平位置（单位mm）。
         * @param y
         *            绘制的QRCode二维码的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的QRCode二维码的水平宽度（单位mm）。
         * @return 打印成功与否？
         */
		draw2DQRCode: function (text, x, y, width) {
			var data = typeof text === "object" ? text : {
				text: (text || ''),
				x: (x || 0),
				y: (y || 0),
				width: (width || 0)
			};
			return B.execSync(service, 'draw2DQRCode', JSON.stringify(data));
		},
        /**
         * 打印 Pdf417 二维码。
         *
         * @param text
         *            需要绘制的Pdf417二维码的内容。
         * @param x
         *            绘制的Pdf417二维码的左上角水平位置（单位mm）。
         * @param y
         *            绘制的Pdf417二维码的左上角垂直位置（单位mm）。
         * @param width
         *            绘制的Pdf417二维码的水平宽度（单位mm）。
         * @param height
         *            绘制的Pdf417二维码的垂直高度（单位mm）。
         * @return 打印成功与否？
         */
		draw2DPdf417: function (text, x, y, width, height) {
			var data = typeof text === "object" ? text : {
				text: (text || ''),
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0)
			};
			return B.execSync(service, 'draw2DPdf417', [JSON.stringify(data)]);
		},
        /**
         * 打印图片。
         *
         * @param image
         *            	如果iamge为字符串，则表示图片路径，或者base64字符串；
         * @param x
         *            打印对象在水平方向上的位置(单位mm)。
         * @param y
         *            打印对象在垂直方向上的位置(单位mm)。
         * @param width
         *            打印对象的宽度(单位mm)。
         * @param height
         *            打印对象的高度(单位mm)。
         * @param threshold
         *            绘制位图的灰度阀值。
         *            256 表示绘制灰度图片；
         *            257 表示绘制原色图片；
         *            0～255表示绘制黑白图片，原图颜色>=灰度阀值的点会被认为是白色，而原图颜色<灰度阀值的点会被认为是黑色。默认值为192。
         * @return 打印成功与否？
         */
		drawImage: function (image, x, y, width, height, threshold) {
			var data = typeof image === "object" ? image : {
				image: (image || ''),
				x: (x || 0),
				y: (y || 0),
				width: (width || 0),
				height: (height || 0),
				threshold: (threshold || 192)
			}
			return B.execSync(service, 'drawImage', [JSON.stringify(data)]);
		}
	};
}, true);
