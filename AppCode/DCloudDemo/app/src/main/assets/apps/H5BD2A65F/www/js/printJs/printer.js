		var printer;
		var printList=new Array();
		document.addEventListener('plusready', function () {
			// Android处理返回键
			plus.key.addEventListener('backbutton', function () {
				plus.runtime.quit();
			}, false);

			printer = plus.LPAPI;
			printer.getPrinters(function (values) {
				if (values.length && values.length > 0) {
					values.forEach(item => {
						printList.push(item.shownName);
					});
				} else {
				alert("未检测到打印机");
				}

			});
			printLabel_10();
		}, false);

		/**
		 * 连接打印机；
		 */
		function printLabel_10() {
			var printerSelect = document.getElementById("printerSelect");
			var value=printList[0];
			printer.openPrinter({ name: value }, function (msg) {
				//alert("打开成功；msg = " + JSON.stringify(msg));
			}, function () {
				alert("打开失败；msg = " + JSON.stringify(msg));
			});
		}
		/**
		 * 获取打印机相关信息；
		 */
		function printLabel_11() {
			printer.getPrinterInfo(function (msg) {
				alert("PrinterInfo = " + JSON.stringify(msg));
			});
		}
		/**
		 * 断开打印机连接；
		 */
		function printLabel_12() {
			printer.closePrinter();
		}

        function dateformat(date, format) {
                     var o = {
                         "M+": date.getMonth() + 1, //month
                         "d+": date.getDate(),    //day
                         "h+": date.getHours(),   //hour
                         "m+": date.getMinutes(), //minute
                         "s+": date.getSeconds(), //second
                         "q+": Math.floor((date.getMonth() + 3) / 3),  //quarter
                         "S": date.getMilliseconds() //millisecond
                     };
                     if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
                         (date.getFullYear() + "").substr(4 - RegExp.$1.length));
                     for (var k in o) if (new RegExp("(" + k + ")").test(format))
                         format = format.replace(RegExp.$1,
                             RegExp.$1.length == 1 ? o[k] :
                                 ("00" + o[k]).substr(("" + o[k]).length));
                     return format;
                 }

        /**
        * 打印标签
        */
        function printLabelWithQrCode(asset){

            var width = 50;
            var height = 30;
            if (!_.isDate(asset.BuyTime)) {
                asset.BuyTime = new Date(asset.BuyTime);
            }
            printer.startJob({ width: width, height: height});
            printer.setItemHorizontalAlignment({ alignment: 1 });
            printer.drawText({ text: asset.SupplierName, y: 4, width: 46, height: 6, fontHeight: 2.5, fontStyle: 0 });

            printer.setItemHorizontalAlignment({ alignment: 0 });
            printer.draw2DPdf417({ text: asset.Code, x: 2, y: 10, width: 15 })

            printer.setItemHorizontalAlignment({ alignment: 0 });
			printer.drawText({ text: "资产名称："+asset.Name,x:20, y: 10, width: 46, height: 6, fontHeight: 2.5, fontStyle: 0 });

            printer.setItemHorizontalAlignment({ alignment: 0 });
			printer.drawText({ text: "购入日期："+dateformat(asset.BuyTime, "yyyy-MM-dd"),x:20, y: 15, width: 46, height: 6, fontHeight: 2.5, fontStyle: 0 });
            printer.commitJob();
        }

         /**
        * 打印一维码标签
        */
         function printLabelWithBarCode(asset){
            var width = 50;
            var height = 30;
            var marginleft = -3;
            if (!_.isDate(asset.BuyTime)) {
                asset.BuyTime = new Date(asset.BuyTime);
            }
            printer.startJob({ width: width, height: height});

            printer.setItemHorizontalAlignment({ alignment: 1 });
            printer.drawText({ text: asset.SupplierName, y: 4, width: 46, height: 6, fontHeight: 2.5, fontStyle: 0 });

            printer.setItemHorizontalAlignment({ alignment: 0 });
            printer.drawText({ text: "资产名称："+asset.Name,x:3, y: 10, width: 46, height: 6, fontHeight: 2.5, fontStyle: 0 });

            printer.setItemHorizontalAlignment({ alignment: 0 });
            printer.drawText({ text: "购入日期："+dateformat(asset.BuyTime, "yyyy-MM-dd"),x:3, y: 15, width: 46, height: 6, fontHeight: 2.5, fontStyle: 0 });

            printer.setItemHorizontalAlignment({ alignment: 0 });
			printer.draw1DBarcode({ text: asset.Code, x: 3, y: 20, width: 40, height: 8, fontHeight: 3 });
            printer.commitJob();
        }
         /**
        * 打印表格标签
        */
        function printLabelWithTable1(asset){
            var width = 50;
            var height = 30;
            if (!_.isDate(asset.BuyTime)) {
                asset.BuyTime = new Date(asset.BuyTime);
            }
            var contentWidth = width - 3 * 3;
            var tablelineheight = 3;
            var w1 = 10, w2 = 12, textleft = 0.1, texttop = 0.6, fontHeight = 2, barcodeheight = 4;
            var x1 = 2, x2 = x1 + w1, x3 = x2 + w2, x4 = x3 + w1, x5 = x4 + w2;
            var y0 = 0, y1 = 3,
                y2 = y1 + tablelineheight,
                y3 = y2 + tablelineheight,
                y4 = y3 + tablelineheight,
                y5 = y4 + tablelineheight,
                y6 = y5 + tablelineheight,
                y7 = y6 + barcodeheight + fontHeight;
            var linewidth = 0.2;

            printer.startJob({ width: width, height: height});

            printer.setItemHorizontalAlignment({ alignment: 1 });
			printer.drawText({ text: asset.SupplierName,x:3, y: 0.1, width: contentWidth, height: 3, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawLine({ x1: x1, y1: y1, x2: x5, y2: y1, lineWidth: linewidth });
            printer.drawLine({ x1: x1, y1: y2, x2: x5, y2: y2, lineWidth: linewidth });
            printer.drawLine({ x1: x1, y1: y3, x2: x5, y2: y3, lineWidth: linewidth });
            printer.drawLine({ x1: x1, y1: y4, x2: x5, y2: y4, lineWidth: linewidth });
            printer.drawLine({ x1: x1, y1: y5, x2: x5, y2: y5, lineWidth: linewidth });
            printer.drawLine({ x1: x1, y1: y6, x2: x5, y2: y6, lineWidth: linewidth });
            printer.drawLine({ x1: x1, y1: y7, x2: x5, y2: y7, lineWidth: linewidth });

			printer.drawLine({ x1: x1, y1: y1, x2: x1, y2: y7, lineWidth: linewidth });
            printer.drawLine({ x1: x2, y1: y1, x2: x2, y2: y6, lineWidth: linewidth });
            printer.drawLine({ x1: x3, y1: y2, x2: x3, y2: y6, lineWidth: linewidth });
            printer.drawLine({ x1: x4, y1: y2, x2: x4, y2: y6, lineWidth: linewidth });
            printer.drawLine({ x1: x5, y1: y1, x2: x5, y2: y7, lineWidth: linewidth });

            printer.setItemHorizontalAlignment({ alignment: 0 });
            printer.drawText({ text: "资产编码：",x:textleft + x1, y: texttop + y1, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.SN,x:textleft + x2, y: texttop + y1, width: w1 + w2 * 2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "资产名称：",x:textleft + x1, y: texttop + y2, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.Name,x:textleft + x2, y: texttop + y2, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "使用人员：",x:textleft + x3, y: texttop + y2, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.UsedEmployeeName,x:textleft + x4, y: texttop + y2, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "规格型号：",x:textleft + x1, y: texttop + y3, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.Model,x:textleft + x2, y: texttop + y3, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "使用部门：",x:textleft + x3, y:texttop + y3, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.UsedDepartmentName,x:textleft + x4, y:texttop + y3, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "存放地点：",x: textleft + x1, y:texttop + y4, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.StoredAddressName,x: textleft + x2, y:texttop + y4, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "计量单位：",x: textleft + x3, y:texttop + y4, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.UnitName,x: textleft + x4, y:texttop + y4, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "取得日期：",x: textleft + x1, y:texttop + y5, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: dateformat(asset.BuyTime, "yyyy-MM-dd"),x: textleft + x2, y:texttop + y5, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: "资产价值：",x: textleft + x3, y: texttop + y5, width: w1, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });
			printer.drawText({ text: asset.Total * asset.Price+"元",x: textleft + x4, y:texttop + y5, width: w2, height: tablelineheight, fontHeight: fontHeight, fontStyle: 0 });

            printer.setItemHorizontalAlignment({ alignment: 0 });
			printer.draw1DBarcode({ text: asset.Code, x: x1, y: texttop+1+y6, width: contentWidth, height: barcodeheight, fontHeight: fontHeight });
            printer.commitJob();
        }

        function assetPrinter(asset, assetLabelTemplate){
            if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "表格类型1")){
                printLabelWithTable1(asset);
            }else if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "条形码")){
                printLabelWithBarCode(asset);
            }else if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "二维码")){
                printLabelWithQrCode(asset);
            }
        }