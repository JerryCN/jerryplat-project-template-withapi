(function(w){
// 空函数
function shield(){
	return false;
}
document.addEventListener('touchstart', shield, false);//取消浏览器的所有事件，使得active的样式在手机上正常生效
document.oncontextmenu=shield;//屏蔽选择函数
// H5 plus事件处理
var ws=null,as='pop-in';
function plusReady(){
	ws=plus.webview.currentWebview();
	plus.key.addEventListener('backbutton', function(){
		back();
	},false);
}
if(w.plus){
	plusReady();
}else{
	document.addEventListener('plusready', plusReady, false);
}
// DOMContentLoaded事件处理
document.addEventListener('DOMContentLoaded', function(){
	document.body.onselectstart=shield;
},false);
// 返回
w.back=function(hide){
	if(w.plus){
		ws||(ws=plus.webview.currentWebview());
		(hide||ws.preate)?ws.hide('auto'):ws.close('auto');
	}else if(history.length>1){
		history.back();
	}else{
		w.close();
	}
};
// 处理点击事件
var openw=null;
/**
 * 打开新窗口
 * @param {URIString} id : 要打开页面url
 * @param {String} t : 页面标题名称
 * @param {JSON} ws : Webview窗口属性
 */
w.clicked=function(id, t, ws){
	if(openw){//避免多次打开同一个页面
		return null;
	}
	if(w.plus){
		ws=ws||{};
		ws.scrollIndicator||(ws.scrollIndicator='none');
		ws.scalable||(ws.scalable=false);
		ws.backButtonAutoControl||(ws.backButtonAutoControl='close');
		ws.titleNView=ws.titleNView||{autoBackButton:true};
		ws.titleNView.backgroundColor = '#D74B28';
		ws.titleNView.titleColor = '#CCCCCC';
		ws.doc&&(ws.titleNView.buttons=ws.titleNView.buttons||[],ws.titleNView.buttons.push({fontSrc:'_www/helloh5.ttf',text:'\ue301',fontSize:'20px',onclick:'javascript:openDoc()'}));
		t&&(ws.titleNView.titleText=t);
		openw = plus.webview.create(id, id, ws);
		openw.addEventListener('loaded', function(){
			openw.show(as);
		}, false);
		openw.addEventListener('close', function(){
			openw=null;
		}, false);
		return openw;
	}else{
		w.open(id);
	}
	return null;
};
/**
 * 创建新窗口（无原始标题栏），
 * @param {URIString} id : 要打开页面url
 * @param {JSON} ws : Webview窗口属性
 */
w.createWithoutTitle=function(id, ws){
	if(openw){//避免多次打开同一个页面
		return null;
	}
	if(w.plus){
		ws=ws||{};
		ws.scrollIndicator||(ws.scrollIndicator='none');
		ws.scalable||(ws.scalable=false);
		ws.backButtonAutoControl||(ws.backButtonAutoControl='close');
		openw = plus.webview.create(id, id, ws);
		openw.addEventListener('close', function(){
			openw=null;
		}, false);
		return openw;
	}else{
		w.open(id);
	}
	return null;
};
})(window);
