/*!
 * Vue.jerry.helper.js v1
 * (c) 2018-2020 Jerry 15802775429
 * Released under the MIT License.
 */
const clearErrors = form => {
    Array.from( form.querySelectorAll('.is-error') ).map( el => el.classList.remove('is-error') )
}

const willvalidate =  form => {
	var firstError=null
	Array.from( form ).map( field => {
		if (field.willValidate && !field.checkValidity()) 
		{
		  if (!firstError) firstError = field
		  field.parentElement.classList.add('is-error')
		} 
	})
	if (firstError) firstError.focus()
	return firstError === null
}


var vueApp = null;
(function (global, factory) {
    global.helper = factory();
})(this, (function (defaultValue) {
    'use strict';

    var _default = _.merge({
    	ApiBaseUrl:"http://assetapi.bkant.cn",
    	Token:"access_token",
        Api: "/Api",
        LoginUrl:"",
        LoadingText: "正在努力加载中....",
        MessageTitle: "消息",
        ConfirmTitle: "提示",
        OK: "确定",
        Cancel: "取消",
        SelectRecordFirst: "请先选择要操作的记录。",
        Delete: "确实要删除记录吗?",
        InValidModel: "请先填写有效的数据.",
        constantHelper:{
			Confirm: "Confirm",
			Error: "Error",
			Existed: "Existed",
			Invalid: "Invalid",
			Logout: "Logout",
			NotFound: "NotFound",
			Ok: "Ok"
        },
        Callback: function () { },
        Vue: {
            AppElement: "#JerryPlatApp",
            DefaultDataList: "getPageList",
            DefaultTable: "List",
            DefaultForm: "List.ModelDialog",
            DefaultIdName:"#DefaultModel"
        },
        AllPageParam: {
            PageIndex: 1,
            PageSize: 1000
        }
    }, defaultValue);

    //#region Loading Dialog
    var _openLoadingCount = 0;
    function _openLoading() {
        if (_openLoadingCount == 0) {
        	if(window.plus){
        		plus.nativeUI.showWaiting(_default.LoadingText);
        	}
        	 
        }
        _openLoadingCount++;
    }

    function _closeLoading() {
        if (_openLoadingCount == 1) {
        	if(window.plus){
        		plus.nativeUI.closeWaiting();
        	}
        }
        if (_openLoadingCount > 0) {
            _openLoadingCount--;
        }
    }
    //#endregion

    //#region Alert & Confirm Dialog
    function _message(message, type, okCallback) {
//      vueApp && (vueApp.$alert(message, _default.MessageTitle, {
//          type: type,
//          closeOnClickModal: true,
//          closeOnPressEscape: true,
//          confirmButtonText: _default.OK,
//          callback: okCallback || _default.Callback
//      }));
		if(window.plus){			
			plus.nativeUI.alert( message,okCallback || _default.Callback)
		}
		 
    }

    function _alert(message, okCallback) {
        _message(message, "info", okCallback);
    }

    function _error(message, okCallback) {
        _message(message, "error", okCallback);
    }

    function _confirm(message, okCallback, cacelCallback) {
//      vueApp && (vueApp.$confirm(message, _default.ConfirmTitle, {
//          confirmButtonText: _default.OK,
//          cancelButtonText: _default.Cancel,
//          type: 'warning'
//      }).then(okCallback || _default.Callback).catch(cacelCallback || _default.Callback));
	if(window.plus){
	plus.nativeUI.confirm(message, function(e){
		if (e.index==0) {
			okCallback || _default.Callback
		}else{
			cacelCallback || _default.Callback
		}
	});		
	}
	
    }

    function _delete(okCallback, cacelCallback) {
        _confirm(_default.Delete, okCallback, cacelCallback);
    }
    //#endregion

    //#region private functions
    function _setOptions(options) {
        _default = _.merge(_default, options);
    }

    function _redirect(url) {
		mui.openWindow({
			url: url,
			id: url
		});
    }

    function _getArrayItem(array, where) {
        if (array == null) {
            return null;
        }
        where = where || function () { return false; };
        for (var i = 0; i < array.length; i++) {
            if (where(array[i])) {
                return array[i];
            }
        }
        return null;
    }

    function _update(target, source, ignoreKeys) {
        _.each(target, function (value, key) {
            if (_.has(source, key) && _.indexOf(ignoreKeys, key) == -1) {
                target[key] = source[key];
            }
        });
    }

    function _getItemValue(array, where, key) {
        var item = _getArrayItem(array, where);
        key = key || "Name";
        return item && item[key];
    }

    function _addDays(date, days) {
        return new Date(date.getTime() + 24 * 60 * 60 * 1000 * days);
    }

    /*
    * context: {A:{B:{C:1}}}
    * name: "A.B.C"
    */
    function _getContext(context, keyList, split) {
        split = split || ".";
        var keys = keyList.split(split);
        var value = context;
        _.each(keys, function (item) {
            value = value[item];
        });
        return value;
    }

    function _getIndentRepeatString(count) {
        return _.repeat("&nbsp;", count * 4);
    }

    function _getCookie(key) {
        return localStorage.getItem(key);
    }

    function _getArea() {
        return _default.ApiBaseUrl + _default.Api;
    }

    function _getUrl(url) {
        return _getArea() + url;
    }

    function _getApiUrl(url) {
        return _getUrl(url, true);
    }

    function _getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }

    function _getFileName(fileFullPath, split) {
        if (fileFullPath == null) {
            return "";
        }
        split = split || "/";
        return fileFullPath.substr(fileFullPath.lastIndexOf(split) + 1);
    }

    function _format(date, format) {
        var o = {
            "M+": date.getMonth() + 1, //month
            "d+": date.getDate(),    //day
            "h+": date.getHours(),   //hour
            "m+": date.getMinutes(), //minute
            "s+": date.getSeconds(), //second
            "q+": Math.floor((date.getMonth() + 3) / 3),  //quarter
            "S": date.getMilliseconds() //millisecond
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
            ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    }

    //strDate : "/Date(1516172795107)/"
    //return 1516172795107
    function _getDate(strDate) {
        if (strDate == "" || strDate == null) {
            return null;
        }
        if (strDate[0] == "/") {
            return eval(strDate.substring(6, strDate.length - 2));
        }
        return strDate;
    }

    function _login() {
        _redirect(_default.LoginUrl);
    }

    function _isNullOrEmpty(str) {
        return str == null || str == '';
    }
    
    //#endregion

    //#region Ajax
    function _getAuthorization() {
        var token = _getCookie(_default.Token);
        if (token != null && token != "") {
            var tokenType = _getCookie(_default.TokenType) || "bearer";
            return { Authorization: tokenType + ' ' + token };
        }
        return null;
    }

    function _getHttpOptions(url, method, data, isUploadFile, successcallback, failcallback, beforeSendCallBack, bIsApi) {
        var options = {
            url: url,
            type: method,
            data: data || {},
            headers: {},
            beforeSend: beforeSendCallBack || _default.Callback,
            success: successcallback || _default.Callback,
            error: failcallback || _default.Callback
        };

        if (typeof (data) == "object" & !_.isArray(data)) {
            if (!bIsApi && method == "POST") {
                options.data.__RequestVerificationToken = $("input[name=__RequestVerificationToken]").val();
            }

            if (isUploadFile) {
                options.processData = false;
				options.cache = true;
                options.data = _getFormData(options.data);
                options.contentType = "multipart/form-data";
            }
        } else {
            if (_.isArray(data)) {
                options.data = JSON.stringify(options.data);
            }
            options.contentType = "application/json";
        }

        if (bIsApi) {
            options.headers = _.merge({}, options.headers, _getAuthorization());
            //var token = _getCookie(constantHelper.Token);
            //if (token != null && token != "") {
            //    options.headers = options.headers || {};
            //    var tokenType = _getCookie(constantHelper.TokenType) || "bearer";
            //    options.headers["Authorization"] = tokenType + ' ' + token;
            //}
        }

        return options;
    }

    function _getFormData(model) {
        var formData = new FormData();
        if (model != null) {
            _.each(model, function (value, key) {
                formData.append(key, value);
            });
        }
        return formData;
    }

    function _checkApiToken() {
        var token = _getCookie(_default.Token);
        if (!_isNullOrEmpty(token)) {
            return 1;
        }

        var refreshToken = _getCookie(_default.RefreshToken);
        if (!_isNullOrEmpty(refreshToken)) {
            return 0;
        }
        return -1;
    }

    function _ajax(method, url, data, successcallback, failcallback, isUploadFile, beforeSendCallBack, bIsApi, bIsNeedToken) {
        if (bIsNeedToken == null) {
            bIsNeedToken = true;
        }

        if (bIsApi && bIsNeedToken) {
            var result = _checkApiToken();
            switch (result) {
                case 1:
                    break;
                case 0:
                default://-1
                    _getRefreshToken(null, function () {
                        _ajax(method, url, data, successcallback, failcallback, isUploadFile, beforeSendCallBack, bIsApi);
                    }, null, bIsNeedToken);
                    return;
            }
        }

        successcallback = successcallback || _default.Callback;
        failcallback = failcallback || _default.Callback;

        _openLoading();
		console.log(url);
        $.ajax(_getHttpOptions(url, method, data, isUploadFile,
            function (response) {
            	console.log(JSON.stringify(response));
                setTimeout(_closeLoading, 100);
                if (response.Status == _default.constantHelper.Ok) {
                    successcallback(response.Data, response);
                    return;
                }

                if (response.Status == _default.constantHelper.Confirm) {
					plus.nativeUI.confirm(response.Message, function(e) {
						if(e.index == 0) {
                      data.IsConfirmed = true;
                      _ajax(method, url, data, successcallback, failcallback, isUploadFile, beforeSendCallBack, bIsApi);
						}
					});
//                  _confirm(response.Message, function () {
//                      data.IsConfirmed = true;
//                      _ajax(method, url, data, successcallback, failcallback, isUploadFile, beforeSendCallBack, bIsApi);
//                  });
                    return;
                }

                if (response.Status == _default.constantHelper.Logout) {
                    _alert(response.Message, function () {
                        _redirect(response.Data);
                    });
                    return;
                }

                var callback = _default.Callback;
                if (response.Status == _default.constantHelper.RefreshTokenFaild) {
                    callback = _login;
                }

                _alert(response.Message, callback);
            }, function (xhr, error, ex) {
            	console.log(JSON.stringify(xhr));
                setTimeout(_closeLoading, 100);
                switch (xhr.status) {
                    case -1://
                        _error(error);
                        return;
                    case 401:
                        _getRefreshToken(null, function () {
                            _ajax(method, url, data, successcallback, failcallback, isUploadFile, beforeSendCallBack, bIsApi);
                        });
                        return;
                    default:
                        _error((xhr.responseJSON && xhr.responseJSON.Message) || xhr.statusText);
                        break;
                }
                failcallback(xhr);
            }, beforeSendCallBack, bIsApi));
    }
	var num=0;
    function _getRefreshToken(message, successcallback, failcallback, bIsNeedToken) {

        message = message || "获取Refresh Token失败，请重新登陆。";
        var refreshToken = _getCookie(_default.RefreshToken)
        if (_isNullOrEmpty(refreshToken)) {
   //      	mui.openWindow({
			// 	url: "Login.html",
			// 	id: "Login"
			// });
			num++;
			if(num==1){
				_alert(message);
			};
			plus.webview.currentWebview().close()
			  var h=plus.webview.getWebviewById( "Login" );
			  plus.webview.show(h);
            return;
        }
        failcallback = failcallback || _login;
        _post(_default.RefreshTokenUrl, { refrshtoken: refreshToken }, successcallback, failcallback);
    }

    function _get(url, data, successcallback, failcallback, bIsApi, bIsNeedToken) {
        _ajax("GET", url, data, successcallback, failcallback, false, null, bIsApi, bIsNeedToken);
    }

    function _post(url, data, successcallback, failcallback, bIsApi, bIsNeedToken) {
        _ajax("POST", url, data, successcallback, failcallback, false, null, bIsApi, bIsNeedToken);
    }

    function _postFile(url, data, successcallback, failcallback, beforeSendCallBack, bIsApi, bIsNeedToken) {
        _ajax("POST", url, data, successcallback, failcallback, true, beforeSendCallBack, bIsApi, bIsNeedToken);
    }

    function _apiGet(url, data, successcallback, failcallback, bIsNeedToken) {
        _ajax("GET", _getApiUrl(url), data, successcallback, failcallback, false, null, true, bIsNeedToken);
    }

    function _apiPost(url, data, successcallback, failcallback, bIsNeedToken) {
        _ajax("POST", _getApiUrl(url), data, successcallback, failcallback, false, null, true, bIsNeedToken);
    }

    function _apiPostFile(url, data, successcallback, failcallback, beforeSendCallBack, bIsNeedToken) {
        _ajax("POST", _getApiUrl(url), data, successcallback, failcallback, true, beforeSendCallBack, true, bIsNeedToken);
    }
    //#endregion

    //#region Vue
    var bus = new Vue();
    function _onBus(name, callback) {
        bus.$on(name, callback);
    };
    function _emitBus(name, callback) {
        bus.$emit(name, callback);
    };
    
    function _getActionUrl(controller, action){
        return "/"+controller+"/"+ action;
    }

    function _getActionList(controller, table, options) {
        table = table || _default.Vue.DefaultTable;
        var actionList = _.merge({
            View: _getActionUrl(controller, "GetPage" + table),
            Detail: _getActionUrl(controller, "GetDetail"),
            Delete: _getActionUrl(controller, "Delete"),
            DeleteList: _getActionUrl(controller, "DeleteList"),
            SoftDelete: _getActionUrl(controller, "SoftDelete"),
            SoftDeleteList: _getActionUrl(controller, "SoftDeleteList"),
            Enabled: _getActionUrl(controller, "Enabled"),
            Recover: _getActionUrl(controller, "Recover"),
            Lock: _getActionUrl(controller, "Lock"),
            UnLock: _getActionUrl(controller, "UnLock"),
            Add: _getActionUrl(controller, "Add"),
            OrderIndex: _getActionUrl(controller, "GetNewOrderIndex"),
            Code: _getActionUrl(controller, "GetCode"),
            Edit: _getActionUrl(controller, "Edit"),
            Import: _getActionUrl(controller, "Import"),
            Export: _getActionUrl(controller, "Export"),
        }, options);
        return actionList;
    }

    function _getInitList(model, options, contoller) {
        model = model || {};
        return _.merge({
        	IsPageList: true,
            PageParam: {
                PageIndex: 1,
                PageSize: 10
            },
            PageModel: {
                TotalItem: 0,
                TotalPage: 1
            },
            /* Action: String --> Not With Api Or Area*/
            Action: _getActionList(contoller),
            SearchModel: getSearchModel(),
            Data: [],
            MultipleSelection: [],
            ModelDialog: {
                Model: model,
                CopyModel: model,
                Visible: false,
                Progress: [0]
            }
        }, options);
    }

    function _getTreeModel(treeName, action, options) {
        var model = {
            data: {
                TreeList: {}
            },
            mounted: function () {
                var _this = this;
                _this.getBaseDataList(action, function (data) {
                    _this.TreeList.NavigationIdList = data;
                });
            },
            methods: {
                overrideResetTree: function (model) {
                    this.resetTree(treeName + "Tree", this.TreeList[treeName], model[treeName]);
                },
                handleAddBefore: function (model) {
                    this.overrideResetTree(model);
                    return model;
                },
                handleEditBefore: function (model) {
                    this.overrideResetTree(model);
                    return model;
                }
            }
        };
        model.data.TreeList[treeName] = [];
        return _.merge(model, options);
    }

    function _getImportModel(controller, options) {
        var model = {
            ModelDialog: {
                NewId: 1,
                Visible: false,
                Action: "/" + controller + "/GetNewId",
                Model: {
                    ExcelPath: ""
                },
                Progress: [0]
            }
        };
        return _.merge(model, options);
    }

    function _getVueOptions(options) {
        options = _.merge({
            el: _default.Vue.AppElement,
            data: {
				PageSize:10,
				currentPage:1,
                Common: {
                    MenuVisible: true,
                    CloseOnClickModal: _default.CloseOnClickModal,
                    CloseOnPressEscape: _default.CloseOnPressEscape,
                    DialogWidth: _default.DialogWidth,
                    SideWidth: _default.SideWidth,
                    FormLabelWidth: _default.FormLabelWidth,
                    FormLabelSuffix: _default.FormLabelSuffix
                }
            }
        }, options);
        return options;
    }
    function _getTableByForm(form) {
        return form.split('.')[0];
    }

    function _checkSelectValue(rule, value, callback) {
        if (_isNullOrEmpty(value) || value < 1) {
            callback(new Error("请先选择该项。"));
            return;
        }
        callback();
    }

    function _checkListValue(rule, value, callback) {
        if (!_.isArray(value) || value.length < 1) {
            callback(new Error("请先添加该项。"));
            return;
        }
        callback();
    }

    function _checkPassword(rule, value, callback) {
        if (value === '') {
            callback(new Error('请再次输入密码'));
        } else if (vueApp && value !== vueApp.User.ModelDialog.Model.Password) {
            callback(new Error('两次输入密码不一致!'));
        } else {
            callback();
        }
    }

    function _getVueModelOptions(options) {
        options = _.merge(getCustomizedOptions(), {
            data: {
                IsFromApi: true,
                ButtonRoles: getButtonRules(),
                SearchModel: { Code: { Auto: true } },
                User: {
                    Action: { Save: "/AdminUser/ChangePassword" },
                    ModelDialog: {
                        Visible: false,
                        Model: {
                            Original: "",
                            Password: "",
                            Confirm: ""
                        }
                    }
                },
                Validation: {
                    Required: [{ required: true, message: '请输入信息' }],
                    ConfirmPassword: [{ required: true, message: '请输入确认密码' }, { validator: _checkPassword, trigger: 'change' }],
                    Email: [{ required: true, message: '请输入邮箱' }, { type: 'email', message: '请输入正确的邮箱地址', trigger: ['blur', 'change'] }],
                    Phone: [{ required: true, message: '请输入手机' }, { type: 'string', pattern: '^0?(13|14|15|17|18|19)[0-9]{9}$', message: '请输入正确的手机', trigger: ['blur', 'change'] }],
                    Select: [{ required: true }, { validator: _checkSelectValue, trigger: ['change'] }],
                    List: [{ required: true }, { validator: _checkListValue }],
                }
            },
            mounted: function () {
                this.setBaseDataList();
            },
            created: function () {
                this.initPage();
            },
            methods: {
                /*
                * title: String --> Popup Name
                * modelDialog: Object -->  List.ModelDialog
                */
                getTitle: function (title, modelDialog, bIsAdd) {
                    if (bIsAdd != null) {
                        return (bIsAdd ? _default.AddTitle : _default.EditTitle) + " " + title;
                    }
                    modelDialog = modelDialog || _getContext(this, _default.Vue.DefaultForm);
                    if (modelDialog.Model.Id == null) {
                        return title;
                    }
                    return (modelDialog.Model.Id == 0 ? _default.AddTitle : _default.EditTitle) + " " + title;
                },
                getFileName: function (fileName) {
                    return _getFileName(fileName);
                },
                existFile: function (fileName) {
                    return !_isNullOrEmpty(fileName);
                },
                getUrl: function (url, bIsApi) {
                    return _getUrl(url, this.getIsApi(bIsApi));
                },
                getIsApi: function (bIsApi) {
                    if (bIsApi == null) {
                        bIsApi = this.IsFromApi;
                    }
                    return bIsApi;
                },
                getSex: function (sex) {
                    return sex == 1 ? "男" : "女";
                },
                /*
                 * date: 2018-08-08 08:08:08
                 */
                getDate: function (date, bIsDate) {
                    if (bIsDate && date != null && date.length > 10) {
                        return date.substring(0, 10);
                    }
                    return date;
                },
                getDatePrintString: function (date, format) {
                    if (!_.isDate(date)) {
                        date = new Date(date);
                    }
                    alert(JSON.stringify(date));
                    format = format || "yyyy 年 MM 月 dd 日";
                    return _format(date, format);
                },
                getNowString: function () {
                    return this.getDatePrintString(new Date());
                },
                getSingleAmountClass: function(n){
                    switch(n){
                        case 11:
                            return "my-noborder";
                        case 9:
                            return "my-amount-yuan";
                        default:
                            return "";
                    }
                },
                getUpcaseAmount: function (amount) {
                    var strAmount = this.getPrintAmountString(amount);
                    var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
                    strUnit = strUnit.substr(strUnit.length - strAmount.length);
                    var strUpcase = "零壹贰叁肆伍陆柒捌玖";
                    var strOutput = "";
                    for (var i = 0; i < strAmount.length; i++) {
                        strOutput += strUpcase.substr(strAmount.substr(i, 1), 1) + strUnit.substr(i, 1);
                    }
                    return strOutput.replace(/零角零分$/, '整')
                        .replace(/零[仟佰拾]/g, '零')
                        .replace(/零{2,}/g, '零')
                        .replace(/零([亿|万])/g, '$1')
                        .replace(/零+元/, '元')
                        .replace(/亿零{0,3}万/, '亿')
                        .replace(/^元/, "零元");
                },
                getAmountFormat:function(amount){
                    var strAmount = amount.toFixed(2);
                    var strOutput = "";
                    var intFixed = 0;
                    for (var i = strAmount.length - 1; i >= 0; i--) {
                        strOutput = strAmount[i] + strOutput;
                        intFixed++;
                        if (strAmount[i] == ".") {
                            intFixed = 0;
                        }
                        if (intFixed == 3 && i > 0) {
                            strOutput = "," + strOutput;
                            intFixed = 0;
                        }
                    }
                    return strOutput;
                },
                getPrintAmountString: function (amount) {
                    return amount.toFixed(2).replace('.', '');
                },
                getSingleAmount: function (amount, index) {
                    var strAmount = this.getPrintAmountString(amount);
                    index = 11 - index;
                    if (index > strAmount.length) {
                        return "";
                    }
                    return strAmount[strAmount.length - index - 1];
                },
                getTotalAmount: function (ary, func) {
                    func = func || function (item) { return item.Total * item.Price; };
                    var total = 0;
                    _.each(ary, function (item) {
                        total += func(item);
                    });
                    return total;
                },
                getProp: function (field, index, prop) {
                    return field + "[" + index + "]." + prop;
                },
                existButton: function (codes) {
                    var aryCode = codes.split(',');
                    for (var i = 0; i < aryCode.length; i++) {
                        if (!_isNullOrEmpty(this.getButtonText(aryCode[i]))) {
                            return true;
                        }
                    }
                    return false;
                },
                getButtonText: function (code) {
                    return _getItemValue(this.ButtonRoles, o => o.Code == code, 'Name');
                },
                getButtonIcon: function (code) {
                    return _getItemValue(this.ButtonRoles, o => o.Code == code, 'Icon');
                },
                getArrayItem: function (array, id) {
                    return _getArrayItem(array, o => o.Id == id);
                },
                /*
                 * key : Default "Name"
                 */
                getItemValue: function (array, id, key) {
                    key = key || "Name";
                    return _getItemValue(array, o => o.Id == id, key);
                },
                getUploadData: function () {
                    return {
                        __RequestVerificationToken: $("input[name=__RequestVerificationToken]").val()
                    };
                },
                initPage: function () {
                },
                handleSelect: function (url) {
                    _redirect(url);
                },
                setMutipleSelectModel: function (mutipleSelectModel, field, modelField) {
                    var modelList = _.map(mutipleSelectModel, function (item) {
                        var obj = {};
                        obj[field] = item;
                        return obj;
                    });
                    eval("this." + modelField + "=" + JSON.stringify(modelList));
                },
                setMutipleSelectData: function (modelList, field, modelField) {
                    var modelValue = _.map(modelList, function (item) {
                        return item[field];
                    });
                    eval("this." + modelField + "=" + JSON.stringify(modelValue));
                },
                updateSelectTextBind: function (aryData, key, field) {
                    var value = this.getItemValue(aryData, key);
                    eval("this." + field + "='" + value + "'");
                },
                handleMenuCommand: function (command) {
                    switch (command) {
                        case "change-password":
                            this.User.ModelDialog.Visible = true;
                            break;
                    }
                },
                /*
                * form: String --> "List.ModelDialog"
                * model: Object
                */
                openDialog: function (form, model,IdName) {
                    form = form || _default.Vue.DefaultForm;
                    IdName=IdName || _default.Vue.DefaultIdName;
                    var modelDialog = _getContext(this, form);
                    modelDialog.Visible = true;
                    modelDialog.Model = model;
                    mui(IdName).popover('show');
                    this.resetForm(form, model);
                },
                closeDialogBefore: function () {
                    return true;
                },
                /*
                * form: String --> "List.ModelDialog"
                */
                closeDialog: function (form,IdName) {
//              	mui('.mui-content').pullRefresh().setStopped(false);
                    if (!this.closeDialogBefore()) {
                        return;
                    }
                    var _this = this;
                    form = form || _default.Vue.DefaultForm;
                    IdName = IdName || _default.Vue.DefaultIdName;
                    mui(IdName).popover('hide');
                    var modelDialog = _getContext(this, form);
					 _this.resetForm(form);
                    if (modelDialog.Progress != null) {
                        _.each(modelDialog.Progress, function (item, index) {
                            Vue.set(modelDialog.Progress, index, 0);//https://cn.vuejs.org/v2/api/#Vue-set
                        });
                    }
                },
                getProgress: function (context, index) {
                    var progress = context && context.Progress;
                    if (!progress || progress.length - 1 < index) {
                        return 0;
                    }
                    return progress[index];
                },
                setBaseDataList: function () {
                    var _this = this;
                    _.each(_this.SelectList, function (value, key) {
                        if (_.isArray(value) || value.Controller == null) {
                            return;
                        }
                        var action = value.Action || "GetList";
                        _this.getBaseDataList("/" + value.Controller + "/" + action, value.SearchModel, function (data) {
                            _this.SelectList[key].Data = data;
                        });
                    });
                },
                getCacheList: function (controller, id, action_data, target, cache_target) {
                    target = target || controller + "Id";
                    cache_target = cache_target || target;
                    var _this = this;
                    if (id == 0) {
                        _this.Cache[cache_target].push({ Id: id, Data: [] });
                    }
                    var cache = _.find(_this.Cache[cache_target], o=>o.Id == id);
                    if (cache != null) {
                    	console.log("cache"+JSON.stringify(cache)+id+target);
                        _this.SelectList[target].Data = cache.Data;
                        return;
                    }
                    action_data = action_data || { Id: id };
                    helper.apiPost("/" + controller + "/GetList", action_data, function (data) {
                        _this.Cache[cache_target].push({ Id: id, Data: data });
                        _this.SelectList[target].Data = data;
                    });
                },
                /*
                 * action: String --> null, Not With Api Or Admin
                 */
                getBaseDataList: function (action, searchModel,callback) {
                    var _this = this;
                    _post(_this.getUrl(action), searchModel, function (data) {
                        callback(data);
                    }, null, this.IsFromApi);
                },
                /*
                * formName: String --> "List.ModelDialog"
                */
                submitFormBefore: function (formName) {
                    return true;
                },
                /*
                * formName: String --> "List.ModelDialog"
                * action: String --> null, Not With Api Or Admin
                */
                submitForm: function (formName, callback, aftersubmitcallback, table, action, bIsApi) {
//              	mui('.mui-content').pullRefresh().setStopped(false);
                    if (!this.submitFormBefore(formName)) {
                        return;
                    }

                    var _this = this;
                    _this.validateForm(formName, function () {
                        callback = callback || _this["handleSave"];
                        //aftersubmitcallback = aftersubmitcallback || _this[_default.Vue.DefaultDataList];
                        callback(formName, aftersubmitcallback, action, bIsApi);
                    });
                },
                /*
                * form： String --> "List.ModelDialog"
                * table: String --> "List"
                * action: String --> Not With Api Or Area
                */
                handleSubmit: function (form, callback, action, table, bIsApi) {
                    var table = _getTableByForm(form);
                    var _this = this;
                    var modelDialog = _getContext(this, form);
                    //callback = callback || _this[_default.Vue.DefaultDataList];
                    _post(_this.getUrl(action, bIsApi), modelDialog.Model, function (data) {
                        _this.closeDialog(form, null, modelDialog);
                        if (callback != null) {
                            _resetPage(_this[table]);
                            callback(table);
                        }
                    }, null, _this.getIsApi(bIsApi));
                },
                /*
                * form： String --> "List.ModelDialog"
                * table: String --> "List"
                */
                handleSave: function (form, callback, table, action, bIsApi) {
                    var table = _getTableByForm(form);
                    var modelDialog = _getContext(this, form);
                    var cmd = modelDialog.Model.Id == null ? "Save" : (modelDialog.Model.Id == 0 ? "Add" : "Edit");
                    action = action || this[table].Action[cmd];
                    this.handleSubmit(form, callback, action, bIsApi);
                },
                /*
               * formName: String --> "List.ModelDialog"
               */
                validateForm: function (formName, validCallback) {
                    var _this = this;
                    if(!willvalidate(_this.$refs[formName])){
                    	_alert(_default.InValidModel);
                        return;
                    }
                    validCallback && validCallback();
                },
                /*
                * formName: String --> "List.ModelDialog"
                */
                resetForm: function (formName, model) {
                    var _this = this;
                    if (model) {
                        if (_this.getItemId(model) == 0) {//Add
                            setTimeout(function () {
                                _this.$refs[formName] && clearErrors(_this.$refs[formName]);
                            }, 50);
                        }
                        return;
                    }
                    setTimeout(function () {
                        // _this.$refs[formName] && clearErrors(_this.$refs[formName]).resetFields();
                    }, 50);
                },
                setMenu: function () {
                    var _this = this;
                    this.Common.MenuVisible = !this.Common.MenuVisible;
                    var targetWidth = (_this.Common.MenuVisible ? _default.SideWidth : _default.SideHideWidth);
                    var interval = setInterval(function () {
                        if (_this.Common.SideWidth == targetWidth) {
                            clearInterval(interval);
                            return;
                        }
                        _this.Common.SideWidth = _this.Common.SideWidth + 20 * (_this.Common.MenuVisible ? 1 : -1);
                    }, 10);
                },
                getSearchButtonIcon: function (code) {
                    return _isNullOrEmpty(code) ? "el-icon-loading" : "el-icon-refresh";
                },
                getSearchText: function (aryObj, callback) {
                    var strSearchText = "";
                    _.each(aryObj, function (item) {
                        var strResult = callback(item);
                        if (!_isNullOrEmpty(strResult)) {
                            strSearchText += "," + strResult;
                        }
                    });
                    return strSearchText.substr(1);
                },
                getCodeSearchModel: function (cmd) {
                    return this.SearchModel.Code;
                },
                getCode: function (action, model, form, bIsApi) {
                    model.Code = "";
                    _post(this.getUrl(action), this.getCodeSearchModel(form), function (data) {
                        model.Code = data;
                        model.SN = data;
                    }, null, this.getIsApi(bIsApi));
                },
                getOrderIndex: function (action, model, bIsApi) {
                    _post(this.getUrl(action), null, function (data) {
                        model.OrderIndex = data;
                    }, null, this.getIsApi(bIsApi));
                },
                handleSpecialRule: function (action, model, form) {
                    var _this = this;
                    if (this.SearchModel.Code.Auto && _.has(model, "Code") && _isNullOrEmpty(model.Code)) {
                        _this.getCode(action.Code, model, form);
                    }
                    if (_.has(model, "OrderIndex") && model.OrderIndex == 0) {
                        _this.getOrderIndex(action.OrderIndex, model);
                    }
                    if (_.has(model, "SessionName") && _isNullOrEmpty(model.SessionName)) {
						var User=eval("(" + localStorage.getItem("Session") + ")");
                        model.SessionName = User.UserName;

                    }
                },
                handleCopyDefaultBefore: function (model) {
                    model.Id = 0;
                    if (_.has(model, "Code")) {
                        model.Code = "";
                    }
                    if (_.has(model, "OrderIndex")) {
                        model.OrderIndex = 0;
                    }
                    return model;
                },
                handleCopyBefore: function (model) {
                    return model;
                },
                /*
               * form: String --> "List.ModelDialog"
               * bIsFromRemote: boolen --> true, false
               */
                handleCopy: function (row, form, bIsFromRemote, bIsApi) {
                    var _this = this;
                    var modelDialog = _getContext(_this, form);
                    var action = _this[form.split('.')[0]].Action;
                    if (!bIsApi) {
                        var copyModel = _.cloneDeep(row);
                        copyModel = _this.handleCopyDefaultBefore(copyModel);
                        copyModel = _this.handleCopyBefore(copyModel);
                        _this.handleSpecialRule(action, copyModel, form);
                        modelDialog.Model.CopyModel = copyModel;
                        _this.handleAdd(form, null, bIsApi);
                        return;
                    }
                  
                    var action_data = { Id: _this.getItemId(row) };
                    _get(_this.getUrl(action.Detail, bIsApi), action_data, function (data) {
                        data.Id = 0;
                        var copyModel = data;
                        copyModel = _this.handleCopyDefaultBefore(copyModel);
                        copyModel = _this.handleCopyBefore(copyModel);
                        _this.handleSpecialRule(action, copyModel, form);
                        modelDialog.Model.CopyModel = copyModel;
                        _this.handleAdd(form, null, bIsApi);
                    }, null, _this.getIsApi(bIsApi));
                },
                handleOpenPrintDialog: function (model, modelDialog) {
                    modelDialog.Model = model;
                    modelDialog.Visible = true;
                },
                handleOpenDialog: function (model, modelDialog,IdName) {
                    modelDialog.Model = model;
					IdName = IdName || _default.Vue.DefaultIdName;
                    mui(IdName).popover('show');
                },
                handlePrint: function (printNodeId) {
                    if (!this.$htmlToPaper) {
                        _error("请使用_PrintLayout.cshtml.");
                        return;
                    }
                    this.$htmlToPaper(printNodeId);
                }
            }
        }, options);
        return _getVueOptions(options);
    }

    /*
    * action: String --> null, Not With Api Or Admin
    */
    function _getVueListOptions(controller, options, pageSize) {
        options = _.merge({
            data: {
                List: _getInitList(null, { Action: _getActionList(controller), PageParam: { PageSize: pageSize } })
            },
            watch: getSearchModelWatch(_default.Vue.DefaultTable, function () { this.getPageList() }),
            methods: {
                isSelectable: function (row) {
                    return !this.isLockedItem(row);
                },
                isLockedItem: function (row) {
                    return row.IsLocked;
                },
                getCustomizedSearchModel: function (table, options) {
                    table = table || _default.Vue.DefaultTable;
                    var context = _getContext(this, table);
                    var searchModel = context.IsPageList ? {
                        PageParam: context.PageParam,
                        SearchModel: context.SearchModel
                    }: context.SearchModel;
                    return _.merge(searchModel, options);
                },
                getSearchModel: function (table) {
                    return this.getCustomizedSearchModel(table);
                },
                /*
                * index
                */
                getCharIndex: function (index) {
                    return "ABCDEFGHIJKLMN"[index];
                },
                /*
                * table: String --> "List"
                * action: String --> null, Not With Api Or Admin
                */
                getPageList: function (table, bIsApi) {
                    table = table || _default.Vue.DefaultTable;
                    var _this = this;
                    var action = _this[table].Action.View;
                    _post(_this.getUrl(action, bIsApi), _this.getSearchModel(table), function (data) {
                        _setPageModel(data, _this[table]);
                    }, null, _this.getIsApi(bIsApi));
                },
                /*
                * table: Object --> List
                */
                handleSelectionChange: function (val, table) {
                    table = table || this[_default.Vue.DefaultTable];
                    table.MultipleSelection = val;
					
                },
                /*
                * table: String --> List
                */
                handleSortChange: function (sort, table) {
                    table = table || _default.Vue.DefaultTable;
                    if (_isNullOrEmpty(sort) || _isNullOrEmpty(sort.prop)) {
                        this[table].SearchModel.Sort = "";
                    } else {
                        this[table].SearchModel.Sort = sort.prop + " " + (sort.order == "ascending" ? "ASC" : "DESC");
                    }
                    this.getPageList(table);
                },
                getItemId: function (row) {
                    return row.Id
                },
                /*
                * table: Object --> List
                */
                getSelectedIdList: function (table) {
                    var _this = this;
                    table = table || _this[_default.Vue.DefaultTable];
                    var idList = [];
                    _.each(table.MultipleSelection, function (item) {
                        idList.push(_this.getItemId(item));
                    });
                    return idList;
                }
            }
        }, options);

        return _getVueModelOptions(options);
    }

    /*
    * controller：String --> "ControllerName"
    * initModel: Object --> {List:_getInitList({Id:0,Name:""}), OtherList:_getInitList({Id:0,Name:""})}
                        --> {List:{ModelDialog:{Model:{Id:0,Name:""}}}, OtherList:{ModelDialog:{Model:{Id:0,Name:""}}}}
    *         OR Function --> function(){return {Id:0,Name:""}}
    */
    function _getPageVueOptions(controller, initModel, options, pageSize) {
        if (typeof (initModel) == "function") {
            initModel = { List: { ModelDialog: { Model: initModel } } };
        } else {
            initModel = _.merge({ List: { ModelDialog: { Model: function () { return {} } } } }, initModel);
        }
        options = _.merge({
            data: {
                List: {
                    ModelDialog: {
                        Model: _getContext(initModel, _default.Vue.DefaultForm).Model()
                    }
                }
            },
            methods: {
                initPage: function () {
                    this.getPageList();
                },
                getArrayLeafKeys: function (array) {
                    var _this = this;
                    var keys = [];
                    array.forEach(function (item) {
                        if (item.Children == null || item.Children.length == 0) {
                            keys.push(item.Id);
                            return;
                        }
                        _this.getArrayLeafKeys(item.Children).forEach(function (key) {
                            keys.push(key);
                        });
                    });
                    return keys;
                },
                getSelectedLeafKeys: function (allTreeList, idList) {
                    return this.getArrayLeafKeys(allTreeList).filter(item => idList.findIndex(o => o == item) >= 0);
                },
                resetTree: function (refTree, allTreeList, idList) {
                    var selectedLeafKeys = this.getSelectedLeafKeys(allTreeList, idList);
                    var tree = this.$refs[refTree];
                    tree && tree.setCheckedKeys(selectedLeafKeys);
                },
                getTreeSelectedIdList: function (refTree) {
                    var tree = this.$refs[refTree];
                    return (tree.getCheckedKeys().concat(tree.getHalfCheckedKeys())).sort();
                },
                setTreeSelectedKeys: function (refTree, model, field) {
                    model[field] = this.getTreeSelectedIdList(refTree);
                },
                /*
                * form: String --> "List.ModelDialog"
                */
                handleAddBefore: function (model, form) {
                    return model;
                },
                /*
                * form: String --> "List.ModelDialog"
                */
                handleAdd: function (form, handleAddBefore, bIsApi,IdName) {
//					mui('.mui-content').pullRefresh().setStopped(true);
                    form = form || _default.Vue.DefaultForm;
                    var _this = this;
                    var modelDialog = _getContext(this, form);
                    var model = null;
                    if (modelDialog.Model.CopyModel != null) {
                        model = modelDialog.Model.CopyModel;
                        modelDialog.Model.CopyModel = null;
                    } else {
                        model = _getContext(initModel, form).Model();
                    }

                    handleAddBefore = handleAddBefore || _this.handleAddBefore;
                    model = handleAddBefore(model, form);
					var table = _getTableByForm(form);
                    var action = _this[table].Action;
                    _this.handleSpecialRule(action, model, form);
                    _this.openDialog(form, model,IdName);
                },
                handleEditBefore: function (model, form) {
                    return model;
                },
                /*
                * form: String --> "List.ModelDialog"
                * bIsFromRemote: boolen --> true, false
                */
                handleEdit: function (index, row, form, bIsFromRemote, handleEditBefore, bIsApi) {
                    var _this = this;
                    form = form || _default.Vue.DefaultForm;
                    handleEditBefore = handleEditBefore || _this.handleEditBefore;
                    if (!bIsFromRemote) {
                        var model = handleEditBefore(JSON.parse(JSON.stringify(row)), form);
                        _this.openDialog(form, model);//https://cn.vuejs.org/v2/api/#data
                        return;
                    }
                    var action = _this[form.split('.')[0]].Action.Detail;
                    var action_data = { Id: _this.getItemId(row) };
                    _get(_this.getUrl(action, bIsApi), action_data, function (data) {
                        var model = handleEditBefore(data, form);
                        _this.openDialog(form, model);
                    }, null, _this.getIsApi(bIsApi));
                },
                handleAddRowBefore: function (index, collection, form) {
                    return true;
                },
                handleAddRow: function (item, collection, form, handleAddRowBefore) {
                    handleAddRowBefore = handleAddRowBefore || this.handleAddRowBefore;
                    if (!handleAddRowBefore(item, collection, form)) {
                        return;
                    }
                    collection.push(item);
                },
                handleDeleteRowBefore: function (index, collection, cmd) {
                    return true;
                },
                handleDeleteRow: function (index, collection, cmd, handleDeleteRowBefore) {
                    handleDeleteRowBefore = handleDeleteRowBefore || this.handleDeleteRowBefore;
                    if (!handleDeleteRowBefore(index, collection, cmd)) {
                        return;
                    }
                    collection.splice(index, 1);
                },
                /*
                * table: String --> "List"
                * action: String --> Not With Api Or Area
                */
                handleDelete: function (index, row, callback, table, bIsApi) {
                    table = table || _default.Vue.DefaultTable;
                    var _this = this;
                    var action = _this[table].Action.Delete;
                    callback = callback || this[_default.Vue.DefaultDataList];
                    var action_data = { Data: _this.getItemId(row) };
					plus.nativeUI.confirm("确定删除这条记录？", function(e){
						if (e.index==0) {
                        _post(_this.getUrl(action, bIsApi), action_data, function (data) {
                            _resetPage(_this[table]);
                            callback(table);
                        }, null, _this.getIsApi(bIsApi));
							}				 
						})
//                  _delete(function () {
//                      _post(_this.getUrl(action, bIsApi), action_data, function (data) {
//                          _resetPage(_this[table]);
//                          callback(table);
//                      }, null, _this.getIsApi(bIsApi));
//                  });
                },
				
				 isMultipleSelection: function (table) {
                    table = table || _default.Vue.DefaultTable;
                    if (this[table].MultipleSelection.length == 0) {
                        _alert(_default.SelectRecordFirst);
                        return false;
                    }
                    return true;
                },
                /*
                * table: String --> "List"
                * action: String --> Not With Api Or Area
                */
                handleDeleteList: function (callback, table, bIsApi, bIsSoftDelete) {
                    var _this = this;
                    table = table || _default.Vue.DefaultTable;

                    if (_this[table].MultipleSelection.length == 0) {
                        _alert(_default.SelectRecordFirst);
                        return;
                    }

                    var action = _this[table].Action[bIsSoftDelete ? "SoftDeleteList" : "DeleteList"];
                    callback = callback || this[_default.Vue.DefaultDataList];
                    var action_data = { Data: _this.getSelectedIdList(_this[table]) };
                    _delete(function () {
                        _post(_this.getUrl(action, bIsApi), action_data, function (data) {
                            _resetPage(_this[table]);
                            callback(table);
                        }, null, _this.getIsApi(bIsApi));
                    });
                },
                getEnabeldText: function (bIsEnabled) {
                    return bIsEnabled ? "禁用" : "启用";
                },
                getEnabeldType: function (bIsEnabled) {
                    return bIsEnabled ? 'warning' : 'success';
                },
                /*
                 * action: String --> Not With Api Or Area
                 */
                handleEnabled: function (row, table, bIsApi) {
                    table = table || _default.Vue.DefaultTable;
                    var _this = this;
					plus.nativeUI.confirm("确定要" + _this.getEnabeldText(row.Enabled) + "该记录吗？", function(e){
						if (e.index==0) {
                        var action = _this[table].Action.Enabled;
                        _post(_this.getUrl(action + "/" + _this.getItemId(row), bIsApi), null, function (data) {
                            _this.getPageList();
                        }, null, _this.getIsApi(bIsApi));												 
						}
					});
                    
//                  _confirm("确定要" + _this.getEnabeldText(row.Enabled) + "该记录吗？", function () {
//                      var action = _this[table].Action.Enabled;
//                      _post(_this.getUrl(action + "/" + _this.getItemId(row), bIsApi), null, function (data) {
//                          _this.getPageList();
//                      }, null, _this.getIsApi(bIsApi));
//                  });
                },
                getLockCode: function (bIsLocked) {
                    return bIsLocked ? "Unlock" : "Lock";
                },
                getLockText: function (bIsLocked) {
                    return bIsLocked ? "解锁" : "锁定";
                },
                getLockType: function (bIsLocked) {
                    return bIsLocked ? 'success' : 'danger';
                },
                /*
                * action: String --> Not With Api Or Area
                */
                handleLock: function (index, row, table, bIsApi) {
                    var _this = this;
                    _confirm("确定要" + _this.getSoftDeleteText(row.IsLocked)+ "该记录吗？", function () {
                        var action = _this[table].Action[_this.getLockCode(row.IsLocked)];
                        _post(_this.getUrl(action + "/" + _this.getItemId(row), bIsApi), null, function (data) {
                            _this.getPageList();
                        }, null, _this.getIsApi(bIsApi));
                    });
                },
                getSoftDeleteCode: function (bIsDeleted) {
                    return bIsDeleted ? "Recover" : "SoftDelete";
                },
                getSoftDeleteText: function (bIsDeleted) {
                    return bIsDeleted ? "恢复" : "删除";
                },
                getSoftDeletedType: function (bIsDeleted) {
                    return bIsDeleted ? 'success' : 'danger';
                },
                /*
                * action: String --> Not With Api Or Area
                */
                handleSoftDelete: function (index, row, table, bIsApi) {
                    var _this = this;
                    
					plus.nativeUI.confirm("确定要" + _this.getSoftDeleteText(row.IsDeleted) + "该记录吗？", function(e){
						if (e.index==0) {
                        var action = _this[table].Action[_this.getSoftDeleteCode(row.IsDeleted)];
                        _post(_this.getUrl(action + "/" + _this.getItemId(row), bIsApi), null, function (data) {
                            _this.getPageList();
                        }, null, _this.getIsApi(bIsApi));				 
						}
					});
//                  _confirm("确定要" + _this.getSoftDeleteText(row.IsDeleted) + "该记录吗？", function () {
//                      var action = _this[table].Action[_this.getSoftDeleteCode(row.IsDeleted)];
//                      _post(_this.getUrl(action + "/" + _this.getItemId(row), bIsApi), null, function (data) {
//                          _this.getPageList();
//                      }, null, _this.getIsApi(bIsApi));
//                  });
                },
                /*
                * table: String --> "List"
                * action: String --> Not With Api Or Area
                */
                handleSoftDeleteList: function (callback, table, bIsApi) {
                    this.handleDeleteList(callback, table, bIsApi, true);
                },
                handleImportDialog: function (table, bIsApi) {
                    table = table || _default.Vue.DefaultTable;
                    var _this = this;
                    var action = _this[table].ModelDialog.Action;
                    _post(_this.getUrl(action, bIsApi), null, function (data) {
                        _this[table].ModelDialog.NewId = data;
                        _this[table].ModelDialog.Visible = true
                    }, null, _this.getIsApi(bIsApi));
                },
                /*
                * form： String --> "List.ModelDialog"
                * table: String --> "List"
                * action: String --> Not With Api Or Area
                */
                handleImport: function (form, callback, table, action, bIsApi) {
                    table = table || _default.Vue.DefaultTable;
                    action = action || this[table].Action.Import;
                    this.handleSubmit(form, callback, action, table, bIsApi);
                },
                /*
                * fileName: Employee_Export
                * table: String --> "List"
                * bIsApi
                */
                handleExport: function (fileName, table, action, bIsApi) {
                    table = table || _default.Vue.DefaultTable;
                    action = action || this.getUrl(this[table].Action.Export, false);
                    var searchModel = this[table].SearchModel;
                    var querystring = "FileName=" + fileName;
                    for (var key in searchModel) {
                        var value = searchModel[key];
                        if (key == "FileName" || value == null || value == "" || (typeof (value) == "number" && value == 0)) {
                            continue;
                        }
                        querystring += "&" + key + "=" + value;
                    }
                    window.open(action + "?" + querystring);
                },
                /*
                * progressNo: index since 0
                * modelDialog: Object --> List.ModelDialog
                */
                handleBeforeUpload: function (progressNo, modelDialog, file) {
                    modelDialog = modelDialog || _getContext(this, _default.Vue.DefaultForm);
                    Vue.set(modelDialog.Progress, progressNo, 0);//https://cn.vuejs.org/v2/api/#Vue-set
                    return true;
                },
                /*
                * progressNo: index since 0
                * modelDialog: Object --> List.ModelDialog
                */
                handleUploadProgress: function (progressNo, modelDialog, event, file, fileList) {
                    modelDialog = modelDialog || _getContext(this, _default.Vue.DefaultForm);
                    Vue.set(modelDialog.Progress, progressNo, event.percent);//https://cn.vuejs.org/v2/api/#Vue-set
                },
                /*
                * modelDialog: Object --> List.ModelDialog
                */
                handleUploadSuccess: function (field, modelDialog, response, file, fileList) {
                    if (response.Status == _default.Logout) {
                        _alert(response.Message, function () {
                            _redirect(response.Data);
                        });
                        return;
                    }

                    if (response.Status != _default.Ok) {
                        _alert(response.Message);
                        return;
                    }

                    modelDialog = modelDialog || _getContext(this, _default.Vue.DefaultForm);
                    var cmd = "modelDialog.Model";
                    _.each(field.split('.'), function (item) {
                        cmd += "['" + item + "']";
                    });
                    cmd += "=response.Data;";
                    eval(cmd);
                },
                getUploadUrl: function (folder, isApi) {
                    return _getArea(isApi) + "/Upload/" + folder;
                },
                getUploadHeaders: function () {
                    return _getAuthorization();
                },
                /*
               * table: String --> List
               */
                handlePageChange: function (val, table, cmd, callback) {
                    table = table || _default.Vue.DefaultTable;
                    callback = callback || this[_default.Vue.DefaultDataList];
                    this[table].PageParam[cmd] = val;
                    callback(table);
                },
                messageCallBack: function (message, table) {
                    table = table || _default.Vue.DefaultTable;
                    var _this = this;
                    _alert(message, function () {
                        _this.getPageList(table);
                    });
                },
                importCallback: function (table) {
                    this.messageCallBack("导入数据成功。");
                },
            }
        }, options);
        return _getVueListOptions(controller, options, pageSize);
    }


    function _resetPage(table, pageIndex) {
        table = table || (vueApp && (vueApp[_default.Vue.DefaultTable]));
        table && table.PageParam && (table.PageParam.PageIndex = (pageIndex || 1));
    }

    function _setPageModel(data, table) {
        table = table || (vueApp && (vueApp[_default.Vue.DefaultTable]));
        if (table == null) {
            return;
        }

        if (data.Data == null && typeof (data) == "object") {
            table.Data = data;
            return;
        }

        table.Data = data.Data;

        //https://cn.vuejs.org/v2/guide/reactivity.html
        table.PageParam = Object.assign({}, table.PageParam, data.PageParam);
        table.PageModel = Object.assign({}, table.PageModel, data.PageModel);
    }

    function _setModelField(model, key, value) {
        if (!_.isObject(model)) {
            return;
        }
        key = key || "Id";
        value = value || 0;
        if (!_.isArray(model)) {
            model[key] = value;
            return;
        }
        _.each(model, function (item) {
            item[key] = value;
        });
    }

    function _setModel(valueModel, targetModel) {
        if (targetModel == null) {
            var modelDialog = vueApp && (_getContext(vueApp, _default.Vue.DefaultForm));
            targetModel = modelDialog && modelDialog.Model;
        }
        if (targetModel != null) {
            targetModel = Object.assign({}, targetModel, valueModel);
        }
    }
    //#endregion

    return {
        "default": _default,
        setOptions: _setOptions,

        alert: _alert,
        error: _error,
        confirm: _confirm,
        delete: _delete,

        isNullOrEmpty: _isNullOrEmpty,

        redirect: _redirect,

        getIndentRepeatString: _getIndentRepeatString,
        getCookie: _getCookie,
        getUrl: _getUrl,
        getApiUrl: _getApiUrl,
        getQueryString: _getQueryString,
        getFileName: _getFileName,
        addDays: _addDays,
        getDate: _getDate,
        getItemValue: _getItemValue,
        update: _update,
        format: _format,

        ajax: _ajax,
        get: _get,
        post: _post,
        postFile: _postFile,

        apiGet: _apiGet,
        apiPost: _apiPost,
        apiPostFile: _apiPostFile,

        on: _onBus,
        emit: _emitBus,

        getActionUrl: _getActionUrl,
        getActionList: _getActionList,
        getInitList: _getInitList,
        getImportModel: _getImportModel,
        getTreeModel: _getTreeModel,
        getVueOptions: _getVueOptions,
        getVueModelOptions: _getVueModelOptions,
        getVueListOptions: _getVueListOptions,
        getPageVueOptions: _getPageVueOptions,

        getContext: _getContext,

        setPageModel: _setPageModel,
        setModel: _setModel,
        setModelField: _setModelField,
        resetPage: _resetPage
    };
}));

function getCustomizedOptions() {
    return {
        data: {
            Cache: {
                UsedEmployeeId: [],
                StoredAddressId: []
            },
            SelectList: {
                AssetStatus: {
                    Data: [
                        { Id: 1, Name: "闲置", Type: "success" },
                        { Id: 2, Name: "借用", Type: "danger" },
                        { Id: 3, Name: "在用", Type: "" },
                        { Id: 4, Name: "报废", Type: "warn" }
                    ]
                },
                UsedDepartmentEmployeeId: { Data: [] },
                UsedEmployeeId: { Data: [] },
                StoredAddressId: { Data: [] }
            },
        },
        methods: {
            isDisabled: function (row, key) {
                switch (key) {
                    case "选择部门":
                        return this.isCompany(row);
		            case "选择资产类别":
                        return this.isParent(row);
                    default:
                        return false;
                }
            },
            isParent: function (row) {
                return !row.IsLeaf;
            },
            isCompany: function (row) {
                return row.DepartmentType == 1;
            },
            getDepartmentTypeClass: function (row) {
                return this.isCompany(row) ? "my-text-success" : "my-text-info";
            },
            getSpaceString:function(cnt){
            	return helper.getIndentRepeatString(cnt);
            },
            getDepartmentName: function (row) {
                return helper.getIndentRepeatString(row.LayerIndex - 1) + row.Name;
            },
            getCompany: function (value, departmentList) {
                if (value == 0) {
                    return { Id: 0, Name: "" };
                }
                try {
                    departmentList = departmentList || this.SelectList.UsedDepartmentId.Data;
                } catch (e) { throw "The SelectList.UsedDepartmentId.Data is not defined."; }
                var row = this.getArrayItem(departmentList, value);
                while (!this.isCompany(row)) {
                    row = this.getArrayItem(departmentList, row.ParentId);
                }
                return row;
            },
            getEmployeeCacheList: function (departmentId) {
                this.getCacheList("Employee", departmentId, { Id1: departmentId }, "UsedEmployeeId")
            },
            getDepartmentEmployeeCacheList: function (departmentId) {
                this.getCacheList("Employee", departmentId, { Id1: departmentId }, "UsedDepartmentEmployeeId", "UsedEmployeeId")
            },
            getStoredAddressCacheList: function (departmentId) {
                this.getCacheList("StoredAddress", departmentId, { Id: departmentId, Id1: 1 })
            },
            handleDepartmentChange: function (form, value, employeeCacheCallback, storedAddressCacheCallback) {
                var _this = this;
                var context = helper.getContext(_this, form);
                if (_.has(context.Model, "UsedCompanyId")) {
                    var company = _this.getCompany(value);
                    context.Model.UsedCompanyId = company.Id;
                    context.Model.UsedCompanyName = company.Name;
                }
                
                if (_.has(context.Model, "UsedEmployeeId")) {
                    context.Model.UsedEmployeeId = 0;
                    context.Model.UsedEmployeeName = "";
                    employeeCacheCallback = employeeCacheCallback || _this.getEmployeeCacheList;
                    employeeCacheCallback(value);
                }

                if (_.has(context.Model, "StoredAddressId")) {
                    context.Model.StoredAddressId = 0;
                    context.Model.StoredAddressName = "";
                    storedAddressCacheCallback = storedAddressCacheCallback || _this.getStoredAddressCacheList;
                    storedAddressCacheCallback(value);
                }
            },
        }
    }
}

    function getSelectAssetListOptions(options) {
        return _.merge({
            data: {
                Cache: {
                    UsedEmployeeId: []
                },
                SelectedAssetList: helper.getInitList(null, {
                    AssetStatus: 0,
                    AssetItemCallback: null,
                    AssetItemUniqueCallback: null,
                    AssetItemStatuses: [],
                    SelectedAssetList: []
                }, "AssetLocationItem"),
                SelectList: {
                    UsedDepartmentId: { Controller: "Department", Data: [] },
                    UsedDepartmentEmployeeId: { Data: [] }
                },
            },
            watch: getSearchModelWatchList([{ TableName: "SelectedAssetList", InitEvent: function () { this.getPageList("SelectedAssetList"); }, IgnoreList: ["Id1", "Id2", "Id4"] }]),
            methods: {
                handleAssetDepartmentChange: function (value) {
                    var row = this.getArrayItem(this.SelectList.UsedDepartmentId.Data, value);
                    if (value == 0 || row == null || this.isCompany(row)) {
                        this.SelectedAssetList.SearchModel.Id1 = value;
                        this.SelectedAssetList.SearchModel.Id2 = 0;
                        this.SelectedAssetList.SearchModel.Id3 = 0;
                        this.SelectList.UsedEmployeeId.EmployeeData = [];
                    } else {
                        this.SelectedAssetList.SearchModel.Id1 = 0;
                        this.SelectedAssetList.SearchModel.Id2 = value;
                        this.getDepartmentEmployeeCacheList(value);
                    }
                    this.getPageList("SelectedAssetList");
                },
                resetSelectedAsset: function (model) {
                    this.SelectedAssetList.AssetItemStatuses = [];
                    this.SelectedAssetList.SelectedAssetList = [];
                    return model;
                },

                selectAssets: function (assetStatus, aryItems, dtoCallback, uniqueCallback) {
                    this.SelectedAssetList.AssetStatus = assetStatus;
                    this.SelectedAssetList.AssetItemCallback = dtoCallback;
                    this.SelectedAssetList.AssetItemUniqueCallback = uniqueCallback || ((o, item) =>
                        o.AssetId == item.AssetItemStatus.Asset.Id
                        & o.UsedCompanyId == item.UsedCompanyId
                        & o.UsedDepartmentId == item.UsedDepartmentId
                        & o.UsedEmployeeId == item.UsedEmployeeId
                        & o.RegionId == item.RegionId
                        & o.StoredAddressId == item.StoredAddressId);
                    this.SelectedAssetList.AssetItemStatuses = aryItems;
                    this.SelectedAssetList.ModelDialog.Data = [];
                    if (this.SelectedAssetList.SearchModel.Id == assetStatus) {
                        this.getPageList("SelectedAssetList");
                    } else {
                        this.SelectedAssetList.SearchModel.Id = assetStatus;
                    }
                    this.openDialog("SelectedAssetList.ModelDialog");
                },
                saveSelectedAssetDialog: function () {
                    if (!this.isMultipleSelection("SelectedAssetList")) {
                        return;
                    }
                    var _this = this;
                    _.each(this.SelectedAssetList.MultipleSelection, function (item, index) {
                        var asset = _.cloneDeep(item.AssetItemStatus.Asset);
                        asset.Total = item.Total;
                        var itemTemp = null;
                        if (_this.SelectedAssetList.AssetItemCallback != null){
                            itemTemp = _this.SelectedAssetList.AssetItemCallback();
                            helper.update(itemTemp, asset, ["Id"]);
                            helper.update(itemTemp, item, ["Id"]);
                            helper.update(itemTemp, { AssetId: item.AssetItemStatus.Asset.Id, Asset: asset });
                        }else{
                            itemTemp = {
                                AssetId: item.AssetItemStatus.Asset.Id,
                                UsedCompanyId: item.UsedCompanyId,
                                UsedDepartmentId: item.UsedDepartmentId,
                                UsedEmployeeId: item.UsedEmployeeId,
                                RegionId: item.RegionId,
                                StoredAddressId: item.StoredAddressId,
                                Total: item.Total
                            };
                        }
                        
                        var index = _.findIndex(_this.SelectedAssetList.AssetItemStatuses,
                                o=>_this.SelectedAssetList.AssetItemUniqueCallback(o, item));

                        if (index >= 0) {
                            Vue.set(_this.SelectedAssetList.AssetItemStatuses, index, itemTemp);
                            Vue.set(_this.SelectedAssetList.SelectedAssetList, index, asset);
                        } else {
                            _this.SelectedAssetList.AssetItemStatuses.push(itemTemp);
                            _this.SelectedAssetList.SelectedAssetList.push(asset);
                        }
                    });
                    this.closeDialog('SelectedAssetList.ModelDialog');
                }
            }
        }, options);
    }

function getButtonRules() {
    return [];
}

function getVueOptions() {
    return helper.getVueOptions();
}

	function showReload(){
		var ws=plus.webview.currentWebview();
		ws.reload();
	}