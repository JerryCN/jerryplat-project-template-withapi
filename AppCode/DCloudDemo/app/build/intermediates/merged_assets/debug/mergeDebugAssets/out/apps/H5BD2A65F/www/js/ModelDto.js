
var d=new Date();
function getDepartmentDto() {
    return {"Id":0,"Code":null,"Name":null,"ParentId":0,"ParentName":null,"DepartmentType":0,"Enabled":false,"LayerIndex":0,"Children":null}
}

function getEmployeeDto() {
    return {"Id":0,"Code":null,"Name":null,"CompanyId":0,"CompanyName":null,"DepartmentId":0,"DepartmentName":null,"Phone":null,"Email":null,"WorkingStatusId":0,"WorkingStatus":null,"IsDeleted":false}
}

function getRegionDto() {
    return {"Id":0,"Name":null,"OrderIndex":0,"IsLocked":false}
}

function getAssetTypeDto() {
    return {"Id":0,"Name":null,"ParentId":0,"ParentName":null,"IsLocked":false,"LayerIndex":0,"IsLeaf":false,"Children":null};
}

 function getAssetBaseDto() {
    return {"Id":0,"AssetStorageId":0,"Code":null,"AssetTypeId":0,"AssetTypeName":null,"Name":null,"Model":null,"SN":null,"UnitId":0,"Total":0,"UnitName":null,"Price":0.0,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"BuyTime":helper.format(d, "yyyy-MM-dd"),"UsedEmployeeId":0,"UsedEmployeeName":null,"SessionId":0,"SessionName":null,"RegionId":0,"RegionName":null,"StoredAddressId":0,"StoredAddressName":null,"UsedMonth":0,"SourceId":0,"SourceName":null,"Note":null,"PicPath":null,"SupplierId":0,"SupplierName":null,"Contract":null,"ContractWay":null,"Director":null,"EndDate":helper.format(d, "yyyy-MM-dd"),"Discription":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"UpdateTime":helper.format(d, "yyyy-MM-dd")};
}

function getAssetOtherInfoDto() {
    return {"Id":0,"AssetId":0,"AssetName":null,"Key":null,"Value":null};
}

function getAssetStorageDto() {
    return {"Id":0,"Code":null,"SessionId":0,"SessionName":null,"StorageTime":helper.format(d, "yyyy-MM-dd"),"CreateTime":helper.format(d, "yyyy-MM-dd"),"Description":null,"StorageType":1,"Assets":[]};
}

function getAssetDto() {
    return {"IsEdited":false,"IsDeleted":false,"AssetItemStatuses":[],"AssetOperationRecords":[],"AssetOtherInfos":[],"AssetBorrowItems":[],"AssetBorrowReturnItems":[],"AssetUsedItems":[],"AssetUsedBackItems":[],"AssetUpdateItems":[],"AssetClearItems":[],"AssetFinancialUpdates":[],"AssetOtherInfo":"","Id":0,"AssetStorageId":0,"Code":null,"AssetTypeId":0,"AssetTypeName":null,"Name":null,"Model":null,"SN":null,"UnitId":0,"Total":0,"UnitName":null,"Price":0.0,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"BuyTime":helper.format(d, "yyyy-MM-dd"),"UsedEmployeeId":0,"UsedEmployeeName":null,"SessionId":0,"SessionName":null,"RegionId":0,"RegionName":null,"StoredAddressId":0,"StoredAddressName":null,"UsedMonth":0,"SourceId":0,"SourceName":null,"Note":null,"PicPath":null,"SupplierId":0,"SupplierName":null,"Contract":null,"ContractWay":null,"Director":null,"EndDate":helper.format(d, "yyyy-MM-dd"),"Discription":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"UpdateTime":helper.format(d, "yyyy-MM-dd")};
}

function getAssetUsedDto() {
    return {"Id":0,"Code":null,"UsedTime":helper.format(d, "yyyy-MM-dd"),"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"RegionId":0,"RegionName":null,"StoredAddressId":0,"StoredAddressName":null,"EstimatedReturnTime":helper.format(d, "yyyy-MM-dd"),"SessionId":0,"SessionName":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"Description":null,"AssetUsedItems":[]};
}

function getAssetUsedBackDto() {
    return {"Id":0,"Code":null,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"SessionId":0,"SessionName":null,"Description":null,"BackTime":helper.format(d, "yyyy-MM-dd"),"CreateTime":helper.format(d, "yyyy-MM-dd"),"AssetUsedBackItems":[]};
}

function getAssetBorrowDto() {
    return {"Id":0,"Code":null,"LendingTime":helper.format(d, "yyyy-MM-dd"),"EstimatedReturnTime":helper.format(d, "yyyy-MM-dd"),"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"RegionId":0,"RegionName":null,"StoredAddressId":0,"StoredAddressName":null,"SessionId":0,"SessionName":null,"Description":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"AssetBorrowItems":[]};
}

function getAssetBorrowReturnDto() {
    return {"Id":0,"Code":null,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"ReturnTime":helper.format(d, "yyyy-MM-dd"),"SessionId":0,"SessionName":null,"Description":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"AssetBorrowReturnItems":[]};
}

function getAssetUpdateDto() {
    return {"Id":0,"Code":null,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"Description":null,"SessionId":0,"SessionName":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"AssetUpdateItems":[]};
}

function getAssetUpdateItemDto() {
    return {"Id":0,"AssetUpdateId":0,"AssetId":0,"AssetTypeId":0,"AssetTypeName":null,"Name":null,"Model":null,"SN":null,"UnitId":0,"UnitName":null,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"BuyTime":helper.format(d, "yyyy-MM-dd"),"RegionId":0,"RegionName":null,"StoredAddressId":0,"StoredAddressName":null,"UsedMonth":0,"SourceId":0,"SourceName":null,"Note":null,"PicPath":null,"SupplierId":0,"SupplierName":null,"Contract":null,"ContractWay":null,"Director":null,"EndDate":helper.format(d, "yyyy-MM-dd"),"Discription":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"SessionId":0,"SessionName":null,"UpdateRecord":null,"Asset":null};
}

function getAssetFinancialUpdateDto() {
    return {"Id":0,"Code":null,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"Description":null,"SessionId":0,"SessionName":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"AssetFinancialUpdateItems":[]};
}

function getAssetFinancialUpdateItemDto() {
    return {"Id":0,"AssetUpdateId":0,"AssetId":0,"Price":0.0,"CreateTime":helper.format(d, "yyyy-MM-dd"),"SessionId":0,"SessionName":null,"UpdateRecord":null,"Asset":null};
}

function getAssetClearDto() {
    return {"Id":0,"Code":null,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"UsedEmployeeId":0,"UsedEmployeeName":null,"SessionId":0,"SessionName":null,"Description":null,"ClearTime":helper.format(d, "yyyy-MM-dd"),"CreateTime":helper.format(d, "yyyy-MM-dd"),"AssetClearItems":[]};
}

function getAssetBaseDto() {
    return {"Id":0,"AssetStorageId":0,"Code":null,"AssetTypeId":0,"AssetTypeName":null,"Name":null,"Model":null,"SN":null,"UnitId":0,"Total":0,"UnitName":null,"Price":0.0,"UsedCompanyId":0,"UsedCompanyName":null,"UsedDepartmentId":0,"UsedDepartmentName":null,"BuyTime":helper.format(d, "yyyy-MM-dd"),"UsedEmployeeId":0,"UsedEmployeeName":null,"SessionId":0,"SessionName":null,"RegionId":0,"RegionName":null,"StoredAddressId":0,"StoredAddressName":null,"UsedMonth":0,"SourceId":0,"SourceName":null,"Note":null,"PicPath":null,"SupplierId":0,"SupplierName":null,"Contract":null,"ContractWay":null,"Director":null,"EndDate":helper.format(d, "yyyy-MM-dd"),"Discription":null,"CreateTime":helper.format(d, "yyyy-MM-dd"),"UpdateTime":helper.format(d, "yyyy-MM-dd"),"AttachmentPath":null,"AssetOtherInfos":[],"AssetOtherInfo":""};
}

function getInventoryDto() {
    return {"Id":0,"Name":null,"Note":null,"SessionId":0,"SessionName":null,"StartTime":helper.format(d, "yyyy-MM-dd"),"EndTime":helper.format(d, "yyyy-MM-dd"),"CreateTime":helper.format(d, "yyyy-MM-dd"),"InventoryStatus":1,"Description":null,"InventoryUsers":[],"InventoryDepartments":[],"InventoryAssetTypes":[],"InventoryRegions":[]};
}

function getInventoryItemDto() {
    return {"UsedCompanyName":null,"UsedDepartmentName":null,"UsedEmployeeName":null,"RegionName":null,"StoredAddressName":null,"OldTotal":0,"NewTotal":0,"SessionName":null,"Asset":null,"Id":0,"InventoryId":0,"AssetStatus":0,"AssetId":0,"UsedCompanyId":0,"UsedDepartmentId":0,"UsedEmployeeId":0,"RegionId":0,"StoredAddressId":0,"SessionId":0,"CreateTime":helper.format(d, "yyyy-MM-dd")};
}

function getInventoryStatus() {
    return [{Id:1,Name:"创建盘点",FieldName:"Created"},{Id:2,Name:"正在盘点",FieldName:"Incomplete"},{Id:3,Name:"完成盘点",FieldName:"Complete"},{Id:4,Name:"盘点入库",FieldName:"Submitted"}];
}

function getAssetStatus() {
    return [{Id:1,Name:"闲置",FieldName:"Free"},{Id:2,Name:"借用",FieldName:"Borrow"},{Id:3,Name:"在用",FieldName:"Used"},{Id:4,Name:"报废",FieldName:"Clear"}];
}

function getAssetLabelDto() {
    return {"Id":0,"Width":5.0,"Height":3.0,"IsDefaultUsed":false,"AssetLabelItems":[]};
}

function getOperationType() {
    return [{Id:1,Name:"资产入库",FieldName:"Create"},{Id:2,Name:"资产信息变更",FieldName:"Update"},{Id:3,Name:"资产财务信息变更",FieldName:"FinancialUpdate"},{Id:4,Name:"资产借出",FieldName:"Borrow"},{Id:5,Name:"资产归还",FieldName:"BorrowReturn"},{Id:6,Name:"资产领用",FieldName:"Used"},{Id:7,Name:"资产退库",FieldName:"UsedBack"},{Id:8,Name:"资产清理报废",FieldName:"Clear"},{Id:9,Name:"资产盘点",FieldName:"Inventory"}];
}

function getAssetStatus() {
    return [{Id:1,Name:"闲置",FieldName:"Free"},{Id:2,Name:"借用",FieldName:"Borrow"},{Id:3,Name:"在用",FieldName:"Used"},{Id:4,Name:"报废",FieldName:"Clear"}];
}

function getOperationType() {
    return [{Id:1,Name:"资产入库",FieldName:"Create"},{Id:2,Name:"资产信息变更",FieldName:"Update"},{Id:3,Name:"资产财务信息变更",FieldName:"FinancialUpdate"},{Id:4,Name:"资产借出",FieldName:"Borrow"},{Id:5,Name:"资产归还",FieldName:"BorrowReturn"},{Id:6,Name:"资产领用",FieldName:"Used"},{Id:7,Name:"资产退库",FieldName:"UsedBack"},{Id:8,Name:"资产清理报废",FieldName:"Clear"},{Id:9,Name:"资产盘点",FieldName:"Inventory"}];
}

function getStoredAddressDto() {
    return {"Id":0,"DepartmentId":0,"DepartmentName":null,"Name":null,"OrderIndex":0,"Enabled":true,"IsDeleted":false};
}








