function getDebounceEvent(initEvent) {
        	return _.debounce(initEvent, 500);
        }

        function getSearchModel() {
        	return {
        		"Id1": 0,
        		"Id2": 0,
        		"Id3": 0,
        		"Id4": 0,
        		"SearchText": null,
        		"StartTime": null,
        		"EndTime": null,
        		"FileName": null,
        		"Id": 0,
        		"Sort": null
        	};
        }
        /*
         *table："List"
         *initEvent: function () { this.getPageList("List") }
         *ignoreList: null or ["Id","Id1"]
         */
        function getSearchModelWatch(table, initEvent, ignoreList) {
        	return (function(table, initEvent, ignoreList) {
        		function getEvent(name, bIsString) {
        			if (ignoreList == null || !_.includes(name)) {
        				return bIsString ? getDebounceEvent(initEvent) : initEvent;
        			}
        			return helper.default.Callback;
        		}
        		var watch = {};
        		watch[table + '.SearchModel.Id1'] = getEvent('Id1', false);
        		watch[table + '.SearchModel.Id2'] = getEvent('Id2', false);
        		watch[table + '.SearchModel.Id3'] = getEvent('Id3', false);
        		watch[table + '.SearchModel.Id4'] = getEvent('Id4', false);
        		watch[table + '.SearchModel.SearchText'] = getEvent('SearchText', true);
        		watch[table + '.SearchModel.StartTime'] = getEvent('StartTime', false);
        		watch[table + '.SearchModel.EndTime'] = getEvent('EndTime', false);
        		watch[table + '.SearchModel.Id'] = getEvent('Id', false);
        		return watch;
        	})(table, initEvent, ignoreList);
        }
        /*
         *aryOptions: [{TableName:"List", InitEvent:function () { this.getPageList("List") }, IgnoreList: ["Id","Id1"]}]
         *TableName："List"
         *InitEvent: function () { this.getPageList("List") }
         *IgnoreList: null or ["Id","Id1"]
         */
        function getSearchModelWatchList(aryOptions, otherOptions) {
        	var options = {};
        	_.each(aryOptions, function(item, index) {
        		options = _.merge(options, getSearchModelWatch(item.TableName, item.InitEvent, item.IgnoreList));
        	});
        	return _.extend(options, otherOptions);
        };
		var ws = null,
			wc = null;
		function readyFunction(){
				ws=plus.webview.currentWebview();
				// 用户点击后
				ws.addEventListener("maskClick",function(){
					wc.close("auto");
				},false);
					ws.setStyle({mask:"none"});
					wc=null;
				document.addEventListener("swiperight", function(e) {
				    //默认滑动角度在-45度到45度之间，都会触发右滑菜单，为避免误操作，可自定义限制滑动角度；  
				    if (Math.abs(e.detail.angle) < 4) {  
				       showSide();  
				    }  					
					
				});				
		};
			// 显示侧滑页面
			function showSide() {
				// 防止快速点击可能导致多次创建
				if(wc){
					return;
				}
				// 开启遮罩
				ws.setStyle({mask:"rgba(0,0,0,0.5)"});
				// 创建侧滑页面 
				wc = plus.webview.create("../webview_mask_side.html", "side", {
					right: "30%",
					width: "70%",
					popGesture: "none"
				});
				// 侧滑页面关闭后关闭遮罩
				wc.addEventListener('close',function(){
					ws.setStyle({mask:"none"});
					wc=null;
				},false);
				// 侧滑页面加载后显示（避免白屏）
				wc.addEventListener("loaded", function() {
					wc.show("slide-in-left", 200);
				}, false);
			};		

           // var ApiUrl="http://192.168.50.35:81"
//      var ApiUrl="http://192.168.1.104:81"
        // var ApiUrl="http://192.168.43.18:81"
     var ApiUrl="http://assetapi.bkant.cn";
