﻿using System.Web.Mvc;

namespace JerryPlat.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
#if DEBUG
            return View();
#else
            return Redirect("/Admin/");
#endif
        }

        public ActionResult Download() {
            return View();
        }
    }
}