﻿using JerryPlat.Utils.Models;
using System.Web.Mvc;

namespace JerryPlat.Web.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(int? code)
        {
            ErrorModel errorModel = new ErrorModel();

            if(code == 404)
            {
                errorModel.Title = "404-资源不存在";
                errorModel.Error = "抱歉，您访问的资源不存在！请确认您输入的网址是否正确！";
            }
            else
            {
                string strCode = code.HasValue ? code.ToString() : "未知";
                errorModel.Title = $"页面发生了{strCode}错误";
            }

            ViewBag.ErrorModel = errorModel;
            return View("Error");
        }
    }
}