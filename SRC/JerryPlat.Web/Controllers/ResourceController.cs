﻿using JerryPlat.Utils.Helpers;
using System.Web.Mvc;

namespace JerryPlat.Web.Controllers
{
    public class ResourceController : Controller
    {
        // GET: Resource
        [OutputCache(Duration = 60 * 60 * 24, VaryByParam = "*")]
        public ActionResult Js(string name)
        {
            string strScript = string.Empty;
            if ("Constant" == name)
            {
                strScript = ConstantHelper.GetScript();
            }

            return JavaScript(strScript);
        }

        [OutputCache(Duration = 60 * 60 * 24, VaryByParam = "*")]
        // GET: Resource
        public ActionResult Css(string name)
        {
            return Content("");
        }
    }
}