﻿using JerryPlat.Utils.Helpers;
using System.Drawing;
using System.Web.Mvc;

namespace JerryPlat.Web.Controllers
{
    public class CommonController : Controller
    {
        [OutputCache(Duration = 60 * 60 * 24, VaryByParam = "*")]
        public ActionResult QrCode(string code, int size = 300)
        {
            Bitmap bitMap = CodeHelper.CreateQrCode(code, size);
            return File(TypeHelper.Image2Byte(bitMap), "image/png");
        }

        [OutputCache(Duration = 60 * 60 * 24, VaryByParam = "*")]
        public ActionResult BarCode(string code, int width = 300, int height = 80)
        {
            Image bitMap = CodeHelper.CreateBarCode(code, width, height);
            return File(TypeHelper.Image2Byte(bitMap), "image/png");
        }

        public ActionResult VerifyCode(int len = 4)
        {
            string strVerifyCode = RandomHelper.CreateCode(len);
            SessionHelper.VerifyCode.SetSession(strVerifyCode);
            return File(IOHelper.GenerateValidateGraphic(strVerifyCode), "image/Jpeg");
        }
    }
}