﻿using JerryPlat.Utils.Helpers;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace JerryPlat.Web.App_Start.Filter
{
    public class CompressAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                LogHelper.Error(filterContext.Exception);
                return;
            }

            HttpRequestBase request = filterContext.HttpContext.Request;
            string strAcceptEncoding = request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(strAcceptEncoding))
            {
                return;
            }

            strAcceptEncoding = strAcceptEncoding.ToUpperInvariant();

            HttpResponseBase response = filterContext.HttpContext.Response;

            //判断IIS或者其他承载设备是是否启用了GZip或DeflateStream
            if (response.Filter is GZipStream || response.Filter is DeflateStream)
                return;

            if (strAcceptEncoding.Contains("GZIP"))
            {
                response.Headers.Remove("Content-Encoding");
                response.AppendHeader("Content-encoding", "gzip");
                response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
            }
            else if (strAcceptEncoding.Contains("DEFAULT"))
            {
                response.Headers.Remove("Content-Encoding");
                response.AppendHeader("Content-encoding", "default");
                response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
            }
        }
    }
}