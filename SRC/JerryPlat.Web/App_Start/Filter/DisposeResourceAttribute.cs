﻿using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace JerryPlat.Web.App_Start.Filter
{
    public class DisposeResourceAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            SingleInstanceHelper.DisposeInstance();

            base.OnActionExecuted(filterContext);
        }
    }
}