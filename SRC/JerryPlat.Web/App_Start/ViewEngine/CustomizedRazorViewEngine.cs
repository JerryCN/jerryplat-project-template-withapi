﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JerryPlat.Web.App_Start.ViewEngine
{
    public class CustomizedRazorViewEngine : RazorViewEngine
    {
        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
#if DEBUG
            //不处理
            return new RazorView(controllerContext, viewPath, masterPath, true, base.FileExtensions, base.ViewPageActivator);
#else
            //压缩输出
            return new CustomizedRazorView(controllerContext, viewPath, masterPath, true, base.FileExtensions, base.ViewPageActivator);  
#endif
        }
    }
}