﻿using JerryPlat.Utils.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;

namespace JerryPlat.Web.App_Start.ViewEngine
{
    //https://blog.csdn.net/u011511086/article/details/82911297
    public class CustomizedRazorView : RazorView
    {
        public CustomizedRazorView(ControllerContext controllerContext, string viewPath, string layoutPath, bool runViewStartPages, IEnumerable<string> viewStartFileExtensions)
            :base (controllerContext,viewPath,layoutPath,runViewStartPages,viewStartFileExtensions)
        {

        }
        
        public CustomizedRazorView(ControllerContext controllerContext, string viewPath, string layoutPath, bool runViewStartPages, IEnumerable<string> viewStartFileExtensions, IViewPageActivator viewPageActivator)
            :base(controllerContext,viewPath,layoutPath,runViewStartPages, viewStartFileExtensions, viewPageActivator)
        {

        }
        
        protected override void RenderView(ViewContext viewContext, TextWriter writer, object instance)
        {
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(stringWriter);
            base.RenderView(viewContext, tw, instance);
            string html = stringWriter.ToString();
            html = IOHelper.Compress(html);
            //输出到页面
            writer.Write(html);
        }
    }
}