﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using JerryPlat.Web.Areas.Base;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JerryPlat.Web.Areas.Admin.Controllers
{
    public class LoginController : AdminBaseController<AdminUserHelper>
    {
        [HttpPost]
        public async Task<ActionResult> Login(LoginModel loginModel)
        {
            loginModel.Password = EncryptHelper.Encrypt(loginModel.Password);
            AdminUserDto user = await _helper.GetAsync(loginModel);
            if (user == null)
            {
                return Faild("您输入的用户名或密码错误。");
            }

            loginModel.IsFromApi = false;
            TokenModel model = await ApiHelper.Instance.GetTokenAsync(loginModel);
            if (model == null)
            {
                return Faild("获取Token失败，请稍候重试。");
            }

            CookieHelper.SetTokenCookie(model);

            _helper.SetSession(user);

            AdminNavigationHelper navigatoinHelper = new AdminNavigationHelper();

            string strReturnUrl = navigatoinHelper.GetReturnUrl(user.GroupId, loginModel.ReturnUrl);

            return Success(strReturnUrl);
        }

        [HttpPost]
        public async Task<ActionResult> RefreshToken(string refrshtoken)
        {
            if (string.IsNullOrEmpty(refrshtoken))
            {
                return RefreshTokenFaild("获取Refresh Token失败，请重新登陆。");
            }

            TokenModel model = await ApiHelper.Instance.GetRefreshTokenAsync(refrshtoken);

            if (model == null)
            {
                return RefreshTokenFaild("获取Refresh Token失败，请重新登陆。");
            }

            CookieHelper.SetTokenCookie(model);

            return Success();
        }

        /// <summary>
        /// 注销  删除session+cookie
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            SessionHelper.ClearSession();
            CookieHelper.ClearTokenCookie();
            return Redirect("/Admin/Home/Index");
        }
    }
}