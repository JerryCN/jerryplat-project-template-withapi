﻿using JerryPlat.Utils.Helpers;
using JerryPlat.Web.Areas.Base;
using System.Web.Mvc;

namespace JerryPlat.Web.Areas.Admin.Controllers
{
    public class SystemController : AdminAuthurizeBaseController
    {
        [HttpPost]
        public ActionResult Restart()
        {
            SiteHelper.RestartAppDomain();
            return Success();
        }
    }
}