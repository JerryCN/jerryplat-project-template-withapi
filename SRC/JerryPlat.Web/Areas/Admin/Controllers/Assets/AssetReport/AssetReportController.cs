﻿using JerryPlat.BLL;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using JerryPlat.Web.Areas.Base;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JerryPlat.Web.Areas.Admin.Controllers
{
    public class AssetReportController : AdminAuthurizeBaseController
    {
        public virtual async Task<ActionResult> Checklist()
        {
            return await Task.FromResult(View());
        }

        public virtual async Task<ActionResult> OperateRecord()
        {
            return await Task.FromResult(View());
        }

        public virtual async Task<ActionResult> ClearRecord()
        {
            return await Task.FromResult(View());
        }
    }
}