﻿using JerryPlat.DAL;
using JerryPlat.DAL.Context;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Web.App_Start.Filter;

namespace JerryPlat.Web.Areas.Base
{
    #region AdminBaseController

    public class AdminBaseController : BaseSessionController<AdminUserDto>
    { }

    public class AdminBaseController<THelper> : BaseSessionController<THelper, AdminUserDto>
        where THelper : DbContextBaseHelper<JerryPlatDbContext, AdminUserDto>, new()
    { }

    public class AdminBaseController<THelper, TEntity, TQueryableEntity> : BaseSessionController<THelper, TEntity, TQueryableEntity, AdminUserDto>
        where THelper : BaseSessionHelper<TEntity, TQueryableEntity, AdminUserDto>, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    { }

    public class AdminBaseController<THelper, TEntity> : AdminBaseController<THelper, TEntity, TEntity>
        where THelper : BaseSessionHelper<TEntity, TEntity, AdminUserDto>, new()
        where TEntity : class, new()
    { }

    public class AdminBaseHelperController<TEntity, TQueryableEntity> : BaseSessionHelperController<TEntity, TQueryableEntity, AdminUserDto>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    { }

    public class AdminBaseHelperController<TEntity> : AdminBaseHelperController<TEntity, TEntity>
        where TEntity : class, new()
    { }

    #endregion AdminBaseController

    #region AdminAuthurizeBaseController

    [LoginAuthorize]
    public class AdminAuthurizeBaseController : AdminBaseController
    { }

    [LoginAuthorize]
    public class AdminAuthurizeBaseController<THelper> : AdminBaseController<THelper>
        where THelper : DbContextBaseHelper<JerryPlatDbContext, AdminUserDto>, new()
    { }

    [LoginAuthorize]
    public class AdminAuthurizeBaseController<THelper, TEntity, TQueryableEntity> : AdminBaseController<THelper, TEntity, TQueryableEntity>
        where THelper : BaseSessionHelper<TEntity, TQueryableEntity, AdminUserDto>, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    { }

    //[LoginAuthorize]
    public class AdminAuthurizeBaseController<THelper, TEntity> : AdminAuthurizeBaseController<THelper, TEntity, TEntity>
        where THelper : BaseSessionHelper<TEntity, TEntity, AdminUserDto>, new()
        where TEntity : class, new()
    { }

    [LoginAuthorize]
    public class AdminAuthurizeBaseHelperController<TEntity, TQueryableEntity> : AdminBaseHelperController<TEntity, TQueryableEntity>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    { }

    public class AdminAuthurizeBaseHelperController<TEntity> : AdminAuthurizeBaseHelperController<TEntity, TEntity>
        where TEntity : class, new()
    { }

    #endregion AdminAuthurizeBaseController
}