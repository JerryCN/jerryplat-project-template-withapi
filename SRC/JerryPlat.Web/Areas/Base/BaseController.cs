﻿using JerryPlat.DAL;
using JerryPlat.DAL.Context;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using JerryPlat.Web.App_Start;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JerryPlat.Web.Areas.Base
{
    #region BaseSessionController

    public class BaseSessionController<TSession> : Controller
        where TSession : class
    {
        #region Session

        private TSession _session { get; set; }

        protected TSession _Session
        {
            get
            {
                if (_session == null)
                {
                    _session = GetSession();
                }
                return _session;
            }
        }

        private TSession GetSession()
        {
            string strSessionKey = typeof(TSession).Name;
            if (SessionHelper.KeyValues.ContainsKey(strSessionKey))
            {
                return SessionHelper.KeyValues[strSessionKey].GetSession<TSession>();
            }
            return null;
        }

        #endregion Session

        public virtual async Task<ActionResult> Index(int id = 0)
        {
            return await Task.FromResult(View());
        }

        #region Response Result

        protected ActionResult Success()
        {
            return Return(ResponseModel<string>.Ok(""));
        }

        protected ActionResult Success<T>(T data, string strMessage = "")
        {
            return Return(ResponseModel<T>.Ok(data, strMessage));
        }

        protected ActionResult Confirm(string strMsg)
        {
            return Return(ResponseModel.Confirm(strMsg));
        }

        protected ActionResult Faild(string strMsg)
        {
            return Return(ResponseModel.Error(strMsg));
        }

        protected ActionResult Invalid(string strMsg)
        {
            return Return(ResponseModel.Invalid(strMsg));
        }

        protected ActionResult NotFound()
        {
            return Return(ResponseModel.NotFound(MessageHelper.NotFound));
        }

        protected ActionResult RefreshTokenFaild(string strMsg)
        {
            return Return(ResponseModel.RefreshTokenFaild(strMsg));
        }

        protected ActionResult Existed(string strMsg = "")
        {
            return Return(ResponseModel.Existed(strMsg));
        }

        private ActionResult Return<T>(ResponseModel<T> responseModel)
        {
            return new JsonNetResult { Data = responseModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            //return Json(responseModel);
        }

        #endregion Response Result
    }

    public class BaseSessionController<THelper, TSession> : BaseSessionController<TSession>
        where THelper : DbContextBaseHelper<JerryPlatDbContext, TSession>, new()
        where TSession : class
    {
        #region Helper

        private THelper helper;

        protected THelper _helper
        {
            get
            {
                if (helper == null)
                {
                    helper = new THelper();
                    helper.InitSession(this._Session);
                }
                return helper;
            }
        }

        #endregion Helper
    }

    public class BaseSessionController<THelper, TEntity, TQueryableEntity, TSession>
            : BaseSessionController<THelper, TSession>
        where THelper : BaseSessionHelper<TEntity, TQueryableEntity, TSession>, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
        where TSession : class
    {
        #region Common Request

        [HttpPost]
        public virtual ActionResult GetLockedIdList()
        {
            return Success(_helper.GetLockedIdList<TEntity>());
        }

        [HttpPost]
        public virtual async Task<ActionResult> GetNewId()
        {
            int intMaxId = await _helper.GetNewIdAsync<TEntity>();
            return Success(intMaxId);
        }

        [HttpPost]
        public virtual async Task<ActionResult> GetCode(SearchModel searchModel)
        {
            string strCode = await _helper.GetCodeAsync(searchModel);
            return Success(strCode);
        }

        [HttpPost]
        public virtual async Task<ActionResult> GetNewOrderIndex()
        {
            int intMaxOrderIndex = await _helper.GetNewOrderIndexAsync<TEntity>();
            return Success(intMaxOrderIndex);
        }

        [HttpPost]
        public virtual async Task<ActionResult> GetPageList(SearchModel searchModel, PageParam pageParam)
        {
            PageData<TQueryableEntity> pageData = await _helper.GetPageListAsync(searchModel, PageHelper.GetKeyExpression<TQueryableEntity, int>(new string[] { "OrderIndex", "Id" }), pageParam, true);
            return Success(pageData);
        }

        [HttpPost]
        public virtual async Task<ActionResult> GetList(SearchModel searchModel)
        {
            List<TQueryableEntity> pageData = await _helper.GetListAsync(searchModel);
            return Success(pageData);
        }

        [HttpGet]
        public virtual async Task<ActionResult> GetDetail(int id)
        {
            TQueryableEntity entity = await _helper.GetByIdAsync(id);
            return Success(entity);
        }

        protected virtual async Task<ActionResult> Save(TQueryableEntity entity)
        {
            if (_helper.IsLockedRecord<TEntity>(TypeHelper.GetIdPropertyValue(entity)))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.SaveAsync(entity);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Add(TQueryableEntity entity)
        {
            return await Save(entity);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(TQueryableEntity entity)
        {
            return await Save(entity);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Delete(ConfirmModel<int> model)
        {
            if (_helper.IsLockedRecord<TEntity>(model.Data))
            {
                return Faild(MessageHelper.NoAction);
            }

            if (!model.IsConfirmed && _helper.ExistCascadingRelation<TEntity>())
            {
                return Confirm(MessageHelper.CascadingDelete);
            }

            bool result = await _helper.DeleteAsync(model.Data);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> DeleteList(ConfirmModel<List<int>> model)
        {
            if (_helper.IsLockedRecord<TEntity>(model.Data))
            {
                return Faild(MessageHelper.NoActionList);
            }
            if (!model.IsConfirmed && _helper.ExistCascadingRelation<TEntity>())
            {
                return Confirm(MessageHelper.CascadingDeleteList);
            }
            bool result = await _helper.DeleteListAsync(model.Data);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Enabled(int id)
        {
            if (_helper.IsLockedRecord<TEntity>(id))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.EnabledAsync<TEntity>(id);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> SoftDelete(int id)
        {
            if (_helper.IsLockedRecord<TEntity>(id))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusAsync<TEntity>(id, true);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> SoftDeleteList(List<int> idList)
        {
            if (_helper.IsLockedRecord<TEntity>(idList))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusListAsync<TEntity>(idList, true);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Recover(int id)
        {
            if (_helper.IsLockedRecord<TEntity>(id))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusAsync<TEntity>(id, false);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> RecoverList(List<int> idList)
        {
            if (_helper.IsLockedRecord<TEntity>(idList))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusListAsync<TEntity>(idList, false);
            return Success(result);
        }

        [HttpPost]
        public async Task<ActionResult> Import(ImprotModel model)
        {
            if (model.IsPreImport)
            {
                return await PreImport(model);
            }

            string result = await _helper.ImportAsync(model.ExcelPath);

            if (result == ConstantHelper.Ok)
            {
                return Success();
            }
            return Faild($"{MessageHelper.ImportError}具体错误为：{result}");
        }

        [HttpPost]
        public async Task<ActionResult> PreImport(ImprotModel model)
        {
            if (!model.IsPreImport)
            {
                return await Import(model);
            }

            List<TEntity> importList = await _helper.PreImportAsync(model.ExcelPath);
            return Success(importList);
        }

        [HttpPost]
        public async Task<ActionResult> Export(SearchModel searchModel)
        {
            string strExportFileUrl = await _helper.ExportAsync(searchModel);
            return Success(strExportFileUrl);
        }

        #endregion Common Request
    }

    public class BaseSessionController<THelper, TEntity, TSession>
         : BaseSessionController<THelper, TSession>
     where THelper : BaseSessionHelper<TEntity, TEntity, TSession>, new()
     where TEntity : class, new()
     where TSession : class
    {
    }

    public class BaseSessionHelperController<TEntity, TQueryableEntity, TSession>
    : BaseSessionController<BaseSessionHelper<TEntity, TQueryableEntity, TSession>, TEntity, TQueryableEntity, TSession>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
        where TSession : class
    {
    }

    public class BaseSessionHelperController<TEntity, TSession>
        : BaseSessionHelperController<TEntity, TEntity, TSession>
        where TEntity : class, new()
        where TSession : class
    {
    }

    #endregion BaseSessionController

    #region BaseController

    public class BaseController : BaseSessionController<object>
    { }

    public class BaseController<THelper> : BaseSessionController<THelper, object>
        where THelper : DbContextBaseHelper<JerryPlatDbContext, object>, new()
    { }

    public class BaseController<THelper, TEntity, TQueryableEntity> : BaseSessionController<THelper, TEntity, TQueryableEntity, object>
        where THelper : BaseSessionHelper<TEntity, TQueryableEntity, object>, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    { }

    public class BaseController<THelper, TEntity> : BaseController<THelper, TEntity, TEntity>
        where THelper : BaseSessionHelper<TEntity, TEntity, object>, new()
        where TEntity : class, new()
    { }

    public class BaseHelperController<TEntity> : BaseSessionHelperController<TEntity, object>
        where TEntity : class, new()
    { }

    #endregion BaseController
}