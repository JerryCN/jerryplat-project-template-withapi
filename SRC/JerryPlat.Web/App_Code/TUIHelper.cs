﻿using ASP;
using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.WebPages;

namespace JerryPlat.Web
{
    public static class TUIHelper
    {
        #region Private
        public static string GetFormString(string strAction="List", string strDialogName = "ModelDialog")
        {
            return $"{strAction}.{strDialogName}";
        }
        private static string GetOrderString(bool bIsOrder)
        {
            return bIsOrder ? " sortable=\"custom\"" : "";
        }
        private static string GetRequiredString(bool bIsRequired)
        {
            return bIsRequired ? "required" : "";
        }
        private static string GetTitleString(string strTitle, string strTable, string strDialogName = "ModelDialog")
        {
            if (string.IsNullOrEmpty(strTitle))
            {
                return "'对话框'";
            }
            bool bIsTitleOnly = strTitle.StartsWith(":");
            if (bIsTitleOnly)
            {
                strTitle = strTitle.Substring(1);
                return $"'{strTitle}'";
            }
            return $"getTitle('{strTitle}',{strTable}.{strDialogName})";
        }
        private static string GetRulesString(string strRules)
        {
            return string.IsNullOrEmpty(strRules) ? "" : $":rules=\"{strRules}\"";
        }
        private static string GetWidthString(int intWidth)
        {
            if (intWidth > 0)
            {
                return $":width=\"{intWidth}\"";
            }
            return string.Empty;
        }
        private static string GetTemplateString(string strTemplate)
        {
            if (string.IsNullOrEmpty(strTemplate))
            {
                return string.Empty;
            }
            return $"<template slot-scope=\"scope\">{strTemplate}</template>";
        }

        private static string GetActionHtml(string strTable, string strDialogName, string strAction)
        {
            string[] aryTemp = strAction.Split('|');
            string strVIf = " & !isLockedItem(scope.row)";

            switch (aryTemp[0])
            {
                #region Outside Table
                case "Add":
                    return ButtonHelper.Add(strTable: strTable, strDialogName: strDialogName).ToString();
                case "Import":
                    return ButtonHelper.Import(strTable: "Import").ToString();
                case "Export":
                    string strExportFileName = aryTemp.Length > 1 ? aryTemp[1] : "Export";
                    return ButtonHelper.Export(strTable: strTable, strFileName: strExportFileName).ToString();
                case "DeleteList":
                    return ButtonHelper.DeleteList(strTable: strTable).ToString();
                #endregion

                #region Inner Table
                case "Edit":
                    bool bIsEditFromRemote = aryTemp.Length > 1 && Convert.ToBoolean(aryTemp[1]);
                    return ButtonHelper.Edit(strTable: strTable, strDialogName: strDialogName, bIsFromRemote: bIsEditFromRemote, strVIf: strVIf).ToString();
                case "Delete":
                    return ButtonHelper.Delete(strTable: strTable, strVIf: strVIf).ToString();
                case "Enabled":
                    return ButtonHelper.Enabled(strTable: strTable, strVIf: strVIf).ToString();
                case "SoftDelete":
                    return ButtonHelper.SoftDelete(strTable: strTable, strVIf: strVIf).ToString();
                case "Recover":
                    return ButtonHelper.Recover(strTable: strTable, strVIf: strVIf).ToString();
                case "Lock":
                    return ButtonHelper.Lock(strTable: strTable).ToString();
                case "UnLock":
                    return ButtonHelper.UnLock(strTable: strTable).ToString();
                #endregion
                default:
                    return "";
            }
        }

        private static string GetActionListHtml(string strTable, string strDialogName, string strActionList, string strPageName = "")
        {
            if (string.IsNullOrEmpty(strActionList))
            {
                return string.Empty;
            }
            if (!string.IsNullOrEmpty(strPageName))
            {
                strPageName = $"|{strPageName}";
            }
            string[] aryAction = strActionList.Split(',');
            StringBuilder sbAction = new StringBuilder();
            foreach (string action in aryAction)
            {
                sbAction.AppendLine($"{GetActionHtml(strTable, strDialogName, action + strPageName)}");
            }
            return sbAction.ToString();
        }
        #endregion
        
        public static MvcHtmlString Table<TModel>(this HtmlHelper helper, string strName = "", bool bIsNeedPagination = true)
             where TModel : class, new()
        {
            string strEntity = typeof(TModel).Name;
            string strKey = $"PageView_{UIType.Table.ToString()}_{strEntity}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }

            UIDto uiDto = UIDbHelper.Instance.GetUIDto<TModel>(UIType.Table, strName);
            if (uiDto == null)
            {
                return MvcHtmlString.Create($"请先配置{strEntity}的UIType={UIType.Table.ToString()}数据库。");
            }
            
            bool bIsNeedSelection = !string.IsNullOrEmpty(uiDto.SelectionIf);

            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"<!----Table {uiDto.TableName} Start---->");
            sbHtml.AppendLine($"<el-table :data=\"{uiDto.TableName}.Data\" border stripe :row-key=\"getItemId\"");
            sbHtml.AppendLine($"          header-row-class-name=\"my-main-body-table-header\"");
            if (bIsNeedSelection)
            {
                sbHtml.AppendLine($"          @selection-change=\"handleSelectionChange\"");
            }
            sbHtml.AppendLine($"          @sort-change=\"function(sort){{handleSortChange(sort,'{uiDto.TableName}');}}\"");
            sbHtml.AppendLine($"          class=\"my-table my-table-{uiDto.TableName.ToLower()}\">");

            if (bIsNeedSelection)
            {
                sbHtml.AppendLine($"    <el-table-column type=\"selection\" width=\"55\" align=\"center\"");
                sbHtml.AppendLine($"                     :selectable=\"isSelectable\"");
                sbHtml.AppendLine($"                     v-if=\"{uiDto.SelectionIf}\"></el-table-column>");
            }

            foreach(UIRow uiRow in uiDto.UIRows)
            {
                sbHtml.AppendLine($"    <el-table-column label=\"{uiRow.Title}\" {GetWidthString(uiRow.Width)} prop=\"{uiRow.FieldName}\" {GetOrderString(uiRow.IsOrder)}>");
                sbHtml.AppendLine($"    {GetTemplateString(uiRow.Template)}");
                sbHtml.AppendLine($"    </el-table-column>");
            }
            
            if (!string.IsNullOrEmpty(uiDto.Action))
            {
                sbHtml.AppendLine($"    <el-table-column label=\"{Template.Table.Action}\" {GetWidthString(uiDto.ActionWidth)} align=\"center\"");
                sbHtml.AppendLine($"                     v-if=\"existButton('{uiDto.Action}')\">");
                sbHtml.AppendLine($"        <template slot-scope=\"scope\">");
                sbHtml.AppendLine(GetActionListHtml(uiDto.TableName, uiDto.DialogName, uiDto.Action));
                sbHtml.AppendLine($"        </template>");
                sbHtml.AppendLine($"    </el-table-column>");
            }
            sbHtml.AppendLine($"</el-table>");
            if (bIsNeedPagination)
            {
                sbHtml.AppendLine($"{helper.Partial("_Pagination", uiDto.TableName)}");
            }
            sbHtml.AppendLine($"<!----Table {uiDto.TableName} End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }
        public static MvcHtmlString Import<TModel>(this HtmlHelper helper, bool bIsPreImport = false)
            where TModel : class, new()
        {
            string strEntity = typeof(TModel).Name;
            string strKey = $"PageView_{UIType.Import.ToString()}_{strEntity}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }

            UIDto uiDto = UIDbHelper.Instance.GetUIDto<TModel>(UIType.Import);
            if(uiDto == null)
            {
                return MvcHtmlString.Create($"请先配置{strEntity}的UIType={UIType.Import.ToString()}数据库。");
            }
            
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"<!----Import {uiDto.TableName} Start---->");
            sbHtml.AppendLine($"<el-dialog title=\"数据导入\" :visible=\"{uiDto.TableName}.{uiDto.DialogName}.Visible\" @close=\"closeDialog('{uiDto.TableName}.{uiDto.DialogName}')\"");
            sbHtml.AppendLine($"           :width=\"Common.DialogWidth\"");
            sbHtml.AppendLine($"           :close-on-click-modal=\"Common.CloseOnClickModal\"");
            sbHtml.AppendLine($"           :close-on-press-escape=\"Common.CloseOnPressEscape\"");
            sbHtml.AppendLine($"           class=\"my-import-dialog-{uiDto.TableName.ToLower()}\">");
            sbHtml.AppendLine($"    <el-form :model=\"{uiDto.TableName}.{uiDto.DialogName}.Model\" status-icon :show-message=\"false\" ref=\"{uiDto.TableName}.{uiDto.DialogName}\"");
            sbHtml.AppendLine($"             :label-width=\"Common.FormLabelWidth\"");
            sbHtml.AppendLine($"             :label-suffix=\"Common.FormLabelSuffix\">");

            sbHtml.AppendLine($"        <el-form-item class=\"my-import-instruction\" label=\"Excel模板文件\">");
            sbHtml.AppendLine($"            <a href=\"{uiDto.Template}\">{IOHelper.GetFileName(uiDto.Template, false)}</a>");
            foreach (UIRow uiRow in uiDto.UIRows)
            {
                uiRow.Description = uiRow.Description.Replace("{{NewId}}", "{{" + uiDto.TableName + $".{uiDto.DialogName}.NewId}}}}");
                sbHtml.AppendLine($"            <p>{uiRow.Title}列：{uiRow.Description}</p>");
            }
            sbHtml.AppendLine($"        </el-form-item>");

            sbHtml.AppendLine($"        {UIHelper.UploadExcel(uiDto.TableName)}");
            sbHtml.AppendLine($"    </el-form>");
            sbHtml.AppendLine($"    <div slot=\"footer\" class=\"dialog-footer\">");
            sbHtml.AppendLine($"        <el-button @click=\"closeDialog('{uiDto.TableName}.{uiDto.DialogName}')\" icon=\"el-icon-close\">取 消</el-button>");

            string strImport = bIsPreImport ? "handlePreImport" : "handleImport";
            string strImportCallback = bIsPreImport ? "preImportCallback" : "importCallback";
            string strImportText = bIsPreImport ? "预览" : "自动导入";
            sbHtml.AppendLine($"        <el-button type=\"primary\" @click=\"submitForm('{uiDto.TableName}.{uiDto.DialogName}', {strImport}, {strImportCallback}, null, true)\" icon=\"el-icon-upload2\">{strImportText}</el-button>");
            sbHtml.AppendLine($"    </div>");
            sbHtml.AppendLine($"</el-dialog>");
            sbHtml.AppendLine($"<!----Import {uiDto.TableName} End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }

        public static MvcHtmlString Edit<TModel>(this HtmlHelper helper, int intColCount = 1, string strPageName = "", string strName = "")
            where TModel : class, new()
        {
            string strEntity = typeof(TModel).Name;
            string strKey = $"PageView_{UIType.Edit.ToString()}_{strEntity}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }

            UIDto uiDto = UIDbHelper.Instance.GetUIDto<TModel>(UIType.Edit, strName);
            if (uiDto == null)
            {
                TModel model = new TModel();
                if (model is BaseSettingDto)
                {
                    strEntity = typeof(BaseSettingDto).Name;
                }
                return MvcHtmlString.Create($"请先配置{strEntity}的UIType={UIType.Edit.ToString()}数据库。");
            }

            if (string.IsNullOrEmpty(strPageName))
            {
                strPageName = uiDto.PageName;
            }

            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"<!----Edit {uiDto.TableName} Start---->");
            sbHtml.AppendLine($"<el-dialog :title=\"{GetTitleString(strPageName, uiDto.TableName, uiDto.DialogName)}\" :visible=\"{uiDto.TableName}.{uiDto.DialogName}.Visible\" @close=\"closeDialog('{uiDto.TableName}.{uiDto.DialogName}')\"");
            string strDialogWidth = Math.Min(Math.Max(intColCount, 2) * 30, 90) + "%";
            sbHtml.AppendLine($"           width=\"{strDialogWidth}\"");
            sbHtml.AppendLine($"           :close-on-click-modal=\"Common.CloseOnClickModal\"");
            sbHtml.AppendLine($"           :close-on-press-escape=\"Common.CloseOnPressEscape\"");
            string strClassTemp = string.IsNullOrEmpty(strName) ? "" : $"-{strName}";
            sbHtml.AppendLine($"           class=\"my-edit-dialog-{uiDto.TableName.ToLower()}{strClassTemp}\">");
            sbHtml.AppendLine($"    <el-form :model=\"{uiDto.TableName}.{uiDto.DialogName}.Model\"");
            sbHtml.AppendLine($"             class=\"my-dialog-form-{intColCount}\"");
            sbHtml.AppendLine($"             status-icon");
            sbHtml.AppendLine($"             :show-message=\"false\"");
            sbHtml.AppendLine($"             ref=\"{uiDto.TableName}.{uiDto.DialogName}\"");
            sbHtml.AppendLine($"             :label-width=\"{uiDto.LabelWidth}\"");
            sbHtml.AppendLine($"             :label-suffix=\"Common.FormLabelSuffix\">");
            foreach(UIRow uiRow in uiDto.UIRows)
            {
                sbHtml.AppendLine($"        <el-form-item label=\"{uiRow.Title}\" prop=\"{uiRow.FieldName}\" {GetRulesString(uiRow.Rules)}>");
                string strFormItem = uiRow.Template
                    .Replace("{{Append}}", uiRow.AppendText)
                    .Replace("{{Event}}", uiRow.Event)
                    .Replace("{{Description}}", uiRow.Description)
                    .Replace("{{Disabled}}", uiRow.Disabled)
                    .Replace("{{Title}}", uiRow.Title)
                    .Replace("{{DefaultValue}}", uiRow.DefaultValue.ToString())
                    .Replace("{{Table}}", uiDto.TableName)
                    .Replace("{{DialogName}}", uiDto.DialogName)
                    .Replace("{{Field}}", uiRow.FieldName);
                sbHtml.AppendLine($"            {strFormItem}");
                sbHtml.AppendLine($"        </el-form-item>");
            }
            sbHtml.AppendLine($"    </el-form>");
            sbHtml.AppendLine($"    <div slot=\"footer\" class=\"dialog-footer\">");
            sbHtml.AppendLine($"        <el-button @click=\"closeDialog('{uiDto.TableName}.{uiDto.DialogName}')\" icon=\"el-icon-close\">取 消</el-button>");
            string strAfterSubmitCallback = uiDto.AfterSubmitCallback.Replace("{{Table}}", uiDto.TableName);
            if (string.IsNullOrEmpty(strAfterSubmitCallback))
            {
                strAfterSubmitCallback = "null";
            }
            sbHtml.AppendLine($"        <el-button type=\"primary\" @click=\"submitForm('{uiDto.TableName}.{uiDto.DialogName}',handleSave,{strAfterSubmitCallback})\"  icon=\"el-icon-check\">保 存</el-button>");
            sbHtml.AppendLine($"    </div>");
            sbHtml.AppendLine($"</el-dialog>");
            sbHtml.AppendLine($"<!----Edit {uiDto.TableName} End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }

        public static MvcHtmlString Header(this HtmlHelper helper, string strPageName, string strTableName="List",string strDialogName = "ModelDialog", string strActionList = "Add,DeleteList")
        {
            string strKey = $"PageView_Header_{strPageName}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"<!----Header Start---->");
            sbHtml.AppendLine($"<div class=\"my-main-body-button\">");
            sbHtml.AppendLine($"    <span class=\"my-main-body-title\">");
            sbHtml.AppendLine($"        <i class=\"el-icon-view\"></i>");
            sbHtml.AppendLine($"        {strPageName}管理");
            sbHtml.AppendLine($"    </span>");
            sbHtml.AppendLine(GetActionListHtml(strTableName, strDialogName, strActionList, strPageName));
            sbHtml.AppendLine($"</div>");
            sbHtml.AppendLine($"<!----Header End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }

        public static MvcHtmlString Page<TModel>(this HtmlHelper helper, string strPageName, string strTableName = "List", string strDialogName = "ModelDialog", string strActionList = "Add,DeleteList")
            where TModel : class, new()
        {
            string strEntity = typeof(TModel).Name;
            string strKey = $"PageView_{UIType.Page.ToString()}_{strEntity}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }
            
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"{helper.Header(strPageName, strTableName, strDialogName, strActionList)}");
            sbHtml.AppendLine($"{helper.Table<TModel>().ToHtmlString()}");
            sbHtml.AppendLine($"{helper.Edit<TModel>(strPageName: strPageName)}");
            if (TypeHelper.IsInheritOf<TModel, BaseSettingDto>())
            {
                sbHtml.AppendLine($"{helper.Import<BaseSettingDto>()}");
            }
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }

        public static MvcHtmlString PageJs<TModel>(this HtmlHelper helper, string strControllerName)
             where TModel : class, new()
        {
            string strKey = $"PageView_Js_BaseSetting_{strControllerName}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"{helper.DtoModel<TModel>()}");
            sbHtml.AppendLine($"<!----Js {strControllerName} Start---->");
            sbHtml.AppendLine($"<script>");
            sbHtml.AppendLine($"function getVueOptions() {{");
            sbHtml.AppendLine($"    return helper.getPageVueOptions(\"{strControllerName}\", get{typeof(TModel).Name}");
            if (TypeHelper.IsInheritOf<TModel, BaseSettingDto>())
            {
                sbHtml.AppendLine($",{{data:{{Import: helper.getImportModel(\"{strControllerName}\")}}}}");
            }
            sbHtml.AppendLine(");");
            sbHtml.AppendLine($"}}");
            sbHtml.AppendLine($"</script>");
            sbHtml.AppendLine($"<!----Js {strControllerName} End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }

        public static MvcHtmlString DtoModel<TModel>(this HtmlHelper helper)
             where TModel : class, new()
        {
            string strClassName = typeof(TModel).Name;
            string strKey = $"DtoModle_Js_{strClassName}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"<!----Js {strClassName} Start---->");
            sbHtml.AppendLine($"<script>");
            sbHtml.AppendLine($"function get{strClassName}() {{");
            sbHtml.AppendLine($"    return {helper.Raw(TypeHelper.ToJson<TModel>())};");
            sbHtml.AppendLine($"}}");
            sbHtml.AppendLine($"</script>");
            sbHtml.AppendLine($"<!----Js {strClassName} End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }

        public static MvcHtmlString DtoEnum<TEnum>(this HtmlHelper helper)
        {
            string strClassName = typeof(TEnum).Name;
            string strKey = $"DtoEnum_Js_{strClassName}";
            MvcHtmlString cache = CacheHelper.GetCache<MvcHtmlString>(strKey);
            if (cache != null)
            {
                return cache;
            }
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.AppendLine($"<!----Js {strClassName} Start---->");
            sbHtml.AppendLine($"<script>");
            sbHtml.AppendLine($"function get{strClassName}() {{");
            sbHtml.AppendLine($"    return {helper.Raw(TypeHelper.GetEnumScript<TEnum>())};");
            sbHtml.AppendLine($"}}");
            sbHtml.AppendLine($"</script>");
            sbHtml.AppendLine($"<!----Js {strClassName} End---->");
            cache = MvcHtmlString.Create(sbHtml.ToString());
            CacheHelper.SetCache(cache, strKey);
            return cache;
        }
    }
}