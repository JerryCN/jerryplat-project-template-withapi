/**
* Created by HuDianxing on 2016/12/8.
 * LastModified on 2018/06/11.
*/
/**
* 标签打印机类，对OCX插件对象的封装
*/
var LPAPI = (function () {
    function LPAPI() {
        this.init();
    }

    var defaultLineWidth = 0.3;
    var defaultCornerRadius = 1.5;

    /**
    * 在Html加载完毕之后需要调用该函数，通过ocx组件来初始化LPAPI实例。
    */
    LPAPI.prototype.init = function () {
        if (!this.dtPrinter)
            this.dtPrinter = document.getElementById("dtPrinter");
        if (this.dtPrinter)
            return;

        // 添加ocx标签
        try{
            var div = document.createElement("div");
            div.style.display = "none";
            div.innerHTML = '<object id="dtPrinter" classid="clsid:9D846E42-C3EF-4A5D-9805-A269CE7AA470"></object>';
            if (document.body)
                document.body.appendChild(div);
            else
                document.appendChild(div);

            this.dtPrinter = document.getElementById("dtPrinter");
            
            try {
                this.dtPrinter.GetAllPrinters(true);
            } catch (e) {
		alert("未检测到标签打印机插件，请注册插件（不支持非IE浏览器）！\n" + e.toString());
            }
        } catch(e){}
    };

    /**
    * 获取打印动作的顺时针旋转角度
    * @returns {number}
    *      0：  顺时针旋转0度；
    *      1： 顺时针旋转90度；
    *      2：顺时针旋转180度；
    *      3：顺时针旋转270度；
    */
    LPAPI.prototype.getItemOrientation = function () {
        return this.dtPrinter.ItemOrientation;
    };

    /**
    * 设置后续打印动作的顺时针旋转角度
    * @param nNewValue
    *      0：  顺时针旋转0度；
    *      90： 顺时针旋转90度；
    *      180：顺时针旋转180度；
    *      270：顺时针旋转270度；
    */
    LPAPI.prototype.setItemOrientation = function (nNewValue) {
        this.dtPrinter.ItemOrientation = nNewValue;
    };

    /**
    * 获取打印动作的水平对齐方式
    * @returns {number}
    *      0：水平居左；
    *      1：水平居中；
    *      2：水平居右。
    */
    LPAPI.prototype.getItemHorizontalAlignment = function () {
        return this.dtPrinter.ItemHorizontalAlignment;
    };

    /**
    * 设置后续打印动作的水平对齐方式
    * @param nNewValue
    *      0：水平居左；
    *      1：水平居中；
    *      2：水平居右。
    */
    LPAPI.prototype.setItemHorizontalAlignment = function (nNewValue) {
        this.dtPrinter.ItemHorizontalAlignment = nNewValue;
    };

    /**
    * 获取当前打印动作的垂直对齐方式
    * @returns {number}
    *      0：垂直居上；
    *      1：垂直居中；
    *      2：垂直居下。
    */
    LPAPI.prototype.getItemVerticalAlignment = function () {
        return this.dtPrinter.ItemVerticalAlignment;
    };

    /**
    * 设置后续打印动作的垂直对齐方式
    * @param nNewValue
    *      0：垂直居上；
    *      1：垂直居中；
    *      2：垂直居下。
    */
    LPAPI.prototype.setItemVerticalAlignment = function (nNewValue) {
        this.dtPrinter.ItemVerticalAlignment = nNewValue;
    };

    /**
    *  打开指定名称的打印机对象。
    * @param printerName 打印机名称
    *  为 NULL 或空字符串时，函数会自动搜索当前系统安装的第一个 LPAPI 支持的打印机。当指定打印机名称时，函数会
    *  优先根据打印机名称进行完整匹配，如果没有匹配上，则函数会再次匹配以指定名称开始的、后面加上了 #1、#2 等字样的打印机名称对象。
    * @returns {number} 成功与否。
    */
    LPAPI.prototype.openPrinter = function (printerName) {
        return this.dtPrinter.OpenPrinter(printerName) === 0;
    };

    /**
    * 得到当前使用的打印机名称.
    * @returns {string} 当前使用的打印机名称。
    */
    LPAPI.prototype.getPrinterName = function () {
        return this.dtPrinter.GetPrinterName();
    };

    /**
    * 判断当前打印机是否打开。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    */
    LPAPI.prototype.isPrinterOpened = function () {
        return this.dtPrinter.IsPrinterOpened() === 0;
    };

    /**
    * 判断当前打印机是否在线
    * @returns {boolean}
    * true：成功。
    * false：失败。
    */
    LPAPI.prototype.isPrinterOnline = function () {
        return this.dtPrinter.IsPrinterOnline() === 0;
    };

    /**
    * 关闭当前使用的打印机
    * 注意：关闭打印机时，当前还有未打印的任务/数据将会被自动提交打印，同时所有参数设置将会被保留。
    */
    LPAPI.prototype.closePrinter = function () {
        this.dtPrinter.ClosePrinter();
    };

    /**
    * 得到支持的打印机名称.
    * @param onlyOnline 是否仅仅返回已连接至电脑的打印机？
    * @returns {string}
    * 本机安装的所有打印机名称，多个打印机名称之间用逗号分隔。
    */
    LPAPI.prototype.getSupportedPrinters = function (onlyOnline) {
        onlyOnline = (onlyOnline === undefined ? false : onlyOnline);
        this.init();

        try{
            return this.dtPrinter.GetSupportedPrinters(onlyOnline);
        } catch (e){
        }

        return false;
    };

    /**
    *  得到本机安装的所有打印机名称.
    * @param onlyOnline 是否仅仅返回已连接的打印机？
    * @returns {string}
    * 本机安装的所有打印机名称，多个打印机名称之间用逗号分隔。
    */
    LPAPI.prototype.getAllPrinters = function (onlyOnline) {
        onlyOnline = (onlyOnline === undefined ? false : onlyOnline);
        this.init();

        try{
            return this.dtPrinter.GetAllPrinters(onlyOnline);
        } catch (e){
        }

        return false;
    };

    /**
     * 打印参数ID：
     * GapType 纸张类型ID，值 0-3,255
     * PrintDarkness 打印浓度，值：6-15
     * PrintSpeed 打印速度，值：1-5
     */
    LPAPI.prototype.ParamID = {
        GapType : 1,        ///< 纸张类型
        PrintDarkness : 2,  ///< 打印浓度
        PrintSpeed : 3      ///< 打印速度
    };

    LPAPI.prototype.GapType = {
        Unset : 255,            ///< 随打印机
        None : 0,               ///< 连续纸，没有分隔
        Hole : 1,               ///< 定位孔
        Gap : 2,                ///< 间隙纸
        Black : 3               ///< 黑标纸    
    };
    
    /**
     * 获取打印参数；
     * @param {number} paramID 打印参数ID，ID值可参考 ParamID 属性；
     */
    LPAPI.prototype.getParam = function(paramID) {
        return this.dtPrinter.GetParam(paramID);
    };
    
    /**
     * 设置打印参数；
     * @param {number} paramID 打印参数ID，ID值可参考 ParamID 属性；
     * @param {number} paramValue 打印参数 Value 值；
     */
    LPAPI.prototype.setParam = function(paramID, paramValue) {
        return this.dtPrinter.SetParam(paramID, paramValue);
    };

    /**
     * 获取纸张类型，类型结果可参考GapType属性；
     */
    LPAPI.prototype.getGapType = function(){
        return this.dtPrinter.getParam(this.ParamID.GapType);
    };

    /**
     * 设置纸张类型，类型值参考GapType属性；
     * @param {number} value 纸张类型；
     */
    LPAPI.prototype.setGapType = function(value) {
        value = parseInt(value);
        if (isNaN(value))
            return;
        return this.setParam(this.ParamID.GapType, value) === 0;
    };

    /**
     * 获取当前打印机的打印速度；
     * 打印速度值的有效区间为 0-4之间，255表示随打印机；
     */
    LPAPI.prototype.getPrintSpeed = function() {
        return this.getParam(this.ParamID.PrintSpeed);
    };

    /**
     * 设置打印速度；
     * 当打印机打开后调用该函数有效；
     * @param {number} value 打印速度值，有效值为 1-5之间，255表示随打印机；
     */
    LPAPI.prototype.setPrintSpeed = function(value) {
        value = parseInt(value);
        if (isNaN(value))
            return;
        return this.setParam(this.ParamID.PrintSpeed, value) === 0;
    };

    /**
     * 获取打印浓度；
     * 打印浓度有效值为 5-14之间，255表示随打印机；
     */
    LPAPI.prototype.getPrintDarkness = function(){
        return this.getParam(this.ParamID.PrintDarkness);
    };

    /**
     * 设置打印机的打印浓度；
     * 当打印机打开成功后调用该函数有效；
     * @param {number} value 打印浓度 value值，有效值为 5-14之间，255表示随打印机；
     */
    LPAPI.prototype.setPrintDarkness = function(value) {
        value = parseInt(value);
        if (isNaN(value))
            return;
        return this.setParam(this.ParamID.PrintDarkness, value) === 0;
    };

    /**
    * 开始一打印任务.
    * @param width 标签宽度（基于打印视图，不考虑标签旋转。单位毫米(mm)）。
    * @param height 标签高度（基于打印视图，不考虑标签旋转。单位毫米(mm)）。
    * @param orientation 提交打印时标签页面的顺时针旋转角度，0/90/180/270。
    * @param jobName 打印任务名称。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    *  使用说明：开始打印任务时，如果没有打开打印机对象，则本函数会自动打开当前系统安装的第一个 LPAPI 支持的打印机，用于打印。
    *  开始打印任务时，当前还有未打印的任务/数据将会被全部丢弃。
    */
    LPAPI.prototype.startJob = function (width, height, orientation, jobName) {
        orientation = (typeof orientation !== "number" ? 0 : orientation);

        return this.dtPrinter.StartJob(width * 100, height * 100, 30, orientation, 1, jobName || "") === 0;
    };

    /**
    *  取消一打印任务
    *  使用说明：当前还有未打印的任务/数据将会被全部丢弃，但是所有参数设置将会被保留。
    */
    LPAPI.prototype.abortJob = function () {
        this.dtPrinter.AbortJob();
    };

    /**
    * 提交打印任务，进行真正的打印。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    */
    LPAPI.prototype.commitJob = function () {
        return this.dtPrinter.CommitJob() === 0;
    };

    /**
    * 开始一打印页面。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    *  使用说明：如果之前没有调用 StartJob，则本函数会自动调用 StartJob，然后再开始一打印页面。此后调用 EndPage 结束打印时，打印任务会被自动提交打印。
    *  页面旋转角度非 0 打印时，必须在打印动作之前设置打印页面尺寸信息。
    */
    LPAPI.prototype.startPage = function () {
        return this.dtPrinter.StartPage() === 0;
    };

    /**
    * 结束一打印页面。
    *  使用注意：如果之前没有调用 StartJob 而直接调用 StartPage，则本函数会自动提交打印。
    */
    LPAPI.prototype.endPage = function () {
        this.dtPrinter.EndPage();
    };

    /**
     * Win10下如果字体为粗体或者斜体，会显示乱码的问题，所以在drawText的时候需要进行些处理；
     */
    LPAPI.prototype.isWin10 = function(){
        return navigator.userAgent.match(/windows\s*nt\s*(10\.[0-9]*)/i);
    };

    /*********************************************************************
    * 绘制相关内容。
    *********************************************************************/
    /**
    * 打印文本字符串
    * @param text 需要打印的文本字符串。
    * @param x 打印矩形框水平位置（单位毫米(mm)）。
    * @param y 打印矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印矩形框水平宽度（单位毫米(mm)）。如果 width 为 0，则会根据打印文本的显示宽度，根据当前对齐方式进行以 x 为基准点的左中右对齐。
    * @param height 打印矩形框垂直高度（单位毫米(mm)）。如果 height 为 0，则会根据打印文本的显示高度，根据当前对齐方式进行以 y 为基准点的上中下对齐。
    * @param fontName 字体名称。为空时函数会自动按照下面的搜索顺序来对系统安装的字体进行搜索：微软雅黑、黑体、宋体、新宋体。
    * @param fontHeight 字体高度，单位毫米(mm)，不是字号。
    * @param fontStyle 字体风格，
    *  0：一般；
    *  1：粗体；
    *  2：斜体；
    *  3：粗斜体；
    *  4：下划线；
    *  8：删除线。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    *  使用注意：
    *      如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage 开始一打印页面，然后进行打印。
    *  	打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    *      fontHeight是以像素或毫米为单位，单位毫米(mm)。字号和毫米的转换公式为：
    *          字号对应的打印高度（毫米）= 字号 * 25.4 / 72
    *          比方说 9 号字为 3.175 毫米，12 号字为 4.233 毫米
    */
    LPAPI.prototype.drawText = function (text, x, y, width, height, fontName, fontHeight, fontStyle) {
        fontHeight = fontHeight || height;
        fontStyle = this.isWin10() ? 0 : (typeof fontStyle === "number" ? fontStyle : 0);
        return this.dtPrinter.DrawText(text, x * 100, y * 100, width * 100, height * 100, fontName || "黑体", fontHeight * 100, fontStyle) === 0;
    };

    /**
    * 打印一维条码。
    * @param text 需要打印的文本字符串。
    * @param type 条码编码类型 ，参考条码类型。
    * @param x 打印一维码水平位置（单位毫米(mm)）。
    * @param y 打印一维码垂直位置（单位毫米(mm)）。
    * @param width 打印一维码水平宽度（单位毫米(mm)）。
    * @param height 打印一维码垂直高度（单位毫米(mm)）。
    * @param textHeight 供人识读文本的高度（单位毫米(mm)）。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    *  @使用注意：
    *   	如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage 开始一打印页面，然后进行打印。
    *	    打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    *      默认的条码和供人识读文本之间的距离是供人识读文本的 1/4 高度。
    */
    LPAPI.prototype.draw1DBarcode = function (text, type, x, y, width, height, textHeight) {
        if (type <= 20 || type > 60) type = 60;
        return this.dtPrinter.Draw1DBarcode(text, type, x * 100, y * 100, width * 100, height * 100, (textHeight || 0) * 100) === 0;
    };

    /**
    * 打印 QrCode 二维码。
    * @param text 需要打印的二维码内容。
    * @param x 打印二维码水平位置（单位毫米(mm)）。
    * @param y 打印二维码垂直位置（单位毫米(mm)）。
    * @param width 打印二维码水平宽度（单位毫米(mm)）。如果 width 为 0，则会根据二维码的显示宽度（二维码点宽度为0.25毫米），根据当前对齐方式进行以 x 为基准点的左中右对齐。
    * @param height 打印二维码垂直高度（单位毫米(mm)）。如果 height 为 0，则会根据二维码的显示宽度（二维码点宽度为0.25毫米），根据当前对齐方式进行以 y 为基准点的上中下对齐。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *      如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage 开始一打印页面，然后进行打印。
    *      打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.draw2DQRCode = function (text, x, y, width, height) {
        height = height || width;
        return this.dtPrinter.Draw2DQRCode(text, x * 100, y * 100, width * 100, height * 100) === 0;
    };

    /**
    * 打印 Pdf417 二维码。
    * @param text 需要打印的二维码内容。
    * @param x 打印二维码水平位置（单位毫米(mm)）。
    * @param y 打印二维码垂直位置（单位毫米(mm)）。
    * @param width 打印二维码水平宽度（单位毫米(mm)）。如果 width 为 0，则会根据二维码的显示宽度（二维码点宽度为0.25毫米），根据当前对齐方式进行以 x 为基准点的左中右对齐。
    * @param height 打印二维码垂直高度（单位毫米(mm)）。如果 height 为 0，则会根据二维码的显示宽度（二维码点宽度为0.25毫米），根据当前对齐方式进行以 y 为基准点的上中下对齐。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *      如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *      打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.draw2DPdf417 = function (text, x, y, width, height) {
        return this.dtPrinter.Draw2DPdf417(text, x * 100, y * 100, width * 100, height * 100) === 0;
    };

    /**
    * 以指定的线宽，打印矩形框。
    * @param x 打印矩形框水平位置（单位毫米(mm)）。
    * @param y 打印矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印矩形框水平宽度（单位毫米(mm)）。
    * @param height 打印矩形框垂直高度（单位毫米(mm)）。
    * @param lineWidth 矩形框的线宽（单位毫米(mm)）。矩形框的线宽是向矩形框内部延伸的。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.drawRectangle = function (x, y, width, height, lineWidth) {
        lineWidth = lineWidth || defaultLineWidth;
        return this.dtPrinter.DrawRectangle(x * 100, y * 100, width * 100, height * 100, lineWidth * 100) === 0;
    };

    /**
    * 打印填充的矩形框。
    * @param x 打印矩形框水平位置（单位毫米(mm)）。
    * @param y 打印矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印矩形框水平宽度（单位毫米(mm)）。
    * @param height 打印矩形框垂直高度（单位毫米(mm)）。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.fillRectangle = function (x, y, width, height) {
        return this.dtPrinter.FillRectangle(x * 100, y * 100, width * 100, height * 100) === 0;
    };

    /**
    * 以指定的线宽，打印圆角矩形框
    * @param x 打印矩形框水平位置（单位毫米(mm)）。
    * @param y 打印矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印矩形框水平宽度（单位毫米(mm)）。
    * @param height 打印矩形框垂直高度（单位毫米(mm)）。
    * @param cornerWidth 圆角宽度（单位毫米(mm)）。
    * @param cornerHeight 圆角高度（单位毫米(mm)）。
    * @param lineWidth 圆角矩形框的线宽（单位毫米(mm)）。圆角矩形框的线宽是向圆角矩形框内部延伸的。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.drawRoundRectangle = function (x, y, width, height, cornerWidth, cornerHeight, lineWidth) {
        cornerWidth = typeof cornerWidth === "number" ? cornerWidth : defaultCornerRadius;
        cornerHeight = typeof cornerHeight === "number" ? cornerHeight : cornerWidth;
        lineWidth = lineWidth || defaultLineWidth;
        return this.dtPrinter.DrawRoundRectangle(x * 100, y * 100, width * 100, height * 100, cornerWidth * 100, cornerHeight * 100, lineWidth * 100) === 0;
    };

    /**
    * 打印填充的圆角矩形框
    * @param x 打印圆角矩形框水平位置（单位毫米(mm)）。
    * @param y 打印圆角矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印圆角矩形框水平宽度（单位毫米(mm)）。
    * @param height 打印圆角矩形框垂直高度（单位毫米(mm)）。
    * @param cornerWidth 圆角宽度（单位毫米(mm)）。
    * @param cornerHeight 圆角高度（单位毫米(mm)）。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.fillRoundRectangle = function (x, y, width, height, cornerWidth, cornerHeight) {
        cornerWidth = typeof cornerWidth === "number" ? cornerWidth : defaultCornerRadius;
        cornerHeight = typeof cornerHeight === "number" ? cornerHeight : cornerWidth;
        return this.dtPrinter.FillRoundRectangle(x * 100, y * 100, width * 100, height * 100, cornerWidth * 100, cornerHeight * 100) === 0;
    };

    /**
    * 以指定的线宽，打印椭圆/圆
    * @param x 打印椭圆矩形框水平位置（单位毫米(mm)）。
    * @param y 打印椭圆矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印椭圆矩形框水平宽度（单位毫米(mm)）。
    * @param height 打印椭圆矩形框垂直高度（单位毫米(mm)）。
    * @param lineWidth 椭圆的线宽（单位毫米(mm)）。椭圆的线宽是向椭圆内部延伸的。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    *       width 和 height 相等时就是打印圆。
    */
    LPAPI.prototype.drawEllipse = function (x, y, width, height, lineWidth) {
        lineWidth = lineWidth || defaultLineWidth;
        return this.dtPrinter.DrawEllipse(x * 100, y * 100, width * 100, height * 100, lineWidth * 100) === 0;
    };

    /**
    * 打印填充的椭圆/圆
    * @param x 打印椭圆矩形框水平位置（单位毫米(mm)）。
    * @param y 打印椭圆矩形框垂直位置（单位毫米(mm)）。
    * @param width 打印椭圆矩形框水平宽度（单位毫米(mm)）。
    * @param height 打印椭圆矩形框垂直高度（单位毫米(mm)）。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    *       width 和 height 相等时就是打印圆。
    */
    LPAPI.prototype.fillEllipse = function (x, y, width, height) {
        return this.dtPrinter.FillEllipse(x * 100, y * 100, width * 100, height * 100) === 0;
    };

    /**
    *  打印线（直线/斜线）
    * @param x1 线的起点（单位毫米(mm)）。
    * @param y1 线的起点（单位毫米(mm)）。
    * @param x2 线的终点（单位毫米(mm)）。
    * @param y2 线的终点（单位毫米(mm)）。
    * @param lineWidth 线宽（单位毫米(mm)）。线宽是向线的下方延伸的。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.drawLine = function (x1, y1, x2, y2, lineWidth) {
        lineWidth = lineWidth || defaultLineWidth;
        return this.dtPrinter.DrawLine(x1 * 100, y1 * 100, x2 * 100, y2 * 100, lineWidth * 100) === 0;
    };

    /**
    *  打印点划线
    * @param x1 线的起点（单位毫米(mm)）。
    * @param y1 线的起点（单位毫米(mm)）。
    * @param x2 线的终点（单位毫米(mm)）。
    * @param y2 线的终点（单位毫米(mm)）。
    * @param lineWidth 线宽（单位毫米(mm)）。线宽是向线的下方延伸的。
    * @param dashLen1 点划线第一段的长度（单位毫米(mm)）。
    * @param dashLen2 点划线第二段的长度（单位毫米(mm)）。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.drawDashLine2 = function (x1, y1, x2, y2, lineWidth, dashLen1, dashLen2) {
        lineWidth = lineWidth || defaultLineWidth;
        dashLen1 = dashLen1 || 0.5;
        dashLen2 = dashLen2 || dashLen1 * 0.5;
        return this.dtPrinter.DrawDashLine2(x1 * 100, y1 * 100, x2 * 100, y2 * 100, lineWidth * 100, dashLen1 * 100, dashLen2 * 100) === 0;
    };

    /**
    *  打印点划线
    * @param x1 线的起点（单位毫米(mm)）。
    * @param y1 线的起点（单位毫米(mm)）。
    * @param x2 线的终点（单位毫米(mm)）。
    * @param y2 线的终点（单位毫米(mm)）。
    * @param lineWidth 线宽（单位毫米(mm)）。线宽是向线的下方延伸的。
    * @param dashLen1 点划线第一段的长度（单位毫米(mm)）。
    * @param dashLen2 点划线第二段的长度（单位毫米(mm)）。
    * @param dashLen3 点划线第三段的长度（单位毫米(mm)）。
    * @param dashLen4 点划线第四段的长度（单位毫米(mm)）。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    */
    LPAPI.prototype.drawDashLine4 = function (x1, y1, x2, y2, lineWidth, dashLen1, dashLen2, dashLen3, dashLen4) {
        lineWidth = lineWidth || defaultLineWidth;
        dashLen1 = dashLen1 || 0.5;
        dashLen2 = dashLen2 || dashLen1 * 0.5;
        dashLen3 = dashLen3 || dashLen1;
        dashLen4 = dashLen4 || dashLen2;
        return this.dtPrinter.DrawDashLine4(x1 * 100, y1 * 100, x2 * 100, y2 * 100, lineWidth * 100, dashLen1 * 100, dashLen2 * 100, dashLen3 * 100, dashLen4 * 100) === 0;
    };

    /**
    *  打印指定文件的图片
    * @param imageFile 位图文件路径名称，图片类型支持 bmp, jpg, gif, png, tiff 等常见位图文件格式。路径类型支持：本地文件路径和URL路径。
    * @param x 打印位图水平位置（单位毫米(mm)）。
    * @param y 打印位图垂直位置（单位毫米(mm)）。
    * @param width 打印位图水平宽度（单位毫米(mm)）。如果 width 为 0，则采用加载的位图的宽度。
    * @param height 打印位图垂直高度（单位毫米(mm)）。如果 height 为 0，则采用加载的位图的高度。
    * @param threshold 黑白打印的灰度阀值。0 表示使用参数设置中的值；256 表示取消黑白打印，用灰度打印；257 表示直接打印图片原来的颜色。
    * @returns {boolean}
    * true：成功。
    * false：失败。
    * @使用注意：
    *       如果之前没有调用 StartPage 而直接进行打印，则打印函数会自动调用 StartPage开始一打印页面，然后进行打印。
    *       打印位置和宽度高度是基于当前页面的位置和方向，不考虑页面和打印动作的旋转角度。
    *       图片打印时会被缩放到指定的宽度和高度。
    *       标签打印都是黑白打印，因此位图会被转变成灰度图片（RGB三分量相同，0～255取值的颜色）之后，然后根据一阀值将位图再次转换黑白位图再进行打印。默认灰度阀值为 192，也就是说 >= 192 的会被认为是白色，而 < 192 的会被认为是黑色。
    */
    LPAPI.prototype.drawImage = function (imageFile, x, y, width, height, threshold) {
        return this.dtPrinter.DrawImage(imageFile, x * 100, y * 100, width * 100, height * 100, threshold || 192) === 0;
    };

    return LPAPI;
})();
