﻿class LabelPrinter {
    constructor(json, lpapi) {
        json = json || {};
        this.LPAPI = lpapi;

        this.Printer = json.Printer || this.DefaultPrinter || "";

        this.GapType = json.GapType || 255; //[255, 0, 1, 2] [随打印机, 连续纸, 定位孔, 间隙纸]
        this.PrintSpeed = json.PrintSpeed || 255;    //[255, 1, 2, 3, 4] [随打印机, (特慢), (慢), (正常), (快), (特快)]
        this.PrintDarkness = json.PrintDarkness || 255; //[255, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14][255:随打印机, 6:(正常), 10:(较浓), 15:(特浓)]
    }

    get DefaultPrinter() {
        return this.ExistPrint ? this.PrinterList[0] : "";
    }

    get PrinterList() {
        if (this.LPAPI) {
            console.log('LPAPIWeb');
            var printerInfo = this.LPAPI.getPrinters();
            let printerNames = printerInfo ? printerInfo.printers : [];
            if (printerNames) {
                return printerNames;
            }

            console.log("%c 未检测到打印机!!!", 'color:#f00;');
        }

        return [];
    }

    get ExistPrint() {
        return this.PrinterList.length > 0;
    }

    Open() {
        if (!this.Printer) return false;
        return this.LPAPI.openPrinter(this.Printer)
    }

    Update() {
        if (!this.Open()) return false;

        this.LPAPI.setGapType(this.GapType);
        this.LPAPI.setPrintSpeed(this.PrintSpeed);
        this.LPAPI.setPrintDarkness(this.PrintDarkness);

        return true;
    }

    _dateformat(date, format) {
        var o = {
            "M+": date.getMonth() + 1, //month
            "d+": date.getDate(),    //day
            "h+": date.getHours(),   //hour
            "m+": date.getMinutes(), //minute
            "s+": date.getSeconds(), //second
            "q+": Math.floor((date.getMonth() + 3) / 3),  //quarter
            "S": date.getMilliseconds() //millisecond
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
            (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
                RegExp.$1.length == 1 ? o[k] :
                    ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    }

    isEmpty(obj) {
        if (typeof obj == "undefined" || obj == null || obj == "") {
            return true;
        } else {
            return false;
        }
    }

    Print(asset, assetLabelTemplate, printTotal) {
        if (!this.Update()) return false;
        var width = 70;
        var height = 50;
        var marginleft = 0;
        var fontName = "微软雅黑";

        if (!_.isDate(asset.BuyTime)) {
            asset.BuyTime = new Date(asset.BuyTime);
        }

        printTotal = printTotal || 1;

        while (printTotal--) {
            if (!this.LPAPI.startJob(width, height, 90))
                return;

            var title = this.isEmpty(asset.UsedCompanyName) ? " " : asset.UsedCompanyName;

            if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "表格类型1")) {
                var w1 = 13, w2 = 20, textleft = 0.2, texttop = 0.5, fontHeight = 4, barcodeheight = fontHeight * 3;
                var x1 = 1, x2 = x1 + w1, x3 = x2 + w2, x4 = x3 + w1, x5 = x4 + w2, tablelineheight = fontHeight + 1;
                var y0 = fontHeight, y1 = y0 + fontHeight + 1,
                    y2 = y1 + tablelineheight,
                    y3 = y2 + tablelineheight,
                    y4 = y3 + tablelineheight,
                    y5 = y4 + tablelineheight,
                    y6 = y5 + tablelineheight,
                    y7 = y6 + barcodeheight + fontHeight - 2;
                var linewidth = 0.2;
                var barCodePadding = 20;
                var contentWidth = (w1 + w2) * 2;

                this.LPAPI.setItemHorizontalAlignment(1);

                this.LPAPI.drawText(title, x1, y0, contentWidth, fontHeight + 1, fontHeight + 1, 0, fontName);

                this.LPAPI.drawLine(x1, y1, x5, y1, linewidth);
                this.LPAPI.drawLine(x1, y2, x5, y2, linewidth);
                this.LPAPI.drawLine(x1, y3, x5, y3, linewidth);
                this.LPAPI.drawLine(x1, y4, x5, y4, linewidth);
                this.LPAPI.drawLine(x1, y5, x5, y5, linewidth);
                this.LPAPI.drawLine(x1, y6, x5, y6, linewidth);
                this.LPAPI.drawLine(x1, y7, x5, y7, linewidth);

                this.LPAPI.drawLine(x1, y1, x1, y7, linewidth);
                this.LPAPI.drawLine(x2, y1, x2, y6, linewidth);
                this.LPAPI.drawLine(x3, y2, x3, y6, linewidth);
                this.LPAPI.drawLine(x4, y2, x4, y6, linewidth);
                this.LPAPI.drawLine(x5, y1, x5, y7, linewidth);

                this.LPAPI.setItemHorizontalAlignment(0);
                this.LPAPI.drawText("资产编码：",
                    textleft + x1, texttop + y1, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.SN) ? " " : asset.SN,
                    textleft + x2, texttop + y1, w1 + w2 * 2, tablelineheight, fontHeight, 0, fontName);

                this.LPAPI.drawText("资产名称：",
                    textleft + x1, texttop + y2, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.Name) ? " " : asset.Name,
                    textleft + x2, texttop + y2, w2, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText("使用人员：",
                    textleft + x3, texttop + y2, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.UsedEmployeeName) ? " " : asset.UsedEmployeeName,
                    textleft + x4, texttop + y2, w2, tablelineheight, fontHeight, 0, fontName);

                this.LPAPI.drawText("规格型号：",
                    textleft + x1, texttop + y3, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.Model) ? " " : asset.Model,
                    textleft + x2, texttop + y3, w2, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText("使用部门：",
                    textleft + x3, texttop + y3, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.UsedDepartmentName) ? " " : asset.UsedDepartmentName,
                    textleft + x4, texttop + y3, w2, tablelineheight, fontHeight, 0, fontName);

                this.LPAPI.drawText("存放地点：",
                    textleft + x1, texttop + y4, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.StoredAddressName) ? " " : asset.StoredAddressName,
                    textleft + x2, texttop + y4, w2, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText("计量单位：",
                    textleft + x3, texttop + y4, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this.isEmpty(asset.UnitName) ? " " : asset.UnitName,
                    textleft + x4, texttop + y4, w2, tablelineheight, fontHeight, 0, fontName);

                this.LPAPI.drawText("取得日期：",
                    textleft + x1, texttop + y5, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(this._dateformat(asset.BuyTime, "yyyy-MM-dd"),
                    textleft + x2, texttop + y5, w2, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText("资产价值：",
                    textleft + x3, texttop + y5, w1, tablelineheight, fontHeight, 0, fontName);
                this.LPAPI.drawText(asset.Total * asset.Price + "元",
                    textleft + x4, texttop + y5, w2, tablelineheight, fontHeight, 0, fontName);

                this.LPAPI.setItemHorizontalAlignment(1);
                this.LPAPI.draw1DBarcode(asset.Code, x1 + barCodePadding, y6 + 1, contentWidth - barCodePadding * 2, barcodeheight, fontHeight, 28);

            } else if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "表格类型2")) {

            } else if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "条形码")) {
                this.LPAPI.setItemHorizontalAlignment(1);
                this.LPAPI.drawText(title, 0 + marginleft, 4, 50, 6, 3, 0, fontName);

                this.LPAPI.setItemHorizontalAlignment(0);
                this.LPAPI.drawText("资产名称：" + asset.Name, 0 + marginleft, 10, 50, 5, 3, 0, fontName);

                this.LPAPI.setItemHorizontalAlignment(0);
                this.LPAPI.drawText("购入日期：" + this._dateformat(asset.BuyTime, "yyyy-MM-dd"), 0 + marginleft, 15, 50, 5, 3, 0, fontName);

                this.LPAPI.setItemHorizontalAlignment(1);
                this.LPAPI.draw1DBarcode(asset.Code, 0 + marginleft, 20, 50, 8, 3, 0);

            } else if (assetLabelTemplate.AssetLabelItems.some(x => x.AssetLabelBase.Name == "二维码")) {
                var marginleft = 0;

                this.LPAPI.setItemHorizontalAlignment(1);
                this.LPAPI.drawText(title, 0 + marginleft, 4, 50, 6, 3, 0, fontName);

                this.LPAPI.setItemHorizontalAlignment(0);
                this.LPAPI.draw2DQRCode(asset.Code, 0 + marginleft, 10, 20, 15);

                this.LPAPI.setItemHorizontalAlignment(0);
                this.LPAPI.drawText("资产名称：" + asset.Name, 15 + marginleft, 10, 35, 5, 3, 0, fontName);

                this.LPAPI.setItemHorizontalAlignment(0);
                this.LPAPI.drawText("购入日期：" + this._dateformat(asset.BuyTime, "yyyy-MM-dd"), 15 + marginleft, 15, 35, 5, 3, 0, fontName);
            }
            this.LPAPI.commitJob();
        }
    }
}