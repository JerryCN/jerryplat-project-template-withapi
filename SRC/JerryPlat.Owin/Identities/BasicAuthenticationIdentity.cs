﻿using System.Security.Principal;

namespace JerryPlat.Owin.Identities
{
    public class BasicAuthenticationIdentity<T> : GenericIdentity
    {
        public T Session { get; set; }

        public BasicAuthenticationIdentity(string name, T session)
            : base(name, "Basic")
        {
            this.Session = session;
        }
    }
}