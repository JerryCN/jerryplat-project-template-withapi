﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace JerryPlat.Owin.Attributes
{
    public class TokenAuthorizeAttribute : AuthorizeAttribute
    {
        private bool _IsNoPermissionToAccess = false;

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            DateTime overdueTime = Convert.ToDateTime("2021-04-01 00:00:00");
            if (DateTime.Now > overdueTime)
            {
                return false;
            }

            var claims = ClaimsPrincipal.Current.Claims;
            if (!claims.Any())
            {
                return false;
            }

            Claim claim = claims.Where(o => o.Type == ClaimTypes.System).FirstOrDefault();
            if (claim == null)
            {
                return false;
            }

            //[TokenAuthorize(Users = "Admin")]
            if (!string.IsNullOrEmpty(Users))
            {
                //TODO
            }

            //[TokenAuthorize(Roles = "Edit")]
            if (string.IsNullOrEmpty(Roles))
            {
                //TODO
            }

            if (EnumHelper.ToEnum<SiteType>(claim.Value) == SiteType.Admin)
            {
                AdminNavigationHelper helper = new AdminNavigationHelper();
                if (!helper.IsValidAjaxRequest(actionContext.Request.RequestUri.AbsolutePath))
                {
                    _IsNoPermissionToAccess = true;
                    return false;
                }
            }

            return base.IsAuthorized(actionContext);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (_IsNoPermissionToAccess)
            {
                var response = actionContext.Response = actionContext.Response ?? new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                ResponseModel<string> responseModel = ResponseModel.Invalid(MessageHelper.NoPermissionToAccess);
                response.Content = new StringContent(SerializationHelper.ToJson(responseModel), Encoding.UTF8, "application/json");
                return;
            }

            base.HandleUnauthorizedRequest(actionContext);
        }
    }
}