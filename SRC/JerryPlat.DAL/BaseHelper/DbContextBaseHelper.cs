﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ESUPER.Code.Common.DbHelper;
using JerryPlat.Models.Db;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace JerryPlat.DAL
{
    public class DbContextBaseHelper<TDbContext>
       where TDbContext : DbContext, new()
    {
        #region ORM

        #region Basic

        private TDbContext db;
        protected TDbContext _Db => db ?? (db = SingleInstanceHelper.GetInstance<TDbContext>());
        protected void ResetDbContext()
        {
            db = SingleInstanceHelper.GetInstance<TDbContext>(true);
        }

        public virtual DbSet<TEntity> GetDbSet<TEntity>()
            where TEntity : class, new()
        {
            return _Db.Set<TEntity>();
        }

        public virtual DbSet GetDbSet(Type type)
        {
            return _Db.Set(type);
        }

        #endregion Basic

        #region Locked

        public bool IsLockedRecord<TEntity>(TEntity entity)
            where TEntity : class, new()
        {
            return IsLockedRecord<TEntity>(TypeHelper.GetIdPropertyValue(entity));
        }

        public bool IsLockedRecord<TEntity>(int intId)
            where TEntity : class, new()
        {
            return IsLockedRecord<TEntity>(new List<int> { intId });
        }

        public bool IsLockedRecord<TEntity>(List<int> idList)
            where TEntity : class, new()
        {
            List<int> lockedIdList = GetLockedIdList<TEntity>();
            if (lockedIdList == null)
            {
                return false;
            }
            return idList.Intersect(lockedIdList).Any();
        }

        public virtual List<int> GetLockedIdList<TEntity>()
            where TEntity : class, new()
        {
            if (!TypeHelper.IsInheritOf<TEntity, ILockedEntity>())
            {
                return null;
            }

            string strEntityType = typeof(TEntity).FullName;

            List<int> lockedIdList = CacheHelper.GetCache<List<int>>(strEntityType);
            if (lockedIdList == null)
            {
                IEnumerable<ILockedEntity> queryableLockedEntities = GetDbSet<TEntity>() as IEnumerable<ILockedEntity>;
                lockedIdList = queryableLockedEntities.Where(o => o.IsLocked).Select(o => o.Id).ToList();
                CacheHelper.SetCache(lockedIdList, strEntityType);
            }

            return lockedIdList;
        }

        #endregion Locked

        #region Attach

        public virtual void Attach<TEntity>(TEntity entity)
            where TEntity : class, new()
        {
            DbContextHelper.Attach(_Db, entity);
        }

        public virtual void Attach<TEntity>(TEntity entity, EntityState entityState)
              where TEntity : class, new()
        {
            DbContextHelper.Attach(_Db, entity, entityState);
        }


        public virtual void Attach<TEntity>(DbSet<TEntity> entities, TEntity entity, EntityState entityState)
               where TEntity : class, new()
        {
            DbContextHelper.Attach(_Db, entity, entityState, entities);
        }

        #endregion Attach

        #region GetQueryableList

        public virtual IQueryable<TEntity> GetQueryableList<TEntity>(IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            return queryEntity ?? (GetDbSet<TEntity>() as IQueryable<TEntity>);
        }

        public virtual IQueryable<TQueryableEntity> GetQueryableList<TEntity, TQueryableEntity>(IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
            where TQueryableEntity : class, new()
        {
            return (queryEntity ?? GetDbSet<TEntity>()).ProjectTo<TQueryableEntity>();
        }

        public virtual IQueryable<TEntity> GetQueryableList<TEntity>(SearchModel searchModel, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(queryEntity);

            return PageHelper.GetOrderQueryable(queryEntity, searchModel?.Sort);
        }

        public virtual IQueryable<TEntity> GetQueryableList<TEntity, TKey>(Expression<Func<TEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true, string strOrder = "", IQueryable<TEntity> queryEntity = null)
          where TEntity : class, new()
        {
            queryEntity = GetQueryableList(queryEntity);
            return PageHelper.GetOrderQueryable(queryEntity, orderByKeySelector, bIsAscOrder, strOrder);
        }

        public virtual IQueryable<TEntity> GetQueryableList<TEntity, TKey>(SearchModel searchModel, Expression<Func<TEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            return GetQueryableList(orderByKeySelector, bIsAscOrder, searchModel?.Sort, queryEntity);
        }

        #endregion GetQueryableList

        #region GetList

        public virtual List<TEntity> GetList<TEntity>(IQueryable<TEntity> queryEntity = null)
         where TEntity : class, new()
        {
            queryEntity = GetQueryableList(queryEntity);
            return queryEntity.ToList();
        }

        public virtual async Task<List<TEntity>> GetListAsync<TEntity>(IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(queryEntity);
            return await queryEntity.ToListAsync();
        }

        public virtual List<TEntity> GetList<TEntity>(SearchModel searchModel, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(searchModel, queryEntity);
            return queryEntity.ToList();
        }

        public virtual async Task<List<TEntity>> GetListAsync<TEntity>(SearchModel searchModel, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(searchModel, queryEntity);
            return await queryEntity.ToListAsync();
        }

        public virtual List<TEntity> GetList<TEntity, TKey>(Expression<Func<TEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(orderByKeySelector, bIsAscOrder, null, queryEntity);
            return queryEntity.ToList();
        }

        public virtual async Task<List<TEntity>> GetListAsync<TEntity, TKey>(Expression<Func<TEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(orderByKeySelector, bIsAscOrder, null, queryEntity);
            return await queryEntity.ToListAsync();
        }

        public virtual List<TEntity> GetList<TEntity, TKey>(SearchModel searchModel, Expression<Func<TEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(searchModel, orderByKeySelector, bIsAscOrder, queryEntity);
            return queryEntity.ToList();
        }

        public virtual async Task<List<TEntity>> GetListAsync<TEntity, TKey>(SearchModel searchModel, Expression<Func<TEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            queryEntity = GetQueryableList(searchModel, orderByKeySelector, bIsAscOrder, queryEntity);
            return await queryEntity.ToListAsync();
        }

        #endregion GetList

        #region GetPageList

        public virtual PageData<TEntity> GetPageList<TEntity>(SearchModel searchModel, PageParam pageParam, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            Expression<Func<TEntity, int>> keySelector = PageHelper.GetDefaultKeyExpression<TEntity, int>();
            IQueryable<TEntity> queryList = GetQueryableList(searchModel, queryEntity);
            return PageHelper.GetPageData(queryList, keySelector, pageParam, bIsAscOrder, searchModel?.Sort);
        }

        public virtual async Task<PageData<TEntity>> GetPageListAsync<TEntity>(SearchModel searchModel, PageParam pageParam, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
           where TEntity : class, new()
        {
            Expression<Func<TEntity, int>> keySelector = PageHelper.GetDefaultKeyExpression<TEntity, int>();
            IQueryable<TEntity> queryList = GetQueryableList(searchModel, queryEntity);
            return await PageHelper.GetPageDataAsync(queryList, keySelector, pageParam, bIsAscOrder, searchModel?.Sort);
        }

        public virtual PageData<TEntity> GetPageList<TEntity, TKey>(SearchModel searchModel, Expression<Func<TEntity, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            IQueryable<TEntity> queryList = GetQueryableList(searchModel, queryEntity);
            return PageHelper.GetPageData(queryList, orderByKeySelector, pageParam, bIsAscOrder, searchModel?.Sort);
        }

        public virtual async Task<PageData<TEntity>> GetPageListAsync<TEntity, TKey>(SearchModel searchModel, Expression<Func<TEntity, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true, IQueryable<TEntity> queryEntity = null)
            where TEntity : class, new()
        {
            IQueryable<TEntity> queryList = GetQueryableList(searchModel, queryEntity);
            return await PageHelper.GetPageDataAsync(queryList, orderByKeySelector, pageParam, bIsAscOrder, searchModel?.Sort);
        }

        #endregion GetPageList

        #region Exist
        public virtual bool Exist<TEntity>(Expression<Func<TEntity, bool>> predicate, IQueryable<TEntity> entities = null)
          where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            return entities.Any(predicate);
        }

        public virtual async Task<bool> ExistAsync<TEntity>(Expression<Func<TEntity, bool>> predicate, IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            return await entities.AnyAsync(predicate);
        }

        public virtual bool ExistById<TEntity>(int id, IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            Expression<Func<TEntity, bool>> predicate = PageHelper.GetPredicateById<TEntity>(id);
            return Exist(predicate, entities);
        }

        public virtual async Task<bool> ExistByIdAsync<TEntity>(int id, IQueryable<TEntity> entities = null)
          where TEntity : class, new()
        {
            Expression<Func<TEntity, bool>> predicate = PageHelper.GetPredicateById<TEntity>(id);
            return await ExistAsync(predicate, entities);
        }

        #endregion ExistById

        #region Exist Cascading Relation
        public bool ExistCascadingRelation<TEntity>()
            where TEntity : class, new()
        {
            return TypeHelper.IsExistGenericTypeDefinition<TEntity>();
        }
        #endregion

        #region GetById

        public virtual TEntity GetById<TEntity>(int id, IQueryable<TEntity> entities = null, bool bIsNotNull = true)
          where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            Expression<Func<TEntity, bool>> predicate = PageHelper.GetPredicateById<TEntity>(id);
            TEntity entity = entities.FirstOrDefault(predicate);
            if (bIsNotNull && entity == null)
            {
                throw new Exception("Not exist [" + typeof(TEntity).Name + "] entity with Id=" + id);
            }
            return entity;
        }

        public virtual async Task<TEntity> GetByIdAsync<TEntity>(int id, IQueryable<TEntity> entities = null, bool bIsNotNull = true)
             where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            Expression<Func<TEntity, bool>> predicate = PageHelper.GetPredicateById<TEntity>(id);
            TEntity entity = await entities.FirstOrDefaultAsync(predicate);
            if (bIsNotNull && entity == null)
            {
                throw new Exception("Not exist [" + typeof(TEntity).Name + "] entity with Id=" + id);
            }
            return entity;
        }

        #endregion GetById

        #region GetNewId
        public virtual int GetNewId<TEntity>(IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            DbContextHelper.DBCC_CHECKIDENT<TEntity>(_Db);

            entities = entities ?? GetDbSet<TEntity>();

            int intNewId = 1;

            if (entities.Any())
            {
                intNewId = entities.AsNoTracking().Max(PageHelper.GetDefaultKeyExpression<TEntity, int>()) + 1;
            }

            return intNewId;
        }

        public virtual async Task<int> GetNewIdAsync<TEntity>(IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            await DbContextHelper.DBCC_CHECKIDENTAsync<TEntity>(_Db);

            entities = entities ?? GetDbSet<TEntity>();

            int intNewId = 1;

            if (await entities.AnyAsync())
            {
                intNewId = await entities.AsNoTracking().MaxAsync(PageHelper.GetDefaultKeyExpression<TEntity, int>()) + 1;
            }

            return intNewId;
        }

        #endregion GetNewId

        #region GetNewOrderIndex
        public virtual int GetNewOrderIndex<TEntity>(IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            TypeHelper.CheckInheritOf<TEntity, IOrderIndexEntity>();

            entities = entities ?? GetDbSet<TEntity>();

            int intOrderIndex = 1;

            if (entities.Any())
            {
                intOrderIndex = entities.AsNoTracking().Max(PageHelper.GetKeyExpression<TEntity, int>("OrderIndex")) + 1;
            }

            return intOrderIndex;
        }

        public virtual async Task<int> GetNewOrderIndexAsync<TEntity>(IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            TypeHelper.CheckInheritOf<TEntity, IOrderIndexEntity>();

            entities = entities ?? GetDbSet<TEntity>();

            int intOrderIndex = 1;

            if (entities.Any())
            {
                intOrderIndex = await entities.AsNoTracking().MaxAsync(PageHelper.GetKeyExpression<TEntity, int>("OrderIndex")) + 1;
            }

            return intOrderIndex;
        }

        #endregion GetNewId

        #region GetDetail & GetById & GetByIdList

        public virtual TEntity GetDetail<TEntity>(int id, IQueryable<TEntity> entities = null, bool bIsNotNull = true)
          where TEntity : class, new()
        {
            return GetById(id, entities, bIsNotNull);
        }

        public virtual async Task<TEntity> GetDetailAsync<TEntity>(int id, IQueryable<TEntity> entities = null, bool bIsNotNull = true)
             where TEntity : class, new()
        {
            return await GetByIdAsync(id, entities, bIsNotNull);
        }

        public virtual List<TEntity> GetByIdList<TEntity>(List<int> idList, IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            Expression<Func<TEntity, bool>> predicate = PageHelper.GetPredicateByIdList<TEntity>(idList);
            List<TEntity> entityList = entities.Where(predicate).ToList();
            return entityList;
        }

        public virtual async Task<List<TEntity>> GetByIdListAsync<TEntity>(List<int> idList, IQueryable<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            Expression<Func<TEntity, bool>> predicate = PageHelper.GetPredicateByIdList<TEntity>(idList);
            List<TEntity> entityList = await entities.Where(predicate).ToListAsync();
            return entityList;
        }

        #endregion GetDetail & GetById & GetByIdList

        #region Delete

        public bool Delete<TEntity>(int id, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            TEntity entity = GetById<TEntity>(id);
            return Delete(entity, entities);
        }

        public void SetChildrenDeleted<TEntity>(int intParentId, DbSet<TEntity> entities = null)
             where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            List<ITreeEntity> chilren = TypeHelper.GetChilrenList(entities.AsNoTracking() as IEnumerable<ITreeEntity>, intParentId);
            if (chilren == null)
            {
                return;
            }
            chilren.ForEach(item =>
            {
                Attach(entities, item as TEntity, EntityState.Deleted);
            });
        }

        public virtual void DeleteOnly<TEntity>(TEntity entity, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();

            if (entity is ITreeEntity enabledEntity)
            {
                SetChildrenDeleted(enabledEntity.Id, entities);
            }

            DeleteBefore(entity);

            Attach(entities, entity, EntityState.Deleted);
        }

        protected virtual int GetSessionId()
        {
            return 0;
        }

        protected virtual int GetSessionUsedCompanyId()
        {
            return 0;
        }



        public virtual void DeleteBefore<TEntity>(TEntity entity)
            where TEntity : class, new()
        {
            CascadeHelper.Run(_Db, GetSessionUsedCompanyId(),GetSessionId(), entity, CascadeType.Delete);
        }

        public bool Delete<TEntity>(TEntity entity, DbSet<TEntity> entities = null, bool bIsSaveChanges = true)
            where TEntity : class, new()
        {
            DeleteOnly(entity, entities);
            return SaveChanges(bIsSaveChanges);
        }

        public bool DeleteList<TEntity>(List<int> idList, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            List<TEntity> entityList = GetByIdList<TEntity>(idList);
            return DeleteList(entityList, entities);
        }

        public bool DeleteList<TEntity>(List<TEntity> entityList, DbSet<TEntity> entities = null)
           where TEntity : class, new()
        {
            foreach (TEntity entity in entityList)
            {
                DeleteOnly(entity, entities);
            }
            return SaveChanges();
        }

        public async Task<bool> DeleteAsync<TEntity>(int id, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            TEntity entity = await GetByIdAsync<TEntity>(id);
            return await DeleteAsync(entity, entities);
        }

        public async Task<bool> DeleteAsync<TEntity>(TEntity entity, DbSet<TEntity> entities = null, bool bIsSaveChanges = true)
         where TEntity : class, new()
        {
            DeleteOnly(entity, entities);
            return await SaveChangesAsync(bIsSaveChanges);
        }

        public async Task<bool> DeleteListAsync<TEntity>(List<int> idList, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            List<TEntity> entityList = await GetByIdListAsync<TEntity>(idList);
            return await DeleteListAsync(entityList, entities);
        }

        public async Task<bool> DeleteListAsync<TEntity>(List<TEntity> entityList, DbSet<TEntity> entities = null)
           where TEntity : class, new()
        {
            foreach (TEntity item in entityList)
            {
                await DeleteAsync(item, entities, false);
            }
            return await SaveChangesAsync();
        }

        #endregion Delete

        #region Save: Add & Edit

        protected void CheckNullEntity<TEntity>(TEntity entity)
            where TEntity : class, new()
        {
            if (entity == null)
            {
                throw new Exception("[" + typeof(TEntity).Name + "] entity can not be null!");
            }
        }

        protected virtual void AddBefore<TEntity>(TEntity entity)
            where TEntity : class, new()
        {
            CascadeHelper.Run(_Db,GetSessionUsedCompanyId(), GetSessionId(), entity, CascadeType.Add);
        }

        protected virtual void EditBefore<TEntity>(TEntity entity)
            where TEntity : class, new()
        {
            CascadeHelper.Run(_Db, GetSessionUsedCompanyId(),GetSessionId(), entity, CascadeType.Edit);
        }

        public void CheckDefaultUsed<TEntity>(TEntity entity, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            if (!(entity is IDefaultUsedEntity defaultUsedEntity))
            {
                return;
            }
            
            if (!defaultUsedEntity.IsDefaultUsed)
            {
                return;
            }

            IQueryable<IDefaultUsedEntity> defaultUsedEntities = (entities ?? GetDbSet<TEntity>()) as IQueryable<IDefaultUsedEntity>;

            defaultUsedEntities.Where(o => o.Id != defaultUsedEntity.Id & o.IsDefaultUsed).ToList()
                .ForEach(o => o.IsDefaultUsed = false);
        }

        public virtual void BeforeChanging<TEntity>(TEntity entity)
           where TEntity : class, new()
        {

        }

        public virtual void Changing<TEntity>(TEntity entity, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            CheckNullEntity(entity);

            BeforeChanging(entity);

            entities = entities ?? GetDbSet<TEntity>();

            if (PageHelper.IsAdd<TEntity, int>(entity))
            {
                AddBefore(entity);
                DbContextHelper.DBCC_CHECKIDENT<TEntity>(_Db);
                entities.Add(entity);
            }
            else
            {
                EditBefore(entity);
                Attach(entities, entity, EntityState.Modified);
            }

            CheckDefaultUsed(entity, entities);

            Changed(entity);
        }

        public virtual void Changed<TEntity>(TEntity entity)
            where TEntity : class, new()
        {

        }

        public bool Save<TEntity>(TEntity entity, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            Changing(entity, entities);
            return SaveChanges();
        }

        public async Task<bool> SaveAsync<TEntity>(TEntity entity, DbSet<TEntity> entities = null)
             where TEntity : class, new()
        {
            Changing(entity, entities);
            return await SaveChangesAsync();
        }

        #endregion Save: Add & Edit

        #region SaveChanges

        public bool SaveChanges(bool bIsSaveChanges = true)
        {
            if (bIsSaveChanges)
            {
                try
                {
                    return (_Db.SaveChanges()) > 0;
                }
                catch (OptimisticConcurrencyException ex)//并发冲突异常
                {
                    ((IObjectContextAdapter)_Db).ObjectContext.Refresh(RefreshMode.ClientWins, _Db);
                    return (_Db.SaveChanges()) > 0;
                }
            }
            return true;
        }

        public async Task<bool> SaveChangesAsync(bool bIsSaveChanges = true)
        {
            if (bIsSaveChanges)
            {
                try
                {
                    return (await _Db.SaveChangesAsync()) > 0;
                }
                catch (OptimisticConcurrencyException ex)//并发冲突异常
                {
                    ((IObjectContextAdapter)_Db).ObjectContext.Refresh(RefreshMode.ClientWins, _Db);
                    return (await _Db.SaveChangesAsync()) > 0;
                }
            }
            return true;
        }

        #endregion SaveChanges

        #region Lock
        public async Task<bool> LockAsync<TEntity>(int id, bool bIsLocked, DbSet<TEntity> entities = null, bool bIsSaveChanges = true)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            TEntity entity = await GetByIdAsync<TEntity>(id, entities);

            if (entity is ILockedEntity lockedEntity)
            {
                if (lockedEntity.IsLocked == bIsLocked)
                {
                    return true;
                }

                lockedEntity.IsLocked = bIsLocked;
                return await SaveChangesAsync(bIsSaveChanges);
            }

            throw new Exception($"The class {typeof(TEntity).Name} must inherit the interface ILockedEntity.");
        }

        #endregion Enabled

        #region Enabled

        public void SetChildrenEnabled<TEntity>(int intParentId, bool bEnabled, DbSet<TEntity> entities = null)
             where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            List<ITreeEntity> chilren = TypeHelper.GetChilrenList(entities.AsNoTracking() as IEnumerable<ITreeEntity>, intParentId);
            if (chilren == null)
            {
                return;
            }
            chilren.ForEach(item =>
            {
                (item as IEnabledEntity).Enabled = bEnabled;
                Attach(entities, item as TEntity, EntityState.Modified);
            });
        }

        public async Task<bool> EnabledAsync<TEntity>(int id, DbSet<TEntity> entities = null, bool bIsSaveChanges = true)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            TEntity entity = await GetByIdAsync<TEntity>(id, entities);

            if (entity is IEnabledEntity enabledEntity)
            {
                enabledEntity.Enabled = !enabledEntity.Enabled;

                if (entity is ITreeEntity)
                {
                    SetChildrenEnabled(enabledEntity.Id, enabledEntity.Enabled, entities);
                }

                return await SaveChangesAsync(bIsSaveChanges);
            }

            throw new Exception($"The class {typeof(TEntity).Name} must inherit the interface IEnabledEntity.");
        }

        #endregion Enabled

        #region SoftDelete

        public void SetChildrenDeletedStatus<TEntity>(int intParentId, bool bIsDeleted, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            List<ITreeEntity> chilren = TypeHelper.GetChilrenList(entities.AsNoTracking() as IEnumerable<ITreeEntity>, intParentId);
            if (chilren == null)
            {
                return;
            }
            chilren.ForEach(item =>
            {
                (item as ISoftDeletedEntity).IsDeleted = bIsDeleted;
                Attach(entities, item as TEntity, EntityState.Modified);
            });
        }

        private void ChangeDeletedStatusOnly<TEntity>(TEntity entity, bool bIsDeleted, DbSet<TEntity> entities = null)
            where TEntity : class, new()

        {
            if (entity is ISoftDeletedEntity softDeletedEntity)
            {
                if (softDeletedEntity.IsDeleted == bIsDeleted)
                {
                    return;
                }

                softDeletedEntity.IsDeleted = bIsDeleted;
                entities = entities ?? GetDbSet<TEntity>();
                if (entity is ITreeEntity)
                {
                    SetChildrenDeletedStatus(softDeletedEntity.Id, bIsDeleted, entities);
                }
                Attach(entities, softDeletedEntity as TEntity, EntityState.Modified);
                return;
            }

            throw new Exception($"The class {typeof(TEntity).Name} must inherit the interface ISoftDeletedEntity.");
        }

        public async Task<bool> ChangeDeletedStatusAsync<TEntity>(int id, bool bIsDeleted, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            TEntity entity = await GetByIdAsync<TEntity>(id);
            return await ChangeDeletedStatusAsync(entity, bIsDeleted, entities);
        }

        public async Task<bool> ChangeDeletedStatusAsync<TEntity>(TEntity entity, bool bIsDeleted, DbSet<TEntity> entities = null, bool bIsSaveChanges = true)
         where TEntity : class, new()
        {
            ChangeDeletedStatusOnly(entity, bIsDeleted, entities);
            return await SaveChangesAsync(bIsSaveChanges);
        }

        public async Task<bool> ChangeDeletedStatusListAsync<TEntity>(List<int> idList, bool bIsDeleted, DbSet<TEntity> entities = null)
            where TEntity : class, new()
        {
            entities = entities ?? GetDbSet<TEntity>();
            List<TEntity> entityList = await GetByIdListAsync<TEntity>(idList);
            return await ChangeDeletedStatusListAsync(entityList, bIsDeleted, entities);
        }

        public async Task<bool> ChangeDeletedStatusListAsync<TEntity>(List<TEntity> entityList, bool bIsDeleted, DbSet<TEntity> entities = null)
           where TEntity : class, new()
        {
            foreach (TEntity entity in entityList)
            {
                ChangeDeletedStatusOnly(entity, bIsDeleted, entities);
            }
            return await SaveChangesAsync();
        }

        #endregion SoftDelete

        #region Code
        public virtual async Task<string> GetCodeProfixAsync(SearchModel searchModel)
        {
            return await Task.FromResult<string>("");
        }
        public virtual async Task<string> GetMaxSubCodeAsync<TEntity>(SearchModel searchModel, string strStartWith = "")
            where TEntity : class, new()
        {
            TypeHelper.CheckInheritOf<TEntity, ICodeEntity>();

            IEnumerable<ICodeEntity> codeEntities = GetDbSet<TEntity>() as IEnumerable<ICodeEntity>;
            if (!string.IsNullOrEmpty(strStartWith))
            {
                codeEntities = codeEntities.Where(o => o.Code.StartsWith(strStartWith));
            }

            ICodeEntity codeEntity = codeEntities.OrderByDescending(o => o.Code).FirstOrDefault();

            return await Task.FromResult<string>(codeEntity == null ? "" : codeEntity.Code);
        }

        public async Task<string> GetCodeAsync<TEntity>(Func<string, Task<bool>> IsExistCodeAsync = null, SearchModel searchModel = null)
            where TEntity : class, new()
        {
            TypeHelper.CheckInheritOf<TEntity, ICodeEntity>();

            string strKey = $"{typeof(TEntity).Name}_Code_Role";
            string strCodeRole = SystemConfigModel.GetConfig(strKey);

            string strMatch = "{Profix}";
            if (strCodeRole.Contains(strMatch))
            {
                string strCodeProfix = await GetCodeProfixAsync(searchModel);
                strCodeRole = strCodeRole.Replace("{Profix}", strCodeProfix);
            }

            strCodeRole = ToolHelper.GetDateCode(strCodeRole);

            strMatch = @"{Order:\d+}";
            string strMaxSubCode = string.Empty;
            if (Regex.IsMatch(strCodeRole, strMatch))
            {
                string strStartWith = ToolHelper.GetCodeStartWith(strCodeRole);
                strMaxSubCode = await GetMaxSubCodeAsync<TEntity>(searchModel, strStartWith);
            }

            return await ToolHelper.GetCodeAsync(IsExistCodeAsync, strCodeRole, strMaxSubCode, searchModel.SearchText);
        }
        #endregion

        #endregion ORM
    }

    public class DbContextBaseHelper<TDbContext, TSession> : DbContextBaseHelper<TDbContext>
        where TDbContext : DbContext, new()
        where TSession : class
    {
        #region Session

        private TSession _session { get; set; }
        protected TSession _Session => _session ?? (_session = GetSession());

        protected int usedCompanyId { get { return GetSessionUsedCompanyId(); } set { } }

        private bool? isFromApi { get; set; }

        private bool _IsFromApi
        {
            get
            {
                if (isFromApi == null)
                {
                    isFromApi = Thread.CurrentPrincipal != null
                        && (Thread.CurrentPrincipal as ClaimsPrincipal).Claims.Any();
                }
                return isFromApi.Value;
            }
        }

        private string _sessionKey { get { return this._IsFromApi ? null : typeof(TSession).Name; } }

        private TSession GetSession()
        {
            if (this._IsFromApi)
            {
                if (Thread.CurrentPrincipal is ClaimsPrincipal clainsPrincipal)
                {
                    Claim claim = clainsPrincipal.Claims.Where(o => o.Type == ClaimTypes.UserData).FirstOrDefault();
                    if (claim != null)
                    {
                        return SerializationHelper.JsonToObject<TSession>(claim.Value);
                    }
                }
            }
            else if (SessionHelper.KeyValues.ContainsKey(this._sessionKey))
            {
                return SessionHelper.KeyValues[this._sessionKey].GetSession<TSession>();
            }

            return null;
        }

        public void InitSession(TSession session)
        {
            this._session = session;
        }

        public virtual void SetSession(object target)
        {
            TSession session = null;

            if (target is TSession)
            {
                session = target as TSession;
            }
            else
            {
                session = Mapper.Map<TSession>(target);
            }

            InitSession(session);

            if (!this._IsFromApi)
            {
                if (!SessionHelper.KeyValues.ContainsKey(this._sessionKey))
                {
                    throw new Exception("Not exist Session with Key = " + this._sessionKey);
                }
                SessionHelper.KeyValues[this._sessionKey].SetSession(session);
            }
        }

        public bool IsNullSession()
        {
            return this._Session == null;
        }

        protected override int GetSessionId()
        {
            if (IsNullSession())
            {
                return base.GetSessionId();
            }

            return TypeHelper.GetIdPropertyValue(_Session);
        }

        protected override int GetSessionUsedCompanyId()
        {
            if (IsNullSession())
            {
                return base.GetSessionUsedCompanyId();
            }

            return TypeHelper.GetUsedCompanyIdPropertyValue(_Session);
        }
        #endregion Session
    }

    public class DbContextBaseHelper<TDbContext, TEntity, TQueryableEntity, TSession> : DbContextBaseHelper<TDbContext, TSession>
        where TDbContext : DbContext, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
        where TSession : class
    {
        #region private Fields

        private DbSet<TEntity> _entities;
        private DbSet<TEntity> _Entites => _entities ?? (_entities = GetDbSet<TEntity>());

        private bool? _isSameEntity;
        private bool _IsSameEntity => _isSameEntity ?? (_isSameEntity = typeof(TEntity) == typeof(TQueryableEntity)).Value;

        #endregion private Fields

        #region private functions

        public DbSet<TEntity> GetEntities()
        {
            return _Entites;
        }

        //protected override void CheckEntityIfDiffThenMustOverride()
        //{
        //    if (!_IsSameEntity)
        //    {
        //        throw new Exception("This method must be override!");
        //    }
        //}

        #endregion private functions

        #region public functions

        #region GetQueryableList

        public virtual IQueryable<TQueryableEntity> GetQueryableList(SearchModel searchModel)
        {
            IQueryable<TEntity> queryableList = _Entites;

            if (TypeHelper.IsInheritOf<TEntity, INoShowSoftDeleteEntity>())
            {
                queryableList = queryableList.Where(PageHelper.GetPredicate<TEntity, bool>("IsDeleted", false));
            }

            if (TypeHelper.IsInheritOf<TEntity, INoShowDisabledEntity>())
            {
                queryableList = queryableList.Where(PageHelper.GetPredicate<TEntity, bool>("Enabled", true));
            }

            if (TypeHelper.IsInheritOf<TEntity, INoShowInvalidEntity>())
            {
                queryableList = queryableList.Where(PageHelper.GetPredicate<TEntity, bool>("IsValid", true));
            }

            IQueryable<TQueryableEntity> queryableDtoList = _IsSameEntity
                ?
                queryableList as IQueryable<TQueryableEntity>
                :
                queryableList.ProjectTo<TQueryableEntity>();

            SetQueryableFilter(ref queryableDtoList);

            if (searchModel != null)
            {
                SetSearchQueryableList(ref queryableDtoList, searchModel);
            }

            return queryableDtoList;
        }

        public virtual void SetQueryableFilter(ref IQueryable<TQueryableEntity> queryableDtoList)
        {

        }

        public virtual void SetSearchQueryableList(ref IQueryable<TQueryableEntity> queryableDtoList, SearchModel searchModel)
        {

        }

        #endregion GetQueryableList

        #region GetList

        public virtual List<TQueryableEntity> GetList(SearchModel searchModel)
        {
            var queryableList = GetQueryableList(searchModel);
            SetQueryableFilter(ref queryableList);
            return GetList(searchModel, queryableList);
        }

        public virtual async Task<List<TQueryableEntity>> GetListAsync(SearchModel searchModel)
        {
            var queryableList = GetQueryableList(searchModel);
            SetQueryableFilter(ref queryableList);
            return await GetListAsync(searchModel, queryableList);
        }

        public virtual List<TQueryableEntity> GetList<TKey>(SearchModel searchModel, Expression<Func<TQueryableEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true)
        {
            var queryableList = GetQueryableList(searchModel);
            SetQueryableFilter(ref queryableList);

            return GetList(searchModel, orderByKeySelector, bIsAscOrder, queryableList);
        }

        public virtual async Task<List<TQueryableEntity>> GetListAsync<TKey>(SearchModel searchModel, Expression<Func<TQueryableEntity, TKey>> orderByKeySelector, bool bIsAscOrder = true)
        {
            var queryableList = GetQueryableList(searchModel);
            SetQueryableFilter(ref queryableList);

            return await GetListAsync(searchModel, orderByKeySelector, bIsAscOrder, queryableList);
        }

        #endregion GetList

        #region GetPageList

        public virtual PageData<TQueryableEntity> GetPageList(SearchModel searchModel, PageParam pageParam, bool bIsAscOrder = true)
        {
            return GetPageList(searchModel, pageParam, bIsAscOrder, GetQueryableList(searchModel));
        }

        public virtual async Task<PageData<TQueryableEntity>> GetPageListAsync(SearchModel searchModel, PageParam pageParam, bool bIsAscOrder = true)
        {
            return await GetPageListAsync(searchModel, pageParam, bIsAscOrder, GetQueryableList(searchModel));
        }

        public virtual PageData<TQueryableEntity> GetPageList<TKey>(SearchModel searchModel, Expression<Func<TQueryableEntity, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true)
        {
            return GetPageList(searchModel, orderByKeySelector, pageParam, bIsAscOrder, GetQueryableList(searchModel));
        }

        public virtual async Task<PageData<TQueryableEntity>> GetPageListAsync<TKey>(SearchModel searchModel, Expression<Func<TQueryableEntity, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true)
        {
            return await GetPageListAsync(searchModel, orderByKeySelector, pageParam, bIsAscOrder, GetQueryableList(searchModel));
        }

        #endregion GetPageList

        #region GetById

        public virtual TQueryableEntity GetById(int id, bool bIsNotNull = true)
        {
            return GetDetail(id, GetQueryableList(SearchModel.Instance), bIsNotNull);
        }

        public virtual async Task<TQueryableEntity> GetByIdAsync(int id, bool bIsNotNull = true)
        {
            return await GetDetailAsync(id, GetQueryableList(SearchModel.Instance), bIsNotNull);
        }

        #endregion GetById

        #region Delete
        public virtual async Task<bool> DeleteAsync(int id)
        {
            return await DeleteAsync<TEntity>(id, null);
        }

        public virtual async Task<bool> DeleteListAsync(List<int> idList)
        {
            return await DeleteListAsync<TEntity>(idList, null);
        }

        #endregion Delete

        #region Save：Add & Edit
        protected override void AddBefore<TBaseEntity>(TBaseEntity entity)
        {
            if (entity is ISessionEntity sessionEntity)
            {
                sessionEntity.SessionId = GetSessionId();
            }

            if (entity is ICreateTimeEntity createTimeEntity)
            {
                createTimeEntity.CreateTime = DateTime.Now;
            }

            if (entity is IUpdateTimeEntity updateTimeEntity)
            {
                updateTimeEntity.UpdateTime = DateTime.Now;
            }

            if (entity is IUsedManagementEntity usedManagementEntity && usedManagementEntity.UsedCompanyId == 0)
            {
                usedManagementEntity.UsedCompanyId = GetSessionUsedCompanyId();
            }

            base.AddBefore<TBaseEntity>(entity);
        }

        protected override void EditBefore<TBaseEntity>(TBaseEntity entity)
        {
            if (entity is IUpdateTimeEntity updateTimeEntity)
            {
                updateTimeEntity.UpdateTime = DateTime.Now;
            }

            base.EditBefore<TBaseEntity>(entity);
        }

        public virtual bool Save(TQueryableEntity queryableEntity)
        {
            TEntity entity = Mapper.Map<TQueryableEntity, TEntity>(queryableEntity);
            return Save(entity, null);
        }

        public virtual async Task<bool> SaveAsync(TQueryableEntity queryableEntity)
        {
            TEntity entity = Mapper.Map<TQueryableEntity, TEntity>(queryableEntity);
            return await SaveAsync(entity, null);
        }

        #endregion Save：Add & Edit

        #region Import & Export
        protected virtual object GetExcelValue(int row, int col, object value, NPOIPropertyInfo npoiPropertyInfo)
        {
            if (value == null)
            {
                return null;
            }

            string strValue = value.ToString().Trim();

            Type propType = npoiPropertyInfo.PropertyInfo.PropertyType;
            
            if (propType.Name == typeof(bool).Name)
            {
                return TypeHelper.ToBoolen(strValue);
            }

            if (propType.IsEnum)
            {
                return EnumHelper.ToEnum(propType, strValue);
            }

            if (TypeHelper.UnwrapNullableType(propType).Name == typeof(DateTime).Name
                && value.GetType().Name != typeof(DateTime).Name)
            {
                return DateTime.ParseExact(strValue, npoiPropertyInfo.Excel.Formatter, null);
            }

            return value;
        }

        protected virtual string GetExcelValue(string fieldName, object value)
        {
            return null;
        }

        public virtual async Task<string> ImportAsync(string excelPath, Func<int, int, object, NPOIPropertyInfo, object> valueConverter = null)
        {
            try
            {
                List<TEntity> list = await PreImportAsync(excelPath, valueConverter);
                if (usedCompanyId != 1)
                {
                    if (TypeHelper.IsInheritOf<TEntity, Department>())
                    {
                        if (list.Any(x => (x as Department).ParentId != usedCompanyId)){
                            return "不允许导入其他单位的数据。";
                        }
                    }
                    if (TypeHelper.IsInheritOf<TEntity, IUsedManagementEntity>())
                    {
                        if (list.Any(x => (x as IUsedManagementEntity).UsedCompanyId != usedCompanyId))
                        {
                            return "不允许导入其他单位的数据。";
                        }
                    }
                }
                await ImportAsync(list);
                return ConstantHelper.Ok;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
                return ex.Message;
            }
        }

        public async Task<List<TEntity>> PreImportAsync(string excelPath, Func<int, int, object, NPOIPropertyInfo, object> valueConverter = null)
        {
            if (valueConverter == null)
            {
                valueConverter = ((row, col, value, npoiPropertyInfo) =>
                {
                    return GetExcelValue(row, col, value, npoiPropertyInfo);
                });
            }

            return await Task.FromResult(ExcelHelper.LoadExcel<TEntity>(HttpHelper.GetLocalPath(excelPath).ToMapPath(), valueConverter, GetValueMappingValue).ToList());
        }


        private Dictionary<string, DataTable> _dicTableCache = new Dictionary<string, DataTable>();
        private Dictionary<string, object> _dicValueCache = new Dictionary<string, object>();
        public virtual object GetValueMappingValue(ValueMappingAttribute valueMapping, object value)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                return value;
            }

            var key = valueMapping.GetCacheKey(value);
            if (_dicValueCache.ContainsKey(key))
            {
                return _dicValueCache[key];
            }

            DataTable cache = null;
            if (!_dicTableCache.ContainsKey(valueMapping.Table))
            {
                cache = SqlServerHelper.GetDataTable(valueMapping.GetSql());
                _dicTableCache[valueMapping.Table] = cache;
            }
            else
            {
                cache = _dicTableCache[valueMapping.Table];
            }

            DataRow dataRow = cache.Select($"{valueMapping.From}='{value.ToString().Trim()}'").FirstOrDefault();
            if(dataRow == null)
            {
                if (valueMapping.To.ToUpper().EndsWith("ID") 
                    && RegexHelper.IsNumberOnly(value.ToString()))
                {
                    return value;
                }

                throw new Exception($"未找到{valueMapping.From}={value}的记录。");
            }

            return dataRow[valueMapping.To];
        }

        public virtual async Task ImportAsync(List<TEntity> list)
        {
            GetDbSet<TEntity>().AddRange(list);
            await SaveChangesAsync();

            ImportAfter();
        }

        public virtual void ImportAfter()
        {
            var updateSql = typeof(TEntity).GetCustomAttribute<UpdateSqlAttribute>();
            if (updateSql == null)
            {
                return;
            }

            SqlServerHelper.ExecuteNonQuery(updateSql.Sql);
        }

        public async Task<string> ExportAsync(SearchModel searchModel, Func<string, object, string> valueConverter = null)
        {
            if (valueConverter == null)
            {
                valueConverter = GetExcelValue;
            }

            List<TQueryableEntity> dtoList = await GetListAsync(searchModel);

            return ExcelHelper.SaveExcelFile(dtoList, searchModel.FileName);
        }

        #endregion Import & Export

        #region Code
        public virtual async Task<bool> IsExistCodeAsync(string strCode)
        {
            return await _Entites.AsNoTracking().AnyAsync(PageHelper.GetPredicate<TEntity, string>("Code", strCode));
        }

        public virtual async Task<string> GetCodeAsync(SearchModel searchModel)
        {
            return await base.GetCodeAsync<TEntity>(IsExistCodeAsync, searchModel);
        }
        #endregion
        #endregion public functions
    }

    //public class DbContextBaseHelper<TDbContext, TEntity, TSession>
    //    : DbContextBaseHelper<TDbContext, TEntity, TEntity, TSession>
    //    where TDbContext : DbContext, new()
    //    where TEntity : class, new()
    //    where TSession : class
    //{
    //}
}