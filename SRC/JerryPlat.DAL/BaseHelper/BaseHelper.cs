﻿using JerryPlat.DAL.Context;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Linq;

namespace JerryPlat.DAL
{
    #region BaseSessionHelper

    public class BaseSessionHelper<TSession> : DbContextBaseHelper<JerryPlatDbContext, TSession>
        where TSession : class
    { }

    public class BaseSessionHelper<TEntity, TQueryableEntity, TSession> : DbContextBaseHelper<JerryPlatDbContext, TEntity, TQueryableEntity, TSession>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
        where TSession : class
    { }

    public class BaseSessionHelper<TEntity, TSession> : BaseSessionHelper<TEntity, TEntity, TSession>
        where TEntity : class, new()
        where TSession : class
    { }

    #endregion BaseSessionHelper

    #region AdminBaseSessionHelper

    public class AdminBaseSessionHelper : BaseSessionHelper<AdminUserDto>
    {
        public virtual void SetQueryableFilter<TQueryableEntity>(ref IQueryable<TQueryableEntity> queryableDtoList)
            where TQueryableEntity : class, new()
        {
            if (_Session == null)
            {
                return;
            }

            if (_Session.UsedCompanyId == 1)
            {
                return;
            }

            if (TypeHelper.IsInheritOf<TQueryableEntity, IUsedManagementDto>())
            {
                queryableDtoList = queryableDtoList.Where(PageHelper.GetPredicate<TQueryableEntity, int>("UsedCompanyId", _Session.UsedCompanyId));
            }
        }
    }

    public class AdminBaseSessionHelper<TEntity, TQueryableEntity> : BaseSessionHelper<TEntity, TQueryableEntity, AdminUserDto>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    {
        public virtual void SetQueryableFilter<TQueryable>(ref IQueryable<TQueryable> queryableDtoList)
            where TQueryable : class, new()
        {
            if (_Session == null)
            {
                return;
            }

            if (_Session.UsedCompanyId == 1)
            {
                return;
            }

            if (TypeHelper.IsInheritOf<TQueryable, IUsedManagementDto>())
            {
                queryableDtoList = queryableDtoList.Where(PageHelper.GetPredicate<TQueryable, int>("UsedCompanyId", _Session.UsedCompanyId));
            }
        }

        public override void SetQueryableFilter(ref IQueryable<TQueryableEntity> queryableDtoList)
        {
            SetQueryableFilter(ref queryableDtoList);
        }
    }

    public class AdminBaseSessionHelper<TEntity> : AdminBaseSessionHelper<TEntity, TEntity>
     where TEntity : class, new()
    { }

    #endregion AdminBaseSessionHelper

    #region BaseHelper

    public class BaseHelper : BaseSessionHelper<object>
    { }

    public class BaseHelper<TEntity, TQueryableEntity> : BaseSessionHelper<TEntity, TQueryableEntity, object>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    { }

    public class BaseHelper<TEntity> : BaseHelper<TEntity, TEntity>
        where TEntity : class, new()
    { }

    #endregion BaseHelper
}