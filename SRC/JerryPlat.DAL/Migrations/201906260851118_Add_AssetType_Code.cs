namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_AssetType_Code : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AssetType", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AssetType", "Code");
        }
    }
}
