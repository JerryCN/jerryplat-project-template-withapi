namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_AssetTypeOrderIndex : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AssetType", "OrderIndex");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AssetType", "OrderIndex", c => c.Int(nullable: false));
        }
    }
}
