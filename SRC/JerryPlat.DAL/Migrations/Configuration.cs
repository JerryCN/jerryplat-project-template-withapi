﻿namespace JerryPlat.DAL.Migrations
{
    using Context;
    using Models.Db;
    using Models.Dto;
    using Models.NPOI;
    using System.Data.Entity.Migrations;
    using Utils.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<JerryPlatDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "JerryPlat.DAL.Context.JerryPlatDbContext";
        }

        protected override void Seed(JerryPlatDbContext context)
        {
#if DEBUG
            DbSeedHelper.Seed(context);
#endif
        }
    }
}