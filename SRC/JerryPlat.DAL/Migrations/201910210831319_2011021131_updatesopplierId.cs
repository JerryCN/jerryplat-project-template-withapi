namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2011021131_updatesopplierId : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AssetUpdateItem", new[] { "SupplierId" });
            AlterColumn("dbo.AssetUpdateItem", "SupplierId", c => c.Int());
            CreateIndex("dbo.AssetUpdateItem", "SupplierId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.AssetUpdateItem", new[] { "SupplierId" });
            AlterColumn("dbo.AssetUpdateItem", "SupplierId", c => c.Int(nullable: false));
            CreateIndex("dbo.AssetUpdateItem", "SupplierId");
        }
    }
}
