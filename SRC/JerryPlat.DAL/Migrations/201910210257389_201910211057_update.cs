namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201910211057_update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 200),
                        UserName = c.String(nullable: false, maxLength: 200),
                        Password = c.String(nullable: false, maxLength: 200),
                        IsAllowedManualInventory = c.Boolean(nullable: false),
                        GroupId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .ForeignKey("dbo.Group", t => t.GroupId)
                .Index(t => t.GroupId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.AssetBorrowReturn",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        ReturnTime = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 2000),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetBorrowReturnItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetBorrowReturnId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetBorrowReturn", t => t.AssetBorrowReturnId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.AssetBorrowReturnId)
                .Index(t => t.AssetId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId);
            
            CreateTable(
                "dbo.Asset",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetStorageId = c.Int(nullable: false),
                        Code = c.String(maxLength: 200),
                        AssetTypeId = c.Int(nullable: false),
                        Name = c.String(maxLength: 200),
                        Model = c.String(maxLength: 200),
                        SN = c.String(maxLength: 200),
                        UnitId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        BuyTime = c.DateTime(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        SessionId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        BookkeepingTime = c.DateTime(nullable: false),
                        BookkeepingVoucher = c.String(maxLength: 200),
                        StoredAddressId = c.Int(nullable: false),
                        UsedMonth = c.Int(nullable: false),
                        SourceId = c.Int(nullable: false),
                        Note = c.String(maxLength: 2000),
                        PicPath = c.String(maxLength: 200),
                        SupplierId = c.Int(),
                        Contract = c.String(maxLength: 200),
                        ContractWay = c.String(maxLength: 200),
                        Director = c.String(maxLength: 200),
                        EndDate = c.DateTime(nullable: false),
                        Discription = c.String(maxLength: 2000),
                        AttachmentPath = c.String(maxLength: 200),
                        CreateTime = c.DateTime(nullable: false),
                        UpdateTime = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.AssetType", t => t.AssetTypeId)
                .ForeignKey("dbo.Source", t => t.SourceId)
                .ForeignKey("dbo.Supplier", t => t.SupplierId)
                .ForeignKey("dbo.Unit", t => t.UnitId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.AssetStorage", t => t.AssetStorageId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.AssetStorageId)
                .Index(t => t.AssetTypeId)
                .Index(t => t.UnitId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.SessionId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId)
                .Index(t => t.SourceId)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.AssetBorrowItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetBorrowId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetBorrow", t => t.AssetBorrowId)
                .Index(t => t.AssetBorrowId)
                .Index(t => t.AssetId);
            
            CreateTable(
                "dbo.AssetBorrow",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        LendingTime = c.DateTime(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        EstimatedReturnTime = c.DateTime(nullable: false),
                        SessionId = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        CreateTime = c.DateTime(nullable: false),
                        Region_Id = c.Int(),
                        Region_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Region", t => t.Region_Id)
                .ForeignKey("dbo.Region", t => t.Region_Id1)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId)
                .Index(t => t.SessionId)
                .Index(t => t.Region_Id)
                .Index(t => t.Region_Id1);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssetClearItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetClearId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetClear", t => t.AssetClearId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.AssetClearId)
                .Index(t => t.AssetId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId);
            
            CreateTable(
                "dbo.AssetClear",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        ClearTime = c.DateTime(nullable: false),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        Name = c.String(maxLength: 200),
                        ParentId = c.Int(nullable: false),
                        DepartmentType = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        Name = c.String(maxLength: 200),
                        CompanyId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        Phone = c.String(maxLength: 200),
                        Email = c.String(maxLength: 200),
                        WorkingStatusId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkingStatus", t => t.WorkingStatusId)
                .ForeignKey("dbo.Department", t => t.CompanyId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.CompanyId)
                .Index(t => t.DepartmentId)
                .Index(t => t.WorkingStatusId);
            
            CreateTable(
                "dbo.AssetFinancialUpdate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        Asset_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.Asset", t => t.Asset_Id)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.SessionId)
                .Index(t => t.Asset_Id);
            
            CreateTable(
                "dbo.AssetFinancialUpdateItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetFinancialUpdateId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreateTime = c.DateTime(nullable: false),
                        SessionId = c.Int(nullable: false),
                        UpdateRecord = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetFinancialUpdate", t => t.AssetFinancialUpdateId)
                .Index(t => t.AssetFinancialUpdateId)
                .Index(t => t.AssetId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetLocationItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetItemStatusId = c.Int(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssetItemStatus", t => t.AssetItemStatusId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .Index(t => t.AssetItemStatusId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId);
            
            CreateTable(
                "dbo.AssetItemStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetId = c.Int(nullable: false),
                        AssetStatus = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        UpdateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .Index(t => t.AssetId);
            
            CreateTable(
                "dbo.StoredAddress",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentId = c.Int(nullable: false),
                        Name = c.String(maxLength: 500),
                        OrderIndex = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.AssetUpdateItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetUpdateId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        AssetTypeId = c.Int(nullable: false),
                        Name = c.String(maxLength: 200),
                        Model = c.String(maxLength: 200),
                        SN = c.String(maxLength: 200),
                        UnitId = c.Int(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        BuyTime = c.DateTime(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        UsedMonth = c.Int(nullable: false),
                        SourceId = c.Int(nullable: false),
                        Note = c.String(maxLength: 2000),
                        PicPath = c.String(maxLength: 200),
                        SupplierId = c.Int(nullable: false),
                        Contract = c.String(maxLength: 200),
                        ContractWay = c.String(maxLength: 200),
                        Director = c.String(maxLength: 200),
                        EndDate = c.DateTime(nullable: false),
                        Discription = c.String(maxLength: 2000),
                        CreateTime = c.DateTime(nullable: false),
                        SessionId = c.Int(nullable: false),
                        UpdateRecord = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetType", t => t.AssetTypeId)
                .ForeignKey("dbo.AssetUpdate", t => t.AssetUpdateId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.Source", t => t.SourceId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Supplier", t => t.SupplierId)
                .ForeignKey("dbo.Unit", t => t.UnitId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .Index(t => t.AssetUpdateId)
                .Index(t => t.AssetId)
                .Index(t => t.AssetTypeId)
                .Index(t => t.UnitId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId)
                .Index(t => t.SourceId)
                .Index(t => t.SupplierId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(maxLength: 200),
                        IsLocked = c.Boolean(nullable: false),
                        ParentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InventoryAssetType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        AssetTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssetType", t => t.AssetTypeId)
                .ForeignKey("dbo.Inventory", t => t.InventoryId)
                .Index(t => t.InventoryId)
                .Index(t => t.AssetTypeId);
            
            CreateTable(
                "dbo.Inventory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Note = c.String(maxLength: 2000),
                        SessionId = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        Description = c.String(),
                        InventoryStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.InventoryDepartment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .ForeignKey("dbo.Inventory", t => t.InventoryId)
                .Index(t => t.InventoryId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.InventoryItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        AssetStatus = c.Int(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        OldTotal = c.Int(nullable: false),
                        NewTotal = c.Int(nullable: false),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.Inventory", t => t.InventoryId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .Index(t => t.InventoryId)
                .Index(t => t.AssetId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.InventoryRegion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Inventory", t => t.InventoryId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .Index(t => t.InventoryId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.InventoryUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        SessionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Inventory", t => t.InventoryId)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .Index(t => t.InventoryId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetUpdate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.Source",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Supplier",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Unit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssetUsedBackItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetUsedBackId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetUsedBack", t => t.AssetUsedBackId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .Index(t => t.AssetUsedBackId)
                .Index(t => t.AssetId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId);
            
            CreateTable(
                "dbo.AssetUsedBack",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        BackTime = c.DateTime(nullable: false),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetUsed",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        UsedTime = c.DateTime(nullable: false),
                        UsedCompanyId = c.Int(nullable: false),
                        UsedDepartmentId = c.Int(nullable: false),
                        UsedEmployeeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        StoredAddressId = c.Int(nullable: false),
                        EstimatedReturnTime = c.DateTime(nullable: false),
                        SessionId = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.StoredAddress", t => t.StoredAddressId)
                .ForeignKey("dbo.Department", t => t.UsedCompanyId)
                .ForeignKey("dbo.Department", t => t.UsedDepartmentId)
                .ForeignKey("dbo.Employee", t => t.UsedEmployeeId)
                .Index(t => t.UsedCompanyId)
                .Index(t => t.UsedDepartmentId)
                .Index(t => t.UsedEmployeeId)
                .Index(t => t.RegionId)
                .Index(t => t.StoredAddressId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetUsedItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetUsedId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AssetUsed", t => t.AssetUsedId)
                .Index(t => t.AssetUsedId)
                .Index(t => t.AssetId);
            
            CreateTable(
                "dbo.WorkingStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssetOperationRecord",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        AssetId = c.Int(nullable: false),
                        OperationType = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        SessionId = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .Index(t => t.AssetId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.AssetOtherInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetId = c.Int(nullable: false),
                        Key = c.String(nullable: false, maxLength: 200),
                        Value = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Asset", t => t.AssetId)
                .Index(t => t.AssetId);
            
            CreateTable(
                "dbo.AssetStorage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 200),
                        SessionId = c.Int(nullable: false),
                        StorageTime = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 2000),
                        StorageType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.SessionId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupId = c.Int(nullable: false),
                        NavigationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Group", t => t.GroupId)
                .ForeignKey("dbo.Navigation", t => t.NavigationId)
                .Index(t => t.GroupId)
                .Index(t => t.NavigationId);
            
            CreateTable(
                "dbo.Navigation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Code = c.String(maxLength: 200),
                        Icon = c.String(maxLength: 200),
                        RequestUrl = c.String(maxLength: 200),
                        ParentId = c.Int(nullable: false),
                        OrderIndex = c.Int(nullable: false),
                        SiteType = c.Int(nullable: false),
                        NavigationType = c.Int(nullable: false),
                        IsNeedAuthurized = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssetLabelBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Template = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssetLabelItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetLabelId = c.Int(nullable: false),
                        AssetLabelBaseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssetLabel", t => t.AssetLabelId)
                .ForeignKey("dbo.AssetLabelBase", t => t.AssetLabelBaseId)
                .Index(t => t.AssetLabelId)
                .Index(t => t.AssetLabelBaseId);
            
            CreateTable(
                "dbo.AssetLabel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Width = c.Double(nullable: false),
                        Height = c.Double(nullable: false),
                        IsDefaultUsed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DbBackupRestore",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BackupPath = c.String(maxLength: 200),
                        RestoreCount = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        RestoreTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OwinToken",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.String(maxLength: 200),
                        ClientSecret = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RequestRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SystemConfig",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Config = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UIRow",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UIId = c.Int(nullable: false),
                        FieldName = c.String(maxLength: 200),
                        Title = c.String(maxLength: 200),
                        IsOrder = c.Boolean(nullable: false),
                        Template = c.String(maxLength: 1000),
                        Rules = c.String(maxLength: 200),
                        OrderIndex = c.Int(nullable: false),
                        Description = c.String(maxLength: 200),
                        AppendText = c.String(maxLength: 200),
                        Width = c.Int(nullable: false),
                        Event = c.String(),
                        DefaultValue = c.Int(nullable: false),
                        Disabled = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UI", t => t.UIId)
                .Index(t => t.UIId);
            
            CreateTable(
                "dbo.UI",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UIType = c.Int(nullable: false),
                        Name = c.String(maxLength: 200),
                        TableName = c.String(maxLength: 200),
                        DialogName = c.String(maxLength: 200),
                        PageName = c.String(maxLength: 200),
                        Action = c.String(maxLength: 200),
                        ActionWidth = c.Int(nullable: false),
                        LabelWidth = c.String(),
                        SelectionIf = c.String(maxLength: 200),
                        Template = c.String(maxLength: 200),
                        AfterSubmitCallback = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UIRow", "UIId", "dbo.UI");
            DropForeignKey("dbo.AssetLabelItem", "AssetLabelBaseId", "dbo.AssetLabelBase");
            DropForeignKey("dbo.AssetLabelItem", "AssetLabelId", "dbo.AssetLabel");
            DropForeignKey("dbo.Role", "NavigationId", "dbo.Navigation");
            DropForeignKey("dbo.Role", "GroupId", "dbo.Group");
            DropForeignKey("dbo.AdminUser", "GroupId", "dbo.Group");
            DropForeignKey("dbo.AssetBorrowReturn", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetBorrowReturn", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetBorrowReturn", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetBorrowReturnItem", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetBorrowReturnItem", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetBorrowReturnItem", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetBorrowReturnItem", "AssetBorrowReturnId", "dbo.AssetBorrowReturn");
            DropForeignKey("dbo.Asset", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.Asset", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.Asset", "AssetStorageId", "dbo.AssetStorage");
            DropForeignKey("dbo.AssetStorage", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetOtherInfo", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetOperationRecord", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetOperationRecord", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetFinancialUpdate", "Asset_Id", "dbo.Asset");
            DropForeignKey("dbo.AssetBorrowReturnItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetBorrow", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetBorrow", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetBorrow", "RegionId", "dbo.Region");
            DropForeignKey("dbo.Asset", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetClearItem", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetClearItem", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetClearItem", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetClear", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetClear", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.InventoryItem", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.Employee", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.InventoryItem", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.Employee", "CompanyId", "dbo.Department");
            DropForeignKey("dbo.Employee", "WorkingStatusId", "dbo.WorkingStatus");
            DropForeignKey("dbo.Asset", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetLocationItem", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetLocationItem", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetLocationItem", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.StoredAddress", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetUsed", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetUsed", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetUsed", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetUsed", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetUsed", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetUsedItem", "AssetUsedId", "dbo.AssetUsed");
            DropForeignKey("dbo.AssetUsedItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetUsed", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetUsedBackItem", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetUsedBackItem", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetUsedBackItem", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetUsedBackItem", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetUsedBackItem", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetUsedBack", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetUsedBack", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetUsedBack", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetUsedBack", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetUsedBackItem", "AssetUsedBackId", "dbo.AssetUsedBack");
            DropForeignKey("dbo.AssetUsedBackItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetUpdateItem", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetUpdateItem", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetUpdateItem", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetUpdateItem", "UnitId", "dbo.Unit");
            DropForeignKey("dbo.Asset", "UnitId", "dbo.Unit");
            DropForeignKey("dbo.AssetUpdateItem", "SupplierId", "dbo.Supplier");
            DropForeignKey("dbo.Asset", "SupplierId", "dbo.Supplier");
            DropForeignKey("dbo.AssetUpdateItem", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetUpdateItem", "SourceId", "dbo.Source");
            DropForeignKey("dbo.Asset", "SourceId", "dbo.Source");
            DropForeignKey("dbo.AssetUpdateItem", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetUpdate", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetUpdate", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetUpdate", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetUpdate", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetUpdateItem", "AssetUpdateId", "dbo.AssetUpdate");
            DropForeignKey("dbo.Inventory", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.InventoryUser", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.InventoryUser", "InventoryId", "dbo.Inventory");
            DropForeignKey("dbo.InventoryRegion", "RegionId", "dbo.Region");
            DropForeignKey("dbo.InventoryRegion", "InventoryId", "dbo.Inventory");
            DropForeignKey("dbo.InventoryItem", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.InventoryItem", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.InventoryItem", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.InventoryItem", "RegionId", "dbo.Region");
            DropForeignKey("dbo.InventoryItem", "InventoryId", "dbo.Inventory");
            DropForeignKey("dbo.InventoryItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.InventoryDepartment", "InventoryId", "dbo.Inventory");
            DropForeignKey("dbo.InventoryDepartment", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.InventoryAssetType", "InventoryId", "dbo.Inventory");
            DropForeignKey("dbo.InventoryAssetType", "AssetTypeId", "dbo.AssetType");
            DropForeignKey("dbo.AssetUpdateItem", "AssetTypeId", "dbo.AssetType");
            DropForeignKey("dbo.Asset", "AssetTypeId", "dbo.AssetType");
            DropForeignKey("dbo.AssetUpdateItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetUpdateItem", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.Asset", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetLocationItem", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetClearItem", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetBorrow", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetBorrowReturnItem", "StoredAddressId", "dbo.StoredAddress");
            DropForeignKey("dbo.AssetLocationItem", "RegionId", "dbo.Region");
            DropForeignKey("dbo.AssetLocationItem", "AssetItemStatusId", "dbo.AssetItemStatus");
            DropForeignKey("dbo.AssetItemStatus", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetFinancialUpdate", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetFinancialUpdate", "UsedDepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetFinancialUpdate", "UsedCompanyId", "dbo.Department");
            DropForeignKey("dbo.AssetFinancialUpdate", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetFinancialUpdateItem", "AssetFinancialUpdateId", "dbo.AssetFinancialUpdate");
            DropForeignKey("dbo.AssetFinancialUpdateItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetFinancialUpdateItem", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetClear", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetClearItem", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetBorrow", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetBorrowReturn", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AssetBorrowReturnItem", "UsedEmployeeId", "dbo.Employee");
            DropForeignKey("dbo.AdminUser", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.AssetClear", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetClearItem", "AssetClearId", "dbo.AssetClear");
            DropForeignKey("dbo.AssetClearItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.AssetBorrow", "Region_Id1", "dbo.Region");
            DropForeignKey("dbo.AssetBorrow", "Region_Id", "dbo.Region");
            DropForeignKey("dbo.AssetBorrowItem", "AssetBorrowId", "dbo.AssetBorrow");
            DropForeignKey("dbo.AssetBorrow", "SessionId", "dbo.AdminUser");
            DropForeignKey("dbo.AssetBorrowItem", "AssetId", "dbo.Asset");
            DropForeignKey("dbo.Asset", "SessionId", "dbo.AdminUser");
            DropIndex("dbo.UIRow", new[] { "UIId" });
            DropIndex("dbo.AssetLabelItem", new[] { "AssetLabelBaseId" });
            DropIndex("dbo.AssetLabelItem", new[] { "AssetLabelId" });
            DropIndex("dbo.Role", new[] { "NavigationId" });
            DropIndex("dbo.Role", new[] { "GroupId" });
            DropIndex("dbo.AssetStorage", new[] { "SessionId" });
            DropIndex("dbo.AssetOtherInfo", new[] { "AssetId" });
            DropIndex("dbo.AssetOperationRecord", new[] { "SessionId" });
            DropIndex("dbo.AssetOperationRecord", new[] { "AssetId" });
            DropIndex("dbo.AssetUsedItem", new[] { "AssetId" });
            DropIndex("dbo.AssetUsedItem", new[] { "AssetUsedId" });
            DropIndex("dbo.AssetUsed", new[] { "SessionId" });
            DropIndex("dbo.AssetUsed", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetUsed", new[] { "RegionId" });
            DropIndex("dbo.AssetUsed", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetUsed", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetUsed", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetUsedBack", new[] { "SessionId" });
            DropIndex("dbo.AssetUsedBack", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetUsedBack", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetUsedBack", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "RegionId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "AssetId" });
            DropIndex("dbo.AssetUsedBackItem", new[] { "AssetUsedBackId" });
            DropIndex("dbo.AssetUpdate", new[] { "SessionId" });
            DropIndex("dbo.AssetUpdate", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetUpdate", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetUpdate", new[] { "UsedCompanyId" });
            DropIndex("dbo.InventoryUser", new[] { "SessionId" });
            DropIndex("dbo.InventoryUser", new[] { "InventoryId" });
            DropIndex("dbo.InventoryRegion", new[] { "RegionId" });
            DropIndex("dbo.InventoryRegion", new[] { "InventoryId" });
            DropIndex("dbo.InventoryItem", new[] { "SessionId" });
            DropIndex("dbo.InventoryItem", new[] { "StoredAddressId" });
            DropIndex("dbo.InventoryItem", new[] { "RegionId" });
            DropIndex("dbo.InventoryItem", new[] { "UsedEmployeeId" });
            DropIndex("dbo.InventoryItem", new[] { "UsedDepartmentId" });
            DropIndex("dbo.InventoryItem", new[] { "UsedCompanyId" });
            DropIndex("dbo.InventoryItem", new[] { "AssetId" });
            DropIndex("dbo.InventoryItem", new[] { "InventoryId" });
            DropIndex("dbo.InventoryDepartment", new[] { "DepartmentId" });
            DropIndex("dbo.InventoryDepartment", new[] { "InventoryId" });
            DropIndex("dbo.Inventory", new[] { "SessionId" });
            DropIndex("dbo.InventoryAssetType", new[] { "AssetTypeId" });
            DropIndex("dbo.InventoryAssetType", new[] { "InventoryId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "SessionId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "SupplierId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "SourceId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "RegionId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "UnitId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "AssetTypeId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "AssetId" });
            DropIndex("dbo.AssetUpdateItem", new[] { "AssetUpdateId" });
            DropIndex("dbo.StoredAddress", new[] { "DepartmentId" });
            DropIndex("dbo.AssetItemStatus", new[] { "AssetId" });
            DropIndex("dbo.AssetLocationItem", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetLocationItem", new[] { "RegionId" });
            DropIndex("dbo.AssetLocationItem", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetLocationItem", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetLocationItem", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetLocationItem", new[] { "AssetItemStatusId" });
            DropIndex("dbo.AssetFinancialUpdateItem", new[] { "SessionId" });
            DropIndex("dbo.AssetFinancialUpdateItem", new[] { "AssetId" });
            DropIndex("dbo.AssetFinancialUpdateItem", new[] { "AssetFinancialUpdateId" });
            DropIndex("dbo.AssetFinancialUpdate", new[] { "Asset_Id" });
            DropIndex("dbo.AssetFinancialUpdate", new[] { "SessionId" });
            DropIndex("dbo.AssetFinancialUpdate", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetFinancialUpdate", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetFinancialUpdate", new[] { "UsedCompanyId" });
            DropIndex("dbo.Employee", new[] { "WorkingStatusId" });
            DropIndex("dbo.Employee", new[] { "DepartmentId" });
            DropIndex("dbo.Employee", new[] { "CompanyId" });
            DropIndex("dbo.AssetClear", new[] { "SessionId" });
            DropIndex("dbo.AssetClear", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetClear", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetClear", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetClearItem", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetClearItem", new[] { "RegionId" });
            DropIndex("dbo.AssetClearItem", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetClearItem", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetClearItem", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetClearItem", new[] { "AssetId" });
            DropIndex("dbo.AssetClearItem", new[] { "AssetClearId" });
            DropIndex("dbo.AssetBorrow", new[] { "Region_Id1" });
            DropIndex("dbo.AssetBorrow", new[] { "Region_Id" });
            DropIndex("dbo.AssetBorrow", new[] { "SessionId" });
            DropIndex("dbo.AssetBorrow", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetBorrow", new[] { "RegionId" });
            DropIndex("dbo.AssetBorrow", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetBorrow", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetBorrow", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetBorrowItem", new[] { "AssetId" });
            DropIndex("dbo.AssetBorrowItem", new[] { "AssetBorrowId" });
            DropIndex("dbo.Asset", new[] { "SupplierId" });
            DropIndex("dbo.Asset", new[] { "SourceId" });
            DropIndex("dbo.Asset", new[] { "StoredAddressId" });
            DropIndex("dbo.Asset", new[] { "RegionId" });
            DropIndex("dbo.Asset", new[] { "SessionId" });
            DropIndex("dbo.Asset", new[] { "UsedEmployeeId" });
            DropIndex("dbo.Asset", new[] { "UsedDepartmentId" });
            DropIndex("dbo.Asset", new[] { "UsedCompanyId" });
            DropIndex("dbo.Asset", new[] { "UnitId" });
            DropIndex("dbo.Asset", new[] { "AssetTypeId" });
            DropIndex("dbo.Asset", new[] { "AssetStorageId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "StoredAddressId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "RegionId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "UsedCompanyId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "AssetId" });
            DropIndex("dbo.AssetBorrowReturnItem", new[] { "AssetBorrowReturnId" });
            DropIndex("dbo.AssetBorrowReturn", new[] { "SessionId" });
            DropIndex("dbo.AssetBorrowReturn", new[] { "UsedEmployeeId" });
            DropIndex("dbo.AssetBorrowReturn", new[] { "UsedDepartmentId" });
            DropIndex("dbo.AssetBorrowReturn", new[] { "UsedCompanyId" });
            DropIndex("dbo.AdminUser", new[] { "DepartmentId" });
            DropIndex("dbo.AdminUser", new[] { "GroupId" });
            DropTable("dbo.UI");
            DropTable("dbo.UIRow");
            DropTable("dbo.SystemConfig");
            DropTable("dbo.RequestRole");
            DropTable("dbo.OwinToken");
            DropTable("dbo.DbBackupRestore");
            DropTable("dbo.AssetLabel");
            DropTable("dbo.AssetLabelItem");
            DropTable("dbo.AssetLabelBase");
            DropTable("dbo.Navigation");
            DropTable("dbo.Role");
            DropTable("dbo.Group");
            DropTable("dbo.AssetStorage");
            DropTable("dbo.AssetOtherInfo");
            DropTable("dbo.AssetOperationRecord");
            DropTable("dbo.WorkingStatus");
            DropTable("dbo.AssetUsedItem");
            DropTable("dbo.AssetUsed");
            DropTable("dbo.AssetUsedBack");
            DropTable("dbo.AssetUsedBackItem");
            DropTable("dbo.Unit");
            DropTable("dbo.Supplier");
            DropTable("dbo.Source");
            DropTable("dbo.AssetUpdate");
            DropTable("dbo.InventoryUser");
            DropTable("dbo.InventoryRegion");
            DropTable("dbo.InventoryItem");
            DropTable("dbo.InventoryDepartment");
            DropTable("dbo.Inventory");
            DropTable("dbo.InventoryAssetType");
            DropTable("dbo.AssetType");
            DropTable("dbo.AssetUpdateItem");
            DropTable("dbo.StoredAddress");
            DropTable("dbo.AssetItemStatus");
            DropTable("dbo.AssetLocationItem");
            DropTable("dbo.AssetFinancialUpdateItem");
            DropTable("dbo.AssetFinancialUpdate");
            DropTable("dbo.Employee");
            DropTable("dbo.Department");
            DropTable("dbo.AssetClear");
            DropTable("dbo.AssetClearItem");
            DropTable("dbo.Region");
            DropTable("dbo.AssetBorrow");
            DropTable("dbo.AssetBorrowItem");
            DropTable("dbo.Asset");
            DropTable("dbo.AssetBorrowReturnItem");
            DropTable("dbo.AssetBorrowReturn");
            DropTable("dbo.AdminUser");
        }
    }
}
