namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_20200911_alan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdminUser", "CompanyId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdminUser", "CompanyId");
        }
    }
}
