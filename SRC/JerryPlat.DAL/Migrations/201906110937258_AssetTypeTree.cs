namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssetTypeTree : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AssetType", "ParentId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AssetType", "ParentId");
        }
    }
}
