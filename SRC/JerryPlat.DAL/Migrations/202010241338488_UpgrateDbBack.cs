namespace JerryPlat.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpgrateDbBack : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DbBackupRestore", "Name", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DbBackupRestore", "Name");
        }
    }
}
