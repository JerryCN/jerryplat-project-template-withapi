﻿using JerryPlat.Models.Db;
using System.Data.Entity;

namespace JerryPlat.DAL.Context
{
    public class JerryPlatDbContext : DbContext
    {
        public JerryPlatDbContext() : this("JerryPlatDbContext")
        {
        }

        public JerryPlatDbContext(string strName)
            : base("name=" + strName)
        {
#if DEBUG
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
#endif
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Map();
            base.OnModelCreating(modelBuilder);
        }

        #region Organizations

        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<WorkingStatus> WorkingStatuses { get; set; }

        #endregion Organizations

        #region Roles Tables

        public DbSet<AdminUser> AdminUsers { get; set; }
        public DbSet<Navigation> Navigations { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RequestRole> RequestRoles { get; set; }

        #endregion Roles Tables

        #region Owin Tables

        public DbSet<OwinToken> OwinTokens { get; set; }

        #endregion Owin Tables

        #region Db Operation Tables

        public DbSet<DbBackupRestore> DbBackupRestores { get; set; }

        #endregion Db Operation Tables

        #region System Setting Tables

        public DbSet<SystemConfig> SystemConfigs { get; set; }

        #endregion System Setting Tables

        #region Base Settings
        public DbSet<AssetType> AssetTypes { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        #endregion

        #region Assets
        #region AssetStorage
        public DbSet<AssetStorage> AssetStorages { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetItemStatus> AssetItemStatuses { get; set; }
        public DbSet<AssetLocationItem> AssetLocationItems { get; set; }
        public DbSet<AssetOtherInfo> AssetOtherInfos { get; set; }
        #endregion
        #region AssetBorrowReturn
        public DbSet<AssetBorrow> AssetBorrows { get; set; }
        public DbSet<AssetBorrowItem> AssetBorrowItems { get; set; }
        public DbSet<AssetBorrowReturn> AssetBorrowReturns { get; set; }
        public DbSet<AssetBorrowReturnItem> AssetBorrowReturnItems { get; set; }
        #endregion
        #region AssetClear
        public DbSet<AssetClear> AssetClears { get; set; }
        public DbSet<AssetClearItem> AssetClearItems { get; set; }
        #endregion
        #region AssetFinancialUpdate
        public DbSet<AssetFinancialUpdate> AssetFinancialUpdates { get; set; }
        #endregion
        #region AssetOperationRecord
        public DbSet<AssetOperationRecord> AssetOperationRecords { get; set; }
        #endregion
        #region AssetUpdate
        public DbSet<AssetUpdate> AssetUpdates { get; set; }
        public DbSet<AssetUpdateItem> AssetUpdateItems { get; set; }
        #endregion
        #region AssetUsedBack
        public DbSet<AssetUsed> AssetUseds { get; set; }
        public DbSet<AssetUsedItem> AssetUsedItems { get; set; }
        public DbSet<AssetUsedBack> AssetUsedBacks { get; set; }
        public DbSet<AssetUsedBackItem> AssetUsedBackItems { get; set; }
        #endregion
        #region AssetLabel
        public DbSet<AssetLabelBase> AssetLabelBases { get; set; }
        public DbSet<AssetLabel> AssetLabels { get; set; }
        public DbSet<AssetLabelItem> AssetLabelItems { get; set; }
        #endregion
        #endregion

        #region Inventory
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }
        public DbSet<InventoryUser> InventoryUsers { get; set; }
        public DbSet<InventoryDepartment> InventoryDepartments { get; set; }
        public DbSet<InventoryAssetType> InventoryAssetTypes { get; set; }
        public DbSet<InventoryRegion> InventoryRegion { get; set; }
        #endregion

        #region UIs
        public DbSet<UI> UIs { get; set; }
        public DbSet<UIRow> UIRows { get; set; }
        #endregion UIs
    }
}