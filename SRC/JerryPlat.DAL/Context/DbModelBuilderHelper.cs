﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace JerryPlat.DAL.Context
{
    public static class DbModelBuilderHelper
    {
        public static void Map(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}