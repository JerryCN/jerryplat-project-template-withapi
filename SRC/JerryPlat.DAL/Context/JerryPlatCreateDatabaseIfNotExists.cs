﻿using JerryPlat.DAL.Migrations;
using JerryPlat.Models;
using JerryPlat.Models.Db;
using JerryPlat.Utils.Helpers;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace JerryPlat.DAL.Context
{
    public class JerryPlatCreateDatabaseIfNotExists : CreateDatabaseIfNotExists<JerryPlatDbContext>
    {
        protected override void Seed(JerryPlatDbContext context)
        {
#if !DEBUG
            DbSeedHelper.Seed(context);
#endif
        }
    }
}