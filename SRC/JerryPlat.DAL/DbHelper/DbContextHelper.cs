﻿using ESUPER.Code.Common.DbHelper;
using JerryPlat.DAL.Context;
using JerryPlat.DAL.Migrations;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace JerryPlat.DAL
{
    public enum DbContextType
    {
        SqlServer,
        MySql,
        Others
    }
    
    public static class DbContextHelper
    {
        public static void Init()
        {
            using (JerryPlatDbContext context = new JerryPlatDbContext())
            {
                if (context.Database.Exists())
                {
                    Database.SetInitializer(new MigrateDatabaseToLatestVersion<JerryPlatDbContext, Configuration>());
                }
                else
                {
                    Database.SetInitializer(new JerryPlatCreateDatabaseIfNotExists());
                }

                SqlServerHelper.ConnStr = context.Database.Connection.ConnectionString;

                context.Database.Initialize(true);

                SystemConfigModel.Reset(context.SystemConfigs.ToDictionary(o => o.Name, o => o.Config));

                CascadeHelper.Init();
            }
        }

        #region Reset Indentity
        public static DbContextType GetDbContextType(DbContext context)
        {
            Type dbInstanceType = context.Database.Connection.GetType();

            switch (dbInstanceType.Name)
            {
                case "SqlConnection":
                    return DbContextType.SqlServer;
                case "MySqlConnection":
                    return DbContextType.MySql;
                default:
                    return DbContextType.Others;
            }
        }

        public static void DBCC_CHECKIDENT<TEntity>(DbContext context)
        {
            DbContextType dbContextType = GetDbContextType(context);
            switch (dbContextType)
            {
                case DbContextType.SqlServer:
                    context.Database.ExecuteSqlCommand(GetDBCC_CHECKIDENT_Sql<TEntity>(context));
                    break;
                default:
                    break;
            }
        }

        public static async Task DBCC_CHECKIDENTAsync<TEntity>(DbContext context)
        {
            DbContextType dbContextType = GetDbContextType(context);
            switch (dbContextType)
            {
                case DbContextType.SqlServer:
                    await context.Database.ExecuteSqlCommandAsync(GetDBCC_CHECKIDENT_Sql<TEntity>(context));
                    break;
                default:
                    break;
            }
        }

        public static string GetDBCC_CHECKIDENT_Sql<TEntity>(DbContext context)
        {
            string strTableName = typeof(TEntity).Name;
            return GetDBCC_CHECKIDENT_Sql(context, strTableName);
        }
        
        public static string GetDBCC_CHECKIDENT_Sql(DbContext context, string strTableName)
        {
            DbContextType dbContextType = GetDbContextType(context);
            switch (dbContextType)
            {
                case DbContextType.SqlServer:
                    return $@"declare @MaxId int, @Identity int;
                            select @MaxId = isnull(max(ID), 0),@Identity=IDENT_CURRENT('{strTableName}') from {strTableName};
                            if(@MaxId>0 OR @Identity-@MaxId>1)
                            Begin
	                            DBCC CHECKIDENT ({strTableName}, RESEED, @MaxId);
                            End;";
                default:
                    return string.Empty;
            }
        }

        public static string GetDeleteSql<TEntity>(DbContext context, int intSeed = 0)
        {
            string strTableName = typeof(TEntity).Name;
            return GetDeleteSql(context, strTableName, intSeed);
        }

        public static string GetDeleteSql(DbContext context, string strTableName, int intSeed = 0)
        {
            return $"DELETE FROM {strTableName};{GetResetIndentitySql(context, strTableName, intSeed)}";
        }

        public static string GetResetIndentitySql<TEntity>(DbContext context, int intSeed = 0)
        {
            string strTableName = typeof(TEntity).Name;
            return GetResetIndentitySql(context, strTableName, intSeed);
        }

        public static string GetResetIndentitySql(DbContext context, string strTableName, int intSeed = 0)
        {
            DbContextType dbContextType = GetDbContextType(context);
            return GetResetIndentitySql(dbContextType, strTableName, intSeed);
        }

        public static string GetResetIndentitySql(DbContextType dbContextType, string strTableName, int intSeed = 0)
        {
            switch (dbContextType)
            {
                case DbContextType.SqlServer:
                    return $"DBCC CHECKIDENT ({strTableName}, RESEED, {intSeed});";
                case DbContextType.MySql:
                    return $"ALTER TABLE {strTableName} AUTO_INCREMENT = {intSeed};";
                default:
                    return string.Empty;
            }
        }
        #endregion

        #region Attach & Detach
        //http://www.cnblogs.com/scy251147/p/3688844.html
        public static void Detach<TEntity>(DbContext context, TEntity entity)
            where TEntity : class
        {
            var objContext = ((IObjectContextAdapter)context).ObjectContext;
            var objSet = objContext.CreateObjectSet<TEntity>();
            var entityKey = objContext.CreateEntityKey(objSet.EntitySet.Name, entity);

            object foundEntity;
            var exists = objContext.TryGetObjectByKey(entityKey, out foundEntity);

            if (exists)
            {
                objContext.Detach(foundEntity);
            }
        }

        public static void Attach<TEntity>(DbContext context, TEntity entity, DbSet<TEntity> entities = null)
             where TEntity : class, new()
        {
            entities = entities ?? context.Set<TEntity>();

            Detach(context, entity);
            entities.Attach(entity);
        }

        public static void Attach<TEntity>(DbContext context, TEntity entity, EntityState entityState, DbSet<TEntity> entities = null)
               where TEntity : class, new()
        {
            Attach(context, entity, entities);

            DbEntityEntry<TEntity> entry = context.Entry(entity);
            if(entry.State!= entityState)
            {
                entry.State = entityState;
            }
        }
        #endregion

        #region Private Common
        public static List<TSource> GetSourceList<TSource>(DbContext context, List<int> sourceIdList)
            where TSource : class, IEntity, new()
        {
            return context.Set<TSource>().Where(o => sourceIdList.Contains(o.Id)).ToList();
        }
        public static List<TSource> UpdateSourceInfo<TSource, TUpdate>(DbContext context, List<int> sourceIdList, TUpdate update)
           where TSource : class, IEntity, new()
           where TUpdate : class, IEntity, new()
        {
            List<TSource> sourceList = GetSourceList<TSource>(context, sourceIdList);
            foreach (TSource source in sourceList)
            {
                TypeHelper.Update(source, update);
            }
            return sourceList;
        }
        #endregion

    }
}