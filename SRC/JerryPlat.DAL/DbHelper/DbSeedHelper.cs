﻿using JerryPlat.DAL.Context;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.DAL
{
    public static class DbSeedHelper
    {
        public static void Seed(JerryPlatDbContext context)
        {
#if DEBUG
            context.Database.ExecuteSqlCommand($@"{DbContextHelper.GetDeleteSql<UIRow>(context)}
                                                  {DbContextHelper.GetDeleteSql<UI>(context)}
                                                  {DbContextHelper.GetDeleteSql<SystemConfig>(context)}");
            context.SaveChanges();
#endif

            int intId = 1, intSecondParentId = 0, intThirdParentId = 0, intFirstOrderIndex = 1, intSecondOrderIndex = 1, intThirdOrderIndex = 1;
            context.Departments.AddOrUpdate(new Department[] {
                new Department { Id = intId, Code = "001", Name = "单位总账", ParentId = 0, DepartmentType = DepartmentType.Company,Enabled = true, IsLocked = true  }
            });

            intId = 1;
            context.Groups.AddOrUpdate(new Group[] {
                new Group { Id = intId, OrderIndex = intId++, Name = "系统超级管理员", IsLocked = true }
            });

            intId = 1;
            context.AdminUsers.AddOrUpdate(new AdminUser[] {
                new AdminUser { Id = intId++, Email="admin@admin.com", UserName = "超级管理员", Password = EncryptHelper.Encrypt("admin"), IsAllowedManualInventory=true, GroupId = 1,UsedCompanyId=1, DepartmentId=1, IsLocked=true }
            });

            intId = 1;
            context.OwinTokens.AddOrUpdate(new OwinToken[] {
                new OwinToken { Id = intId++, ClientId = "Jerry", ClientSecret = EncryptHelper.Encrypt("Jerry") }
            });

            #region Navigations

            #region 首页

            intId = 1;
            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "首页", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "首页", Icon="el-icon-document", RequestUrl = "/Admin/Home" },
            });

            #endregion 首页

            #region 资产管理

            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "资产管理", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "资产入库", Icon="el-icon-document", RequestUrl = "/Admin/AssetStorage" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetStorage/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus",RequestUrl = "/Api/AssetStorage/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Asset/PreImport$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/AssetStorage/Export$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete",RequestUrl = "/Api/AssetStorage/Delete$" },
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "领用&退库", Icon="el-icon-document", RequestUrl = "/Admin/AssetUsed" },
            });

            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetUsed/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加领用", Code="AddUsed", Icon="el-icon-plus",RequestUrl = "/Api/AssetUsed/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加退库", Code="AddUsedBack", Icon="el-icon-plus", RequestUrl = "/Api/AssetUsedBack/Add$" },
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "借用&归还", Icon="el-icon-document", RequestUrl = "/Admin/AssetBorrow" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetBorrow/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加借用", Code="AddBorrow", Icon="el-icon-plus",RequestUrl = "/Api/AssetBorrow/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加归还", Code="AddBorrowReturn", Icon="el-icon-plus", RequestUrl = "/Api/AssetBorrowReturn/Add$" },
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "实物信息变更", Icon="el-icon-document", RequestUrl = "/Admin/AssetUpdate" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetUpdate/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus",RequestUrl = "/Api/AssetUpdate/Add$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "财务信息变更", Icon="el-icon-document", RequestUrl = "/Admin/AssetFinancialUpdate" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetFinancialUpdate/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus",RequestUrl = "/Api/AssetFinancialUpdate/Add$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "资产处置", Icon="el-icon-document", RequestUrl = "/Admin/AssetClear" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetClear/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus",RequestUrl = "/Api/AssetClear/Add$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "盘点管理", Icon="el-icon-document", RequestUrl = "/Admin/Inventory" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Inventory/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus",RequestUrl = "/Api/Inventory/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit",RequestUrl = "/Api/Inventory/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete",RequestUrl = "/Api/Inventory/Delete$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "盘点记录", Code="ViewInventoryRecord", Icon="el-icon-view",RequestUrl = "/Api/InventoryItem/[1-9]\\d*" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "我要盘点", Code="Inventory", Icon="el-icon-plus",RequestUrl = "/Api/InventoryItem/Add" },
            });
            #endregion 资产管理

            #region 分析报表

            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "分析报表", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "资产清单", Icon="el-icon-document", RequestUrl = "/Admin/AssetReport/Checklist" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/AssetReport/ExportAssetReport$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "资产履历", Icon="el-icon-document", RequestUrl = "/Admin/AssetReport/OperateRecord" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/AssetReport/ExportAssetRecordReport$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "处置清单", Icon="el-icon-document", RequestUrl = "/Admin/AssetReport/ClearRecord" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/AssetReport/ExportAssetClearReport$" }
            });
            #endregion 首页

            #region 基础设置

            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "基础设置", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "资产类别管理", Icon="el-icon-document", RequestUrl = "/Admin/AssetType" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetType/GetPageList$", IsNeedAuthurized = false },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/AssetType/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/AssetType/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/AssetType/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/AssetType/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/AssetType/Export$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "计量单位管理", Icon="el-icon-document", RequestUrl = "/Admin/Unit" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Unit/GetPageList$", IsNeedAuthurized = false },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Unit/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Unit/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/Unit/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/Unit/DeleteList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Unit/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/Unit/Export$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "管理部门管理", Icon="el-icon-document", RequestUrl = "/Admin/Region" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Region/GetPageList$" , IsNeedAuthurized = false},
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Region/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Region/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/Region/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/Region/DeleteList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Region/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/Region/Export$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "来源管理", Icon="el-icon-document", RequestUrl = "/Admin/Source" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Source/GetPageList$" , IsNeedAuthurized = false},
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Source/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Source/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/Source/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/Source/DeleteList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Source/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/Source/Export$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "供应商管理", Icon="el-icon-document", RequestUrl = "/Admin/Supplier" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Supplier/GetPageList$" , IsNeedAuthurized = false},
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Supplier/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Supplier/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/Supplier/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/Supplier/DeleteList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Supplier/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/Supplier/Export$" }
            });
            #endregion 基础设置

            #region 权限管理

            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "权限管理", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "用户管理", Icon="el-icon-document", RequestUrl = "/Admin/AdminUser" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AdminUser/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/AdminUser/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/AdminUser/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/AdminUser/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/AdminUser/DeleteList$" },
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "角色管理", Icon="el-icon-document", RequestUrl = "/Admin/Group" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Group/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Group/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Group/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/Group/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/Group/DeleteList$" },
            });
            #endregion 权限管理

            #region 组织管理
            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "组织管理", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "组织结构", Icon="el-icon-document", RequestUrl = "/Admin/Department" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Department/GetTreeList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加下级组织", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Department/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Department/ForceEdit" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/Department/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Department/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/Department/Export$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "禁用", Code="Enabled", Icon="el-icon-close", RequestUrl = "/Api/Department/Enabled/[1-9]\\d*$" },
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "存储位置", Icon="el-icon-document", RequestUrl = "/Admin/StoredAddress" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/StoredAddress/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/StoredAddress/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/StoredAddress/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="SoftDelete", Icon="el-icon-delete", RequestUrl = "/Api/StoredAddress/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="SoftDeleteList", Icon="el-icon-delete", RequestUrl = "/Api/StoredAddress/DeleteList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/StoredAddress/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/StoredAddress/Export$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "禁用", Code="Enabled", Icon="el-icon-close", RequestUrl = "/Api/StoredAddress/Enabled/[1-9]\\d*$" },
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "员工管理", Icon="el-icon-document", RequestUrl = "/Admin/Employee" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Employee/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/Employee/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/Employee/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="SoftDelete", Icon="el-icon-delete", RequestUrl = "/Api/Employee/SoftDelete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "恢复", Code="Recover", Icon="el-icon-refresh", RequestUrl = "/Api/Employee/Recover/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/Employee/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/Employee/Export$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "在职状态", Icon="el-icon-document", RequestUrl = "/Admin/WorkingStatus" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/WorkingStatus/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/WorkingStatus/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/WorkingStatus/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/WorkingStatus/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/WorkingStatus/DeleteList$" }
            });
            #endregion 组织管理

            #region 标签管理
            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "标签管理", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "基础标签", Icon="el-icon-document", RequestUrl = "/Admin/AssetLabelBase" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetLabelBase/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/AssetLabelBase/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/AssetLabelBase/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/AssetLabelBase/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/AssetLabelBase/DeleteList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导入", Code="Import", Icon="el-icon-upload2", RequestUrl = "/Api/AssetLabelBase/Import$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "导出", Code="Export", Icon="el-icon-download", RequestUrl = "/Api/AssetLabelBase/Export$" }
            });

            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "标签模板", Icon="el-icon-document", RequestUrl = "/Admin/AssetLabel" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/AssetLabel/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "添加", Code="Add", Icon="el-icon-plus", RequestUrl = "/Api/AssetLabel/Add$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "编辑", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/AssetLabel/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "删除", Code="Delete", Icon="el-icon-delete", RequestUrl = "/Api/AssetLabel/Delete/[1-9]\\d*$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "批量删除", Code="DeleteList", Icon="el-icon-delete", RequestUrl = "/Api/AssetLabel/DeleteList$" }
            });
            #endregion

            #region 系统设置

            //First
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = 0, OrderIndex = intFirstOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "系统设置", Icon="el-icon-menu", RequestUrl = ""}
            });

            intSecondParentId = intId - 1; intSecondOrderIndex = 1;
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "系统设置", Icon="el-icon-document", RequestUrl = "/Admin/SystemConfig" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Admin/SystemConfig$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "保存", Code="Edit", Icon="el-icon-edit", RequestUrl = "/Api/SystemConfig/Edit$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "清除数据", Code="Clear", Icon="el-icon-delete", RequestUrl = "/Api/System/ClearData$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "重启站点", Code="Restart", Icon="el-icon-refresh", RequestUrl = "/Api/System/Restart$" },
            });
            //Second
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intSecondParentId, OrderIndex = intSecondOrderIndex++, SiteType = SiteType.Admin, NavigationType = NavigationType.Page,
                    Name = "数据库管理", Icon="el-icon-document", RequestUrl = "/Admin/Db" },
            });
            //Third
            intThirdParentId = intId - 1; intThirdOrderIndex = 1;
            context.Navigations.AddOrUpdate(new Navigation[] {
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "查看", Code="View", Icon="el-icon-view", RequestUrl = "/Api/Db/GetPageList$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "备份数据库", Code="Backup", Icon="el-icon-plus", RequestUrl = "/Api/Db/Backup$" },
                new Navigation { Id = intId++, ParentId = intThirdParentId, OrderIndex = intThirdOrderIndex++, SiteType = SiteType.Admin, NavigationType=NavigationType.Button,
                    Name = "还原数据库", Code="Restore", Icon="el-icon-back", RequestUrl = "/Api/Db/Restore$" }
            });

            #endregion 系统设置

            #endregion Navigations

            #region BaseSettings
            #region Unit
            intId = 1;
            context.Units.AddOrUpdate(new Unit[] {
                new Unit {Id = intId, Name = "台", OrderIndex = intId++, IsLocked = true },
                new Unit {Id = intId, Name = "个", OrderIndex = intId++, IsLocked = true },
                new Unit {Id = intId, Name = "本", OrderIndex = intId++, IsLocked = true }
            });
            #endregion Unit
            //#region Region
            //intId = 1;
            //context.Regions.AddOrUpdate(new Region[] {
            //    new Region {Id = intId, Name = "本地", OrderIndex = intId++, IsLocked = true },
            //    new Region {Id = intId, Name = "文体馆", OrderIndex = intId++, IsLocked = true }
            //});
            //#endregion Region
            //#region Supplier
            //intId = 1;
            //context.Suppliers.AddOrUpdate(new Supplier[] {
            //    new Supplier {Id = intId, Name = "黑蚂蚁", OrderIndex = intId++, IsLocked = true },
            //    new Supplier {Id = intId, Name = "联想", OrderIndex = intId++, IsLocked = true }
            //});
            //#endregion Supplier
            //#region Source
            //intId = 1;
            //context.Sources.AddOrUpdate(new Source[] {
            //    new Source {Id = intId, Name = "购入", OrderIndex = intId++, IsLocked = true }
            //});
            //#endregion Source
            #endregion BaseSettings

            #region Organizations
            intId = 1;
            context.WorkingStatuses.AddOrUpdate(new WorkingStatus[]
            {
               new WorkingStatus { Id = intId, Name="在职", OrderIndex = intId++, IsLocked = true },
               new WorkingStatus { Id = intId, Name="离职", OrderIndex = intId++, IsLocked = true },
               new WorkingStatus { Id = intId, Name="休假", OrderIndex = intId++, IsLocked = true }
            });
            #endregion Organizations

            #region ActionRole

            intId = 1;
            context.RequestRoles.AddOrUpdate(new RequestRole[] {
                //new RequestRole { Id = intId++, Action="/GetNewId$" },
                //new RequestRole { Id = intId++, Action="/GetPageList$" },
                //new RequestRole { Id = intId++, Action="/GetList$" },
                //new RequestRole { Id = intId++, Action="/GetDetail/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/Add$" },
                new RequestRole { Id = intId++, Action="/Edit$" },
                new RequestRole { Id = intId++, Action="/ForceEdit$" },
                new RequestRole { Id = intId++, Action="/Delete/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/DeleteList$" },
                new RequestRole { Id = intId++, Action="/Enabled/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/SoftDelete/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/SoftDeleteList/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/Recover/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/RecoverList$" },
                new RequestRole { Id = intId++, Action="/Import$" },
                new RequestRole { Id = intId++, Action="/Lock/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/UnLock/[1-9]\\d*$" },
                new RequestRole { Id = intId++, Action="/Export" },
                new RequestRole { Id = intId++, Action="/ClearData" },
            });

            #endregion ActionRole

            #region SystemConfig
            intId = 1;
            context.SystemConfigs.AddOrUpdate(new SystemConfig[] {
                #region Code Roles
                new SystemConfig {Id=intId++, Name="Department_Code_Role", Config="{Profix}{Order:3}" },
                new SystemConfig {Id=intId++, Name="Employee_Code_Role", Config="{Date:yyyy}{Order:5}" },
                new SystemConfig {Id=intId++, Name="AssetStorage_Code_Role", Config="RK{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="Asset_Code_Role", Config="ZC{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetUsed_Code_Role", Config="LY{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetUsedBack_Code_Role", Config="TK{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetBorrow_Code_Role", Config="JY{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetBorrowReturn_Code_Role", Config="GH{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetClear_Code_Role", Config="QL{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetUpdate_Code_Role", Config="GX{Date:yyyyMMdd}{Order:3}" },
                new SystemConfig {Id=intId++, Name="AssetFinancialUpdate_Code_Role", Config="CW{Date:yyyyMMdd}{Order:3}" },
                #endregion

                new SystemConfig {Id=intId++, Name="AllowOriginSites", Config="" },

                new SystemConfig {Id=intId++, Name="MasterDbConnStr", Config="server=.;database=master;uid=sa;pwd=websoft9!;Persist Security Info=True;Application Name=EntityFramework;" },
                new SystemConfig {Id=intId++, Name="DbName", Config = context.Database.Connection.Database },
                new SystemConfig {Id=intId++, Name="BackUpDbName", Config= $"{context.Database.Connection.Database}_DbBackup" }
            });
            #endregion

            #region UIs
            intId = 1;
            int intOrderIndex = 0, intChildId = 1;
            #region Cores
            #region PasswordDto
            intOrderIndex = 0;
            string strName = typeof(PasswordDto).Name;
            context.UIs.Add(new UI(UIType.Edit)
            {
                Id = intId++,
                Name = strName,
                TableName = "User",
                PageName = Title.PasswordDto.PageName,
                AfterSubmitCallback = "",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Original",Title=Title.PasswordDto.Original,Rules=Template.Rule.Required,Template=Template.Form.Password,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Password",Title=Title.PasswordDto.Password,Rules=Template.Rule.Required,Template=Template.Form.Password,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Confirm",Title=Title.PasswordDto.Confirm,Rules=Template.Rule.ConfirmPassword,Template=Template.Form.Password,OrderIndex=intOrderIndex++ },
                }
            });
            #endregion

            #region BaseSettings
            #region AssetType
            intOrderIndex = 0;
            strName = typeof(AssetType).Name;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/AssetTypeTemplate.xlsx",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Id",Title=Title.Id, Description=Title.Discription_Id,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="ParentId",Title=Title.Department.ParentId, Description=Title.Department.Description_ParentId,OrderIndex=intOrderIndex++ }
                }
            });
            #endregion
            #region BaseSetting
            intOrderIndex = 0;
            strName = typeof(BaseSettingDto).Name;
            context.UIs.Add(new UI(UIType.Edit)
            {
                Id = intId++,
                Name = strName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.BaseSetting.Name,Rules=Template.Rule.Required,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="OrderIndex",Title=Title.OrderIndex,Rules=Template.Rule.Required,Template=Template.Form.InputNumber,OrderIndex=intOrderIndex++ },
                }
            });
            intOrderIndex = 0;
            context.UIs.Add(new UI(UIType.Table)
            {
                Id = intId++,
                Name = strName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Id",Title=Title.Id,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.BaseSetting.Name,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="OrderIndex",Title=Title.OrderIndex,OrderIndex=intOrderIndex++ },
                }
            });

            intOrderIndex = 0;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/BaseSettingTemplate.xlsx"
            });
            #endregion

            #endregion

            #region Roles

            #region AdminUserDto
            intOrderIndex = 0;
            strName = typeof(AdminUserDto).Name;
            context.UIs.Add(new UI(UIType.Table)
            {
                Id = intId++,
                Name = strName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="UserName",Title=Title.AdminUser.UserName,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Email",Title=Title.AdminUser.Email,Template=Template.Table.Email,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="IsAllowedManualInventory",Title=Title.AdminUser.IsAllowedManualInventory,Template="{{scope.row.IsAllowedManualInventory?\"" + Title.Enabled.True + "\":\"" + Title.Enabled.False + "\"}}",OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="GroupName",Title=Title.AdminUser.GroupName,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentName",Title=Title.AdminUser.DepartmentName,OrderIndex=intOrderIndex++ }
                }
            });
            intOrderIndex = 0;
            context.UIs.Add(new UI(UIType.Edit)
            {
                Id = intId++,
                Name = strName,
                PageName = Title.AdminUser.PageName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Email",Title=Title.AdminUser.Email,Rules=Template.Rule.Email,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="UserName",Title=Title.AdminUser.UserName,Rules=Template.Rule.Required,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Password",Title=Title.AdminUser.Password,Rules=Template.Rule.Required,Template=Template.Form.Password,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="IsAllowedManualInventory",Title=Title.AdminUser.IsAllowedManualInventory,Rules=Template.Rule.Required,Template=Template.Form.Switch,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="GroupId",Title=Title.AdminUser.GroupName,Rules=Template.Rule.Select,Template=Template.Form.Select,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentId",Title=Title.AdminUser.DepartmentName,Rules=Template.Rule.Select,Template=Template.Form.Select,Description="<span :class=\"getDepartmentTypeClass(item)\" v-html=\"getDepartmentName(item)\"></span>",OrderIndex=intOrderIndex++ }
                }
            });
            #endregion

            #region GroupDto
            intOrderIndex = 0;
            strName = typeof(GroupDto).Name;
            context.UIs.Add(new UI(UIType.Table)
            {
                Id = intId++,
                Name = strName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.Group.Name,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="OrderIndex",Title=Title.OrderIndex,OrderIndex=intOrderIndex++ }
                }
            });
            intOrderIndex = 0;
            context.UIs.Add(new UI(UIType.Edit)
            {
                Id = intId++,
                Name = strName,
                PageName = Title.Group.PageName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.Group.Name,Rules=Template.Rule.Required,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="OrderIndex",Title=Title.OrderIndex,Rules=Template.Rule.Required,Template=Template.Form.InputNumber,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="NavigationIdList",Title=Title.Group.NavigationIdList,Rules=Template.Rule.Required,Template=Template.Form.Tree,OrderIndex=intOrderIndex++ }
                }
            });
            #endregion

            #endregion

            #region Organizations
            #region DepartmentDto
            intOrderIndex = 0;
            strName = typeof(Department).Name;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/DepartmentTemplate.xlsx",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Id",Title=Title.Id, Description=Title.Discription_Id,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="ParentId",Title=Title.Department.ParentId, Description=Title.Department.Description_ParentId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentType",Title=Title.Department.DepartmentType, Description=Title.Department.Description_DepartmentType,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Enabled",Title=Title.Enabled.Text, Description=Title.Enabled.Description,OrderIndex=intOrderIndex++ }
                }
            });

            intOrderIndex = 0;
            strName = typeof(DepartmentDto).Name;
            context.UIs.Add(new UI(UIType.Edit)
            {
                Id = intId++,
                Name = strName,
                PageName = Title.Department.PageName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Code",Title=Title.Department.Code,Rules=Template.Rule.Required,Template=Template.Form.InputWithButton,Event="@click=\"getCode('/Department/GetCode',{{Table}}.{{DialogName}}.Model)\" v-if='{{Table}}.{{DialogName}}.Model.Id == 0'", Disabled="{{Table}}.{{DialogName}}.Model.Id > 0",OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.Department.Name,Rules=Template.Rule.Required,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="ParentName",Title=Title.Department.ParentName,Rules=Template.Rule.Required,Template=Template.Form.Input,Disabled=Template.Disabled.True,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentType",Title=Title.Department.DepartmentType,Rules=Template.Rule.Select,Template=Template.Form.RadioGroup,Disabled="{{Table}}.ModelDialog.Model.ParentId==0",OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Enabled",Title=Title.Enabled.Text,Rules=Template.Rule.Required,Template=Template.Form.Switch,Disabled=Template.Disabled.True,OrderIndex=intOrderIndex++ }
                }
            });
            #endregion

            #region StoredAddressDto
            intOrderIndex = 0;
            strName = typeof(StoredAddress).Name;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/StoredAddressTemplate.xlsx",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="DepartmentId",Title=Title.StoredAddress.DepartmentId, Description=Title.StoredAddress.Description_DepartmentId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Enabled",Title=Title.Enabled.Text, Description=Title.Enabled.Description,OrderIndex=intOrderIndex++ }
                }
            });
            #endregion

            #region EmployeeDto
            intOrderIndex = 0;
            strName = typeof(EmployeeDto).Name;
            context.UIs.Add(new UI(UIType.Table)
            {
                Id = intId++,
                Name = strName,
                Action = "Edit,SoftDelete,Recover",
                SelectionIf = "existButton('SoftDeleteList')",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Code",Title=Title.Employee.Code,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.Employee.Name,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="CompanyName",Title=Title.Employee.CompanyName,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentName",Title=Title.Employee.DepartmentName,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Phone",Title=Title.Employee.Phone,Template=Template.Table.Phone,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Email",Title=Title.Employee.Email,Template=Template.Table.Email,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="WorkingStatus",Title=Title.Employee.WorkingStatus,OrderIndex=intOrderIndex++ }
                }
            });
            intOrderIndex = 0;
            context.UIs.Add(new UI(UIType.Edit)
            {
                Id = intId++,
                Name = strName,
                PageName = Title.Employee.PageName,
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="Code",Title=Title.Employee.Code,Rules=Template.Rule.Required,Template=Template.Form.InputWithButton,Event="@click=\"getCode('/Employee/GetCode',{{Table}}.{{DialogName}}.Model)\" v-if='{{Table}}.{{DialogName}}.Model.Id == 0'", Disabled="{{Table}}.{{DialogName}}.Model.Id > 0",OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Name",Title=Title.Employee.Name,Rules=Template.Rule.Required,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="CompanyName",Title=Title.Employee.CompanyName,Rules=Template.Rule.Required,Template=Template.Form.Input,Disabled=Template.Disabled.True,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentId",Title=Title.Employee.DepartmentName,Rules=Template.Rule.Select,Template=Template.Form.Select,Event="@change=\"handleDepartmentEditChange\"",Description="<span :class=\"getDepartmentTypeClass(item)\" v-html=\"getDepartmentName(item)\"></span>", OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Phone",Title=Title.Employee.Phone,Rules=Template.Rule.Phone,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="Email",Title=Title.Employee.Email,Rules=Template.Rule.Email,Template=Template.Form.Input,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="WorkingStatusId",Title=Title.Employee.WorkingStatus,Rules=Template.Rule.Select,Template=Template.Form.Select,OrderIndex=intOrderIndex++ }
                }
            });

            intOrderIndex = 0;
            strName = typeof(Employee).Name;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/EmployeeTemplate.xlsx",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="CompanyId",Title=Title.Employee.CompanyId, Description=Title.Employee.Description_CompanyId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="DepartmentId",Title=Title.Employee.DepartmentId, Description=Title.Employee.Description_DepartmentId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="WorkingStatusId",Title=Title.Employee.WorkingStatusId, Description=Title.Employee.Description_WorkingStatusId,OrderIndex=intOrderIndex++ }
                }
            });
            #endregion
            #endregion

            #region Assets
            intOrderIndex = 0;
            strName = typeof(Asset).Name;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/AssetTemplate.xlsx",
                UIRows = new UIRow[] {
                    new UIRow {Id= intChildId++,FieldName="AssetTypeId",Title=Title.Asset.AssetType, Description=Title.Asset.Description_AssetTypeId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="UnitId",Title=Title.Asset.UnitId, Description=Title.Asset.Description_UnitId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="UsedCompanyId",Title=Title.Asset.UsedCompanyId, Description=Title.Asset.Description_UsedCompanyId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="UsedDepartmentId",Title=Title.Asset.UsedDepartmentId, Description=Title.Asset.Description_UsedDepartmentId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="UsedEmployeeId",Title=Title.Asset.UsedCompanyId, Description=Title.Asset.Description_UsedEmployeeId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="SessionId",Title=Title.Asset.AssetType, Description=Title.Asset.Description_SessionId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="RegionId",Title=Title.Asset.UnitId, Description=Title.Asset.Description_RegionId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="SourceId",Title=Title.Asset.UsedCompanyId, Description=Title.Asset.Description_SourceId,OrderIndex=intOrderIndex++ },
                    new UIRow {Id= intChildId++,FieldName="SupplierId",Title=Title.Asset.UsedCompanyId, Description=Title.Asset.Description_SupplierId,OrderIndex=intOrderIndex++ }
                }
            });

            #endregion

            #region AssetLabelBaseDto
            intOrderIndex = 0;
            strName = typeof(AssetLabelBase).Name;
            context.UIs.Add(new UI(UIType.Import)
            {
                Id = intId++,
                Name = strName,
                Template = "/File/Template/AssetLabelBaseTemplate.xlsx"
            });
            #endregion
            #endregion

            #endregion
        }
    }
}
