﻿using AutoMapper;
using JerryPlat.DAL.Context;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.DAL
{
    public enum CascadeType
    {
        Add,
        Edit,
        Delete
    }

    public static class CascadeHelper
    {
        public static void Init()
        {
            #region Cascade Delete
            #region Roles
            Add<AdminUser>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AdminUser entity = sender as AdminUser;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetStorages);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrows);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowReturns);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUseds);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBacks);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClears);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdates);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetFinancialUpdates);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetOperationRecords);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Inventories);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryUsers);
                });
            Add<Group>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Group entity = sender as Group;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Roles);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AdminUsers);
                });
            #endregion

            #region Base Settings
            Add<AssetType>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetType entity = sender as AssetType;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryAssetTypes);
                });
            Add<Region>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Region entity = sender as Region;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUseds);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBackItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrows);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClearItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetLocationItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryRegions);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryItems);
                });
            Add<Source>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Source entity = sender as Source;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                });
            Add<Supplier>(CascadeType.Delete,
              (sender, context, sessionUsedCompanyId, sessionId) =>
              {
                  Supplier entity = sender as Supplier;
                  CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                  CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
              });
            Add<Unit>(CascadeType.Delete,
              (sender, context, sessionUsedCompanyId, sessionId) =>
              {
                  Unit entity = sender as Unit;
                  CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                  CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
              });
            #endregion

            #region Inventory
            Add<Inventory>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Inventory entity = sender as Inventory;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryUsers);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryDepartments);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryAssetTypes);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryRegions);
                });
            #endregion

            #region Organizations
            Add<WorkingStatus>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    WorkingStatus entity = sender as WorkingStatus;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Employees);
                });

            Add<Employee>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Employee entity = sender as Employee;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrows);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowReturns);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowReturnItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUseds);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBacks);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBackItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdates);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetFinancialUpdates);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClears);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClearItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetLocationItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryItems);
                });

            Add<Department>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Department entity = sender as Department;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AdminUsers);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryDepartments);
                    switch (entity.DepartmentType)
                    {
                        case DepartmentType.Company:
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyEmployees);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssets);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetUseds);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetUsedBacks);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetUsedBackItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetBorrows);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetBorrowReturns);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetBorrowReturnItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetUpdates);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetUpdateItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetFinancialUpdates);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetClears);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetClearItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedAssetLocationItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.CompanyUsedInventoryItems);
                            break;
                        case DepartmentType.Department:
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.StoredAddresses);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentEmployees);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssets);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetUseds);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetUsedBacks);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetUsedBackItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetBorrows);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetBorrowReturns);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetBorrowReturnItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetUpdates);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetUpdateItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetFinancialUpdates);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetClears);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetClearItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedAssetLocationItems);
                            CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.DepartmentUsedInventoryItems);
                            break;
                    }
                });
            Add<StoredAddress>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    StoredAddress entity = sender as StoredAddress;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetLocationItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUseds);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBackItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrows);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClearItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryItems);
                });
            #endregion

            #region UI
            Add<UI>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    UI entity = sender as UI;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.UIRows);
                });
            #endregion

            #region Asset
            Add<AssetStorage>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetStorage entity = sender as AssetStorage;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.Assets);
                });
            Add<Asset>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    Asset entity = sender as Asset;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetItemStatuses);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetOperationRecords);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetOtherInfos);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowReturnItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBackItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClearItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetFinancialUpdates);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetFinancialUpdateItems);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetOperationRecords);
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.InventoryItems);
                });

            Add<AssetItemStatus>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetItemStatus entity = sender as AssetItemStatus;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetLocationItems);
                });

            Add<AssetBorrow>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetBorrow entity = sender as AssetBorrow;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowItems);
                });

            Add<AssetBorrowReturn>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetBorrowReturn entity = sender as AssetBorrowReturn;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetBorrowReturnItems);
                });

            Add<AssetUsed>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetUsed entity = sender as AssetUsed;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedItems);
                });

            Add<AssetUsedBack>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetUsedBack entity = sender as AssetUsedBack;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUsedBackItems);
                });

            Add<AssetUpdate>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetUpdate entity = sender as AssetUpdate;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetUpdateItems);
                });
            Add<AssetFinancialUpdate>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetFinancialUpdate entity = sender as AssetFinancialUpdate;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetFinancialUpdateItems);
                });
            Add<AssetClear>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetClear entity = sender as AssetClear;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetClearItems);
                });
            Add<AssetLabelBase>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetLabelBase entity = sender as AssetLabelBase;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetLabelItems);
                });
            Add<AssetLabel>(CascadeType.Delete,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetLabel entity = sender as AssetLabel;
                    CascadeDelete(context, sessionUsedCompanyId, sessionId, entity.AssetLabelItems);
                });
            #endregion
            #endregion

            #region Cascade Add
            Add<AssetStorage>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetStorage assetStorage = sender as AssetStorage;


                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetStorage, assetStorage.Assets, asset =>
                        {
                            asset.AssetItemStatuses = asset.AssetItemStatuses ?? new List<AssetItemStatus>();
                            asset.AssetItemStatuses.Add(new AssetItemStatus
                            {
                                UsedCompanyId= sessionUsedCompanyId,
                                Total = asset.Total,
                                AssetLocationItems = new List<AssetLocationItem>
                                {
                                Mapper.Map<AssetLocationItem>(asset)
                                }
                            });
                            AddAssetOperationRecord(asset, new AssetOperationRecordDto
                            {
                                Code = assetStorage.Code,
                                OperationType = OperationType.Create,
                                Description = $"资产入库{asset.Total}{GetForeignTableField(context, "Unit", asset.UnitId)}到{GetLocation(context, asset)}",
                                SessionId = sessionId
                            });
                        });
                });

            Add<AssetBorrow>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetBorrow assetBorrow = sender as AssetBorrow;
                    List<Asset> assetList = DbContextHelper.GetSourceList<Asset>(context, assetBorrow.AssetBorrowItems.Select(o => o.AssetId).ToList());
                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetBorrow, assetBorrow.AssetBorrowItems, assetBorrowItem =>
                    {
                        UpdateAssetStatus(context, sessionUsedCompanyId, assetBorrow, assetList, assetBorrowItem.AssetId, AssetStatus.Borrow, assetBorrowItem.Total,
                            asset => new AssetOperationRecordDto
                            {
                                Code = assetBorrow.Code,
                                OperationType = OperationType.Borrow,
                                Description = $"资产借用{assetBorrowItem.Total}{asset.Unit.Name}到{GetLocation(context, assetBorrow)}",
                                SessionId = sessionId
                            });
                    });
                });

            Add<AssetBorrowReturn>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetBorrowReturn assetBorrowReturn = sender as AssetBorrowReturn;
                    List<Asset> assetList = DbContextHelper.GetSourceList<Asset>(context, assetBorrowReturn.AssetBorrowReturnItems.Select(o => o.AssetId).ToList());
                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetBorrowReturn, assetBorrowReturn.AssetBorrowReturnItems, assetBorrowReturnItem =>
                    {
                        UpdateAssetStatus(context, sessionUsedCompanyId, assetBorrowReturnItem, assetList, assetBorrowReturnItem.AssetId, AssetStatus.Borrow, -assetBorrowReturnItem.Total,
                              asset => new AssetOperationRecordDto
                              {
                                  Code = assetBorrowReturn.Code,
                                  OperationType = OperationType.BorrowReturn,
                                  Description = $"资产归还{GetLocation(context, assetBorrowReturnItem)}{assetBorrowReturnItem.Total}{asset.Unit.Name}",
                                  SessionId = sessionId,
                              });
                    });
                });

            Add<AssetUsed>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetUsed assetUsed = sender as AssetUsed;
                    List<Asset> assetList = DbContextHelper.GetSourceList<Asset>(context, assetUsed.AssetUsedItems.Select(o => o.AssetId).ToList());
                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetUsed, assetUsed.AssetUsedItems, assetUsedItem =>
                    {
                        UpdateAssetStatus(context, sessionUsedCompanyId, assetUsed, assetList, assetUsedItem.AssetId, AssetStatus.Used, assetUsedItem.Total,
                               asset => new AssetOperationRecordDto
                               {
                                   Code = assetUsed.Code,
                                   OperationType = OperationType.Used,
                                   Description = $"资产使用{assetUsedItem.Total}{asset.Unit.Name}到{GetLocation(context, assetUsed)}",
                                   SessionId = sessionId,
                               });
                    });
                });

            Add<AssetUsedBack>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetUsedBack assetUsedback = sender as AssetUsedBack;
                    List<Asset> assetList = DbContextHelper.GetSourceList<Asset>(context, assetUsedback.AssetUsedBackItems.Select(o => o.AssetId).ToList());
                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetUsedback, assetUsedback.AssetUsedBackItems, assetUsedBackItem =>
                    {
                        UpdateAssetStatus(context, sessionUsedCompanyId, assetUsedBackItem, assetList, assetUsedBackItem.AssetId, AssetStatus.Used, -assetUsedBackItem.Total,
                                asset => new AssetOperationRecordDto
                                {
                                    Code = assetUsedback.Code,
                                    OperationType = OperationType.UsedBack,
                                    Description = $"资产退库{GetLocation(context, assetUsedBackItem)}{assetUsedBackItem.Total}{asset.Unit.Name}",
                                    SessionId = sessionId,
                                });
                    });
                });

            Add<AssetUpdate>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetUpdate assetUpdate = sender as AssetUpdate;
                    DbSet<Asset> dbSetAsset = context.Set<Asset>();
                    List<int> assetIdList = assetUpdate.AssetUpdateItems.Select(o => o.AssetId).ToList();
                    List<Asset> assetList = dbSetAsset.Where(o => assetIdList.Contains(o.Id)).ToList();
                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetUpdate, assetUpdate.AssetUpdateItems, assetUpdateItem =>
                    {
                        string strUpdateRecord = UpdateAssetInfo(context, assetList, assetUpdateItem,
                            new AssetOperationRecordDto
                            {
                                Code = assetUpdate.Code,
                                OperationType = OperationType.Update,
                                Description = $"资产信息变更：",
                                SessionId = sessionId
                            });
                        assetUpdateItem.UpdateRecord = strUpdateRecord;
                    });
                });


            Add<AssetFinancialUpdate>(CascadeType.Add,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetFinancialUpdate assetFinancialUpdate = sender as AssetFinancialUpdate;
                    DbSet<Asset> dbSetAsset = context.Set<Asset>();
                    List<int> assetIdList = assetFinancialUpdate.AssetFinancialUpdateItems.Select(o => o.AssetId).ToList();
                    List<Asset> assetList = dbSetAsset.Where(o => assetIdList.Contains(o.Id)).ToList();
                    CascadeAdd(context, sessionUsedCompanyId, sessionId, assetFinancialUpdate, assetFinancialUpdate.AssetFinancialUpdateItems, assetFinancialUpdateItem =>
                    {
                        string strUpdateRecord = UpdateAssetInfo(context, assetList, assetFinancialUpdateItem,
                            new AssetOperationRecordDto
                            {
                                Code = assetFinancialUpdate.Code,
                                OperationType = OperationType.FinancialUpdate,
                                Description = $"资产财务信息变更：",
                                SessionId = sessionId
                            });
                        assetFinancialUpdateItem.UpdateRecord = strUpdateRecord;
                    });
                });

            Add<AssetClear>(CascadeType.Add,
                  (sender, context, sessionUsedCompanyId, sessionId) =>
                  {
                      AssetClear assetClear = sender as AssetClear;
                      List<Asset> assetList = DbContextHelper.GetSourceList<Asset>(context, assetClear.AssetClearItems.Select(o => o.AssetId).ToList());
                      CascadeAdd(context, sessionUsedCompanyId, sessionId, assetClear, assetClear.AssetClearItems, assetClearItem =>
                      {
                          UpdateAssetStatus(context, sessionUsedCompanyId,assetClearItem, assetList, assetClearItem.AssetId, AssetStatus.Clear, assetClearItem.Total,
                              asset => new AssetOperationRecordDto
                              {
                                  Code = assetClear.Code,
                                  OperationType = OperationType.Clear,
                                  Description = $"资产清理{GetLocation(context, assetClearItem)}{assetClearItem.Total}{asset.Unit.Name}",
                                  SessionId = sessionId
                              });
                      });
                  });
            #endregion

            #region Cascade Edit
            Add<AssetStorage>(CascadeType.Edit,
                (sender, context, sessionUsedCompanyId, sessionId) =>
                {
                    AssetStorage assetStorage = sender as AssetStorage;
                    CascadeEdit(context, sessionId, assetStorage, assetStorage.Assets, asset =>
                    {
                        if (asset.Id == 0)//Add
                        {
                            asset.AssetItemStatuses = asset.AssetItemStatuses ?? new List<AssetItemStatus>();
                            asset.AssetItemStatuses.Add(new AssetItemStatus
                            {
                                UsedCompanyId= sessionUsedCompanyId,
                                Total = asset.Total,
                                AssetLocationItems = new List<AssetLocationItem>
                                {
                                    Mapper.Map<AssetLocationItem>(asset)
                                }
                            });
                        }
                        else
                        {
                            CascadeEdit(context, sessionId, asset, asset.AssetItemStatuses, assetItemStatus =>
                            {
                                assetItemStatus.Asset = null;
                                CascadeEdit(context, sessionId, assetItemStatus, assetItemStatus.AssetLocationItems);
                            });
                            CascadeEdit(context, sessionId, asset, asset.AssetOtherInfos);
                        }
                    });
                });

            Add<Asset>(CascadeType.Edit,
                (sender, context, SessionUsedCompanyId, sessionId) =>
                {
                    Asset asset = sender as Asset;
                    CascadeEdit(context, sessionId, asset, asset.AssetItemStatuses, assetItemStatus =>
                    {
                        assetItemStatus.Asset = null;
                    });
                    CascadeEdit(context, sessionId, asset, asset.AssetOtherInfos);
                });

            Add<AssetLabel>(CascadeType.Edit,
                (sender, context, SessionUsedCompanyId, sessionId) =>
                {
                    AssetLabel entity = sender as AssetLabel;
                    CascadeEdit(context, sessionId, entity, entity.AssetLabelItems);
                });

            Add<Inventory>(CascadeType.Edit,
                (sender, context, SessionUsedCompanyId, sessionId) =>
                {
                    Inventory entity = sender as Inventory;
                    CascadeEdit(context, sessionId, entity, entity.InventoryUsers);
                    CascadeEdit(context, sessionId, entity, entity.InventoryDepartments);
                    CascadeEdit(context, sessionId, entity, entity.InventoryAssetTypes);
                    CascadeEdit(context, sessionId, entity, entity.InventoryRegions);
                });
            #endregion
        }

        public static void Run<TEntity>(DbContext context, int intSessionUsedCompanyId, int intSessionId, TEntity entity, CascadeType cascadeType)
        {
            List<Action<object, DbContext, int, int>> actionList = GetActionList<TEntity>(cascadeType);
            foreach (Action<object, DbContext, int, int> action in actionList)
            {
                action(entity, context, intSessionUsedCompanyId, intSessionId);
            }
        }

        #region Private AssetOperationRecord
        private static void AddAssetOperationRecord(Asset asset, AssetOperationRecordDto dto)
        {
            asset.AssetOperationRecords = asset.AssetOperationRecords ?? new List<AssetOperationRecord>();
            dto.AssetId = asset.Id;
            asset.AssetOperationRecords.Add(Mapper.Map<AssetOperationRecord>(dto));
        }

        public static string GetLocation(DbContext context, IAssetLocation entity)
        {
            string strLocation = string.Empty;
            strLocation += $"【{GetLocation(context, entity.Region, entity.RegionId)}";
            strLocation += $"/{GetLocation(context, entity.UsedCompany, entity.UsedCompanyId)}";
            strLocation += $"/{GetLocation(context, entity.UsedDepartment, entity.UsedDepartmentId)}";
            strLocation += $"/{GetLocation(context, entity.StoredAddress, entity.StoredAddressId)}";
            strLocation += $"/{GetLocation(context, entity.UsedEmployee, entity.UsedEmployeeId)}】";
            return strLocation;
        }
        public static string GetLocation<TEntity>(DbContext context, TEntity location, int intLocationId)
            where TEntity : class
        {
            string strFieldName = "Name";
            if (location != null)
            {
                return TypeHelper.GetPropertyValue<TEntity, string>(location, strFieldName);
            }
            return GetForeignTableField(context, typeof(TEntity).Name, intLocationId, strFieldName);
        }
        #endregion

        #region Private AssetStatus
        private static void UpdateAssetStatus<TSource>(DbContext context, int sessionUsedCompanyId,TSource source, List<Asset> assetList, int intAssetId, AssetStatus assetStatus, int intTotal, Func<Asset, AssetOperationRecordDto> dtoCallback)
        {
            Asset asset = assetList.FirstOrDefault(o => o.Id == intAssetId);
            if (asset == null)
            {
                throw new Exception("Not exist Asset with ID = " + intAssetId);
            }
            UpdateAssetStatus(context, sessionUsedCompanyId,source, asset, assetStatus, intTotal, dtoCallback(asset));
        }
        private static void UpdateAssetStatus<TSource>(DbContext context,int sessionUsedCompanyId, TSource source, Asset asset, AssetStatus assetStatus, int intTotal, AssetOperationRecordDto dto)
        {
            AssetItemStatus assetItemStatusForFree = asset.AssetItemStatuses.FirstOrDefault(o => o.AssetStatus == AssetStatus.Free);
            if (assetItemStatusForFree == null)
            {
                throw new Exception($"资产[Id={asset.Id}]不存在闲置状态的资产");
            }
            AssetLocationItem assetLocationItemForFree = assetItemStatusForFree.AssetLocationItems.FirstOrDefault();
            if (assetLocationItemForFree == null)
            {
                throw new Exception($"资产[Id={asset.Id}]闲置状态的资产没有存放的位置记录");
            }

            AssetLocationItem assetLocationItemTemp = Mapper.Map<AssetLocationItem>(source);
            AssetLocationItem assetLocationItem = null;

            DbSet<AssetItemStatus> cascadeDbset = context.Set<AssetItemStatus>();

            AssetItemStatus assetItemStatus = asset.AssetItemStatuses.FirstOrDefault(o => o.AssetStatus == assetStatus);
            if (assetItemStatus == null)
            {
                assetLocationItem = assetLocationItemTemp;
                assetItemStatus = new AssetItemStatus
                {
                    UsedCompanyId= asset.UsedCompanyId,
                    AssetId = asset.Id,
                    AssetStatus = assetStatus,
                    Total = 0,
                    AssetLocationItems = new List<AssetLocationItem> { assetLocationItem }
                };
                asset.AssetItemStatuses.Add(assetItemStatus);
                cascadeDbset.Add(assetItemStatus);
            }
            else
            {
                assetLocationItem = assetItemStatus.AssetLocationItems.Where(o =>
                                                        o.UsedCompanyId == assetLocationItemTemp.UsedCompanyId
                                                        & o.UsedDepartmentId == assetLocationItemTemp.UsedDepartmentId
                                                        & o.UsedEmployeeId == assetLocationItemTemp.UsedEmployeeId
                                                        & o.RegionId == assetLocationItemTemp.RegionId
                                                        & o.StoredAddressId == assetLocationItemTemp.StoredAddressId
                                                    ).FirstOrDefault();
                if (assetLocationItem == null)
                {
                    assetLocationItem = assetLocationItemTemp;
                    assetItemStatus.AssetLocationItems = assetItemStatus.AssetLocationItems ?? new List<AssetLocationItem>();
                    assetItemStatus.AssetLocationItems.Add(assetLocationItem);
                }
            }

            assetItemStatusForFree.Total -= intTotal;
            if (assetItemStatusForFree.Total < 0)
            {
                throw new Exception("当前资产状态无法更新，更新数量超过实际数量，请核实再试。");
            }

            assetLocationItemForFree.Total -= intTotal;
            if (assetLocationItemForFree.Total < 0)
            {
                throw new Exception("当前资产位置无法更新，更新数量超过实际数量，请核实再试。");
            }

            assetItemStatus.Total += intTotal;
            if (assetItemStatus.Total < 0)
            {
                throw new Exception("当前资产状态无法更新，更新数量超过实际数量，请核实再试。");
            }

            assetLocationItem.Total += intTotal;
            if (assetLocationItem.Total < 0)
            {
                throw new Exception("当前资产位置无法更新，更新数量超过实际数量，请核实再试。");
            }

            if (assetStatus == AssetStatus.Clear)
            {
                asset.Total -= intTotal;
            }

            AddAssetOperationRecord(asset, dto);
        }

        public static string GetForeignTableField(DbContext context, string strForeignTable, int intId, string strForeignField = "Name")
        {
            string strKey = $"GetForeignTableField-{strForeignTable}-{intId}-{strForeignField}";
            string strFieldValue = CacheHelper.GetCache<string>(strKey);
            if (string.IsNullOrEmpty(strFieldValue))
            {
                strFieldValue = context.Database.SqlQuery<string>($"select {strForeignField} from {strForeignTable} where Id=@Id"
                                , new SqlParameter("@Id", intId)).FirstOrDefault();
                if (string.IsNullOrEmpty(strFieldValue))
                {
                    throw new Exception($"未找到表{strForeignTable}中Id为{intId}的{strForeignField}");
                }

                CacheHelper.SetCache(strFieldValue, strKey);
            }
            return strFieldValue;
        }

        private static string UpdateAssetInfo<TEntity>(DbContext context, List<Asset> assetList, TEntity entity, AssetOperationRecordDto dto)
            where TEntity : class, IEntity, new()
        {
            int intAssetId = TypeHelper.GetPropertyValue<TEntity, int>(entity, "AssetId");
            Asset asset = assetList.FirstOrDefault(o => o.Id == intAssetId);
            if (asset == null)
            {
                throw new Exception($"Not exist Asset with Id = {intAssetId}");
            }

            string strUpdateRecord = TypeHelper.Update(asset, entity, null,
                (source, update, sourceProp, updateProp, objSourceValue, objUpdateValue) =>
                {
                    return TypeHelper.GetUpdateRecord(source, update, sourceProp, updateProp, objSourceValue, objUpdateValue,
                            (strForeignTable, intId, strForeignField) =>
                            {
                                return GetForeignTableField(context, strForeignTable, intId, strForeignField);
                            });
                });

            asset.UpdateTime = DateTime.Now;

            dto.Description += strUpdateRecord;
            AddAssetOperationRecord(asset, dto);

            if (entity is IAssetLocation)
            {
                AssetItemStatus assetItemStatusForFree = asset.AssetItemStatuses.FirstOrDefault(o => o.AssetStatus == AssetStatus.Free);
                if (assetItemStatusForFree == null)
                {
                    throw new Exception($"Not exist AssetItemStatus with AssetId = {intAssetId} and AssetStatus = AssetStatus.Free");
                }

                AssetLocationItem assetLocationItemForFree = assetItemStatusForFree.AssetLocationItems.FirstOrDefault(o => o.Total == assetItemStatusForFree.Total);
                if (assetLocationItemForFree == null)
                {
                    throw new Exception($"Not exist AssetLocationItem with AssetItemStatusId = {entity.Id} and Total={assetItemStatusForFree.Total}");
                }

                TypeHelper.Update(assetItemStatusForFree, entity);
                TypeHelper.Update(assetLocationItemForFree, entity);
            }

            return strUpdateRecord;
        }
        #endregion

        #region Private
        private static Dictionary<Type, List<Action<object, DbContext, int, int>>> _DicAddMapping = new Dictionary<Type, List<Action<object, DbContext, int, int>>>();
        private static Dictionary<Type, List<Action<object, DbContext, int, int>>> _DicEditMapping = new Dictionary<Type, List<Action<object, DbContext, int, int>>>();
        private static Dictionary<Type, List<Action<object, DbContext, int, int>>> _DicDeleteMapping = new Dictionary<Type, List<Action<object, DbContext, int, int>>>();

        //private static IEnumerable<TEntity> GetEntitiesByIdList<TEntity>(DbContext context, IEnumerable<int> idList)
        //     where TEntity : class, IEntity, new()
        //{
        //    return context.Set<TEntity>().Where(o => idList.Contains(o.Id));
        //}

        //private static void CascadeDelete<TCascadeEntity>(DbContext context, IEnumerable<int> idList, bool bIsRecursionDelete = true)
        //    where TCascadeEntity : class, IEntity, new()
        //{
        //    CascadeDelete(context, sessionId, GetEntitiesByIdList<TCascadeEntity>(context, idList), bIsRecursionDelete);
        //}

        public static void CascadeDelete<TCascadeEntity>(DbContext context, int intSessionUsedCompanyId, int intSessionId, IEnumerable<TCascadeEntity> collection, bool bIsRecursionDelete = true)
            where TCascadeEntity : class, IEntity, new()
        {
            if (!collection.Any())
            {
                return;
            }

            List<Action<object, DbContext, int, int>> actionList = bIsRecursionDelete ? GetActionList<TCascadeEntity>(CascadeType.Delete) : null;
            for (int index = collection.Count() - 1; index >= 0; index--)
            {
                TCascadeEntity item = collection.ElementAt(index);

                if (bIsRecursionDelete)
                {
                    foreach (Action<object, DbContext, int, int> action in actionList)
                    {
                        action(item, context, intSessionUsedCompanyId, intSessionId);
                    }
                }

                DbEntityEntry<TCascadeEntity> entry = context.Entry(item);
                if (entry.State != EntityState.Deleted)
                {
                    entry.State = EntityState.Deleted;//This will change collection array's length that will reduce 1;
                }
            }
        }

        public static void CascadeAdd<TEntity, TCascadeEntity>(DbContext context, int intSessionUsedCompanyId, int intSessionId, TEntity entity, ICollection<TCascadeEntity> cascadeEntities, Action<TCascadeEntity> afterCascadeAddCallback = null)
            where TEntity : class, IEntity, new()
            where TCascadeEntity : class, IEntity, new()
        {
            if (!cascadeEntities.Any())
            {
                return;
            }

            TCascadeEntity firstCascadeEntity = cascadeEntities.First();

            bool bIsSessionEntity = (firstCascadeEntity is ISessionEntity) && (entity is ISessionEntity);
            bool bIsCreateTimeEntity = (firstCascadeEntity is ICreateTimeEntity) && (entity is ICreateTimeEntity);
            bool bIsUsedManagementEntity = (firstCascadeEntity is IUsedManagementEntity) && (entity is IUsedManagementEntity);
            
            foreach (var item in cascadeEntities)
            {
                if (bIsSessionEntity)
                {
                    (item as ISessionEntity).SessionId = (entity as ISessionEntity).SessionId;
                }

                if (bIsCreateTimeEntity)
                {
                    (item as ICreateTimeEntity).CreateTime = (entity as ICreateTimeEntity).CreateTime;
                }

                if (bIsUsedManagementEntity)
                {
                    (item as IUsedManagementEntity).UsedCompanyId = (entity as IUsedManagementEntity).UsedCompanyId;
                }

                afterCascadeAddCallback?.Invoke(item);
            }
        }

        public static void CascadeEdit<TEntity, TCascadeEntity>(DbContext context, int intSessionid, ICollection<TEntity> entities, Func<TEntity, ICollection<TCascadeEntity>> func, Action<TEntity> beforeEntityEditCallback = null, Action<TCascadeEntity> beforeCascadeEditCallback = null)
            where TEntity : class, IEntity, new()
            where TCascadeEntity : class, IEntity, new()
        {
            TEntity[] aryEntities = entities.ToArray();
            for (int index = 0; index < aryEntities.Length; index++)
            {
                TEntity entity = aryEntities[index];
                beforeEntityEditCallback?.Invoke(entity);
                if (entity.Id > 0)
                {
                    ICollection<TCascadeEntity> cascadeEntities = func(entity);
                    CascadeEdit(context, intSessionid, entity, cascadeEntities, beforeCascadeEditCallback);
                }
            }
        }

        private static void CascadeEdit<TEntity, TCascadeEntity>(DbContext context, int intSessionid, TEntity entity, ICollection<TCascadeEntity> cascadeEntities, Action<TCascadeEntity> beforeCascadeEditCallback = null)
            where TEntity : class, IEntity, new()
            where TCascadeEntity : class, IEntity, new()
        {
            string strForeignKeyName = $"{typeof(TEntity).Name}Id";

            List<int> newIdList = cascadeEntities.Select(a => a.Id).Distinct().ToList();

            DbSet<TCascadeEntity> cascadeDbset = context.Set<TCascadeEntity>();
            List<TCascadeEntity> cascadeEntityList = cascadeDbset
                .Where(PageHelper.GetPredicate<TCascadeEntity, int>(strForeignKeyName, entity.Id))
                .Where(o => !newIdList.Contains(o.Id)).ToList();

            cascadeDbset.RemoveRange(cascadeEntityList);

            TCascadeEntity[] aryCascadeEntities = cascadeEntities.ToArray();

            PropertyInfo foreignKeyProp = TypeHelper.GetPropertyInfo<TCascadeEntity>(strForeignKeyName);
            for (int index = 0; index < aryCascadeEntities.Length; index++)
            {
                TCascadeEntity item = aryCascadeEntities[index];

                beforeCascadeEditCallback?.Invoke(item);

                if (item.Id == 0)
                {
                    foreignKeyProp.SetValue(item, entity.Id);
                    cascadeDbset.Add(item);
                }
                else
                {
                    DbContextHelper.Attach(context, item, EntityState.Modified);
                }
            }
        }

        private static List<Action<object, DbContext, int, int>> GetActionList<TEntity>(CascadeType cascadeType)
        {
            Type type = typeof(TEntity);
            return GetActionList(type, cascadeType);
        }

        private static Dictionary<Type, List<Action<object, DbContext, int, int>>> GetDictionary(CascadeType cascadeType)
        {
            switch (cascadeType)
            {
                case CascadeType.Add:
                    return _DicAddMapping;
                case CascadeType.Edit:
                    return _DicEditMapping;
                case CascadeType.Delete:
                default:
                    return _DicDeleteMapping;
            }
        }

        private static List<Action<object, DbContext, int, int>> GetActionList(Type type, CascadeType cascadeType)
        {
            var dicMapping = GetDictionary(cascadeType);

            if (!dicMapping.Keys.Contains(type))
            {
                dicMapping.Add(type, new List<Action<object, DbContext, int, int>>());
            }
            return dicMapping[type];
        }

        private static void Add<TEntity>(CascadeType cascadeType, List<Action<object, DbContext, int, int>> list)
        {
            Type type = typeof(TEntity);
            Add(type, cascadeType, list);
        }

        private static void Add(Type type, CascadeType cascadeType, List<Action<object, DbContext, int, int>> list)
        {
            List<Action<object, DbContext, int, int>> collection = GetActionList(type, cascadeType);
            collection.AddRange(list);
        }

        private static void Add<TEntity>(CascadeType cascadeType, Action<object, DbContext, int, int> item)
        {
            Type type = typeof(TEntity);
            Add(type, cascadeType, item);
        }

        private static void Add(Type type, CascadeType cascadeType, Action<object, DbContext, int, int> item)
        {
            List<Action<object, DbContext, int, int>> collection = GetActionList(type, cascadeType);
            collection.Add(item);
        }
        #endregion
    }
}
