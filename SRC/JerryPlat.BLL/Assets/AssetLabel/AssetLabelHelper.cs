﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class AssetLabelHelper : AdminBaseSessionHelper<AssetLabel, AssetLabelDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetLabelDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != -1)
            {
                bool bIsDefaultUsed = searchModel.Id == 1;
                queryableDtoList = queryableDtoList.Where(o => o.IsDefaultUsed == bIsDefaultUsed);
            }
        }
    }
}