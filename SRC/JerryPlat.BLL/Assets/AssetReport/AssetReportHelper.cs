﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
    public class AssetReportHelper : AdminBaseSessionHelper
    {
        #region AssetStatusReport & AssetTypeReport
        public async Task<List<AssetStatusReportDto>> GetAssetStatusReportAsync()
        {
            IQueryable<AssetItemStatusDto> queryableDtoList = GetQueryableList<AssetItemStatus, AssetItemStatusDto>();

            //SetQueryableFilter(ref queryableDtoList);
            var validAssetItemStatusIdList = (await queryableDtoList.ToListAsync()).Select(x => x.Id).ToList();
            if (_Session.UsedCompanyId != 1)
            {
                validAssetItemStatusIdList = (await queryableDtoList.ToListAsync()).Where(x => x.AssetLocationItems.Any(y => y.UsedCompanyId == _Session.UsedCompanyId && y.Total > 0)).Select(x => x.Id).ToList();
            }

            var usedCompanyAssetItemStatusList = await queryableDtoList.Where(x => validAssetItemStatusIdList.Contains(x.Id)).ToListAsync();

            usedCompanyAssetItemStatusList.ForEach(x =>
            {
                if (_Session.UsedCompanyId != 1)
                {
                    x.Total = x.AssetLocationItems.Where(y => y.UsedCompanyId == _Session.UsedCompanyId).Sum(y => y.Total);
                }
                else
                {
                    x.Total = x.AssetLocationItems.Sum(y => y.Total);
                }
            });

            return (from a in usedCompanyAssetItemStatusList
                          group a by a.AssetStatus into g
                          select new AssetStatusReportDto
                          {

                              AssetStatus = g.Key,
                              Total = g.Sum(o => o.Total),
                              Amount = g.Sum(o => o.Total * o.Asset.Price)
                          }).ToList();
        }

        public async Task<List<AssetTypeReportDto>> GetAssetTypeReportAsync()
        {
            IQueryable<AssetBaseDto> queryableDtoList = GetQueryableList<Asset, AssetBaseDto>();

            SetQueryableFilter(ref queryableDtoList);

            return await (from a in queryableDtoList
                          group a by a.AssetTypeName into g
                          select new AssetTypeReportDto
                          {
                              AssetTypeName = g.Key,
                              Total = g.Sum(o => o.Total),
                              Amount = g.Sum(o => o.Total * o.Price)
                          }).ToListAsync();
        }
        #endregion

        #region AssetReport
        public IQueryable<TAssetBaseDto> GetAssetReportQueryable<TAssetBaseDto>(SearchModel searchModel)
            where TAssetBaseDto : AssetBaseDto, new()
        {
            IQueryable<TAssetBaseDto> queryableDtoList = GetQueryableList<Asset, TAssetBaseDto>();

            //SetQueryableFilter(ref queryableDtoList);
            var assetIdList = queryableDtoList.Select(o => o.Id).ToList();
            List<AssetItemStatus> assetItemStatuses = _Db.AssetItemStatuses.Where(o => assetIdList.Contains(o.AssetId)).ToList();
            List<int> validAssetIdList = new List<int>();
            assetItemStatuses = assetItemStatuses.Where(o => o.AssetLocationItems.Any(x => x.UsedCompanyId == _Session.UsedCompanyId)).ToList();
            validAssetIdList.AddRange(assetItemStatuses.Select(o => o.AssetId).ToList());
            if (_Session.UsedCompanyId!=1)
            {
                queryableDtoList = queryableDtoList.Where(o => validAssetIdList.Contains(o.Id));
            }
            //if (validAssetIdList.Any())
            //{
            //    queryableDtoList = queryableDtoList.Where(o => validAssetIdList.Contains(o.Id));
            //}
            if (searchModel.Id != 0)
            {
                List<int> assetTypeIdList = TypeHelper.GetTreeIdList(GetQueryableList<AssetType, AssetTypeDto>(null).ToList(), searchModel.Id);

                queryableDtoList = queryableDtoList.Where(o => assetTypeIdList.Contains(o.AssetTypeId));
            }

            if (searchModel.Id1 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedCompanyId == searchModel.Id1);
            }

            if (searchModel.Id2 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedDepartmentId == searchModel.Id2);
            }

            if (searchModel.Id3 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.StoredAddressId == searchModel.Id3);
            }

            if (searchModel.Id4 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.RegionId == searchModel.Id4);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.BuyTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.BuyTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                                        | o.SN.Contains(searchModel.SearchText)
                                        | o.Name.Contains(searchModel.SearchText));
            }

            return queryableDtoList;
        }

        public async Task<PageData<TAssetBaseDto>> GetAssetReportAsync<TAssetBaseDto, TKey>(SearchModel searchModel, Expression<Func<TAssetBaseDto, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true)
            where TAssetBaseDto : AssetBaseDto, new()
        {
            IQueryable<TAssetBaseDto> queryableDtoList = GetAssetReportQueryable<TAssetBaseDto>(searchModel);
            return await PageHelper.GetPageDataAsync(queryableDtoList, orderByKeySelector, pageParam, bIsAscOrder, searchModel.Sort);
        }

        public async Task<string> ExportAssetReportAsync<TAssetBaseDto>(SearchModel searchModel)
            where TAssetBaseDto : AssetBaseDto, new()
        {
            IQueryable<TAssetBaseDto> queryableDtoList = GetAssetReportQueryable<TAssetBaseDto>(searchModel);
            List<TAssetBaseDto> dtoList = await PageHelper.GetOrderQueryable(queryableDtoList, searchModel?.Sort).ToListAsync();
            return ExcelHelper.SaveExcelFile(dtoList, searchModel.FileName);
        }

        public async Task<PageData<AssetOperationRecordDto>> GetAssetOperationRecordAsync(SearchModel searchModel, PageParam pageParam)
        {
            IQueryable<AssetOperationRecordDto> queryableDtoList = GetQueryableList<AssetOperationRecord, AssetOperationRecordDto>();
            SetQueryableFilter(ref queryableDtoList);

            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.AssetId == searchModel.Id);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText));
            }

            return await PageHelper.GetPageDataAsync(queryableDtoList, o => o.CreateTime, pageParam, false, searchModel.Sort);
        }
        #endregion

        #region AssetRecordReport
        public IEnumerable<AssetOperationRecordDto> GetAssetRecordQueryable(IEnumerable<AssetOperationRecordDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.OperationType == (OperationType)searchModel.Id);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime <= searchModel.EndTime);
            }

            return queryableDtoList;
        }

        private void SetAssetRecordReport<TAssetBaseDto>(List<TAssetBaseDto> assetBaseList, SearchModel searchModel)
            where TAssetBaseDto : AssetBaseDto, new()
        {
            if (!(assetBaseList is List<AssetReportDto>))
            {
                return;
            }

            List<AssetReportDto> assetReportList = assetBaseList as List<AssetReportDto>;
            foreach (var item in assetReportList)
            {
                item.AssetOperationRecords = GetAssetRecordQueryable(item.AssetOperationRecords, searchModel)
                        .OrderByDescending(o => o.CreateTime).ToList();
            }
        }

        public IQueryable<TAssetBaseDto> GetAssetRecordReportQueryable<TAssetBaseDto>(SearchModel searchModel)
            where TAssetBaseDto : AssetBaseDto, new()
        {
            IQueryable<AssetOperationRecordDto> queryableDtoList = GetQueryableList<AssetOperationRecord, AssetOperationRecordDto>();
            SetQueryableFilter(ref queryableDtoList);

            queryableDtoList = GetAssetRecordQueryable(queryableDtoList, searchModel).AsQueryable();

            List<int> assetIdList = queryableDtoList.Select(o => o.AssetId).Distinct().ToList();

            IQueryable<TAssetBaseDto> assetReportList = GetQueryableList<Asset, TAssetBaseDto>();
            SetQueryableFilter(ref assetReportList);
            assetReportList = assetReportList.Where(o => assetIdList.Contains(o.Id));

            return assetReportList;
        }

        public async Task<PageData<TAssetBaseDto>> GetAssetRecordReportAsync<TAssetBaseDto, TKey>(SearchModel searchModel, Expression<Func<TAssetBaseDto, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true)
             where TAssetBaseDto : AssetBaseDto, new()
        {
            IQueryable<TAssetBaseDto> queryableDtoList = GetAssetRecordReportQueryable<TAssetBaseDto>(searchModel);
            PageData<TAssetBaseDto> pageData = await PageHelper.GetPageDataAsync(queryableDtoList, orderByKeySelector, pageParam, bIsAscOrder, searchModel.Sort);
            SetAssetRecordReport(pageData.Data, searchModel);
            return pageData;
        }

        public async Task<string> ExportAssetRecordReportAsync<TAssetBaseDto>(SearchModel searchModel)
            where TAssetBaseDto : AssetBaseDto, new()
        {
            IQueryable<TAssetBaseDto> queryableDtoList = GetAssetRecordReportQueryable<TAssetBaseDto>(searchModel);
            List<TAssetBaseDto> dtoList = await PageHelper.GetOrderQueryable(queryableDtoList, searchModel?.Sort).ToListAsync();
            SetAssetRecordReport(dtoList, searchModel);
            return ExcelHelper.SaveExcelFile(dtoList, searchModel.FileName, (propName, value) =>
            {
                switch (propName)
                {
                    case "AssetOperationRecords.OperationType":
                        return EnumHelper.GetDescription((OperationType)value);
                }
                return null;
            });
        }
        #endregion

        #region AssetClearReprot
        public IEnumerable<AssetItemStatusBaseWithoutAssetDto> GetAssetItemStatusReportQueryable(IEnumerable<AssetItemStatusBaseWithoutAssetDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.AssetStatus == (AssetStatus)searchModel.Id);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UpdateTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UpdateTime <= searchModel.EndTime);
            }

            return queryableDtoList;
        }

        private void SetAssetRecordReport(List<AssetItemReportDto> assetBaseList, SearchModel searchModel)
        {
            if (!(assetBaseList is List<AssetItemReportDto>))
            {
                return;
            }

            List<AssetItemReportDto> assetReportList = assetBaseList as List<AssetItemReportDto>;
            foreach (var item in assetReportList)
            {
                item.AssetItemStatuses = GetAssetItemStatusReportQueryable(item.AssetItemStatuses, searchModel).OrderByDescending(o => o.UpdateTime).ToList();
                item.Total = item.AssetItemStatuses.Sum(o => o.Total);
            }
        }

        public IQueryable<AssetItemReportDto> GetAssetItemReportQueryable(SearchModel searchModel)
        {
            IQueryable<AssetItemStatusBaseWithoutAssetDto> queryableDtoList = GetQueryableList<AssetItemStatus, AssetItemStatusBaseWithoutAssetDto>();
            SetQueryableFilter(ref queryableDtoList);

            queryableDtoList = GetAssetItemStatusReportQueryable(queryableDtoList, searchModel).AsQueryable();

            List<int> assetIdList = queryableDtoList.Select(o => o.AssetId).Distinct().ToList();

            IQueryable<AssetItemReportDto> assetReportList = GetQueryableList<Asset, AssetItemReportDto>();
            SetQueryableFilter(ref assetReportList);
            assetReportList = assetReportList.Where(o => assetIdList.Contains(o.Id));

            return assetReportList;
        }

        public async Task<PageData<AssetItemReportDto>> GetAssetItemReportAsync<TKey>(SearchModel searchModel, Expression<Func<AssetItemReportDto, TKey>> orderByKeySelector, PageParam pageParam, bool bIsAscOrder = true)
        {
            IQueryable<AssetItemReportDto> queryableDtoList = GetAssetItemReportQueryable(searchModel);
            PageData<AssetItemReportDto> pageData = await PageHelper.GetPageDataAsync(queryableDtoList, orderByKeySelector, pageParam, bIsAscOrder, searchModel.Sort);
            SetAssetRecordReport(pageData.Data, searchModel);
            return pageData;
        }

        public async Task<string> ExportAssetItemReportAsync(SearchModel searchModel)
        {
            IQueryable<AssetItemReportDto> queryableDtoList = GetAssetItemReportQueryable(searchModel);
            List<AssetItemReportDto> dtoList = await PageHelper.GetOrderQueryable(queryableDtoList, searchModel?.Sort).ToListAsync();
            SetAssetRecordReport(dtoList, searchModel);
            return ExcelHelper.SaveExcelFile(dtoList, searchModel.FileName);
        }
        #endregion
    }
}
