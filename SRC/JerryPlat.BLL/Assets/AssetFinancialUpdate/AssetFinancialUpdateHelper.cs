﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class AssetFinancialUpdateHelper : AdminBaseSessionHelper<AssetFinancialUpdate, AssetFinancialUpdateDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetFinancialUpdateDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedCompanyId == searchModel.Id);
            }

            if (searchModel.Id1 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedDepartmentId == searchModel.Id1);
            }

            if (searchModel.Id2 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedEmployeeId == searchModel.Id2);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                    | o.Description.Contains(searchModel.SearchText));
            }
        }
    }
}