﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
   public class AssetClearHelper : AdminBaseSessionHelper<AssetClear, AssetClearDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetClearDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.ClearTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.ClearTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                                                                | o.Description.Contains(searchModel.SearchText));
            }
        }
    }
}
