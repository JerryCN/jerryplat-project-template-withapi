﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;
using System;

namespace JerryPlat.BLL
{
    public class AssetHelper : AdminBaseSessionHelper<Asset, AssetDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id1 != 0)
            {
                List<int> assetTypeIdList = TypeHelper.GetTreeIdList(GetQueryableList<AssetType, AssetTypeDto>(null).ToList(), searchModel.Id);
                queryableDtoList = queryableDtoList.Where(o => assetTypeIdList.Contains(o.AssetTypeId));
            }

            if (searchModel.Id2 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedCompanyId == searchModel.Id2);
            }

            if (searchModel.Id3 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedDepartmentId == searchModel.Id3);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.BuyTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.BuyTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                    | o.Name.Contains(searchModel.SearchText)
                    | o.SN.Contains(searchModel.SearchText));
            }
        }

        public async Task<AssetBaseDto> GetAssetByCodeAsync(SearchModel searchModel)
        {
            IQueryable<Asset> queryableDtoList = GetQueryableList<Asset>()
                .Where(o => o.Code == searchModel.SearchText);


            //SetQueryableFilter(ref queryableDtoList);

            bool bIsExistAssetByCode = false;
            if (searchModel.Id != 0)//InventoryId
            {
                Inventory inventory = GetById<Inventory>(searchModel.Id);

                if (!inventory.InventoryUsers.Any(o => o.SessionId == _Session.Id))
                {
                    throw new Exception("当前资产不在您盘点范围，请重新再试。");
                }

                bIsExistAssetByCode = queryableDtoList.Any();

                var assetIdList = queryableDtoList.Select(o => o.Id).ToList();

                List<int> idListDepartments = inventory.InventoryDepartments.Select(o => o.DepartmentId).ToList();
                List<AssetItemStatus> assetItemStatuses = _Db.AssetItemStatuses.Where(o => assetIdList.Contains(o.AssetId)).ToList();
                List<int> validAssetIdList = new List<int>();
                if (idListDepartments.Any())
                {
                    assetItemStatuses = assetItemStatuses.Where(o => o.AssetLocationItems.Any(x=> idListDepartments.Contains(x.UsedDepartmentId))).ToList();

                    validAssetIdList.AddRange(assetItemStatuses.Select(o => o.AssetId).ToList());
                }

                List<int> idListAssetTypes = inventory.InventoryAssetTypes.Select(o => o.AssetTypeId).ToList();

                idListAssetTypes = TypeHelper.GetTreeIdList(GetQueryableList<AssetType, AssetTypeDto>(null).ToList(), idListAssetTypes);

                if (idListAssetTypes.Any())
                {
                    queryableDtoList = queryableDtoList.Where(o => idListAssetTypes.Contains(o.AssetTypeId));
                }

                List<int> idListRegions = inventory.InventoryRegions.Select(o => o.RegionId).ToList();
                if (idListRegions.Any())
                {
                    validAssetIdList.AddRange(assetItemStatuses.Where(o => o.AssetLocationItems.Any(x => idListRegions.Contains(x.RegionId))).Select(o => o.AssetId).ToList());
                }

                validAssetIdList = validAssetIdList.Distinct().ToList();
                if (validAssetIdList.Any())
                {
                    queryableDtoList = queryableDtoList.Where(o => validAssetIdList.Contains(o.Id));
                }

                queryableDtoList = queryableDtoList.Where(o => o.BuyTime >= inventory.StartTime);
                queryableDtoList = queryableDtoList.Where(o => o.BuyTime <= inventory.EndTime);
            }

            Asset asset = await queryableDtoList.FirstOrDefaultAsync();

            AssetBaseDto assetBaseDto = Mapper.Map<AssetBaseDto>(asset);

            if (bIsExistAssetByCode && assetBaseDto == null)
            {
                throw new Exception("当前资产不在您盘点范围，请重新再试。");
            }

            return assetBaseDto;
        }

        public async Task<AssetBaseDto> GetAssetByCodeAsync1(SearchModel searchModel)
        {
            IQueryable<AssetBaseDto> queryableDtoList = GetQueryableList<Asset, AssetBaseDto>()
                .Where(o => o.Code == searchModel.SearchText);

            //SetQueryableFilter(ref queryableDtoList);

            bool bIsExistAssetByCode = false;
            if (searchModel.Id != 0)//InventoryId
            {
                Inventory inventory = GetById<Inventory>(searchModel.Id);

                if (!inventory.InventoryUsers.Any(o => o.SessionId == _Session.Id))
                {
                    throw new Exception("当前资产不在您盘点范围，请重新再试。");
                }

                bIsExistAssetByCode = queryableDtoList.Any();

                List<int> idListDepartments = inventory.InventoryDepartments.Select(o => o.DepartmentId).ToList();
                if (idListDepartments.Any())
                {
                    queryableDtoList = queryableDtoList.Where(o => idListDepartments.Contains(o.UsedDepartmentId));
                }

                List<int> idListAssetTypes = inventory.InventoryAssetTypes.Select(o => o.AssetTypeId).ToList();

                idListAssetTypes = TypeHelper.GetTreeIdList(GetQueryableList<AssetType, AssetTypeDto>(null).ToList(), idListAssetTypes);

                if (idListAssetTypes.Any())
                {
                    queryableDtoList = queryableDtoList.Where(o => idListAssetTypes.Contains(o.AssetTypeId));
                }

                List<int> idListRegions = inventory.InventoryRegions.Select(o => o.RegionId).ToList();
                if (idListRegions.Any())
                {
                    queryableDtoList = queryableDtoList.Where(o => idListRegions.Contains(o.RegionId));
                }

                queryableDtoList = queryableDtoList.Where(o => o.BuyTime >= inventory.StartTime);
                queryableDtoList = queryableDtoList.Where(o => o.BuyTime <= inventory.EndTime);
            }

            AssetBaseDto assetBaseDto = await queryableDtoList.FirstOrDefaultAsync();

            if (bIsExistAssetByCode && assetBaseDto == null)
            {
                throw new Exception("当前资产不在您盘点范围，请重新再试。");
            }

            return assetBaseDto;
        }
    }
}