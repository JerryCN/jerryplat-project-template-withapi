﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class AssetLocationItemHelper : AdminBaseSessionHelper<AssetLocationItem, AssetLocationItemDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetLocationItemDto> queryableDtoList, SearchModel searchModel)
        {
            queryableDtoList = queryableDtoList.Where(o => o.Total > 0);

            if (searchModel.Id != 0)
            {
                if (searchModel.Id == -1)//All Asset Free
                {
                    queryableDtoList = queryableDtoList.Where(o => o.AssetItemStatus.AssetStatus == AssetStatus.Free
                        & o.AssetItemStatus.Total == o.AssetItemStatus.Asset.Total);
                }
                else
                {
                    queryableDtoList = queryableDtoList.Where(o => o.AssetItemStatus.AssetStatus == (AssetStatus)searchModel.Id);
                }
            }

            if (searchModel.Id1 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedCompanyId == searchModel.Id1);
            }

            if (searchModel.Id2 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedDepartmentId == searchModel.Id2);
            }

            if (searchModel.Id3 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedEmployeeId == searchModel.Id3);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.AssetItemStatus.Asset.BuyTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.AssetItemStatus.Asset.BuyTime <= searchModel.EndTime);
            }
            
            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.AssetItemStatus.Asset.Code.Contains(searchModel.SearchText)
                    | o.AssetItemStatus.Asset.Name.Contains(searchModel.SearchText)
                    | o.AssetItemStatus.Asset.SN.Contains(searchModel.SearchText));
            }
        }
    }
}