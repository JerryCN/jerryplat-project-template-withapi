﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class AssetStorageHelper : AdminBaseSessionHelper<AssetStorage, AssetStorageDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetStorageDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.StorageTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.StorageTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                    | o.Description.Contains(searchModel.SearchText));
            }
        }

        //public override Task<bool> SaveAsync(AssetStorageDto queryableEntity)
        //{
        //    if (queryableEntity.UsedCompanyId == 0)
        //    {
        //        queryableEntity.UsedCompanyId = _Session.CompanyId;
        //    }

        //    return base.SaveAsync(queryableEntity);
        //}
    }
}