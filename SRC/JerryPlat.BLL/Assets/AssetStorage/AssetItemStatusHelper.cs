﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class AssetItemStatusHelper : AdminBaseSessionHelper<AssetItemStatus, AssetItemStatusDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetItemStatusDto> queryableDtoList, SearchModel searchModel)
        {
            queryableDtoList = queryableDtoList.Where(o => o.Total > 0);

            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.AssetStatus == (AssetStatus)searchModel.Id);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.Asset.BuyTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.Asset.BuyTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Asset.Code.Contains(searchModel.SearchText)
                    | o.Asset.Name.Contains(searchModel.SearchText)
                    | o.Asset.SN.Contains(searchModel.SearchText));
            }
        }

        public async Task<int> GetAssetTotalAsync(InventoryItemDto searchModel)
        {
            AssetItemStatusDto queryableDto = Mapper.Map<AssetItemStatusDto>(await (_Db.AssetItemStatuses
                .Where(o => o.AssetId == searchModel.AssetId
                            & o.AssetStatus == searchModel.AssetStatus).FirstOrDefaultAsync()));

            if (queryableDto == null)
            {
                return 0;
            }

            AssetLocationItemDto assetLocationItemDto = queryableDto.AssetLocationItems
                .Where(o => o.RegionId == searchModel.RegionId
            & o.UsedDepartmentId == searchModel.UsedDepartmentId
            & o.UsedEmployeeId == searchModel.UsedEmployeeId
            & o.StoredAddressId == searchModel.StoredAddressId).FirstOrDefault();

            if (assetLocationItemDto == null)
            {
                return 0;
            }

            return assetLocationItemDto.Total;
        }
    }
}