﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq;

namespace JerryPlat.BLL
{
    public class AssetTypeHelper : AdminBaseSessionHelper<AssetType, AssetTypeDto>
    {
        private readonly string GetTreeListKey = "GetTreeListAsync";
        private readonly string GetSelectListKey = "GetSelectListAsync";

        private void RemoveCache()
        {
            CacheHelper.RemoveCache(GetTreeListKey);
            CacheHelper.RemoveCache(GetSelectListKey);
        }

        public async Task<List<AssetTypeDto>> GetTreeListAsync(SearchModel searchModel)
        {
            var cache = CacheHelper.GetCache<List<AssetTypeDto>>(GetTreeListKey);
            if (cache == null)
            {
                cache = TypeHelper.GetTreeList(await base.GetListAsync(searchModel), orderByKeySelector: o => o.Code);
                CacheHelper.SetCache(cache, GetTreeListKey);
            }

            return cache;
        }

        public async Task<List<AssetTypeDto>> GetSelectListAsync(SearchModel searchModel)
        {
            var cache = CacheHelper.GetCache<List<AssetTypeDto>>(GetSelectListKey);
            if (cache == null)
            {
                cache = TypeHelper.GetTreeList(await base.GetListAsync(searchModel), bIsTree: false, orderByKeySelector: o => o.Code);
                CacheHelper.SetCache(cache, GetSelectListKey);
            }

            return cache;
        }

        public override async Task<bool> SaveAsync(AssetTypeDto queryableEntity)
        {
            var result  = await base.SaveAsync(queryableEntity);

            RemoveCache();

            return result;
        }

        public override async Task<bool> DeleteAsync(int id)
        {
            var result = await base.DeleteAsync(id);

            RemoveCache();

            return result;
        }


        public override async Task<string> ImportAsync(string excelPath, System.Func<int, int, object, NPOIPropertyInfo, object> valueConverter = null)
        {
            var result = await base.ImportAsync(excelPath, valueConverter);

            RemoveCache();

            return result;
        }
    }
}