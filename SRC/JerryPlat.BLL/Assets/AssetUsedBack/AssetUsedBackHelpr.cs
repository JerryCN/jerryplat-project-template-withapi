﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
   public class AssetUsedBackHelper : AdminBaseSessionHelper<AssetUsedBack, AssetUsedBackDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AssetUsedBackDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.BackTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.BackTime <= searchModel.EndTime);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                                                                | o.Description.Contains(searchModel.SearchText));
            }
        }

        public override void SetQueryableFilter(ref IQueryable<AssetUsedBackDto> queryableDtoList)
        {
            var companyIdList = new List<int> { 0, _Session.UsedCompanyId };
            var IdList = new List<int> { };
            List<AssetUsedBackDto> list = queryableDtoList.ToList();
            foreach (var item in list)
            {
                if (companyIdList.Contains(item.UsedCompanyId))
                {
                    IdList.Add(item.Id);
                }
                foreach (var assetItem in item.AssetUsedBackItems)
                {
                    if (companyIdList.Contains(assetItem.Asset.UsedCompanyId))
                    {
                        IdList.Add(item.Id);
                    }
                }
            }
            if (_Session.UsedCompanyId != 1)
            {
                queryableDtoList = queryableDtoList.Where(o => IdList.Contains(o.Id));
            }
        }
    }
}
