﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JerryPlat.Utils.Models;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class UIDbHelper : BaseHelper<UI, UIDto>
    {
        public static UIDbHelper Instance => SingleInstanceHelper.GetInstance<UIDbHelper>();

        public override void SetSearchQueryableList(ref IQueryable<UIDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UIType == (UIType)searchModel.Id);
            }
            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Name == searchModel.SearchText);
            }
        }
        
        private string GetEntityName<TDto>(UIType uiType)
            where TDto : class, new()
        {
            TDto dto = new TDto();
            if ((dto is BaseSettingDto))
            {
                return typeof(BaseSettingDto).Name;
            }
            return typeof(TDto).Name;
        }

        public UIDto GetUIDto<TDto>(UIType uiType, string strName = "")
             where TDto : class, new()
        {
            string strEntityName = GetEntityName<TDto>(uiType);
            if (!string.IsNullOrEmpty(strName))
            {
                strEntityName += "_" + strName;
            }

            string strKey = $"{uiType.ToString()}_{strEntityName}";

            UIDto uiDto = CacheHelper.GetCache<UIDto>(strKey);
            if (uiDto == null)
            {
                SearchModel searchModel = new SearchModel { Id = (int)uiType, SearchText = strEntityName };
                uiDto = GetQueryableList(searchModel).FirstOrDefault();
                if(uiDto != null)
                {
                    uiDto.UIRows = uiDto.UIRows.OrderBy(o => o.OrderIndex).ToList();
                    CacheHelper.SetCache(uiDto, strKey);
                }
            }
            return uiDto;
        }
    }
}
