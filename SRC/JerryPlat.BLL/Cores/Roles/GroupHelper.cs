﻿using AutoMapper;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
    public class GroupHelper : AdminBaseSessionHelper<Group, GroupDto>
    {
        public override void SetQueryableFilter(ref IQueryable<GroupDto> queryableDtoList)
        {
            var companyIdList = new List<int> { 0, _Session.UsedCompanyId };
            if (_Session.UsedCompanyId != 1)
            {
                queryableDtoList = queryableDtoList.Where(o => companyIdList.Contains(o.UsedCompanyId));
            }
        }
        public override async Task<bool> SaveAsync(GroupDto model)
        {
            Group group = Mapper.Map<Group>(model);
            if (model.Id == 0)
            {
                group.UsedCompanyId = _Session.UsedCompanyId;
                GetDbSet<Group>().Add(group);
                await SaveChangesAsync();
            }
            else
            {
                Attach(group, EntityState.Modified);
            }

            var navRoleDb = GetDbSet<Role>();
            List<Role> navRoleList = navRoleDb.Where(o => o.GroupId == group.Id).ToList();
            navRoleDb.RemoveRange(navRoleList);

            model.NavigationIdList.ForEach(o =>
            {
                navRoleDb.Add(new Role
                {
                    GroupId = group.Id,
                    NavigationId = o
                });
            });

            //CacheHelper.RemoveCache(key => key.StartsWith("Navigation"));

            return await SaveChangesAsync();
        }
    }
}