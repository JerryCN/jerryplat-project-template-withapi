﻿using AutoMapper;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
    public class AdminUserHelper : AdminBaseSessionHelper<AdminUser, AdminUserDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<AdminUserDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.GroupId == searchModel.Id);
            }
            if (searchModel.Id1 != -1)
            {
                bool bIsAllowedManualInventory = searchModel.Id1 == 1;
                queryableDtoList = queryableDtoList.Where(o => o.IsAllowedManualInventory == bIsAllowedManualInventory);
            }
        }

        public AdminUserDto Get(LoginModel loginModel)
        {
            var adminUserDto = Get(loginModel.Email, loginModel.Password);

            adminUserDto.IsEnableLocalStorage = SystemConfigModel.Instance.IsEnableLocalStorage;

            return adminUserDto;
        }

        public async Task<bool> ExistEmailAsync(string strEmail)
        {
            return await _Db.AdminUsers.Where(o => o.Email == strEmail).AnyAsync();
        }

        public AdminUserDto Get(string strEmail, string strPassword)
        {
            return GetQueryableList(null).Where(o => o.Email == strEmail & o.Password == strPassword).FirstOrDefault();
        }

        public async Task<AdminUserDto> GetAsync(LoginModel loginModel)
        {
            return await GetAsync(loginModel.Email, loginModel.Password);
        }

        public async Task<AdminUserDto> GetAsync(string strEmail, string strPassword)
        {
            var adminUser = await _Db.AdminUsers.Where(o => o.Email == strEmail & o.Password == strPassword).FirstOrDefaultAsync();

            if(adminUser == null)
            {
                return null;
            }

            var adminUserDto = Mapper.Map<AdminUserDto>(adminUser);

            adminUserDto.IsEnableLocalStorage = SystemConfigModel.Instance.IsEnableLocalStorage;

            return adminUserDto;
        }

        public override async Task<bool> SaveAsync(AdminUserDto modelDto)
        {
            AdminUser model = Mapper.Map<AdminUser>(modelDto);
            if (!PageHelper.IsAdd(model))
            {
                AdminUser adminUser = await GetByIdAsync<AdminUser>(model.Id);
                if (adminUser == null)
                {
                    return true;
                }

                if (adminUser.Password != model.Password)
                {
                    model.Password = EncryptHelper.Encrypt(model.Password);
                }
            }
            else
            {
                model.Password = EncryptHelper.Encrypt(model.Password);
            }

            if(model.UsedCompanyId == 0)
            {
                model.UsedCompanyId = await new DepartmentHelper().GetManagementDepartmentIdAsync(model.DepartmentId);
            }

            return await SaveAsync(model);
        }

        public async Task<bool> ChangePassword(PasswordDto model)
        {
            if (model.Password != model.Confirm)
            {
                return false;
            }

            if (IsNullSession())
            {
                return false;
            }

            AdminUser adminUser = Mapper.Map<AdminUser>(_Session);
            if (adminUser.Password != EncryptHelper.Encrypt(model.Original))
            {
                return false;
            }

            adminUser.Password = EncryptHelper.Encrypt(model.Password);

            Attach<AdminUser>(adminUser, EntityState.Modified);

            bool bIsSave = await SaveChangesAsync();
            if (bIsSave)
            {
                SetSession(adminUser);
            }
            return bIsSave;
        }
    }
}