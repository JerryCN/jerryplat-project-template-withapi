﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace JerryPlat.BLL
{
    public class NavigationHelper<TSession> : BaseSessionHelper<Navigation, NavigationDto, TSession>
        where TSession : class
    {
        private IQueryable<Navigation> GetNavigationQueryable(SiteType siteType)
        {
            return _Db.Navigations.Where(o => o.SiteType == siteType);
        }

        #region Cache

        protected List<RequestRole> GetActionRoleCacheList()
        {
            List<RequestRole> actionRoleList = CacheHelper.GetCache<List<RequestRole>>(CacheKey.ActionRole);
            if (actionRoleList == null)
            {
                actionRoleList = GetList<RequestRole>();
                CacheHelper.SetCache(actionRoleList, CacheKey.ActionRole);
            }
            return actionRoleList;
        }

        protected List<Navigation> GetButtonCacheList(SiteType siteType)
        {
            string strCacheKey = CacheKey.Navigation.ToString() + siteType.ToString();
            List<Navigation> navigationList = CacheHelper.GetCache<List<Navigation>>(strCacheKey);
            if (navigationList == null)
            {
                navigationList = GetNavigationQueryable(siteType)
                    .Where(o => o.NavigationType == NavigationType.Button & o.IsNeedAuthurized)
                    .OrderBy(o => o.OrderIndex)
                    .ToList();
                CacheHelper.SetCache(navigationList, strCacheKey);
            }
            return navigationList;
        }

        protected List<NavigationDto> GetNavigationDtoCacheList(SiteType siteType)
        {
            string strCacheKey = CacheKey.NavigationDto.ToString() + siteType.ToString();
            List<NavigationDto> navigationDtoList = CacheHelper.GetCache<List<NavigationDto>>(strCacheKey);
            if (navigationDtoList == null)
            {
                navigationDtoList = GetNavigationQueryable(siteType)
                    .ProjectTo<NavigationDto>()
                    .OrderBy(o => o.OrderIndex)
                    .ToList();
                CacheHelper.SetCache(navigationDtoList, strCacheKey);
            }
            return navigationDtoList;
        }

        #endregion Cache

        public string GetFirstPageUrl(SiteType siteType, int intGroupId)
        {
            List<Navigation> navigationList = GetList(siteType, intGroupId, NavigationType.Page);
            return GetFirstPageUrl(navigationList);
        }

        public string GetFirstPageUrl(List<Navigation> navigationList)
        {
            return navigationList.Where(o => o.ParentId > 0)
                .OrderBy(o => o.ParentId).ThenBy(o => o.OrderIndex)
                .Select(o => o.RequestUrl).FirstOrDefault();
        }

        public List<ButtonDto> GetButtonList(SiteType siteType, int intGroupId, int intNavigationId)
        {
            List<Navigation> navigationList = GetList(siteType, intGroupId, NavigationType.Button);
            return Mapper.Map<List<ButtonDto>>(navigationList.Where(o => o.ParentId == intNavigationId).ToList());
        }

        public List<Navigation> GetList(SiteType siteType, int intGroupId = 1, NavigationType navigationType = NavigationType.All)
        {
            //string strCacheKey = $"{CacheKey.Navigation.ToString()}_{siteType.ToString()}_{navigationType.ToString()}_{intGroupId}";
            List<Navigation> navigationList = null; //CacheHelper.GetCache<List<Navigation>>(strCacheKey);
            if (navigationList == null)
            {
                IQueryable<Navigation> navigationQuerable = _Db.Navigations.AsNoTracking().Where(o => o.SiteType == siteType);
                if (navigationType != NavigationType.All)
                {
                    navigationQuerable = navigationQuerable.Where(o => o.NavigationType == navigationType);
                }

                if (intGroupId == 1)
                {
                    navigationList = navigationQuerable.ToList();
                }
                else
                {
                    navigationList = (from a in _Db.Roles.Where(o => o.GroupId == intGroupId)
                                      join b in navigationQuerable on a.NavigationId equals b.Id
                                      select b).ToList();
                }
                //CacheHelper.SetCache(navigationList, strCacheKey);
            }
            return navigationList;
        }

        public List<NavigationDto> GetTreeList(SiteType siteType)
        {
            List<NavigationDto> navigationDtoList = GetNavigationDtoCacheList(siteType);
            return TypeHelper.GetTreeList(navigationDtoList);

            //List<NavigationDto> firstLayerList = navigationDtoList.Where(o => o.ParentId == 0).ToList();

            //firstLayerList.ForEach(second => {
            //    second.Children = navigationDtoList.Where(o => o.ParentId == second.Id & o.NavigationType == NavigationType.Page)
            //                    .OrderBy(o => o.OrderIndex).ToList();

            //    second.Children.ForEach(third =>
            //    {
            //        third.Children = navigationDtoList.Where(o => o.ParentId == third.Id & o.NavigationType == NavigationType.Button)
            //             .OrderBy(o => o.OrderIndex).ToList();
            //    });
            //});
            //return firstLayerList;
        }

        public string GetReturnUrl(SiteType siteType, int intGroupId, string strReturnUrl)
        {
            if (!string.IsNullOrEmpty(strReturnUrl) && strReturnUrl != "null")
            {
                strReturnUrl = HttpUtility.UrlDecode(strReturnUrl);
                if (HttpHelper.IsLocalUrl(strReturnUrl))
                {
                    return strReturnUrl;
                }
            }

            return GetFirstPageUrl(siteType, intGroupId);
        }

        public virtual bool IsValidAjaxRequest(string strRequestUrl)
        {
            return true;
        }

        protected bool IsValidAjaxRequest(SiteType siteType, int intGroupId, string strRequestUrl)
        {
            List<Navigation> allButtonList = GetButtonCacheList(siteType);
            Navigation requestNav = allButtonList.FirstOrDefault(o => Regex.IsMatch(strRequestUrl, o.RequestUrl));
            
            if (requestNav != null)
            {
                List<Navigation> roleButtonList = GetList(siteType, intGroupId, NavigationType.Button);
                if (roleButtonList.Any(o => Regex.IsMatch(strRequestUrl, o.RequestUrl)))
                {
                    return true;
                }
            }
            else
            {
                List<RequestRole> actionRoleList = GetActionRoleCacheList();
                RequestRole requestRole = actionRoleList.FirstOrDefault(o => Regex.IsMatch(strRequestUrl, o.Action));
                if (requestRole == null) {
                    return true;
                }
            }

            return false;
        }
    }

    public class AdminNavigationHelper : NavigationHelper<AdminUserDto>
    {
        public string GetReturnUrl(int intGroupId, string strReturnUrl)
        {
            return GetReturnUrl(SiteType.Admin, intGroupId, strReturnUrl);
        }

        public override bool IsValidAjaxRequest(string strRequestUrl)
        {
            return IsValidAjaxRequest(SiteType.Admin, _Session.GroupId, strRequestUrl);
        }
    }
}