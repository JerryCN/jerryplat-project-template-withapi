﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
    public class DbBackupRestoreHelper : AdminBaseSessionHelper
    {
        public async Task<PageData<DbBackupRestore>> GetPageListAsync<TKey>(SearchModel searchModel,
            Expression<Func<DbBackupRestore, TKey>> orderByKeySelector,
            PageParam pageParam,
            bool bIsAscOrder = true)
        {
            IQueryable<DbBackupRestore> dbBackupRestoreQuerable = GetDbSet<DbBackupRestore>();

            if (searchModel.StartTime.HasValue)
            {
                dbBackupRestoreQuerable = dbBackupRestoreQuerable.Where(o => o.CreateTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                dbBackupRestoreQuerable = dbBackupRestoreQuerable.Where(o => o.CreateTime <= searchModel.EndTime);
            }

            return await PageHelper.GetPageDataAsync(dbBackupRestoreQuerable, orderByKeySelector, pageParam, bIsAscOrder, searchModel.Sort);
        }

        public async Task<string> BackUpAsync(DbBackupRestore backUpInput)
        {
            string strFilePath = ($"{SystemConfigModel.Instance.BackUpDbName}_{DateTime.Now.ToFormat("yyyyMMddhhmmss")}")
                                .GetUploadFilePath("Database", "bak");
            string strResult = await DbHelper.BackUpAsync(SystemConfigModel.Instance.MasterDbConnStr
                , SystemConfigModel.Instance.DbName
                , strFilePath.ToMapPath());

            if (strResult == ConstantHelper.Ok)
            {
                DbBackupRestore model = new DbBackupRestore
                {
                    Name = backUpInput.Name,
                    BackupPath = strFilePath
                };
                await SaveAsync(model);
            }

            return strResult;
        }

        public async Task<string> RestoreAsync(int id)
        {
            DbBackupRestore model = await GetByIdAsync<DbBackupRestore>(id);
            return await RestoreAsync(model);
        }

        public async Task<string> RestoreAsync(DbBackupRestore model)
        {
            string strResult = await DbHelper.RestoreAsync(SystemConfigModel.Instance.MasterDbConnStr
                , SystemConfigModel.Instance.DbName
                , model.BackupPath.ToMapPath());

            try
            {
                DbContextHelper.Init();

                //Restore Db will kill the sysprocesses.
                ResetDbContext();

                if (!await ExistByIdAsync<DbBackupRestore>(model.Id))
                {
                    model.Id = 0;
                }

                model.RestoreCount = model.RestoreCount + 1;
                model.RestoreTime = DateTime.Now;
                await SaveAsync(model);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);

                strResult += ex.Message;
            }

            return strResult;
        }
    }
}