﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;

namespace JerryPlat.BLL
{
    public class EmployeeHelper : AdminBaseSessionHelper<Employee, EmployeeDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<EmployeeDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.UsedCompanyId == searchModel.Id);
            }

            if (searchModel.Id1 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.DepartmentId == searchModel.Id1);
            }

            if (searchModel.Id2 != -1)
            {
                bool bIsDeleted = searchModel.Id2 == 1;
                queryableDtoList = queryableDtoList.Where(o => o.IsDeleted == bIsDeleted);
            }

            if (searchModel.Id3 != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.WorkingStatusId == searchModel.Id3);
            }

            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Code.Contains(searchModel.SearchText)
                    | o.Name.Contains(searchModel.SearchText));
            }
        }

        public override async Task<string> GetCodeProfixAsync(SearchModel searchModel)
        {
            if(searchModel.Id == 0)
            {
                throw new Exception("The SearchModel is invalid for GetCodeProfixAsync method.");
            }
            Department deparemnt = await GetByIdAsync<Department>(searchModel.Id);
            return deparemnt.Code;
        }

        public override async Task<bool> SaveAsync(EmployeeDto queryableEntity)
        {
            if(queryableEntity.UsedCompanyId == 0)
            {
                queryableEntity.UsedCompanyId = await new DepartmentHelper().GetManagementDepartmentIdAsync(queryableEntity.DepartmentId);
            }

            return await base.SaveAsync(queryableEntity);
        }
    }
}