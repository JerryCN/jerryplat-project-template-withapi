﻿using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq;

namespace JerryPlat.BLL
{
    public class DepartmentHelper : AdminBaseSessionHelper<Department, DepartmentDto>
    {
        public override void SetQueryableFilter(ref IQueryable<DepartmentDto> queryableDtoList)
        {
            if (_Session.UsedCompanyId == 1) return;

            Department parent = GetById<Department>(_Session.UsedCompanyId);
            queryableDtoList = queryableDtoList.Where(x => x.Code.StartsWith(parent.Code));
        }

        public async Task<List<DepartmentDto>> GetTreeListAsync(SearchModel searchModel)
        {
            return TypeHelper.GetTreeList(await base.GetListAsync(searchModel), intParentId: GetManagementDepartmentParentId());
        }

        public async Task<List<DepartmentDto>> GetSelectListAsync(SearchModel searchModel)
        {
            return TypeHelper.GetTreeList(await base.GetListAsync(searchModel), intParentId: GetManagementDepartmentParentId(), bIsTree: false);
        }

        public override async Task<string> GetCodeProfixAsync(SearchModel searchModel)
        {
            if (searchModel.Id == 0)
            {
                return string.Empty;
            }
            Department parent = await GetByIdAsync<Department>(searchModel.Id);
            return parent.Code;
        }

        public override async Task<string> GetMaxSubCodeAsync<TEntity>(SearchModel searchModel, string strStartWith = "")
        {
            Department parent = _Db.Departments
                                .Where(o => o.ParentId == searchModel.Id)
                                .OrderByDescending(o => o.Code).FirstOrDefault();
            return await Task.FromResult<string>(parent == null ? "" : parent.Code);
        }

        public override void DeleteOnly<TEntity>(TEntity entity, DbSet<TEntity> entities = null)
        {
            Department department = entity as Department;
            switch (department.DepartmentType)
            {
                case DepartmentType.Department:
                    _Db.Employees.RemoveRange(department.DepartmentEmployees);
                    break;
                case DepartmentType.Company:
                    _Db.Employees.RemoveRange(department.CompanyEmployees);
                    break;
            }
            base.DeleteOnly<TEntity>(entity, entities);
        }

        public int GetManagementDepartmentParentId()
        {
            if (_Session == null) return 0;

            return _Session.UsedCompanyId;
        }

        public async Task<int> GetManagementDepartmentIdAsync(int intDepartmentId)
        {
            while (true)
            {
                Department department = await GetByIdAsync<Department>(intDepartmentId);
                if (department.ParentId <= 1)
                {
                    return department.Id;
                }
                intDepartmentId = department.ParentId;
            }
        }
    }
}