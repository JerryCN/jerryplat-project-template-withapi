﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;

namespace JerryPlat.BLL
{
    public class StoredAddressHelper : AdminBaseSessionHelper<StoredAddress, StoredAddressDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<StoredAddressDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.DepartmentId == searchModel.Id);
            }
            
            if (searchModel.Id1 != -1)
            {
                bool bIsEnabled = searchModel.Id1 == 1;
                queryableDtoList = queryableDtoList.Where(o => o.Enabled == bIsEnabled);
            }
            
            if (!string.IsNullOrEmpty(searchModel.SearchText))
            {
                queryableDtoList = queryableDtoList.Where(o => o.Name.Contains(searchModel.SearchText));
            }
        }

        public override async Task<bool> SaveAsync(StoredAddressDto queryableEntity)
        {
            if (queryableEntity.UsedCompanyId == 0)
            {
                queryableEntity.UsedCompanyId = await new DepartmentHelper().GetManagementDepartmentIdAsync(queryableEntity.DepartmentId);
            }

            return await base.SaveAsync(queryableEntity);
        }
    }
}