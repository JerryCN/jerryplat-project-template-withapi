﻿using JerryPlat.DAL;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;

namespace JerryPlat.BLL
{
    public class SystemHelper : AdminBaseSessionHelper
    {
        public async Task<string> ClearDataAsync()
        {
            return await DbHelper.DropDatabaseAsync(SystemConfigModel.Instance.MasterDbConnStr, _Db.Database.Connection.Database);
        }
    }
}