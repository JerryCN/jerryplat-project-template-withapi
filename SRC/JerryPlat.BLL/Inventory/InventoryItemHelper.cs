﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using AutoMapper;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.BLL
{
    public class InventoryItemHelper : AdminBaseSessionHelper<InventoryItem, InventoryItemDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<InventoryItemDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.InventoryId == searchModel.Id);
            }
        }

        public override void Changed<TEntity>(TEntity entity)
        {
            InventoryItem inventoryItem = entity as InventoryItem;
            if(inventoryItem.Id > 0)
            {
                return;
            }

            Inventory inventory = inventoryItem.Inventory;
            if (inventory == null)
            {
                inventory = GetById<Inventory>(inventoryItem.InventoryId);
            }
            
            if (inventory.InventoryStatus == InventoryStatus.Created)
            {
                inventory.InventoryStatus = InventoryStatus.Incomplete;
            }
        }

        public override async Task<bool> SaveAsync(InventoryItemDto queryableEntity)
        {
            if (queryableEntity.Id == 0)//Make sure there is only one inventory for unique location status.
            {
                InventoryItemBaseDto inventoryItemDto = await (GetQueryableList<InventoryItem,InventoryItemBaseDto>()
                    .Where(o => o.InventoryId == queryableEntity.InventoryId
                            & o.AssetId == queryableEntity.AssetId
                            & o.UsedCompanyId == queryableEntity.UsedCompanyId
                            & o.UsedDepartmentId == queryableEntity.UsedDepartmentId
                            & o.UsedEmployeeId == queryableEntity.UsedEmployeeId
                            & o.RegionId == queryableEntity.RegionId
                            & o.StoredAddressId == queryableEntity.StoredAddressId
                            & o.AssetStatus == queryableEntity.AssetStatus
                        ).FirstOrDefaultAsync());
                if (inventoryItemDto != null)
                {
                    TypeHelper.Update(queryableEntity, inventoryItemDto);
                }
            }

            return await base.SaveAsync(queryableEntity);
        }
    }
}