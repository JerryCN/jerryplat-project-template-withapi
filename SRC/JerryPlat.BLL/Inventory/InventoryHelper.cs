﻿using System.Linq;
using System.Threading.Tasks;
using JerryPlat.DAL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.Data.Entity;
using JerryPlat.Utils.Helpers;
using System;

namespace JerryPlat.BLL
{
    public class InventoryHelper : AdminBaseSessionHelper<Inventory, InventoryDto>
    {
        public override void SetSearchQueryableList(ref IQueryable<InventoryDto> queryableDtoList, SearchModel searchModel)
        {
            if (searchModel.Id != 0)
            {
                queryableDtoList = queryableDtoList.Where(o => o.InventoryStatus == (InventoryStatus)searchModel.Id);
            }

            if (searchModel.StartTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime >= searchModel.StartTime);
            }

            if (searchModel.EndTime.HasValue)
            {
                queryableDtoList = queryableDtoList.Where(o => o.CreateTime <= searchModel.EndTime);
            }
        }

        public async Task<List<InventoryStatusDto>> GetInventoryStatusReportAsync(SearchModel searchModel)
        {
            return await (from a in GetQueryableList(searchModel)
                          group a by a.InventoryStatus into g
                          select new InventoryStatusDto
                          {
                              Id = g.Key,
                              Total = g.Count()
                          }).ToListAsync();
        }

        public async Task<bool> CompleteAsync(int intInventoryId)
        {
            Inventory inventory = await GetByIdAsync<Inventory>(intInventoryId);
            if(inventory.InventoryStatus != InventoryStatus.Incomplete)
            {
                throw new Exception($"当前盘点的状态不为{EnumHelper.GetDescription(InventoryStatus.Incomplete)}, 无法完成此盘点。");
            }
            inventory.InventoryStatus = InventoryStatus.Complete;
            return await SaveChangesAsync();
        }

        public async Task<bool> SubmitInventoryAsync(int intInventoryId)
        {
            Inventory inventory = await GetByIdAsync<Inventory>(intInventoryId);
            if(inventory.InventoryStatus != InventoryStatus.Complete)
            {
                throw new Exception($"当前盘点的状态不为{EnumHelper.GetDescription(InventoryStatus.Complete)}, 无法提交此盘点。");
            }
            
            foreach (InventoryItem inventoryItem in inventory.InventoryItems)
            {
                SubmitInventoryItem(inventory, inventoryItem);
            }

            List<Asset> assetList = inventory.InventoryItems.Select(o => o.Asset).Distinct().ToList();
            foreach (Asset asset in assetList)
            {
                List<AssetItemStatus> assetItemStatusList = asset.AssetItemStatuses.Where(o => o.Id > 0).ToList();
                CascadeHelper.CascadeDelete(_Db,_Session.UsedCompanyId, _Session.Id, assetItemStatusList, true);
            }

            inventory.InventoryStatus = InventoryStatus.Submitted;
            return await SaveChangesAsync();
        }
        
        private void SubmitInventoryItem(Inventory inventory, InventoryItem inventoryItem)
        {
            AssetItemStatus assetItemStatus = inventoryItem.Asset.AssetItemStatuses
                .Where(o => o.AssetStatus == inventoryItem.AssetStatus & o.Id > 0).FirstOrDefault();
            AssetLocationItem assetLocationItem = null;
            if (assetItemStatus != null)
            {
                assetLocationItem = assetItemStatus.AssetLocationItems
                    .Where(o => o.UsedCompanyId == inventoryItem.UsedCompanyId
                                & o.UsedDepartmentId == inventoryItem.UsedDepartmentId
                                & o.RegionId == inventoryItem.RegionId
                                & o.StoredAddressId == inventoryItem.StoredAddressId
                                & o.Id > 0).FirstOrDefault();
                if ((assetLocationItem == null && inventoryItem.OldTotal != 0)
                    || (assetLocationItem != null && inventoryItem.OldTotal != assetLocationItem.Total))
                {
                    throw new Exception($"资产【{inventoryItem.Asset.Code}/{inventoryItem.Asset.Name}/{inventoryItem.Asset.Model}】数据已被修改，无法提交该盘点。");
                }
            }

            assetItemStatus = inventoryItem.Asset.AssetItemStatuses
                .Where(o => o.AssetStatus == inventoryItem.AssetStatus & o.Id == 0).FirstOrDefault();

            if (assetItemStatus == null)
            {
                assetItemStatus = new AssetItemStatus()
                {
                    AssetId = inventoryItem.AssetId,
                    AssetStatus = inventoryItem.AssetStatus
                };
                inventoryItem.Asset.AssetItemStatuses.Add(assetItemStatus);
            }
            assetItemStatus.Total += inventoryItem.NewTotal;

            assetLocationItem = assetItemStatus.AssetLocationItems
                .Where(o => o.UsedCompanyId == inventoryItem.UsedCompanyId
                            & o.UsedDepartmentId == inventoryItem.UsedDepartmentId
                            & o.RegionId == inventoryItem.RegionId
                            & o.StoredAddressId == inventoryItem.StoredAddressId
                            & o.Id > 0).FirstOrDefault();
            if (assetLocationItem == null)
            {
                assetLocationItem = new AssetLocationItem
                {
                    UsedCompanyId = inventoryItem.UsedCompanyId,
                    UsedDepartmentId = inventoryItem.UsedDepartmentId,
                    UsedEmployeeId = inventoryItem.UsedEmployeeId,
                    RegionId = inventoryItem.RegionId,
                    StoredAddressId = inventoryItem.StoredAddressId
                };
                
                assetItemStatus.AssetLocationItems.Add(assetLocationItem);
            }
            
            if(inventoryItem.AssetStatus == AssetStatus.Clear)
            {
                assetLocationItem.Total = inventoryItem.NewTotal;
                return;
            }

            int intOldTotal = inventoryItem.Asset.Total;
            
            assetLocationItem.Total = inventoryItem.NewTotal;

            inventoryItem.Asset.Total = inventoryItem.Asset.Total - inventoryItem.OldTotal + inventoryItem.NewTotal;

            string strUnitName = CascadeHelper.GetForeignTableField(_Db, "Unit", inventoryItem.Asset.UnitId).ToString();

            inventoryItem.Asset.AssetOperationRecords.Add(new AssetOperationRecord
            {
                Code = inventory.Name,
                OperationType = OperationType.Inventory,
                Description = $"资产盘点【数量】由{intOldTotal}{strUnitName}盘点为{inventoryItem.Asset.Total}{strUnitName}",
                SessionId = _Session.Id
            });
        }
    }
}