﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class AssetItemStatusController : AdminAuthurizeBaseApiController<AssetItemStatusHelper, AssetItemStatus, AssetItemStatusDto>
    {

        [HttpPost]
        public async Task<IHttpActionResult> GetAssetTotal(InventoryItemDto searchModel)
        {
            int intTotal = await _helper.GetAssetTotalAsync(searchModel);
            return Success(intTotal);
        }
    }
}