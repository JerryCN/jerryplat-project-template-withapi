﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class AssetController : AdminAuthurizeBaseApiController<AssetHelper, Asset, AssetDto>
    {
        [HttpPost]
        public async Task<IHttpActionResult> GetAssetByCode(SearchModel searchModel)
        {
            AssetBaseDto dto = await _helper.GetAssetByCodeAsync(searchModel);
            if (dto == null)
            {
                return NotFound();
            }
            return Success(dto);
        }
    }
}