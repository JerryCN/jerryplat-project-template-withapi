﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class AssetReportController : AdminAuthurizeBaseApiController<AssetReportHelper>
    {
        [HttpPost]
        public virtual async Task<IHttpActionResult> GetAssetStatusReport()
        {
            return Success(await _helper.GetAssetStatusReportAsync());
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetAssetTypeReport()
        {
            return Success(await _helper.GetAssetTypeReportAsync());
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetAssetReport(PageSearchModel searchModel)
        {
            return Success(await _helper.GetAssetReportAsync<AssetBaseDto, DateTime>(searchModel.SearchModel, o => o.CreateTime, searchModel.PageParam, false));
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetAssetOperationRecord(PageSearchModel searchModel)
        {
            return Success(await _helper.GetAssetOperationRecordAsync(searchModel.SearchModel, searchModel.PageParam));
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetAssetRecordReport(PageSearchModel searchModel)
        {
            return Success(await _helper.GetAssetRecordReportAsync<AssetReportDto, DateTime>(searchModel.SearchModel, o => o.CreateTime, searchModel.PageParam, false));
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetAssetItemReport(PageSearchModel searchModel)
        {
            return Success(await _helper.GetAssetItemReportAsync(searchModel.SearchModel, o => o.UpdateTime, searchModel.PageParam, false));
        }


        [HttpPost]
        public async Task<IHttpActionResult> ExportAssetReport(SearchModel searchModel)
        {
            string strExportFileUrl = await _helper.ExportAssetReportAsync<AssetBaseDto>(searchModel);
            return Success(strExportFileUrl);
        }

        [HttpPost]
        public async Task<IHttpActionResult> ExportAssetRecordReport(SearchModel searchModel)
        {
            string strExportFileUrl = await _helper.ExportAssetRecordReportAsync<AssetReportDto>(searchModel);
            return Success(strExportFileUrl);
        }

        [HttpPost]
        public async Task<IHttpActionResult> ExportAssetItemReport(SearchModel searchModel)
        {
            string strExportFileUrl = await _helper.ExportAssetItemReportAsync(searchModel);
            return Success(strExportFileUrl);
        }
    }
}