﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers.Cores.Roles
{
    public class DepartmentController : AdminAuthurizeBaseApiController<DepartmentHelper, Department, DepartmentDto>
    {
        [HttpPost]
        public async Task<IHttpActionResult> GetTreeList(PageSearchModel model)
        {
            return Success(await _helper.GetTreeListAsync(model.SearchModel));
        }

        [HttpPost]
        public override async Task<IHttpActionResult> GetList(SearchModel searchModel)
        {
            return Success(await _helper.GetSelectListAsync(searchModel));
        }
    }
}