﻿using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;

namespace JerryPlat.API.Controllers
{
    public class WorkingStatusController : AdminAuthurizeBaseHelperApiController<WorkingStatus, WorkingStatusDto>
    {
    }
}