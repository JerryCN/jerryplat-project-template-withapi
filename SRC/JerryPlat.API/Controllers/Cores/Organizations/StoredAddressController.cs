﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;

namespace JerryPlat.API.Controllers
{
    public class StoredAddressController : AdminAuthurizeBaseApiController<StoredAddressHelper, StoredAddress, StoredAddressDto>
    {
    }
}