﻿using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class UploadController : AdminAuthurizeBaseApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> Index(string folder)
        {
            string strFileFolder = FileHelper.GetUploadFolder(folder);

            string strLocalFileFolder = strFileFolder.ToMapPath();
            MultipartFormDataStreamProvider provider = new MultipartFormDataStreamProvider(strLocalFileFolder);
            await Request.Content.ReadAsMultipartAsync(provider);

            if(provider.FileData.Count == 0)
            {
                return Faild(MessageHelper.UploadInvalidFile);
            }
            
            MultipartFileData file = provider.FileData[0];

            //这里获取含有双引号'"'
            string strFileName = file.Headers.ContentDisposition.FileName.Trim('"');
            //获取对应文件后缀名
            string strFileExt = strFileName.Substring(strFileName.LastIndexOf('.'));
            string strPureFileName = strFileName.Substring(0, strFileName.Length - strFileExt.Length);

            FileInfo fileInfo = new FileInfo(file.LocalFileName);

            //string strNewFileName = $"{fileInfo.Name}_{strFileName}";
            string strSaveUrl = Path.Combine(strLocalFileFolder, strFileName);
            int intFileIndex = 1;
            while (IOHelper.IsExistFile(strSaveUrl))
            {
                strFileName = $"{strPureFileName}_{intFileIndex++}{strFileExt}";
                strSaveUrl = Path.Combine(strLocalFileFolder, strFileName);
            }

            fileInfo.MoveTo(strSaveUrl);

            return Success(HttpHelper.GetLocalUrl($"{strFileFolder}/{strFileName}"));
        }
    }
}