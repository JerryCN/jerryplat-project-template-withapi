﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class NavigationController : AdminAuthurizeBaseApiController<AdminNavigationHelper, Navigation, NavigationDto>
    {
        [HttpPost]
        public override Task<IHttpActionResult> GetList(SearchModel searchModel)
        {
            SiteType siteType = (SiteType)searchModel.Id;
            NavigationType navigationType = (NavigationType)searchModel.Id2;
            List<Navigation> navigationList = _helper.GetList(siteType, searchModel.Id1, navigationType);
            return Task.FromResult(Success(navigationList));
        }
    }
}