﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class GroupController : AdminAuthurizeBaseApiController<GroupHelper, Group, GroupDto>
    {
        [HttpPost]
        public IHttpActionResult GetNavigationTreeList()
        {
            return Success(new AdminNavigationHelper().GetTreeList(SiteType.Admin));
        }
    }
}