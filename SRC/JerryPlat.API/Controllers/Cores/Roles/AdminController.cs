﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class AdminController : BaseSessionApiController<AdminUserHelper, AdminUserDto>
    {
        [HttpPost]
        public async Task<IHttpActionResult> Login(LoginModel loginModel)
        {
            loginModel.Password = EncryptHelper.Encrypt(loginModel.Password);
            AdminUserDto user = await _helper.GetAsync(loginModel);
            if (user == null)
            {
                return Faild("您输入的用户名或密码错误。");
            }

            loginModel.IsFromApi = true;
            TokenModel model = await ApiHelper.Instance.GetTokenAsync(loginModel);
            if (model == null)
            {
                return Faild("获取Token失败，请稍候重试。");
            }

            LoginTokenModel<AdminUserDto> loginTokenModel = new LoginTokenModel<AdminUserDto>
            {
                TokenModel = model,
                Session = user
            };

            return Success(loginTokenModel);
        }
    }
}