﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class AdminUserController : AdminAuthurizeBaseApiController<AdminUserHelper, AdminUser, AdminUserDto>
    {
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(PasswordDto model)
        {
            bool result = await _helper.ChangePassword(model);
            return Success(result);
        }

        [HttpPost]
        public override async Task<IHttpActionResult> Add(AdminUserDto entity)
        {
            if (await _helper.ExistEmailAsync(entity.Email))
            {
                return Existed("当前邮箱已存在，请更换邮箱再次添加。");
            }

            return await base.Add(entity);
        }

        [HttpPost]
        public override async Task<IHttpActionResult> Edit(AdminUserDto entity)
        {
            if (entity.Id == 0)
            {
                return await Add(entity);
            }
            return await base.Edit(entity);
        }
    }
}