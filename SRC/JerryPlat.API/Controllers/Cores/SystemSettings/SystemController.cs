﻿using JerryPlat.BLL;
using JerryPlat.Utils.Helpers;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class SystemController : AdminAuthurizeBaseApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> ClearData()
        {
            SystemHelper helper = new SystemHelper();
            string strResult = await helper.ClearDataAsync();
            if (strResult != ConstantHelper.Ok)
            {
                return Faild(strResult);
            }
            return Restart();
        }

        [HttpPost]
        public IHttpActionResult Restart()
        {
            SiteHelper.RestartAppDomain();
            return Success();
        }
    }
}