﻿using JerryPlat.DAL;
using JerryPlat.DAL.Context;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Helpers;

namespace JerryPlat.API.Controllers
{
    #region AdminAuthurizeBaseApiController

    public class AdminAuthurizeBaseApiController : AuthurizeBaseApiController<AdminUserDto>
    {
    }

    public class AdminAuthurizeBaseApiController<THelper> : AuthurizeBaseApiController<THelper, AdminUserDto>
        where THelper : DbContextBaseHelper<JerryPlatDbContext, AdminUserDto>, new()
    {
    }

    public class AdminAuthurizeBaseApiController<THelper, TEntity, TQueryableEntity> : AuthurizeBaseApiController<THelper, TEntity, TQueryableEntity, AdminUserDto>
        where THelper : BaseSessionHelper<TEntity, TQueryableEntity, AdminUserDto>, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    {
    }

    public class AdminAuthurizeBaseApiController<THelper, TEntity> : AdminAuthurizeBaseApiController<THelper, TEntity, TEntity>
        where THelper : BaseSessionHelper<TEntity, TEntity, AdminUserDto>, new()
        where TEntity : class, new()
    {
    }

    public class AdminAuthurizeBaseHelperApiController<TEntity, TQueryableEntity> : AuthurizeBaseHelperApiController<TEntity, TQueryableEntity, AdminUserDto>
        where TEntity : class, new()
        where TQueryableEntity : class, new()
    {
    }

    public class AdminAuthurizeBaseHelperApiController<TEntity> : AdminAuthurizeBaseHelperApiController<TEntity, TEntity>
    where TEntity : class, new()
    {
    }

    #endregion AdminAuthurizeBaseApiController
}