﻿using JerryPlat.DAL;
using JerryPlat.DAL.Context;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    #region BaseSessionApiController

    public class BaseSessionApiController<TSession> : ApiController
        where TSession : class
    {
        #region Session

        private TSession _session { get; set; }
        protected TSession _Session => _session ?? (_session = GetSession());

        private TSession GetSession()
        {
            ClaimsPrincipal clainsPrincipal = User as ClaimsPrincipal;
            if (clainsPrincipal != null)
            {
                Claim claim = clainsPrincipal.Claims.Where(o => o.Type == ClaimTypes.UserData).FirstOrDefault();
                if (claim != null)
                {
                    _session = SerializationHelper.JsonToObject<TSession>(claim.Value);
                }
            }
            return null;
        }

        #endregion Session

        #region Response Result

        protected IHttpActionResult Success()
        {
            return Return(ResponseModel.Ok(""));
        }

        protected IHttpActionResult Success<T>(T data)
        {
            return Return(ResponseModel<T>.Ok(data));
        }

        protected IHttpActionResult Confirm(string strMsg)
        {
            return Return(ResponseModel.Confirm(strMsg));
        }

        protected IHttpActionResult Faild(string strMsg)
        {
            return Return(ResponseModel.Error(strMsg));
        }

        protected IHttpActionResult Invalid(string strMsg)
        {
            return Return(ResponseModel.Invalid(strMsg));
        }

        protected new IHttpActionResult NotFound()
        {
            return Return(ResponseModel.NotFound(MessageHelper.NotFound));
        }

        protected IHttpActionResult Existed(string strMsg = "")
        {
            return Return(ResponseModel.Existed(strMsg));
        }
        
        private IHttpActionResult Return<T>(ResponseModel<T> responseModel)
        {
            return Json(responseModel, SerializationHelper.DefaultJsonSerializerSettings);
            //return Ok<ResponseModel<T>>(responseModel);
        }

        #endregion Response Result
    }

    public class BaseSessionApiController<THelper, TSession> : BaseSessionApiController<TSession>
        where THelper : DbContextBaseHelper<JerryPlatDbContext, TSession>, new()
        where TSession : class
    {
        #region Helper

        private THelper helper;

        protected THelper _helper
        {
            get
            {
                if (helper == null)
                {
                    helper = new THelper();
                    helper.InitSession(this._Session);
                }
                return helper;
            }
        }

        #endregion Helper
    }

    public class BaseSessionApiController<THelper, TEntity, TQueryableEntity, TSession>
            : BaseSessionApiController<THelper, TSession>
        where THelper : BaseSessionHelper<TEntity, TQueryableEntity, TSession>, new()
        where TEntity : class, new()
        where TQueryableEntity : class, new()
        where TSession : class
    {
        #region Common Request

        [HttpPost]
        public virtual IHttpActionResult GetLockedIdList()
        {
            return Success(_helper.GetLockedIdList<TEntity>());
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetNewId()
        {
            int intMaxId = await _helper.GetNewIdAsync<TEntity>();
            return Success(intMaxId);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetCode(SearchModel searchModel)
        {
            string strCode = await _helper.GetCodeAsync(searchModel);
            return Success(strCode);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetNewOrderIndex()
        {
            int intMaxOrderIndex = await _helper.GetNewOrderIndexAsync<TEntity>();
            return Success(intMaxOrderIndex);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetPageList(PageSearchModel model)
        {
            PageData<TQueryableEntity> pageData = await _helper.GetPageListAsync(model.SearchModel, PageHelper.GetKeyExpression<TQueryableEntity, int>(new string[] { "OrderIndex", "Id" }), model.PageParam, true);
            return Success(pageData);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> GetList(SearchModel searchModel)
        {
            List<TQueryableEntity> pageData = await _helper.GetListAsync(searchModel);
            return Success(pageData);
        }

        [HttpGet]
        public virtual async Task<IHttpActionResult> GetDetail(int id)
        {
            TQueryableEntity entity = await _helper.GetByIdAsync(id);
            return Success(entity);
        }

        protected virtual async Task<IHttpActionResult> Save(TQueryableEntity entity, bool bIsForceSave = false)
        {
            if (!bIsForceSave && _helper.IsLockedRecord<TEntity>(TypeHelper.GetIdPropertyValue(entity)))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.SaveAsync(entity);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Add(TQueryableEntity entity)
        {
            return await Save(entity);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Edit(TQueryableEntity entity)
        {
            return await Save(entity);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> ForceEdit(TQueryableEntity entity)
        {
            return await Save(entity, true);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Delete(ConfirmModel<int> model)
        {
            if (_helper.IsLockedRecord<TEntity>(model.Data))
            {
                return Faild(MessageHelper.NoAction);
            }

            if (!model.IsConfirmed && _helper.ExistCascadingRelation<TEntity>())
            {
                return Confirm(MessageHelper.CascadingDelete);
            }

            bool result = await _helper.DeleteAsync(model.Data);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> DeleteList(ConfirmModel<List<int>> model)
        {
            if (_helper.IsLockedRecord<TEntity>(model.Data))
            {
                return Faild(MessageHelper.NoActionList);
            }
            if (!model.IsConfirmed && _helper.ExistCascadingRelation<TEntity>())
            {
                return Confirm(MessageHelper.CascadingDeleteList);
            }
            bool result = await _helper.DeleteListAsync(model.Data);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Enabled(int id)
        {
            if (_helper.IsLockedRecord<TEntity>(id))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.EnabledAsync<TEntity>(id);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Lock(int id)
        {
            bool result = await _helper.LockAsync<TEntity>(id, true);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> UnLock(int id)
        {
            bool result = await _helper.LockAsync<TEntity>(id, false);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> SoftDelete(int id)
        {
            if (_helper.IsLockedRecord<TEntity>(id))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusAsync<TEntity>(id, true);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> SoftDeleteList(List<int> idList)
        {
            if (_helper.IsLockedRecord<TEntity>(idList))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusListAsync<TEntity>(idList, true);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Recover(int id)
        {
            if (_helper.IsLockedRecord<TEntity>(id))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusAsync<TEntity>(id, false);
            return Success(result);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> RecoverList(List<int> idList)
        {
            if (_helper.IsLockedRecord<TEntity>(idList))
            {
                return Faild(MessageHelper.NoAction);
            }

            bool result = await _helper.ChangeDeletedStatusListAsync<TEntity>(idList, false);
            return Success(result);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Import(ImprotModel model)
        {
            if (model.IsPreImport)
            {
                return await PreImport(model);
            }

            string result = await _helper.ImportAsync(model.ExcelPath);

            if (result == ConstantHelper.Ok)
            {
                return Success();
            }
            return Faild($"{MessageHelper.ImportError}具体错误为：{result}");
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> PreImport(ImprotModel model)
        {
            if (!model.IsPreImport)
            {
                return await Import(model);
            }

            List<TEntity> importList = await _helper.PreImportAsync(model.ExcelPath);
            return Success(importList);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Export(SearchModel searchModel)
        {
            string strExportFileUrl = await _helper.ExportAsync(searchModel);
            return Success(strExportFileUrl);
        }

        #endregion Common Request
    }

    public class BaseSessionApiController<THelper, TEntity, TSession>
        : BaseSessionApiController<THelper, TEntity, TEntity, TSession>
        where THelper : BaseSessionHelper<TEntity, TEntity, TSession>, new()
        where TEntity : class, new()
        where TSession : class
    {
    }

    public class BaseSessionHelperApiController<TEntity, TQueryableEntity, TSession>
       : BaseSessionApiController<BaseSessionHelper<TEntity, TQueryableEntity, TSession>, TEntity, TQueryableEntity, TSession>
       where TEntity : class, new()
       where TQueryableEntity : class, new()
       where TSession : class
    {
    }

    public class BaseSessionHelperApiController<TEntity, TSession>
       : BaseSessionHelperApiController<TEntity, TEntity, TSession>
       where TEntity : class, new()
       where TSession : class
    {
    }

    #endregion BaseSessionApiController

    #region BaseApiController

    public class BaseApiController : BaseSessionApiController<object>
    { }

    public class BaseApiController<THelper> : BaseSessionApiController<THelper, object>
         where THelper : DbContextBaseHelper<JerryPlatDbContext, object>, new()
    { }

    public class BaseApiController<THelper, TEntity, IQueryableEntity> : BaseSessionApiController<THelper, TEntity, IQueryableEntity, object>
        where THelper : BaseSessionHelper<TEntity, IQueryableEntity, object>, new()
        where TEntity : class, new()
        where IQueryableEntity : class, new()
    { }

    public class BaseApiController<THelper, TEntity> : BaseApiController<THelper, TEntity, TEntity>
        where THelper : BaseSessionHelper<TEntity, TEntity, object>, new()
        where TEntity : class, new()
    { }

    public class BaseHelperApiController<TEntity>
       : BaseSessionHelperApiController<TEntity, object>
       where TEntity : class, new()
    { }

    #endregion BaseApiController
}