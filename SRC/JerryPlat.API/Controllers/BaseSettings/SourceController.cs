﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;

namespace JerryPlat.API.Controllers
{
    public class SourceController : AdminAuthurizeBaseApiController<SourceHelper, Source, SourceDto>
    {
    }
}