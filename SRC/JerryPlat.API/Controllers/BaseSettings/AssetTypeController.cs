﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class AssetTypeController : AdminAuthurizeBaseApiController<AssetTypeHelper, AssetType, AssetTypeDto>
    {
        [HttpPost]
        public async Task<IHttpActionResult> GetTreeList(PageSearchModel model)
        {
            return Success(await _helper.GetTreeListAsync(model.SearchModel));
        }

        [HttpPost]
        public override async Task<IHttpActionResult> GetList(SearchModel searchModel)
        {
            return Success(await _helper.GetSelectListAsync(searchModel));
        }
    }
}