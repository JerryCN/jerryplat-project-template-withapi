﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class InventoryController : AdminAuthurizeBaseApiController<InventoryHelper, Inventory, InventoryDto>
    {
        [HttpPost]
        public async Task<IHttpActionResult> GetInventoryStatusReport(SearchModel searchModel)
        {
            return Success(await _helper.GetInventoryStatusReportAsync(searchModel));
        }

        [HttpPost]
        public async Task<IHttpActionResult> Complete(int id)
        {
            return Success(await _helper.CompleteAsync(id));
        }

        [HttpPost]
        public async Task<IHttpActionResult> Submit(int id)
        {
            return Success(await _helper.SubmitInventoryAsync(id));
        }
    }
}