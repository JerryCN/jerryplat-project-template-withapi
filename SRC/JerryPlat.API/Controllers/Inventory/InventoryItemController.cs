﻿using JerryPlat.BLL;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using JerryPlat.Utils.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace JerryPlat.API.Controllers
{
    public class InventoryItemController : AdminAuthurizeBaseApiController<InventoryItemHelper, InventoryItem, InventoryItemDto>
    {
    }
}