﻿using JerryPlat.Utils.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace JerryPlat.API.App_Start.Filters
{
    public class DisposeResourceAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            SingleInstanceHelper.DisposeInstance();

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}