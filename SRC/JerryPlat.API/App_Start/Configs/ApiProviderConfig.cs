﻿using JerryPlat.API.App_Start.Providers;
using System.Web.Http;
using System.Web.Http.ValueProviders;

namespace JerryPlat.API.App_Start.Configs
{
    public class ApiProviderConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Add(typeof(ValueProviderFactory), new CookieValueProviderFactory());
            //config.Services.Replace(typeof(IHttpActionSelector), new CustomApiControllerActionSelector());
        }
    }
}