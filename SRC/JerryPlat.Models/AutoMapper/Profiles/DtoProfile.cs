﻿using AutoMapper;
using JerryPlat.Models.Db;
using JerryPlat.Models.Dto;
using System.Linq;

namespace JerryPlat.Models.AutoMapper.Profiles
{
    public class DtoProfile : Profile
    {
        public DtoProfile()
        {
            #region Asset
            #region AssetStorage
            CreateMap<AssetStorage, AssetStorageDto>()
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName))
                .ForMember(o => o.DepartmentCode, o => o.MapFrom(src => src.AdminUser.Department.Code))
                .ForMember(o => o.DepartmentName, o => o.MapFrom(src => src.AdminUser.Department.Name));
            CreateMap<AssetStorageDto, AssetStorage>();

            CreateMap<Asset, AssetBaseDto>()
                .ForMember(o => o.AssetTypeName, o => o.MapFrom(src => src.AssetType.Name))
                .ForMember(o => o.UnitName, o => o.MapFrom(src => src.Unit.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.SourceName, o => o.MapFrom(src => src.Source.Name))
                .ForMember(o => o.SupplierName, o => o.MapFrom(src => src.Supplier.Name));
            CreateMap<AssetBaseDto, Asset>();

            CreateMap<Asset, AssetDto>()
                .ForMember(o => o.AssetTypeName, o => o.MapFrom(src => src.AssetType.Name))
                .ForMember(o => o.UnitName, o => o.MapFrom(src => src.Unit.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name))
                .ForMember(o => o.SourceName, o => o.MapFrom(src => src.Source.Name))
                .ForMember(o => o.SupplierName, o => o.MapFrom(src => src.Supplier.Name));
            CreateMap<AssetDto, Asset>();

            CreateMap<Asset, AssetLocationItem>()
                .ForMember(o => o.Id, o => o.Ignore())
                .ForMember(o => o.AssetItemStatusId, o => o.Ignore());

            CreateMap<Asset, AssetReportDto>()
                .ForMember(o => o.AssetTypeName, o => o.MapFrom(src => src.AssetType.Name))
                .ForMember(o => o.UnitName, o => o.MapFrom(src => src.Unit.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.SourceName, o => o.MapFrom(src => src.Source.Name))
                .ForMember(o => o.SupplierName, o => o.MapFrom(src => src.Supplier.Name));

            CreateMap<Asset, AssetItemReportDto>()
                .ForMember(o => o.AssetTypeName, o => o.MapFrom(src => src.AssetType.Name))
                .ForMember(o => o.UnitName, o => o.MapFrom(src => src.Unit.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.SourceName, o => o.MapFrom(src => src.Source.Name))
                .ForMember(o => o.SupplierName, o => o.MapFrom(src => src.Supplier.Name));

            CreateMap<AssetItemStatus, AssetItemStatusBaseWithoutAssetDto>();
            CreateMap<AssetItemStatus, AssetItemStatusBaseDto>();
            CreateMap<AssetItemStatusBaseDto, AssetItemStatus>()
                .ForMember(o => o.Asset, o => o.Ignore());

            CreateMap<AssetItemStatus, AssetItemStatusDto>();
            CreateMap<AssetItemStatusDto, AssetItemStatus>()
                .ForMember(o => o.Asset, o => o.Ignore());

            CreateMap<AssetLocationItem, AssetLocationItemDto>()
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name));
            CreateMap<AssetLocationItemDto, AssetLocationItem>()
                .ForMember(o => o.AssetItemStatus, o => o.Ignore());

            CreateMap<AssetOtherInfo, AssetOtherInfoDto>()
                .ForMember(o => o.AssetName, o => o.MapFrom(src => src.Asset.Name));
            CreateMap<AssetOtherInfoDto, AssetOtherInfo>();
            #endregion

            #region AssetBorrowReturn
            CreateMap<AssetBorrow, AssetBorrowDto>()
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName));
            CreateMap<AssetBorrowDto, AssetBorrow>();

            CreateMap<AssetBorrowItem, AssetBorrowItemDto>();
            CreateMap<AssetBorrowItemDto, AssetBorrowItem>();

            CreateMap<AssetBorrow, AssetLocationItem>()
                .ForMember(o => o.Id, o => o.Ignore())
                .ForMember(o => o.AssetItemStatusId, o => o.Ignore())
                .ForMember(o => o.Total, o => o.Ignore());

            CreateMap<AssetBorrowReturn, AssetBorrowReturnDto>()
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<AssetBorrowReturnDto, AssetBorrowReturn>();

            CreateMap<AssetBorrowReturnItem, AssetBorrowReturnItemDto>()
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name));
            CreateMap<AssetBorrowReturnItemDto, AssetBorrowReturnItem>();

            CreateMap<AssetBorrowReturnItem, AssetLocationItem>()
                .ForMember(o => o.Id, o => o.Ignore())
                .ForMember(o => o.AssetItemStatusId, o => o.Ignore())
                .ForMember(o => o.Total, o => o.Ignore());
            #endregion

            #region AssetUsedBack
            CreateMap<AssetUsed, AssetUsedDto>()
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName));
            CreateMap<AssetUsedDto, AssetUsed>();

            CreateMap<AssetUsedItem, AssetUsedItemDto>();
            CreateMap<AssetUsedItemDto, AssetUsedItem>();

            CreateMap<AssetUsed, AssetLocationItem>()
                .ForMember(o => o.Id, o => o.Ignore())
                .ForMember(o => o.AssetItemStatusId, o => o.Ignore())
                .ForMember(o => o.Total, o => o.Ignore());

            CreateMap<AssetUsedBack, AssetUsedBackDto>()
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<AssetUsedBackDto, AssetUsedBack>();
            
            CreateMap<AssetUsedBackItem, AssetUsedBackItemDto>()
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name));
            CreateMap<AssetUsedBackItemDto, AssetUsedBackItem>();

            CreateMap<AssetUsedBackItem, AssetLocationItem>()
                .ForMember(o => o.Id, o => o.Ignore())
                .ForMember(o => o.AssetItemStatusId, o => o.Ignore())
                .ForMember(o => o.Total, o => o.Ignore());
            #endregion

            #region AssetUpdate
            CreateMap<AssetUpdate, AssetUpdateDto>()
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<AssetUpdateDto, AssetUpdate>();
            
            CreateMap<AssetUpdateItem, AssetUpdateItemDto>()
                .ForMember(o => o.AssetTypeName, o => o.MapFrom(src => src.AssetType.Name))
                .ForMember(o => o.UnitName, o => o.MapFrom(src => src.Unit.Name))
                .ForMember(o => o.SupplierName, o => o.MapFrom(src => src.Supplier.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name))
                .ForMember(o => o.SourceName, o => o.MapFrom(src => src.Source.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.AdminUser.UserName));
            CreateMap<AssetUpdateItemDto, AssetUpdateItem>()
                .ForMember(o => o.Asset, o => o.Ignore());;
            #endregion

            #region AssetFinancialUpdate
            CreateMap<AssetFinancialUpdate, AssetFinancialUpdateDto>()
                            .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                            .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                            .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                            .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<AssetFinancialUpdateDto, AssetFinancialUpdate>();

            CreateMap<AssetFinancialUpdateItem, AssetFinancialUpdateItemDto>();
            CreateMap<AssetFinancialUpdateItemDto, AssetFinancialUpdateItem>()
                .ForMember(o => o.Asset, o => o.Ignore()); ;
            #endregion

            #region AssetClear
            CreateMap<AssetClear, AssetClearDto>()
                            .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                            .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                            .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                            .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<AssetClearDto, AssetClear>();

            CreateMap<AssetClearItem, AssetClearItemDto>()
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name));
            CreateMap<AssetClearItemDto, AssetClearItem>();

            CreateMap<AssetClearItem, AssetLocationItem>()
                .ForMember(o => o.Id, o => o.Ignore())
                .ForMember(o => o.AssetItemStatusId, o => o.Ignore())
                .ForMember(o => o.Total, o => o.Ignore());
            #endregion

            #region AssetOperationRecord
            CreateMap<AssetOperationRecord, AssetOperationRecordDto>()
              .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<AssetOperationRecordDto, AssetOperationRecord>();
            #endregion

            #region AssetLabel
            CreateMap<AssetLabelBase, AssetLabelBaseDto>();
            CreateMap<AssetLabelBaseDto, AssetLabelBase>();

            CreateMap<AssetLabel, AssetLabelDto>();
            CreateMap<AssetLabelDto, AssetLabel>();

            CreateMap<AssetLabelItem, AssetLabelItemDto>();
            CreateMap<AssetLabelItemDto, AssetLabelItem>()
                .ForMember(o => o.AssetLabelBase, o => o.Ignore());
            #endregion
            #endregion

            #region Inventory
            CreateMap<Inventory, InventoryDto>()
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<InventoryDto, Inventory>();

            CreateMap<InventoryItem, InventoryItemBaseDto>();
            CreateMap<InventoryItem, InventoryItemDto>()
                .ForMember(o => o.UsedEmployeeName, o => o.MapFrom(src => src.UsedEmployee.Name))
                .ForMember(o => o.UsedCompanyName, o => o.MapFrom(src => src.UsedCompany.Name))
                .ForMember(o => o.UsedDepartmentName, o => o.MapFrom(src => src.UsedDepartment.Name))
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name))
                .ForMember(o => o.StoredAddressName, o => o.MapFrom(src => src.StoredAddress.Name))
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<InventoryItemDto, InventoryItem>()
                .ForMember(o => o.Asset, o => o.Ignore());

            CreateMap<InventoryUser, InventoryUserDto>()
                .ForMember(o => o.SessionName, o => o.MapFrom(src => src.Session.UserName));
            CreateMap<InventoryUserDto, InventoryUser>();

            CreateMap<InventoryDepartment, InventoryDepartmentDto>()
                .ForMember(o => o.DepartmentName, o => o.MapFrom(src => src.Department.Name));
            CreateMap<InventoryDepartmentDto, InventoryDepartment>();

            CreateMap<InventoryAssetType, InventoryAssetTypeDto>()
                .ForMember(o => o.AssetTypeName, o => o.MapFrom(src => src.AssetType.Name));
            CreateMap<InventoryAssetTypeDto, InventoryAssetType>();

            CreateMap<InventoryRegion, InventoryRegionDto>()
                .ForMember(o => o.RegionName, o => o.MapFrom(src => src.Region.Name));
            CreateMap<InventoryRegionDto, InventoryRegion>();
            #endregion

            #region Cores

            #region BaseSettings
            CreateMap<AssetType, AssetTypeDto>();
            CreateMap<AssetTypeDto, AssetType>();

            CreateMap<Region, RegionDto>();
            CreateMap<RegionDto, Region>();

            CreateMap<Source, SourceDto>();
            CreateMap<SourceDto, Source>();

            CreateMap<Supplier, SupplierDto>();
            CreateMap<SupplierDto, Supplier>();

            CreateMap<Unit, UnitDto>();
            CreateMap<UnitDto, Unit>();
            #endregion

            #region Roles

            CreateMap<Navigation, NavigationDto>();
            CreateMap<Navigation, ButtonDto>();

            CreateMap<AdminUser, AdminUserDto>()
                .ForMember(o => o.GroupName, o => o.MapFrom(src => src.Group.Name))
                .ForMember(o => o.DepartmentName, o => o.MapFrom(src => src.Department.Name));
            CreateMap<AdminUserDto, SessionDto>();
            CreateMap<AdminUserDto, AdminUser>();

            CreateMap<Group, GroupDto>()
                .ForMember(o => o.NavigationIdList, o => o.MapFrom(src => src.Roles.Select(role => role.NavigationId).ToList()));
            CreateMap<GroupDto, Group>();
            #endregion Roles

            #region Organizations
            CreateMap<Department, DepartmentDto>();
            CreateMap<DepartmentDto, Department>();

            CreateMap<StoredAddress, StoredAddressDto>()
                .ForMember(o => o.DepartmentName, o => o.MapFrom(src => src.Department.Name));
            CreateMap<StoredAddressDto, StoredAddress>();

            CreateMap<Employee, EmployeeDto>()
              .ForMember(o => o.CompanyName, o => o.MapFrom(src => src.Company.Name))
              .ForMember(o => o.DepartmentName, o => o.MapFrom(src => src.Department.Name))
              .ForMember(o => o.WorkingStatus, o => o.MapFrom(src => src.WorkingStatus.Name));
            CreateMap<EmployeeDto, Employee>()
                .ForMember(o => o.WorkingStatus, o => o.Ignore());
          
            CreateMap<WorkingStatus, WorkingStatusDto>();
            CreateMap<WorkingStatusDto, WorkingStatus>();
            #endregion Organizations

            #region UIs
            CreateMap<UI, UIDto>();
            #endregion

            #endregion
        }
    }
}