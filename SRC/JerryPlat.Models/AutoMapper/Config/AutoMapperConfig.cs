﻿using AutoMapper;
using JerryPlat.Models.AutoMapper.Profiles;

namespace JerryPlat.Models.AutoMapper.Config
{
    public class AutoMapperConfig
    {
        public static void Init()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<DtoProfile>();
            });
        }
    }
}