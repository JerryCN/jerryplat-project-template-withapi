﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class InventoryDto : IDto,IUsedManagementDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public int SessionId { get; set; }
        public int UsedCompanyId { get; set; }
        public string SessionName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime CreateTime { get; set; }
        public InventoryStatus InventoryStatus { get; set; }
        public string Description { get; set; }

        public List<InventoryUserDto> InventoryUsers { get; set; }
        public List<InventoryDepartmentDto> InventoryDepartments { get; set; }
        public List<InventoryAssetTypeDto> InventoryAssetTypes { get; set; }
        public List<InventoryRegionDto> InventoryRegions { get; set; }

        public InventoryDto()
        {
            DateTime now = DateTime.Now;
            StartTime = now;
            EndTime = now;
            CreateTime = now;
            InventoryStatus = InventoryStatus.Created;
            InventoryUsers = new List<InventoryUserDto>();
            InventoryDepartments = new List<InventoryDepartmentDto>();
            InventoryAssetTypes = new List<InventoryAssetTypeDto>();
            InventoryRegions = new List<InventoryRegionDto>();
        }
    }
}
