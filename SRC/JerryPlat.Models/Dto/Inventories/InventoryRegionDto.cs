﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class InventoryRegionDto : IDto
    {
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public int RegionId { get; set; }
        public string RegionName { get; set; }
    }
}
