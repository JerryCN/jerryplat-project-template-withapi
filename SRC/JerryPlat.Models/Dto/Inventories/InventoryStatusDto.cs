﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace JerryPlat.Models.Dto
{
    public class InventoryStatusDto
    {
        public InventoryStatus Id { get; set; }
        public int Total { get; set; }
    }
}
