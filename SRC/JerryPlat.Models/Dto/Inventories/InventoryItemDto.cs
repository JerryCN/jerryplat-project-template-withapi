﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;

namespace JerryPlat.Models.Dto
{
    public class InventoryItemDto : InventoryItemBaseDto
    {
        public string UsedCompanyName { get; set; }
        public string UsedDepartmentName { get; set; }
        public string UsedEmployeeName { get; set; }
        public string RegionName { get; set; }
        public string StoredAddressName { get; set; }
        public int OldTotal { get; set; }
        public int NewTotal { get; set; }
        public string SessionName { get; set; }

        public AssetBaseDto Asset { get; set; }
    }
}
