﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;

namespace JerryPlat.Models.Dto
{
    public class InventoryItemBaseDto : IDto, IUsedManagementDto
    {
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public AssetStatus AssetStatus { get; set; }
        public int AssetId { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public int RegionId { get; set; }
        public int StoredAddressId { get; set; }
        public int SessionId { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
