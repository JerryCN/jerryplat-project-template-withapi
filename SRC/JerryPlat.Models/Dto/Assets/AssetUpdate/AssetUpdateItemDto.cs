﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetUpdateItemDto : IAssetLocationDto, IDto
    {
        public int Id { get; set; }
        public int AssetUpdateId { get; set; }
        public int AssetId { get; set; }

        #region Info
        public int AssetTypeId { get; set; }
        public string AssetTypeName { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string SN { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public int UsedCompanyId { get; set; }
        public string UsedCompanyName { get; set; }
        public int UsedDepartmentId { get; set; }
        public string UsedDepartmentName { get; set; }
        public int UsedEmployeeId { get; set; }
        public string UsedEmployeeName { get; set; }
        public DateTime BuyTime { get; set; }
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public int StoredAddressId { get; set; }
        public string StoredAddressName { get; set; }
        public int UsedMonth { get; set; }
        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public string Note { get; set; }
        public string PicPath { get; set; }
        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string Contract { get; set; }
        public string ContractWay { get; set; }
        public string Director { get; set; }
        public DateTime EndDate { get; set; }
        public string Discription { get; set; }
        #endregion

        public DateTime CreateTime { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public string UpdateRecord { get; set; }

        public AssetBaseDto Asset { get; set; }
    }
}
