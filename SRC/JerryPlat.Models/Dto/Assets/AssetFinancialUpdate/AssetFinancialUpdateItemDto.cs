﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetFinancialUpdateItemDto : IDto
    {
        public int Id { get; set; }
        public int AssetUpdateId { get; set; }
        public int AssetId { get; set; }

        #region Info
        public decimal Price { get; set; }
        #endregion

        public DateTime CreateTime { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public string UpdateRecord { get; set; }

        public AssetBaseDto Asset { get; set; }
    }
}
