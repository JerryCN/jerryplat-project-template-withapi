﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
   public class AssetClearDto: IDto, IUsedManagementDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int UsedCompanyId { get; set; }
        public string UsedCompanyName { get; set; }
        public int UsedDepartmentId { get; set; }
        public string UsedDepartmentName { get; set; }
        public int UsedEmployeeId { get; set; }
        public string UsedEmployeeName { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        
        public string Description { get; set; }

        public DateTime ClearTime { get; set; }
        public DateTime CreateTime { get; set; }

        public List<AssetClearItemDto> AssetClearItems { get; set; }

        public AssetClearDto()
        {
            ClearTime = DateTime.Now;
            CreateTime = DateTime.Now;
            AssetClearItems = new List<AssetClearItemDto>();
        }
    }
}
