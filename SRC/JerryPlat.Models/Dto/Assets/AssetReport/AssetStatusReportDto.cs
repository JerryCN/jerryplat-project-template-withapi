﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetStatusReportDto
    {
        public AssetStatus AssetStatus { get; set; }
        public int Total { get; set; }
        public decimal Amount { get; set; }
    }
}
