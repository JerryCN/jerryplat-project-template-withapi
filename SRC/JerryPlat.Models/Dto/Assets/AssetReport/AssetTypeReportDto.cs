﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetTypeReportDto
    {
        public string AssetTypeName { get; set; }
        public int Total { get; set; }
        public decimal Amount { get; set; }
    }
}
