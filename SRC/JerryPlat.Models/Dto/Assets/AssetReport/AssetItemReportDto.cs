﻿using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetItemReportDto : AssetReportDto
    {
        public List<AssetItemStatusBaseWithoutAssetDto> AssetItemStatuses { get; set; }
    }
}
