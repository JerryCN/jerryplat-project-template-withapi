﻿using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetReportDto : AssetBaseDto
    {
        public List<AssetOtherInfoDto> AssetOtherInfos { get; set; }
        [Excel(Title = Title.Asset.AssetOtherInfos)]
        public string AssetOtherInfo
        {
            get
            {
                string strInfo = string.Empty;
                if (AssetOtherInfos != null && this.AssetOtherInfos.Any())
                {
                    foreach (var item in AssetOtherInfos)
                    {
                        if (!string.IsNullOrEmpty(strInfo))
                        {
                            strInfo += Template.ExcelNewLine;
                        }
                        strInfo += $"{item.Key}：{item.Value}";
                    }
                }
                return strInfo;
            }
        }

        [Excel(Title = Title.Asset.AssetOperateRecord)]
        public List<AssetOperationRecordDto> AssetOperationRecords { get; set; }
    }
}
