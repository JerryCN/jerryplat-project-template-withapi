﻿using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JerryPlat.Models.Dto
{
    public class AssetDto : AssetBaseDto
    {
        public bool IsEdited { get { return UpdateTime > CreateTime; } }
        public bool IsDeleted { get; set; }

        public List<AssetItemStatusDto> AssetItemStatuses { get; set; }
        public List<AssetOperationRecordDto> AssetOperationRecords { get; set; }
        public List<AssetBorrowItemDto> AssetBorrowItems { get; set; }
        public List<AssetBorrowReturnItemDto> AssetBorrowReturnItems { get; set; }
        public List<AssetUsedItemDto> AssetUsedItems { get; set; }
        public List<AssetUsedBackItemDto> AssetUsedBackItems { get; set; }
        public List<AssetUpdateItemDto> AssetUpdateItems { get; set; }
        public List<AssetClearItemDto> AssetClearItems { get; set; }
        public List<AssetFinancialUpdateDto> AssetFinancialUpdates { get; set; }

        

        public AssetDto()
        {
            AssetItemStatuses = new List<AssetItemStatusDto>();
            AssetOperationRecords = new List<AssetOperationRecordDto>();
            
            AssetBorrowItems = new List<AssetBorrowItemDto>();
            AssetBorrowReturnItems = new List<AssetBorrowReturnItemDto>();
            AssetUsedItems = new List<AssetUsedItemDto>();
            AssetUsedBackItems = new List<AssetUsedBackItemDto>();
            AssetUpdateItems = new List<AssetUpdateItemDto>();
            AssetClearItems = new List<AssetClearItemDto>();
            AssetFinancialUpdates = new List<AssetFinancialUpdateDto>();
        }
    }
}
