﻿using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JerryPlat.Models.Dto
{
    public class AssetBaseDto : IAssetLocationDto, ISessionDto, IUsedManagementDto
    {
        #region Base Infos
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        public int AssetStorageId { get; set; }
        [Excel(Title = Title.Asset.Code)]
        public string Code { get; set; }
        public int AssetTypeId { get; set; }
        [Excel(Title = Title.Asset.AssetType)]
        public string AssetTypeName { get; set; }
        [Excel(Title = Title.Asset.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.Asset.Model)]
        public string Model { get; set; }
        [Excel(Title = Title.Asset.SN)]
        public string SN { get; set; }
        public int UnitId { get; set; }
        [Excel(Title = Title.Asset.Total)]
        public int Total { get; set; }
        [Excel(Title = Title.Asset.Unit)]
        public string UnitName { get; set; }
        [Excel(Title = Title.Asset.Price)]
        public decimal Price { get; set; }
        public int UsedCompanyId { get; set; }
        [Excel(Title = Title.Asset.UsedCompany)]
        public string UsedCompanyName { get; set; }
        public int UsedDepartmentId { get; set; }
        [Excel(Title = Title.Asset.UsedDepartment)]
        public string UsedDepartmentName { get; set; }
        [Excel(Title = Title.Asset.BuyTime,Formatter = Title.DateFormat)]
        public DateTime BuyTime { get; set; }
        public int UsedEmployeeId { get; set; }
        [Excel(Title = Title.Asset.UsedEmployee)]
        public string UsedEmployeeName { get; set; }
        public int SessionId { get; set; }
        [Excel(Title = Title.Asset.Session)]
        public string SessionName { get; set; }
        public int RegionId { get; set; }
        [Excel(Title = Title.Asset.Region)]
        public string RegionName { get; set; }
        [Excel(Title = Title.Asset.BookkeepingTime)]
        public DateTime BookkeepingTime { get; set; }
        [Excel(Title = Title.Asset.BookkeepingVoucher)]
        public string BookkeepingVoucher { get; set; }
        public int StoredAddressId { get; set; }
        [Excel(Title = Title.Asset.StoredAddress)]
        public string StoredAddressName { get; set; }
        [Excel(Title = Title.Asset.UsedMonth)]
        public int UsedMonth { get; set; }
        public int SourceId { get; set; }
        [Excel(Title = Title.Asset.Source)]
        public string SourceName { get; set; }
        [Excel(Title = Title.Asset.Note)]
        public string Note { get; set; }
        [Excel(Title = Title.Asset.PicPath)]
        public string PicPath { get; set; }
        #endregion

        #region AssetMaintenance
        public int? SupplierId { get; set; }
        [Excel(Title = Title.Asset.Supplier)]
        public string SupplierName { get; set; }
        [Excel(Title = Title.Asset.Contract)]
        public string Contract { get; set; }
        [Excel(Title = Title.Asset.ContractWay)]
        public string ContractWay { get; set; }
        [Excel(Title = Title.Asset.Director)]
        public string Director { get; set; }
        [Excel(Title = Title.Asset.EndDate, Formatter = Title.DateFormat)]
        public DateTime EndDate { get; set; }
        [Excel(Title = Title.Asset.Discription)]
        public string Discription { get; set; }
        #endregion

        #region Date
        [Excel(Title = Title.CreateTime, Formatter = Title.DateTimeFormat)]
        public DateTime CreateTime { get; set; }
        [Excel(Title = Title.UpdateTime, Formatter = Title.DateTimeFormat)]
        public DateTime UpdateTime { get; set; }
        [Excel(Title = Title.Asset.AttachmentPath)]
        public string AttachmentPath { get; set; }
        #endregion

        public List<AssetOtherInfoDto> AssetOtherInfos { get; set; }

        [Excel(Title = Title.Asset.AssetOtherInfos)]
        public string AssetOtherInfo
        {
            get
            {
                string strInfo = string.Empty;
                if (AssetOtherInfos != null && this.AssetOtherInfos.Any())
                {
                    foreach (var item in AssetOtherInfos)
                    {
                        if (!string.IsNullOrEmpty(strInfo))
                        {
                            strInfo += Template.ExcelNewLine;
                        }
                        strInfo += $"{item.Key}：{item.Value}";
                    }
                }
                return strInfo;
            }
        }

        public AssetBaseDto()
        {
            DateTime now = DateTime.Now;
            BuyTime = now;
            CreateTime = now;
            UpdateTime = now;
            EndDate = now;
            BookkeepingTime = now;
            AssetOtherInfos = new List<AssetOtherInfoDto>();
        }
    }
}
