﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetItemStatusDto : AssetItemStatusBaseDto
    {
        public List<AssetLocationItemDto> AssetLocationItems { get; set; }

        public AssetItemStatusDto()
        {
            AssetLocationItems = new List<AssetLocationItemDto>();
        }
    }
}
