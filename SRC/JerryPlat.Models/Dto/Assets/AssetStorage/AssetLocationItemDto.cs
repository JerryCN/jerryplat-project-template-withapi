﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetLocationItemDto : IAssetLocationDto, IDto, IUsedManagementDto
    {
        public int Id { get; set; }
        public int AssetItemStatusId { get; set; }
        public int UsedCompanyId { get; set; }
        public string UsedCompanyName { get; set; }
        public int UsedDepartmentId { get; set; }
        public string UsedDepartmentName { get; set; }
        public int UsedEmployeeId { get; set; }
        public string UsedEmployeeName { get; set; }
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public int StoredAddressId { get; set; }
        public string StoredAddressName { get; set; }
        public int Total { get; set; }
        public int MaxTotal { get { return Total; } }
        
        public AssetItemStatusBaseDto AssetItemStatus { get; set; }

        public AssetLocationItemDto()
        {
            AssetItemStatus = new AssetItemStatusBaseDto();
        }
    }
}
