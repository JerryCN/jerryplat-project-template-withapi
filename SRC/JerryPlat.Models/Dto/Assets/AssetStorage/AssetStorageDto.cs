﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetStorageDto : IDto, IUsedManagementDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public int UsedCompanyId { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public DateTime StorageTime { get; set; }
        public DateTime CreateTime { get; set; }
        public string Description { get; set; }
        public StorageType StorageType { get; set; }
     
        public List<AssetBaseDto> Assets { get; set; }

        public AssetStorageDto()
        {
            StorageTime = DateTime.Now;
            CreateTime = StorageTime;
            StorageType = StorageType.Add;
            Assets = new List<AssetBaseDto>();
        }
    }
}
