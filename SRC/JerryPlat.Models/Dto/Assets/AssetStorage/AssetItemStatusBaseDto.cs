﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetItemStatusBaseDto : AssetItemStatusBaseWithoutAssetDto
    {
        public AssetBaseDto Asset { get; set; }
        public AssetItemStatusBaseDto()
        {
            Asset = new AssetBaseDto();
        }
    }
}
