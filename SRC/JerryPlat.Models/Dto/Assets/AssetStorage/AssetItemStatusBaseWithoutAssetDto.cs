﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetItemStatusBaseWithoutAssetDto : IDto,IUsedManagementDto
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public AssetStatus AssetStatus { get; set; }
        public int Total { get; set; }
        public int MaxTotal { get { return Total; } }
        public DateTime UpdateTime { get; set; }
        public int UsedCompanyId { get; set; }
    }
}
