﻿using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetOperationRecordDto : IDto
    {
        public int Id { get; set; }
        [Excel(Title = Title.AssetOperateRecord.Code)]
        public string Code { get; set; }
        public int AssetId { get; set; }
        [Excel(Title = Title.AssetOperateRecord.OperationType)]
        public OperationType OperationType { get; set; }
        [Excel(Title = Title.AssetOperateRecord.Description)]
        public string Description { get; set; }
        public int SessionId { get; set; }
        [Excel(Title = Title.AssetOperateRecord.SessionName)]
        public string SessionName { get; set; }
        [Excel(Title = Title.AssetOperateRecord.CreateTime, Formatter = Title.DateFormat)]
        public DateTime CreateTime { get; set; }

        public AssetOperationRecordDto()
        {
            this.CreateTime = DateTime.Now;
        }
    }
}
