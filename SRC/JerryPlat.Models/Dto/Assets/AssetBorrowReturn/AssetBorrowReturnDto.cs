﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetBorrowReturnDto : IDto, IUsedManagementDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int UsedCompanyId { get; set; }
        public string UsedCompanyName { get; set; }
        public int UsedDepartmentId { get; set; }
        public string UsedDepartmentName { get; set; }
        public int UsedEmployeeId { get; set; }
        public string UsedEmployeeName { get; set; }
        public DateTime ReturnTime { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }

        public List<AssetBorrowReturnItemDto> AssetBorrowReturnItems { get; set; }

        public AssetBorrowReturnDto()
        {
            ReturnTime = DateTime.Now;
            CreateTime = DateTime.Now;
            AssetBorrowReturnItems = new List<AssetBorrowReturnItemDto>();
        }
    }
}
