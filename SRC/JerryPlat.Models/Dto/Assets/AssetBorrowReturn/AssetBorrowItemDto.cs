﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
   public class AssetBorrowItemDto : IDto
    {
        public int Id { get; set; }
        public int AssetBorrowId { get; set; }
        public int AssetId { get; set; }
        public int Total { get; set; }

        public AssetBaseDto Asset { get; set; }
    }
}
