﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetBorrowDto : IAssetLocationDto, IDto, IUsedManagementDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime LendingTime { get; set; }
        public DateTime EstimatedReturnTime { get; set; }
        public int UsedCompanyId { get; set; }
        public string UsedCompanyName { get; set; }

        public int UsedDepartmentId { get; set; }
        public string UsedDepartmentName { get; set; }
        public int UsedEmployeeId { get; set; }
        public string UsedEmployeeName { get; set; }
        
        public int RegionId { get; set; }
        public string RegionName { get; set; }

        public int StoredAddressId { get; set; }
        public string StoredAddressName { get; set; }

        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }

        public List<AssetBorrowItemDto> AssetBorrowItems { get; set; }

        public AssetBorrowDto()
        {
            LendingTime = DateTime.Now;
            EstimatedReturnTime = DateTime.Now;
            CreateTime = DateTime.Now;
            AssetBorrowItems = new List<AssetBorrowItemDto>();
        }
    }
}
