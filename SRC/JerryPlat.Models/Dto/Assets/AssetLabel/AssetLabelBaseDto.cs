﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetLabelBaseDto : IDto
    {
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        [Excel(Title = Title.AssetLableBase.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.AssetLableBase.Template)]
        public string Template { get; set; }
        [Excel(Title = Title.OrderIndex)]
        public int OrderIndex { get; set; }
    }
}
