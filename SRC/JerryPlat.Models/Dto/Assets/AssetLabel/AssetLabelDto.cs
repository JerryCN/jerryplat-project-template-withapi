﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetLabelDto : IDto
    {
        public int Id { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public bool IsDefaultUsed { get; set; }

        public List<AssetLabelItemDto> AssetLabelItems { get; set; }

        public AssetLabelDto()
        {
            Width = 7d;
            Height = 5d;
            AssetLabelItems = new List<AssetLabelItemDto>();
        }
    }
}
