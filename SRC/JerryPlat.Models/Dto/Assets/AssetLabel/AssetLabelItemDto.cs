﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetLabelItemDto : IDto
    {
        public int Id { get; set; }
        public int AssetLabelId { get; set; }
        public int AssetLabelBaseId { get; set; }

        public AssetLabelBaseDto AssetLabelBase { get; set; }
    }
}
