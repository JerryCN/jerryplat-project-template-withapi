﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public interface IAssetLocationDto
    {
        int UsedCompanyId { get; set; }
        string UsedCompanyName { get; set; }
        int UsedDepartmentId { get; set; }
        string UsedDepartmentName { get; set; }
        int UsedEmployeeId { get; set; }
        string UsedEmployeeName { get; set; }
        int RegionId { get; set; }
        string RegionName { get; set; }
        int StoredAddressId { get; set; }
        string StoredAddressName { get; set; }
    }
}
