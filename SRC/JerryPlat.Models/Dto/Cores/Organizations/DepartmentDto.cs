﻿using JerryPlat.Models.Db;
using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System.Collections.Generic;

namespace JerryPlat.Models.Dto
{
    public class DepartmentDto : ITreeDto
    {
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        [Excel(Title = Title.Department.Code)]
        public string Code { get; set; }
        [Excel(Title = Title.Department.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.Department.ParentId)]
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        [Excel(Title = Title.Department.DepartmentType)]
        public DepartmentType DepartmentType { get; set; }
        [Excel(Title = Title.Enabled.Text)]
        public bool Enabled { get; set; }
        [Excel(Title = Title.IsLocked)]
        public bool IsLocked { get; set; }
        public int LayerIndex { get; set; }
        public bool IsLeaf { get; set; }
        public List<DepartmentDto> Children { get; set; }
    }
}