﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;

namespace JerryPlat.Models.Dto
{
    public class StoredAddressDto : IDto,IUsedManagementDto
    {
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        [Excel(Title = Title.StoredAddress.DepartmentId)]
        public string DepartmentName { get; set; }
        [Excel(Title = Title.StoredAddress.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.OrderIndex)]
        public int OrderIndex { get; set; }
        [Excel(Title = Title.Enabled.Text)]
        public bool Enabled { get; set; }
        [Excel(Title = Title.IsDeleted)]
        public bool IsDeleted { get; set; }
        [Excel(Title = Title.StoredAddress.UsedCompany)]
        public int UsedCompanyId { get; set; }

        public StoredAddressDto()
        {
            Enabled = true;
            IsDeleted = false;
        }
    }
}
