﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;

namespace JerryPlat.Models.Dto
{
    public class EmployeeDto : IUsedManagementDto
    {
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        [Excel(Title = Title.Employee.Code)]
        public string Code { get; set; }
        [Excel(Title = Title.Employee.Name)]
        public string Name { get; set; }
        public int UsedCompanyId { get; set; }
        [Excel(Title = Title.Employee.CompanyName)]
        public string CompanyName { get; set; }
        public int DepartmentId { get; set; }
        [Excel(Title = Title.Employee.DepartmentName)]
        public string DepartmentName { get; set; }
        [Excel(Title = Title.Employee.Phone)]
        public string Phone { get; set; }
        [Excel(Title = Title.Employee.Email)]
        public string Email { get; set; }
        public int WorkingStatusId { get; set; }
        [Excel(Title = Title.Employee.WorkingStatus)]
        public string WorkingStatus { get; set; }
        [Excel(Title = Title.IsDeleted)]
        public bool IsDeleted { get; set; }
    }
}