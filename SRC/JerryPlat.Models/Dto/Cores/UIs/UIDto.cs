﻿using JerryPlat.Models.Db;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class UIDto
    {
        public int Id { get; set; }
        public UIType UIType { get; set; }
        public string Name { get; set; }
        public string TableName { get; set; }
        public string DialogName { get; set; }
        public string PageName { get; set; }
        public string Action { get; set; }
        public int ActionWidth { get; set; }
        public string LabelWidth { get; set; }
        public string SelectionIf { get; set; }
        public string Template { get; set; }
        public string AfterSubmitCallback { get; set; }
        public List<UIRow> UIRows { get; set; }
    }
}
