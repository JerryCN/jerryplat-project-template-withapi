﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class SessionDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int UsedCompanyId { get; set; }
        public bool IsEnableLocalStorage { get; set; }
    }
}
