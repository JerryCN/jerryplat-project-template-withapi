﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Models;

namespace JerryPlat.Models.Dto
{
    public class AdminUserDto : IUsedManagementDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAllowedManualInventory { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int UsedCompanyId { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public bool IsLocked { get; set; }
        public bool IsEnableLocalStorage { get; set; }
    }
}