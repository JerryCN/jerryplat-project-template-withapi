﻿using JerryPlat.Utils.Models;
using System.Collections.Generic;

namespace JerryPlat.Models.Dto
{
    public class GroupDto : IUsedManagementDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrderIndex { get; set; }
        public bool IsLocked { get; set; }
        public int UsedCompanyId { get; set; }

        public List<int> NavigationIdList { get; set; }

        public GroupDto()
        {
            NavigationIdList = new List<int>();
        }
    }
}