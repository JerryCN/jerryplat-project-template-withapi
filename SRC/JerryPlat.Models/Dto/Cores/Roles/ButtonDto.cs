﻿namespace JerryPlat.Models.Dto
{
    public class ButtonDto
    {
        public string Code { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
    }
}