﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Dto
{
    public class AssetTypeDto : ITreeDto
    {
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        [Excel(Title = Title.AssetType.Code)]
        public string Code { get; set; }
        [Excel(Title = Title.AssetType.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.AssetType.ParentId)]
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        [Excel(Title = Title.IsLocked)]
        public bool IsLocked { get; set; }
        public int LayerIndex { get; set; }
        public bool IsLeaf { get; set; }
        public List<AssetTypeDto> Children { get; set; }

        public string DisplayName { get { return $"{Code} - {Name}"; } }
    }
}
