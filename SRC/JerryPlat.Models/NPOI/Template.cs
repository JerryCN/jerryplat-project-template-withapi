﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.NPOI
{
    public static class Template
    {
        public const string ExcelNewLine = @" , ";

        public static class Disabled
        {
            public const string True = "true";
            public const string False = "false";
        }

        public static class Rule
        {
            public const string ConfirmPassword = "Validation.ConfirmPassword";
            public const string Required = "Validation.Required";
            public const string Phone = "Validation.Phone";
            public const string Email = "Validation.Email";
            public const string Select = "Validation.Select";
            public const string Tree = "Validation.Tree";
        }

        public static class Table
        {
            public const string Action = "操作";
            public const string Phone = "";
            public const string Email = "";
        }

        public static class Form
        {
            public const string Input = @"<el-input placeholder=""{{Title}}"" v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}"" :disabled=""{{Disabled}}"" {{Event}}></el-input>";
            public const string InputWithButton = @"<el-input placeholder=""{{Title}}"" v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}"" :disabled=""{{Disabled}}""><el-button slot=""append"" :icon=""getSearchButtonIcon({{Table}}.{{DialogName}}.Model.{{Field}})"" {{Event}}>{{Append}}</el-button></el-input>";
            public const string Password = @"<el-input placeholder=""{{Title}}"" type=""password"" v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}"" :disabled=""{{Disabled}}"" {{Event}}></el-input>";
            public const string InputNumber = @"<el-input-number placeholder=""{{Title}}"" v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}"" :min=""1"" :disabled=""{{Disabled}}"" {{Event}}></el-input-number>";
            public const string Switch = @"<el-switch v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}"" :disabled=""{{Disabled}}"" {{Event}}></el-switch>";
            public const string RadioGroup =
@"<el-radio-group v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}""
                  :disabled=""{{Disabled}}"" {{Event}}>
    <el-radio v-for=""item in SelectList.{{Field}}.Data"" :label=""item.Id"">{{item.Name}}</el-radio>
</el-radio-group>";
            public const string Select =
@"<el-select v-model=""{{Table}}.{{DialogName}}.Model.{{Field}}"" placeholder=""请选择{{Title}}"" :disabled=""{{Disabled}}"" {{Event}}>
    <el-option :key=""{{DefaultValue}}"" label=""请选择{{Title}}"" :value=""{{DefaultValue}}""></el-option>
    <el-option v-for=""item in SelectList.{{Field}}.Data""
                :key=""item.Id""
                :label=""item.Name""
                :value=""item.Id""
                :disabled=""isDisabled(item)"">
        {{Description}}
    </el-option>
</el-select>";
            public const string Tree =
@"<el-tree :data=""TreeList.{{Field}}""
            class=""my-page-tree""
            show-checkbox
            node-key=""Id""
            ref=""{{Field}}Tree""
            :props=""{label: 'Name', children:'Children', disabled:'{{Disabled}}'}""
            :default-expand-all=""true""
            :default-checked-keys=""getSelectedLeafKeys(TreeList.{{Field}},{{Table}}.{{DialogName}}.Model.{{Field}})""
            @check-change=""setTreeSelectedKeys('{{Field}}Tree', {{Table}}.{{DialogName}}.Model, '{{Field}}')""
            {{Event}}>
    <span class=""el-tree-node__label"" slot-scope=""scope"">
        <i :class=""scope.data.Icon""></i>
        {{ scope.node.label }}
    </span>
</el-tree>";
        }
    }
}
