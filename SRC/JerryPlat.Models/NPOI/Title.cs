﻿namespace JerryPlat.Models.NPOI
{
    public static class Title
    {
        public const string Id = "Id";
        public const string Discription_Id = "请填写连续的数字，并以{{NewId}}开始";
        public const string OrderIndex = "排序";
        public const string CreateTime = "创建时间";
        public const string UpdateTime = "更新时间";
        public const string DateFormat = "yyyy-MM-dd";
        public const string DateTimeFormat = "yyyy-MM-dd hh:mm:ss";

        public const string IsDeleted = "是否删除";
        public const string IsLocked = "是否锁定";

        #region BaseSetting
        public static class BaseSetting
        {
            public const string Name = "名称";
            public const string UsedCompany = "使用单位";
        }
        #endregion
        
        #region Enabled
        public static class Enabled
        {
            public const string Text = "是否启用";
            public const string Description = "启用请填写1 或 True，禁用请填写0 或 False";
            public const string True = "启用";
            public const string False = "禁用";
        }
        #endregion

        #region PasswordDto
        public static class PasswordDto
        {
            public const string PageName = "密码";
            public const string Original = "原始密码";
            public const string Password = "密码";
            public const string Confirm = "确认密码";
        }
        #endregion

        #region AdminUser

        public static class AdminUser
        {
            public const string PageName = "用户";
            public const string UserName = "用户名";
            public const string Password = "密码";
            public const string Email = "邮箱";
            public const string IsAllowedManualInventory = "允许手工盘点";
            public const string GroupId = "用户组Id";
            public const string GroupName = "用户组";
            public const string DepartmentId = "所在部门Id";
            public const string DepartmentName = "所在部门";
        }

        #endregion

        #region Group
        public static class Group
        {
            public const string PageName = "角色";
            public const string Name = "角色名称";
            public const string NavigationIdList = "操作权限";
        }
        #endregion

        #region Asset
        public static class Asset
        {
            public const string PageName = "资产";
            public const string Code = "资产条码";
            public const string AssetType = "资产类别";
            public const string AssetTypeId = "资产类别Id";
            public const string Description_AssetTypeId = "请进入资产类别页面，获取资产类别Id";
            public const string Name = "资产名称";
            public const string Model = "规格型号";
            public const string SN = "资产编码";
            public const string Total = "数量";
            public const string Unit = "计量单位";
            public const string UnitId = "计量单位Id";
            public const string Description_UnitId = "请进入计量单位管理页面，获取计量单位Id";
            public const string Price = "金额";
            public const string UsedCompany = "使用单位";
            public const string UsedCompanyId = "使用单位Id";
            public const string Description_UsedCompanyId = "请先导入使用部门，获取使用单位Id";
            public const string UsedDepartment = "使用部门";
            public const string UsedDepartmentId = "使用部门Id";
            public const string Description_UsedDepartmentId = "请先导入使用部门，获取使用部门的Id";
            public const string BuyTime = "购入时间";
            public const string BookkeepingTime = "记账日期";
            public const string BookkeepingVoucher = "记账凭证";
            public const string UsedEmployee = "使用人";
            public const string UsedEmployeeId = "使用人Id";
            public const string Description_UsedEmployeeId = "请先进入员工管理页面，获取员工Id";
            public const string Session = "管理员";
            public const string SessionId = "管理员Id";
            public const string Description_SessionId = "请进入用户管理页面，获取管理员Id";
            public const string Region = "管理部门";
            public const string RegionId = "管理部门Id";
            public const string Description_RegionId = "请进入管理部门管理页面，获取管理部门Id";
            public const string StoredAddress = "存放地点";
            public const string StoredAddressId = "存放地点Id";
            public const string UsedMonth = "使用期限(月)";
            public const string Source = "来源";
            public const string SourceId = "来源Id";
            public const string Description_SourceId = "请进入来源管理页面，获取来源Id";
            public const string Note = "备注";
            public const string PicPath = "照片";
            public const string AttachmentPath = "附件";
            public const string AssetStatus = "资产状态";

            public const string Supplier = "供应商";
            public const string SupplierId = "供应商Id";
            public const string Description_SupplierId = "请进入供应商管理页面，获取供应商Id";
            public const string Contract = "联系人";
            public const string ContractWay = "联系方式";
            public const string Director = "负责人";
            public const string EndDate = "维护到期日";
            public const string Discription = "维护说明";

            public const string AssetOtherInfos = "额外信息";

            public const string AssetOperateRecord = "操作记录";
        }

        public static class AssetOperateRecord
        {
            public const string Code = "处理单号";
            public const string OperationType = "处理类型";
            public const string Description = "处理内容";
            public const string SessionName = "处理人";
            public const string CreateTime = "处理时间";
        }

        public static class AssetLableBase
        {
            public const string Name = "名称";
            public const string Template = "模板";
        }
        #endregion

        #region Department
        public static class AssetType
        {
            public const string PageName = "资产类别";
            public const string Code = "类别编码";
            public const string Name = "类别名称";
            public const string ParentId = "父级类别Id";
            public const string ParentName = "父级类别";
            public const string Description_ParentId = "如果为根类别，请填写0，否则填写根类别Id，或者填写-1";
        }

        #endregion Department

        #region Department
        public static class Department
        {
            public const string PageName = "部门";
            public const string Code = "部门编码";
            public const string Name = "部门名称";
            public const string ParentId = "父级部门Id";
            public const string ParentName = "父级部门";
            public const string Description_ParentId = "如果为根部门，请填写0，否则填写根部门Id，或者填写-1";
            public const string DepartmentType = "部门类型";
            public const string Description_DepartmentType = "单位请填写1 或 Company，部门请填写2 或 Department";
        }

        #endregion Department

        #region StoredAddress
        public static class StoredAddress
        {
            public const string DepartmentId = "部门";
            public const string Description_DepartmentId = "请进入组织结构页面，获取部门Id";
            public const string Name = "地址";
            public const string UsedCompany = "使用单位";
        }
        #endregion

        #region Employee
        public static class Employee
        {
            public const string PageName = "员工";
            public const string Code = "工号";
            public const string Name = "姓名";
            public const string CompanyId = "单位Id";
            public const string Description_CompanyId = "请先导出部门，获取单位Id";
            public const string CompanyName = "单位";
            public const string DepartmentId = "部门Id";
            public const string Description_DepartmentId = "请先导出部门，获取部门Id";
            public const string DepartmentName = "部门";
            public const string Phone = "电话";
            public const string Email = "邮箱";
            public const string WorkingStatusId = "在职状态Id";
            public const string Description_WorkingStatusId = "请进入在职状态页面获取在职状态Id";
            public const string WorkingStatus = "在职状态";
        }
        #endregion

        #region WorkingStatus
        public static class WorkingStatus
        {
            public const string Name = "名称";
            public const string OrderIndex = Title.OrderIndex;
        }
        #endregion
    }
}