﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetType : IEntity, ITreeEntity
    {
        [Key]
        [Excel(Title = Title.Id)]
        public int Id { get; set; }
        [Excel(Title = Title.AssetType.Code)]
        public string Code { get; set; }
        [StringLength(200)]
        [Excel(Title = Title.AssetType.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.IsLocked)]
        public bool IsLocked { get; set; }
        [Excel(Title = Title.AssetType.ParentId)]
        public int ParentId { get; set; }

        #region Relationship
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AssetUpdateItem> AssetUpdateItems { get; set; } 

        public virtual ICollection<InventoryAssetType> InventoryAssetTypes { get; set; }
        #endregion
    }
}
