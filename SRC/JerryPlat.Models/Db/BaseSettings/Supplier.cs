﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class Supplier : IBaseSettingEntity,IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        [StringLength(200)]
        [Excel(Title = Title.BaseSetting.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.OrderIndex)]
        public int OrderIndex { get; set; }
        [Excel(Title = Title.IsLocked)]
        public bool IsLocked { get; set; }
        [Excel(Title = Title.BaseSetting.UsedCompany)]
        [ValueMapping(Table = "Department")]
        public int UsedCompanyId { get; set; }

        #region Relationship
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AssetUpdateItem> AssetUpdateItems { get; set; } 
        #endregion
    }
}
