﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class InventoryAssetType : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public int AssetTypeId { get; set; }

        #region RelationShip
        [ForeignKey("InventoryId")]
        public virtual Inventory Inventory { get; set; }
        [ForeignKey("AssetTypeId")]
        public virtual AssetType AssetType { get; set; }
        #endregion
    }
}
