﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public enum InventoryStatus
    {
        [Description("创建盘点")]
        Created = 1,
        [Description("正在盘点")]
        Incomplete = 2,
        [Description("完成盘点")]
        Complete = 3,
        [Description("盘点入库")]
        Submitted = 4
    }

    public class Inventory : ICreateTimeEntity, ISessionEntity,IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(2000)]
        public string Note { get; set; }
        public int SessionId { get; set; }
        public int UsedCompanyId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime CreateTime { get; set; }
        public string Description { get; set; }
        public InventoryStatus InventoryStatus { get; set; }

        #region RelationShip
        [ForeignKey("SessionId")]
        public virtual AdminUser Session { get; set; }

        public virtual ICollection<InventoryUser> InventoryUsers { get; set; }
        public virtual ICollection<InventoryDepartment> InventoryDepartments { get; set; }
        public virtual ICollection<InventoryAssetType> InventoryAssetTypes { get; set; }
        public virtual ICollection<InventoryRegion> InventoryRegions { get; set; }
        public virtual ICollection<InventoryItem> InventoryItems { get; set; }
        #endregion
    }
}
