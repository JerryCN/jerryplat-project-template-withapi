﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class InventoryItem : ICreateTimeEntity, ISessionEntity
    {
        [Key]
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public int AssetId { get; set; }
        public AssetStatus AssetStatus { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public int RegionId { get; set; }
        public int StoredAddressId { get; set; }
        public int OldTotal { get; set; }
        public int NewTotal { get; set; }
        public int SessionId { get; set; }
        public DateTime CreateTime { get; set; }

        #region RelationShip
        [ForeignKey("InventoryId")]
        public virtual Inventory Inventory { get; set; }
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedInventoryItems")]
        public virtual Department UsedCompany { get; set; }
        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedInventoryItems")]
        public virtual Department UsedDepartment { get; set; }
        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
        [ForeignKey("StoredAddressId")]
        public virtual StoredAddress StoredAddress { get; set; }
        [ForeignKey("SessionId")]
        public virtual AdminUser Session { get; set; }
        #endregion
    }
}
