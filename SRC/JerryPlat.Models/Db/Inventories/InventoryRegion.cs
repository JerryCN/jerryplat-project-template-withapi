﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class InventoryRegion : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public int RegionId { get; set; }

        #region RelationShip
        [ForeignKey("InventoryId")]
        public virtual Inventory Inventory { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
        #endregion
    }
}
