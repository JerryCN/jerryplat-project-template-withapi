﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    //https://blog.csdn.net/x_craft/article/details/40347249
    public class Employee : ISoftDeletedEntity, ICodeEntity,IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Employee.Code)]
        public string Code { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Employee.Name)]
        public string Name { get; set; }

        [Required]
        [Excel(Title = Title.Employee.CompanyId)]
        [ValueMapping(Table = "Department")]
        public int UsedCompanyId { get; set; }

        [Required]
        [Excel(Title = Title.Employee.DepartmentId)]
        [ValueMapping(Table = "Department")]
        public int DepartmentId { get; set; }

        [StringLength(200)]
        [RegularExpression(RegexHelper.Phone, ErrorMessage = MessageHelper.InValidPhone)]
        [Excel(Title = Title.Employee.Phone)]
        public string Phone { get; set; }

        [StringLength(200)]
        [RegularExpression(RegexHelper.Email, ErrorMessage = MessageHelper.InValidEmail)]
        [Excel(Title = Title.Employee.Email)]
        public string Email { get; set; }

        [Required]
        [Excel(Title = Title.Employee.WorkingStatusId)]
        [ValueMapping(Table = "WorkingStatus")]
        public int WorkingStatusId { get; set; }

        public bool IsDeleted { get; set; }

        #region Relationship
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("UsedCompanyId")]
        [System.ComponentModel.DataAnnotations.Schema.InverseProperty("CompanyEmployees")]
        public virtual Department Company { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("DepartmentId")]
        [System.ComponentModel.DataAnnotations.Schema.InverseProperty("DepartmentEmployees")]
        public virtual Department Department { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("WorkingStatusId")]
        public virtual WorkingStatus WorkingStatus { get; set; }

        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AssetLocationItem> AssetLocationItems { get; set; }
        public virtual ICollection<AssetUsed> AssetUseds { get; set; }
        public virtual ICollection<AssetUsedBack> AssetUsedBacks { get; set; }
        public virtual ICollection<AssetUsedBackItem> AssetUsedBackItems { get; set; }
        public virtual ICollection<AssetBorrow> AssetBorrows { get; set; }
        public virtual ICollection<AssetBorrowReturn> AssetBorrowReturns { get; set; }
        public virtual ICollection<AssetBorrowReturnItem> AssetBorrowReturnItems { get; set; }
        public virtual ICollection<AssetUpdate> AssetUpdates { get; set; }
        public virtual ICollection<AssetUpdateItem> AssetUpdateItems { get; set; }
        public virtual ICollection<AssetFinancialUpdate> AssetFinancialUpdates { get; set; }
        public virtual ICollection<AssetClear> AssetClears { get; set; }
        public virtual ICollection<AssetClearItem> AssetClearItems { get; set; }

        public virtual ICollection<InventoryItem> InventoryItems { get; set; }
        #endregion
    }
}