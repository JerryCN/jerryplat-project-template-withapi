﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class StoredAddress : ISoftDeletedEntity, INoShowSoftDeleteEntity, IOrderIndexEntity, IEnabledEntity,IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        [Excel(Title = Title.StoredAddress.DepartmentId)]
        [ValueMapping(Table = "Department")]
        public int DepartmentId { get; set; }
        [StringLength(500)]
        [Excel(Title = Title.StoredAddress.Name)]
        public string Name { get; set; }
        [Excel(Title = Title.OrderIndex)]
        public int OrderIndex { get; set; }
        public bool Enabled { get; set; }
        public bool IsDeleted { get; set; }
        [Excel(Title = Title.StoredAddress.UsedCompany)]
        [ValueMapping(Table = "Department")]
        public int UsedCompanyId { get; set; }

        #region Relationship
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AssetLocationItem> AssetLocationItems { get; set; }
        public virtual ICollection<AssetUpdateItem> AssetUpdateItems { get; set; }
        public virtual ICollection<AssetUsed> AssetUseds { get; set; }
        public virtual ICollection<AssetUsedBackItem> AssetUsedBackItems { get; set; }
        public virtual ICollection<AssetBorrow> AssetBorrows { get; set; }
        public virtual ICollection<AssetBorrowReturnItem> AssetBorrowReturnItems { get; set; }
        public virtual ICollection<AssetClearItem> AssetClearItems { get; set; }
        public virtual ICollection<InventoryItem> InventoryItems { get; set; }
        #endregion

        public StoredAddress()
        {
            Enabled = true;
            IsDeleted = false;
        }
    }
}
