﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JerryPlat.Models.Db
{
    public enum DepartmentType
    {
        Company = 1,
        Department = 2
    }

    [UpdateSql(Sql = "update a set a.ParentId = b.Id from Department as a, Department as b where a.ParentId = -1 and b.Code = SUBSTRING(a.Code,1, len(a.Code)-3)")]
    public class Department : IEnabledEntity, ITreeEntity, ICodeEntity, ILockedEntity
    {
        [Key]
        [Excel(Title = Title.Id)]
        public int Id { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Department.Code)]
        public string Code { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Department.Name)]
        public string Name { get; set; }

        [Excel(Title = Title.Department.ParentId)]
        public int ParentId { get; set; }
        [Excel(Title = Title.Department.DepartmentType)]
        public DepartmentType DepartmentType { get; set; }
        [Excel(Title = Title.Enabled.Text)]
        public bool Enabled { get; set; }
        [Excel(Title = Title.IsLocked)]
        public bool IsLocked { get; set; }

        #region Relationship
        #region AdminUser
        public virtual ICollection<AdminUser> AdminUsers { get; set; }
        #endregion
        #region StoredAddress
        public virtual ICollection<StoredAddress> StoredAddresses { get; set; }
        #endregion

        #region Empoyee
        [InverseProperty("Company")]

        public virtual ICollection<Employee> CompanyEmployees { get; set; }
        [InverseProperty("Department")]

        public virtual ICollection<Employee> DepartmentEmployees { get; set; }
        #endregion

        #region Asset
        [InverseProperty("UsedCompany")]
        public virtual ICollection<Asset> CompanyUsedAssets { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<Asset> DepartmentUsedAssets { get; set; }
        #endregion

        #region AssetLocationItem
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetLocationItem> CompanyUsedAssetLocationItems { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetLocationItem> DepartmentUsedAssetLocationItems { get; set; }
        #endregion

        #region AssetUsed
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetUsed> CompanyUsedAssetUseds { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetUsed> DepartmentUsedAssetUseds { get; set; }
        #endregion

        #region AssetUsedBack
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetUsedBack> CompanyUsedAssetUsedBacks { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetUsedBack> DepartmentUsedAssetUsedBacks { get; set; }
        #endregion

        #region AssetUsedBackItem
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetUsedBackItem> CompanyUsedAssetUsedBackItems { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetUsedBackItem> DepartmentUsedAssetUsedBackItems { get; set; }
        #endregion

        #region Borrow
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetBorrow> CompanyUsedAssetBorrows { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetBorrow> DepartmentUsedAssetBorrows { get; set; }
        #endregion

        #region BorrowReturn
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetBorrowReturn> CompanyUsedAssetBorrowReturns { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetBorrowReturn> DepartmentUsedAssetBorrowReturns { get; set; }
        #endregion

        #region BorrowReturnItem
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetBorrowReturnItem> CompanyUsedAssetBorrowReturnItems { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetBorrowReturnItem> DepartmentUsedAssetBorrowReturnItems { get; set; }
        #endregion

        #region AssetUpdate
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetUpdate> CompanyUsedAssetUpdates { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetUpdate> DepartmentUsedAssetUpdates { get; set; }
        #endregion 

        #region AssetUpdateItem
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetUpdateItem> CompanyUsedAssetUpdateItems { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetUpdateItem> DepartmentUsedAssetUpdateItems { get; set; }
        #endregion

        #region AssetFinancialUpdate
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetFinancialUpdate> CompanyUsedAssetFinancialUpdates { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetFinancialUpdate> DepartmentUsedAssetFinancialUpdates { get; set; }
        #endregion

        #region AssetClear
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetClear> CompanyUsedAssetClears { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetClear> DepartmentUsedAssetClears { get; set; }
        #endregion

        #region AssetClearItem
        [InverseProperty("UsedCompany")]
        public virtual ICollection<AssetClearItem> CompanyUsedAssetClearItems { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<AssetClearItem> DepartmentUsedAssetClearItems { get; set; }
        #endregion

        #region InventoryDepartment
        public virtual ICollection<InventoryDepartment> InventoryDepartments { get; set; }
        #endregion

        #region InventoryItem
        [InverseProperty("UsedCompany")]
        public virtual ICollection<InventoryItem> CompanyUsedInventoryItems { get; set; }
        [InverseProperty("UsedDepartment")]
        public virtual ICollection<InventoryItem> DepartmentUsedInventoryItems { get; set; }
        #endregion
        #endregion

        public Department()
        {
            this.DepartmentType = DepartmentType.Company;
        }
    }
}