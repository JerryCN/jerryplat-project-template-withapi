﻿using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class WorkingStatus : IBaseSettingEntity
    {
        [Key]
        public int Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        public int OrderIndex { get; set; }
        public bool IsLocked { get; set; }

        #region Relationship
        public virtual ICollection<Employee> Employees { get; set; } 
        #endregion
    }
}