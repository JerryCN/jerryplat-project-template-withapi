﻿using JerryPlat.Utils.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class Group : ILockedEntity, IOrderIndexEntity,IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        public int OrderIndex { get; set; }

        public bool IsLocked { get; set; }
        public int UsedCompanyId { get; set; }

        #region Relationship
        public virtual ICollection<AdminUser> AdminUsers { get; set; }
        public virtual ICollection<Role> Roles { get; set; } 
        #endregion
    }
}