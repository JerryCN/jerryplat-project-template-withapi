﻿using JerryPlat.Utils.Models;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class RequestRole : IEntity
    {
        [Key]
        public int Id { get; set; }

        [StringLength(200)]
        public string Action { get; set; }
    }
}