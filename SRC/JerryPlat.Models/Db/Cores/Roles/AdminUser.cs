﻿using JerryPlat.Utils.Helpers;
using JerryPlat.Utils.Models;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JerryPlat.Models.Db
{
    public class AdminUser : ILockedEntity, IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        [RegularExpression(RegexHelper.Email, ErrorMessage = MessageHelper.InValidEmail)]
        public string Email { get; set; }

        [Required]
        [StringLength(200)]
        public string UserName { get; set; }

        [Required]
        [StringLength(200)]
        public string Password { get; set; }

        public bool IsAllowedManualInventory { get; set; }

        [Required]
        public int GroupId { get; set; }

        public int UsedCompanyId { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        public bool IsLocked { get; set; }

        #region Relationship
        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }
        public virtual ICollection<AssetStorage> AssetStorages { get; set; }
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AssetBorrow> AssetBorrows { get; set; }
        public virtual ICollection<AssetBorrowReturn> AssetBorrowReturns { get; set; }
        public virtual ICollection<AssetUsed> AssetUseds { get; set; }
        public virtual ICollection<AssetUsedBack> AssetUsedBacks { get; set; }
        public virtual ICollection<AssetClear> AssetClears { get; set; }
        public virtual ICollection<AssetUpdate> AssetUpdates { get; set; }
        public virtual ICollection<AssetFinancialUpdate> AssetFinancialUpdates { get; set; }
        public virtual ICollection<AssetOperationRecord> AssetOperationRecords { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
        public virtual ICollection<InventoryUser> InventoryUsers { get; set; }
        #endregion
    }
}