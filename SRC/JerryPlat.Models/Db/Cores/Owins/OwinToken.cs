﻿using JerryPlat.Utils.Models;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class OwinToken : IEntity
    {
        [Key]
        public int Id { get; set; }

        [StringLength(200)]
        public string ClientId { get; set; }

        [StringLength(200)]
        public string ClientSecret { get; set; }
    }
}