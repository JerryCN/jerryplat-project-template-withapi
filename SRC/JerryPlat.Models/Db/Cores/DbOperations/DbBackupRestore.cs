﻿using JerryPlat.Utils.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class DbBackupRestore : IEntity
    {
        [Key]
        public int Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string BackupPath { get; set; }

        public int RestoreCount { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? RestoreTime { get; set; }

        public DbBackupRestore()
        {
            CreateTime = DateTime.Now;
        }
    }
}