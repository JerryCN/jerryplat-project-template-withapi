﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class UIRow : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int UIId { get; set; }
        [StringLength(200)]
        public string FieldName { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        /// <summary>
        /// Table: Default true
        /// </summary>
        public bool IsOrder { get; set; }
        /// <summary>
        /// Table, Edit
        /// </summary>
        [StringLength(1000)]
        public string Template { get; set; }
        /// <summary>
        /// Edit
        /// </summary>
        [StringLength(200)]
        public string Rules { get; set; }
        public int OrderIndex { get; set; }
        /// <summary>
        /// Export
        /// Edit: For Select Option Template
        /// </summary>
        [StringLength(200)]
        public string Description { get; set; }
        /// <summary>
        /// Edit: For Input Append Slot
        /// </summary>
        [StringLength(200)]
        public string AppendText { get; set; }
        /// <summary>
        /// Table: Default auto
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Edit: Default Emply
        /// </summary
        public string Event { get; set; }
        /// <summary>
        /// Edit: Default 0
        /// </summary
        public int DefaultValue { get; set; }
        /// <summary>
        /// Edit: Default "false"
        /// </summary
        public string Disabled { get; set; }

        #region Relationship
        [ForeignKey("UIId")]
        public virtual UI UI { get; set; } 
        #endregion

        public UIRow()
        {
            this.IsOrder = true;
            this.Disabled = "false";
        }
    }
}
