﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public enum UIType
    {
        Table = 1,
        Import = 2,
        Edit = 3,
        Page = 4,
    }

    public class UI : IEntity
    {
        [Key]
        public int Id { get; set; }
        public UIType UIType { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        /// <summary>
        /// Table: Default "List"
        /// Edit: Default "List"
        /// Page: Default "List"
        /// Import: Default "Import"
        /// </summary>
        [StringLength(200)]
        public string TableName { get; set; }
        /// <summary>
        /// Edit: Default "ModelDialog"
        /// Page: Default "ModelDialog"
        /// Import: Default "ModelDialog"
        /// </summary>
        [StringLength(200)]
        public string DialogName { get; set; }
        /// <summary>
        /// Edit
        /// </summary>
        [StringLength(200)]
        public string PageName { get; set; }
        /// <summary>
        /// Table: Default "Edit,Delete"
        /// Import：Default "List"
        /// </summary>
        [StringLength(200)]
        public string Action { get; set; }
        /// <summary>
        /// Table: Default 200
        /// </summary>
        public int ActionWidth { get; set; }
        /// <summary>
        /// Table: Default "Common.FormLabelWidth"
        /// </summary>
        public string LabelWidth { get; set; }
        /// <summary>
        /// Table: Default "existButton('DeleteList')"
        /// </summary>
        [StringLength(200)]
        public string SelectionIf { get; set; }
        /// <summary>
        /// Export
        /// </summary>
        [StringLength(200)]
        public string Template { get; set; }
        /// <summary>
        /// Edit: Default "function(){getPageList('{{Table}}')}"
        /// </summary>
        [StringLength(200)]
        public string AfterSubmitCallback { get; set; }

        #region MyRegion
        public virtual ICollection<UIRow> UIRows { get; set; } 
        #endregion

        public UI()
        {
            Action = "Edit,Delete";
            ActionWidth = 200;
            SelectionIf = "existButton('DeleteList')";
            AfterSubmitCallback = "function(){getPageList('{{Table}}')}";
        }

        public UI(UIType uiType) : this()
        {
            this.UIType = uiType;
            this.DialogName = "ModelDialog";
            switch (this.UIType)
            {
                case UIType.Table:
                case UIType.Page:
                    this.TableName = "List";
                    break;
                case UIType.Edit:
                    this.LabelWidth = "Common.FormLabelWidth";
                    this.TableName = "List";
                    break;
                case UIType.Import:
                    this.TableName = "Import";
                    this.Action = "List";
                    break;

            }
        }
    }
}
