﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public enum OperationType
    {
        [Description("资产入库")]
        Create = 1,
        [Description("资产信息变更")]
        Update = 2,
        [Description("资产财务信息变更")]
        FinancialUpdate = 3,
        [Description("资产借出")]
        Borrow = 4,
        [Description("资产归还")]
        BorrowReturn = 5,
        [Description("资产领用")]
        Used = 6,
        [Description("资产退库")]
        UsedBack = 7,
        [Description("资产清理报废")]
        Clear = 8,
        [Description("资产盘点")]
        Inventory = 9
    }

    public class AssetOperationRecord : IEntity, ICreateTimeEntity, ISessionEntity
    {
        [Key]
        public int Id { get; set; }
        [StringLength(200)]
        public string Code { get; set; }
        public int AssetId { get; set; }
        public OperationType OperationType { get; set; }
        [StringLength(2000)]
        public string Description { get; set; }
        public int SessionId { get; set; }
        public DateTime CreateTime { get; set; }

        #region Relationship
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        [ForeignKey("SessionId")]
        public virtual AdminUser Session { get; set; }
        #endregion

        public AssetOperationRecord()
        {
            CreateTime = DateTime.Now;
        }
    }
}
