﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetBorrowReturnItem : IAssetLocation, IEntity, IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetBorrowReturnId { get; set; }
        public int AssetId { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public int RegionId { get; set; }
        public int StoredAddressId { get; set; }
        public int Total { get; set; }

        #region Relationship
        [ForeignKey("AssetBorrowReturnId")]
        public virtual AssetBorrowReturn AssetBorrowReturn { get; set; }
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetBorrowReturnItems")]
        public virtual Department UsedCompany { get; set; }

        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetBorrowReturnItems")]
        public virtual Department UsedDepartment { get; set; }

        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }

        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }

        [ForeignKey("StoredAddressId")]
        public virtual StoredAddress StoredAddress { get; set; }
        #endregion
    }
}
