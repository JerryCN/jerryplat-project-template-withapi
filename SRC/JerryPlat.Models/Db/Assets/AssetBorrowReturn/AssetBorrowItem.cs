﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
   public class AssetBorrowItem : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetBorrowId { get; set; }
        public int AssetId { get; set; }
        public int Total { get; set; }

        #region Relationship
        [ForeignKey("AssetBorrowId")]
        public virtual AssetBorrow AssetBorrow { get; set; }
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        #endregion
    }
}
