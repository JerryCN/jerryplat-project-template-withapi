﻿using JerryPlat.Utils;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetBorrowReturn : ISessionEntity, ICodeEntity, ICreateTimeEntity, IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        [StringLength(200)]
        public string Code { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public DateTime ReturnTime { get; set; }
        [StringLength(2000)]
        public string Description { get; set; }
        public int SessionId { get; set; }
        public DateTime CreateTime { get; set; }

        #region Relationship
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetBorrowReturns")]
        public virtual Department UsedCompany { get; set; }

        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetBorrowReturns")]

        public virtual Department UsedDepartment { get; set; }
        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }
        [ForeignKey("SessionId")]
        public virtual AdminUser Session { get; set; }

        public virtual ICollection<AssetBorrowReturnItem> AssetBorrowReturnItems { get; set; } 
        #endregion

    }
}
