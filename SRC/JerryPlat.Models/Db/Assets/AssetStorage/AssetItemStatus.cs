﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public enum AssetStatus
    {
        [Description("闲置")]
        Free = 1,
        [Description("借用")]
        Borrow = 2,
        [Description("在用")]
        Used = 3,
        [Description("报废")]
        Clear = 4
    }

    public class AssetItemStatus : IEntity,IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetId { get; set; }
        public AssetStatus AssetStatus { get; set; }
        public int Total { get; set; }
        public DateTime UpdateTime { get; set; }
        public int UsedCompanyId { get; set; }

        #region Relationship
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }

        public virtual List<AssetLocationItem> AssetLocationItems { get; set; } 
        #endregion
        public AssetItemStatus()
        {
            this.AssetStatus = AssetStatus.Free;
            this.UpdateTime = DateTime.Now;
            this.AssetLocationItems = new List<AssetLocationItem>();
        }
    }
}
