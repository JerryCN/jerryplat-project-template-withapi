﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using Microsoft.CSharp.RuntimeBinder;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public class Asset : IAssetLocation, ISessionEntity, ICreateTimeEntity, IUpdateTimeEntity, INoShowSoftDeleteEntity, ICodeEntity, IUsedManagementEntity
    {
        #region Base Infos
        [Key]
        public int Id { get; set; }
        public int AssetStorageId { get; set; }
        [StringLength(200)]
        [Excel(Title = Title.Asset.Code)]
        public string Code { get; set; }

        [Excel(Title = Title.Asset.AssetTypeId)]
        [UpdateLog(Title = Title.Asset.AssetType, ForeignTable = "AssetType")]
        [ValueMapping(Table = "AssetType")]
        public int AssetTypeId { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.Name)]
        [UpdateLog(Title = Title.Asset.Name)]
        public string Name { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.Model)]
        [UpdateLog(Title = Title.Asset.Model)]
        public string Model { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.SN)]
        [UpdateLog(Title = Title.Asset.SN)]
        public string SN { get; set; }

        [Excel(Title = Title.Asset.UnitId)]
        [UpdateLog(Title = Title.Asset.Unit, ForeignTable = "Unit")]
        [ValueMapping(Table = "Unit")]
        public int UnitId { get; set; }

        [Excel(Title = Title.Asset.Total)]
        public int Total { get; set; }

        [Excel(Title = Title.Asset.Price)]
        [UpdateLog(Title = Title.Asset.Price)]
        public decimal Price { get; set; }

        [Excel(Title = Title.Asset.UsedCompanyId)]
        [UpdateLog(Title = Title.Asset.UsedCompany, ForeignTable = "Department")]
        [ValueMapping(Table = "Department")]
        public int UsedCompanyId { get; set; }

        [Excel(Title = Title.Asset.UsedDepartmentId)]
        [UpdateLog(Title = Title.Asset.UsedDepartment, ForeignTable = "Department")]
        [ValueMapping(Table = "Department")]
        public int UsedDepartmentId { get; set; }

        [Excel(Title = Title.Asset.BuyTime, Formatter = Title.DateFormat)]
        [UpdateLog(Title = Title.Asset.BuyTime, Formatter = Title.DateFormat)]
        public DateTime BuyTime { get; set; }

        [Excel(Title = Title.Asset.UsedEmployeeId)]
        [UpdateLog(Title = Title.Asset.UsedEmployee, ForeignTable = "Employee")]
        [ValueMapping(Table = "Employee")]
        public int UsedEmployeeId { get; set; }

        public int SessionId { get; set; }

        [Excel(Title = Title.Asset.RegionId)]
        [UpdateLog(Title = Title.Asset.Region, ForeignTable = "Region")]
        [ValueMapping(Table = "Region")]
        public int RegionId { get; set; }

        [Excel(Title = Title.Asset.BookkeepingTime, Formatter = Title.DateFormat)]
        [UpdateLog(Title = Title.Asset.BookkeepingTime, Formatter = Title.DateFormat)]
        public DateTime BookkeepingTime { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.BookkeepingVoucher)]
        [UpdateLog(Title = Title.Asset.PicPath)]
        public string BookkeepingVoucher { get; set; }

        [Excel(Title = Title.Asset.StoredAddressId)]
        [UpdateLog(Title = Title.Asset.StoredAddress, ForeignTable = "StoredAddress")]
        [ValueMapping(Table = "StoredAddress")]
        public int StoredAddressId { get; set; }

        [Excel(Title = Title.Asset.UsedMonth)]
        [UpdateLog(Title = Title.Asset.UsedMonth)]
        public int UsedMonth { get; set; }

        [Excel(Title = Title.Asset.SourceId)]
        [UpdateLog(Title = Title.Asset.Source, ForeignTable = "Source")]
        [ValueMapping(Table = "Source")]
        public int SourceId { get; set; }

        [StringLength(2000)]
        [Excel(Title = Title.Asset.Note)]
        [UpdateLog(Title = Title.Asset.Note)]
        public string Note { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.PicPath)]
        [UpdateLog(Title = Title.Asset.PicPath)]
        public string PicPath { get; set; }
        #endregion

        #region AssetMaintenance
        [Excel(Title = Title.Asset.SupplierId)]
        [UpdateLog(Title = Title.Asset.Supplier, ForeignTable = "Supplier")]
        [ValueMapping(Table = "Supplier")]
        public Nullable<int> SupplierId { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.Contract)]
        [UpdateLog(Title = Title.Asset.Contract)]
        public string Contract { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.ContractWay)]
        [UpdateLog(Title = Title.Asset.ContractWay)]
        public string ContractWay { get; set; }

        [StringLength(200)]
        [Excel(Title = Title.Asset.Director)]
        [UpdateLog(Title = Title.Asset.Director)]
        public string Director { get; set; }

        [Excel(Title = Title.Asset.EndDate, Formatter = Title.DateFormat)]
        [UpdateLog(Title = Title.Asset.EndDate, Formatter = Title.DateFormat)]
        public DateTime EndDate { get; set; }

        [StringLength(2000)]
        [Excel(Title = Title.Asset.Discription)]
        [UpdateLog(Title = Title.Asset.Discription)]
        public string Discription { get; set; }

        [StringLength(200)]
        [UpdateLog(Title = Title.Asset.PicPath)]
        public string AttachmentPath { get; set; }
        #endregion

        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public bool IsDeleted { get; set; }

        #region Relationship

        #region Base Infos
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("AssetStorageId")]
        public virtual AssetStorage AssetStorage { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("AssetTypeId")]
        public virtual AssetType AssetType { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("UnitId")]
        public virtual Unit Unit { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("UsedCompanyId")]
        [System.ComponentModel.DataAnnotations.Schema.InverseProperty("CompanyUsedAssets")]
        public virtual Department UsedCompany { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("UsedDepartmentId")]
        [System.ComponentModel.DataAnnotations.Schema.InverseProperty("DepartmentUsedAssets")]
        public virtual Department UsedDepartment { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("SessionId")]
        public virtual AdminUser AdminUser { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("StoredAddressId")]
        public virtual StoredAddress StoredAddress { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("SourceId")]
        public virtual Source Source { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }
        #endregion

        public virtual ICollection<AssetItemStatus> AssetItemStatuses { get; set; }
        public virtual ICollection<AssetOtherInfo> AssetOtherInfos { get; set; }
        public virtual ICollection<AssetBorrowItem> AssetBorrowItems { get; set; }
        public virtual ICollection<AssetBorrowReturnItem> AssetBorrowReturnItems { get; set; }
        public virtual ICollection<AssetUsedItem> AssetUsedItems { get; set; }
        public virtual ICollection<AssetUsedBackItem> AssetUsedBackItems { get; set; }
        public virtual ICollection<AssetUpdateItem> AssetUpdateItems { get; set; }
        public virtual ICollection<AssetFinancialUpdate> AssetFinancialUpdates { get; set; }
        public virtual ICollection<AssetFinancialUpdateItem> AssetFinancialUpdateItems { get; set; }
        public virtual ICollection<AssetClearItem> AssetClearItems { get; set; }
        public virtual ICollection<AssetOperationRecord> AssetOperationRecords { get; set; }
        public virtual ICollection<InventoryItem> InventoryItems { get; set; }
        #endregion

        public Asset()
        {
            DateTime now = DateTime.Now;
            BuyTime = now;
            CreateTime = now;
            UpdateTime = now;
            EndDate = now;
            PicPath = "/File/Defaut/NoPic.jpg";
            IsDeleted = false;
        }   
    }
}
