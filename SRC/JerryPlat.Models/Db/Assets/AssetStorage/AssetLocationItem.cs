﻿using JerryPlat.Utils.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JerryPlat.Models.Db
{
    public class AssetLocationItem : IAssetLocation, IEntity, IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetItemStatusId { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public int RegionId { get; set; }
        public int StoredAddressId { get; set; }
        public int Total { get; set; }

        #region Relationship
        [ForeignKey("AssetItemStatusId")]
        public virtual AssetItemStatus AssetItemStatus { get; set; }
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetLocationItems")]
        public virtual Department UsedCompany { get; set; }
        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetLocationItems")]
        public virtual Department UsedDepartment { get; set; }
        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
        [ForeignKey("StoredAddressId")]
        public virtual StoredAddress StoredAddress { get; set; } 
        #endregion
    }
}
