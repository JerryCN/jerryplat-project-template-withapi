﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetOtherInfo : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetId { get; set; }
        [Required]
        [StringLength(200)]
        public string Key { get; set; }
        [Required]
        [StringLength(200)]
        public string Value { get; set; }

        #region Relationship
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; } 
        #endregion
    }
}
