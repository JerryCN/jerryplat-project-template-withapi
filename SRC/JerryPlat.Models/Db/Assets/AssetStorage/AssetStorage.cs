﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Models.Db
{
    public enum StorageType
    {
        Add = 1,
        Import = 2
    }

    public class AssetStorage : ICreateTimeEntity, ISessionEntity, ICodeEntity, IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        public int UsedCompanyId { get; set; }
        [StringLength(200)]
        public string Code { get; set; }
        public int SessionId { get; set; }
        public DateTime StorageTime { get; set; }
        public DateTime CreateTime { get; set; }
        [StringLength(2000)]
        public string Description { get; set; }
        public StorageType StorageType { get; set; }

        #region Relationship
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("SessionId")]
        public virtual AdminUser AdminUser { get; set; }

        public virtual ICollection<Asset> Assets { get; set; } 
        #endregion

        public AssetStorage()
        {
            StorageTime = DateTime.Now;
            CreateTime = StorageTime;
            StorageType = StorageType.Add;
        }
    }
}
