﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
   public class AssetUsedItem : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetUsedId { get; set; }
        public int AssetId { get; set; }
        public int Total { get; set; }

        #region Relationship
        [ForeignKey("AssetUsedId")]
        public virtual AssetUsed AssetUsed { get; set; }
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        #endregion
    }
}
