﻿using JerryPlat.Utils;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
   public class AssetUsedBack : ICreateTimeEntity, ISessionEntity, ICodeEntity, IUsedManagementEntity
    {
        [Key]
        [Owner]
        public int Id { get; set; }
        [StringLength(200)]
        [Owner]
        public string Code { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        [StringLength(2000)]
        [Owner]
        public string Description { get; set; }
        [Owner]
        public DateTime BackTime { get; set; }
        [Owner]
        public int SessionId { get; set; }
        [Owner]
        public DateTime CreateTime { get; set; }

        #region Relationship
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetUsedBacks")]
        public virtual Department UsedCompany { get; set; }

        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetUsedBacks")]

        public virtual Department UsedDepartment { get; set; }
        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }
        [ForeignKey("SessionId")]
        public virtual AdminUser Session { get; set; }

        public virtual ICollection<AssetUsedBackItem> AssetUsedBackItems { get; set; } 
        #endregion
    }
}
