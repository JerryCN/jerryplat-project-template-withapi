﻿using JerryPlat.Utils;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetUsed : IAssetLocation, ISessionEntity, ICodeEntity, ICreateTimeEntity, IUsedManagementEntity
    {
        [Key]
        [Owner]
        public int Id { get; set; }
        [StringLength(200)]
        [Owner]
        public string Code { get; set; }
        [Owner]
        public DateTime UsedTime { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public int RegionId { get; set; }
        public int StoredAddressId { get; set; }
        [Owner]
        public DateTime EstimatedReturnTime { get; set; }
        [Owner]
        public int SessionId { get; set; }
        [StringLength(2000)]
        [Owner]
        public string Description { get; set; }
        [Owner]
        public DateTime CreateTime { get; set; }

        #region Relationship
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetUseds")]
        public virtual Department UsedCompany { get; set; }

        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetUseds")]
        public virtual Department UsedDepartment { get; set; }

        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }

        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }

        [ForeignKey("StoredAddressId")]
        public virtual StoredAddress StoredAddress { get; set; }

        [ForeignKey("SessionId")]
        public virtual AdminUser AdminUser { get; set; }

        public virtual ICollection<AssetUsedItem> AssetUsedItems { get; set; } 
        #endregion
    }
}
