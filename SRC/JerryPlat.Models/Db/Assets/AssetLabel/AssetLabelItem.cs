﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetLabelItem : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int AssetLabelId { get; set; }
        public int AssetLabelBaseId { get; set; }

        #region RelationShip
        [ForeignKey("AssetLabelId")]
        public virtual AssetLabel AssetLabel { get; set; }
        [ForeignKey("AssetLabelBaseId")]
        public virtual AssetLabelBase AssetLabelBase { get; set; }
        #endregion
    }
}
