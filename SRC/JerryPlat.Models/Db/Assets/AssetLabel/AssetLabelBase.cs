﻿using JerryPlat.Models.NPOI;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetLabelBase : IOrderIndexEntity
    {
        [Key]
        public int Id { get; set; }
        [Excel(Title = Title.AssetLableBase.Name)]
        [StringLength(200)]
        public string Name { get; set; }
        [Excel(Title = Title.AssetLableBase.Template)]
        [StringLength(200)]
        public string Template { get; set; }
        [Excel(Title = Title.OrderIndex)]
        public int OrderIndex { get; set; }

        #region RelationShip
        public virtual ICollection<AssetLabelItem> AssetLabelItems { get; set; }
        #endregion
    }
}
