﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetLabel : IDefaultUsedEntity
    {
        [Key]
        public int Id { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public bool IsDefaultUsed { get; set; }

        #region RelationShip
        public virtual ICollection<AssetLabelItem> AssetLabelItems { get; set; }
        #endregion
    }
}
