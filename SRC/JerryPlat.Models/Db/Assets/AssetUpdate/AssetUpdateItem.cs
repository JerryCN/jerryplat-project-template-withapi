﻿using JerryPlat.Utils;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetUpdateItem : IAssetLocation, IEntity
    {
        [Key]
        [Owner]
        public int Id { get; set; }
        [Owner]
        public int AssetUpdateId { get; set; }
        [Owner]
        public int AssetId { get; set; }

        #region Info
        public int AssetTypeId { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(200)]
        public string Model { get; set; }
        [StringLength(200)]
        public string SN { get; set; }
        public int UnitId { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        public DateTime BuyTime { get; set; }
        public int RegionId { get; set; }
        public int StoredAddressId { get; set; }
        public int UsedMonth { get; set; }
        public int SourceId { get; set; }
        [StringLength(2000)]
        public string Note { get; set; }
        [StringLength(200)]
        public string PicPath { get; set; }
        public Nullable<int>SupplierId { get; set; }
        [StringLength(200)]
        public string Contract { get; set; }
        [StringLength(200)]
        public string ContractWay { get; set; }
        [StringLength(200)]
        public string Director { get; set; }
        public DateTime EndDate { get; set; }
        [StringLength(2000)]
        public string Discription { get; set; }
        #endregion

        [Owner]
        public DateTime CreateTime { get; set; }
        [Owner]
        public int SessionId { get; set; }
        [Owner]
        [StringLength(2000)]
        public string UpdateRecord { get; set; }

        #region Relationship
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        [ForeignKey("SessionId")]
        public virtual AdminUser AdminUser { get; set; }

        [ForeignKey("AssetTypeId")]
        public virtual AssetType AssetType { get; set; }
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetUpdateItems")]
        public virtual Department UsedCompany { get; set; }
        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetUpdateItems")]
        public virtual Department UsedDepartment { get; set; }
        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }

        [ForeignKey("UnitId")]
        public virtual Unit Unit { get; set; }
        [ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
        [ForeignKey("SourceId")]
        public virtual Source Source { get; set; }

        [ForeignKey("StoredAddressId")]
        public virtual StoredAddress StoredAddress { get; set; }

        [ForeignKey("AssetUpdateId")]
        public virtual AssetUpdate AssetUpdate { get; set; } 
        #endregion
    }
}
