﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public interface IAssetLocation
    {
        int UsedCompanyId { get; set; }
        int UsedDepartmentId { get; set; }
        int UsedEmployeeId { get; set; }
        int RegionId { get; set; }
        int StoredAddressId { get; set; }

        Department UsedCompany { get; set; }
        Department UsedDepartment { get; set; }
        Employee UsedEmployee { get; set; }
        Region Region { get; set; }
        StoredAddress StoredAddress { get; set; }
    }
}
