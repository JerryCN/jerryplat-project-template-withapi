﻿using JerryPlat.Utils;
using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetFinancialUpdateItem : IEntity
    {
        [Key]
        [Owner]
        public int Id { get; set; }
        [Owner]
        public int AssetFinancialUpdateId { get; set; }
        [Owner]
        public int AssetId { get; set; }

        #region Info
        public decimal Price { get; set; }
        #endregion

        [Owner]
        public DateTime CreateTime { get; set; }
        [Owner]
        public int SessionId { get; set; }
        [Owner]
        [StringLength(2000)]
        public string UpdateRecord { get; set; }

        #region Relationship
        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }
        [ForeignKey("SessionId")]
        public virtual AdminUser AdminUser { get; set; }
        [ForeignKey("AssetFinancialUpdateId")]
        public virtual AssetFinancialUpdate AssetFinancialUpdate { get; set; } 
        #endregion
    }
}
