﻿using JerryPlat.Utils;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Models.Db
{
    public class AssetFinancialUpdate : ICreateTimeEntity, ISessionEntity, ICodeEntity, IUsedManagementEntity
    {
        [Key]
        public int Id { get; set; }
        [StringLength(200)]
        public string Code { get; set; }
        public int UsedCompanyId { get; set; }
        public int UsedDepartmentId { get; set; }
        public int UsedEmployeeId { get; set; }
        [StringLength(2000)]
        public string Description { get; set; }
        public int SessionId { get; set; }
        public DateTime CreateTime { get; set; }

        #region Relationship
        [ForeignKey("UsedCompanyId")]
        [InverseProperty("CompanyUsedAssetFinancialUpdates")]
        public virtual Department UsedCompany { get; set; }

        [ForeignKey("UsedDepartmentId")]
        [InverseProperty("DepartmentUsedAssetFinancialUpdates")]
        public virtual Department UsedDepartment { get; set; }

        [ForeignKey("UsedEmployeeId")]
        public virtual Employee UsedEmployee { get; set; }

        [ForeignKey("SessionId")]
        public virtual AdminUser Session { get; set; }

        public virtual ICollection<AssetFinancialUpdateItem> AssetFinancialUpdateItems { get; set; }
        #endregion
    }
}
