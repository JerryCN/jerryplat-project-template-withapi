﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

namespace ESUPER.Code.Common.DbHelper
{
    public class SpParam
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class SqlServerHelper
    {
        private const int Command_Timeout = 600;

        public delegate T RunConn<T>(SqlConnection conn);

        public delegate T RunCmd<T>(SqlCommand cmd);

        public delegate T RunDataReader<T>(SqlDataReader dataReader);

        public delegate T RunDataAdapter<T>(SqlDataAdapter dataAdapter);

        public static string ConnStr = null;

        #region base functions

        public static T RunSqlByConn<T>(RunConn<T> runConn, string strConnStr = "")
        {
            if (string.IsNullOrEmpty(strConnStr))
            {
                strConnStr = ConnStr;
            }

            using (var conn = new SqlConnection(strConnStr))
            {
                try
                {
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    var ret = runConn(conn);

                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                    return ret;
                }
                catch (Exception ex)
                {
                    return default(T);
                }
            }
        }

        public static T RunSqlByCmd<T>(string strSql, RunCmd<T> runCmd, string strConnStr = "")
        {
            return RunSqlByConn<T>(conn =>
            {
                var ret = default(T);
                using (var cmd = new SqlCommand(strSql, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    ret = runCmd(cmd);
                }
                return ret;
            }, strConnStr);
        }

        public static T RunSqlByCmd<T>(string storedProcName, IDataParameter[] parameters, RunCmd<T> runCmd, string strConnStr = "")
        {
            return RunSqlByConn<T>(conn =>
            {
                var ret = default(T);
                using (var cmd = new SqlCommand(storedProcName, conn))
                {
                    cmd.CommandTimeout = Command_Timeout;
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter parameter in parameters)
                    {
                        if (parameter != null)
                        {
                            if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) && (parameter.Value == null))
                            {
                                parameter.Value = DBNull.Value;
                            }
                            cmd.Parameters.Add(parameter);
                        }
                    }
                    ret = runCmd(cmd);
                }
                return ret;
            }, strConnStr);
        }
        
        public static T RunSqlByDataReader<T>(string storedProcName, IDataParameter[] parameters, RunDataReader<T> runDataReader, string strConnStr = "")
        {
            return RunSqlByCmd<T>(storedProcName, parameters, cmd =>
            {
                var ret = default(T);
                using (var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    ret = runDataReader(dr);
                }
                return ret;
            }, strConnStr);
        }

        public static T RunSqlByDataAdapter<T>(string strSql, RunDataAdapter<T> runDataAdapter, string strConnStr = "")
        {
            return RunSqlByCmd<T>(strSql, cmd =>
            {
                var ret = default(T);
                using (var da = new SqlDataAdapter(cmd))
                {
                    ret = runDataAdapter(da);
                }
                return ret;
            }, strConnStr);
        }

        public static T RunSqlByDataAdapter<T>(string storedProcName, IDataParameter[] parameters, RunDataAdapter<T> runDataAdapter, string strConnStr = "")
        {
            return RunSqlByCmd<T>(storedProcName, parameters, cmd =>
            {
                var ret = default(T);
                using (var da = new SqlDataAdapter(cmd))
                {
                    ret = runDataAdapter(da);
                }
                return ret;
            }, strConnStr);
        }

        #endregion base functions

        public static int ExecuteNonQuery(string strSql, string strConnStr = "")
        {
            return RunSqlByCmd<int>(strSql, cmd => { return cmd.ExecuteNonQuery(); }, strConnStr);
        }

        public static int ExecuteNonQuery(string storedProcName, IDataParameter[] para, string strConnStr = "")
        {
            return RunSqlByCmd<int>(storedProcName, para, cmd => { return cmd.ExecuteNonQuery(); }, strConnStr);
        }
        
        public static DataSet GetList(string query, string TableName, string strConnStr = "")
        {
            return RunSqlByDataAdapter<DataSet>(query, dataAdapter =>
            {
                var ds = new DataSet();
                dataAdapter.Fill(ds, TableName);
                return ds;
            }, strConnStr);
        }

        public static DataSet GetList(string storedProcName, IDataParameter[] para, string strConnStr = "")
        {
            return RunSqlByDataAdapter<DataSet>(storedProcName, para, dataAdapter =>
            {
                var ds = new DataSet();
                dataAdapter.Fill(ds);
                return ds;
            }, strConnStr);
        }

        public static DataTable GetDataTable(string query, string strConnStr = "")
        {
            return RunSqlByDataAdapter<DataTable>(query, dataAdapter =>
            {
                var dt = new DataTable();
                dataAdapter.Fill(dt);
                return dt;
            }, strConnStr);
        }
    }
}