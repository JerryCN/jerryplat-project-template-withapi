﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Helpers
{
    public static class RegexHelper
    {
        public const string Number = @"^\d+$";
        public const string Email = @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string Phone = @"^0?(13|14|15|17|18|19)[0-9]{9}$";

        public static Regex RegexNumber = new Regex(Number, RegexOptions.Compiled);
        public static Regex RegexReplace = new Regex(@"{{\w+}}", RegexOptions.Compiled);
        public static Regex RegexRouteParam = new Regex(@"{[^}]+}", RegexOptions.Compiled);

        public static bool IsNumberOnly(string strContent)
        {
            return RegexNumber.IsMatch(strContent);
        }
        
        public static MatchCollection Matches(string strTemplate, string strRegex)
        {
            return Regex.Matches(strTemplate, strRegex);
        }

        public static void Matches(string strTemplate, string strRegex, Action<Match> callback)
        {
            MatchCollection matches = Matches(strTemplate, strRegex);
            foreach (Match match in matches)
            {
                callback(match);
            }
        }
        
        public static string[] SplitCamelCase(string input)
        {
            return Regex.Replace(input, "([A-Z])", " $1", RegexOptions.Compiled).Trim().Split(' ');
        }
    }
}