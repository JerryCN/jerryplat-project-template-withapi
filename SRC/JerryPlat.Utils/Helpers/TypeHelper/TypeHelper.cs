﻿using JerryPlat.Utils.Attributes;
using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

namespace JerryPlat.Utils.Helpers
{
    public static class TypeHelper
    {
        public static TInstance GetInstance<TInstance>(string strInstanceName = "Instance")
            where TInstance : class, new()
        {
            TInstance instance = new TInstance();
            Type type = instance.GetType();
            FieldInfo[] fields = type.GetFields(BindingFlags.Static | BindingFlags.Public);
            FieldInfo field = fields.Where(o => o.Name == strInstanceName).FirstOrDefault();
            if (field == null)
            {
                return default(TInstance);
            }

            object objInstance = field.GetValue(instance);
            if (objInstance is TInstance)
            {
                return objInstance as TInstance;
            }
            return null;
        }

        #region Script

        public static string GetScript<T>(string strObjectName) where T : class, new()
        {
            return GetScript<T>(strObjectName, new T());
        }

        public static string GetScript<T>(string strObjectName, T instance) where T : class
        {
            string strScript = "var " + strObjectName + " = {";

            Type type = instance.GetType();
            FieldInfo[] fields = type.GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (var field in fields)
            {
                strScript += $"{field.Name}:\"{field.GetValue(instance).ToString()}\",";
            }
            strScript = strScript.TrimEnd(',');

            strScript += "};";

            return strScript;
        }

        public static string GetEnumScript<T>()
        {
            string strScript = "[";
            Type type = typeof(T);
            FieldInfo[] fieldInfoList = type.GetFields(BindingFlags.Static | BindingFlags.Public);
            foreach (FieldInfo fieldInfo in fieldInfoList)
            {
                if (strScript != "[")
                {
                    strScript += ",";
                }
                int intValue = (int)Enum.Parse(type, fieldInfo.Name);
                strScript += $"{{Id:{intValue},Name:\"{EnumHelper.GetDescription(fieldInfo, fieldInfo.Name)}\",FieldName:\"{fieldInfo.Name}\"}}";
            }
            return strScript + "]";
        }

        #endregion Script

        #region Watch

        public static string GetVueWatch<TModel>()
             where TModel : class, new()
        {
            string strScript = "(function(table, initEvent, ignoreList){";
            strScript += "function getEvent(name, bIsString){if(ignoreList==null || !_.includes(name)){return ";
            //https://www.lodashjs.com/docs/4.17.5.html#debounce
            strScript += "bIsString?getDebounceEvent(initEvent):initEvent";
            strScript += ";}return helper.default.Callback;}";
            strScript += "var watch = {};";

            DoModel<TModel>(prop =>
            {
                if (prop.HasAttribute<IgnoreWatchAttribute>())
                {
                    return;
                }

                string strIsString = prop.PropertyType.Name == typeof(string).Name ? "true" : "false";

                strScript += $"watch[table+'.SearchModel.{prop.Name}']=getEvent('{prop.Name}',{strIsString});";
            });

            strScript += "return watch;";

            strScript += "})(table, initEvent, ignoreList)";

            return strScript;
        }

        #endregion Watch

        #region Reflection
        public static Dictionary<Type, bool> _dicGenericType = new Dictionary<Type, bool>();
        public static bool IsExistGenericTypeDefinition<TModel>()
        {
            Type type = typeof(TModel);

            if (!_dicGenericType.Keys.Contains(type))
            {
                _dicGenericType.Add(type, type.GetProperties().Any(o => o.PropertyType.IsGenericType
                & o.PropertyType.IsGenericTypeDefinition));
            }
            return _dicGenericType[type];
        }

        public static bool HasAttribute<TAttribute>(this PropertyInfo propInfo)
            where TAttribute : Attribute
        {
            return propInfo.GetCustomAttribute<TAttribute>() != null;
        }

        public static MethodInfo GetMethodInfo<TEntity>(string strKey)
        {
            Type type = typeof(TEntity);
            return GetMethodInfo(type, strKey);
        }

        public static MethodInfo GetMethodInfo(this Type type, string strKey)
        {
            MethodInfo[] methodInfos = type.GetMethods();
            MethodInfo methodInfo = methodInfos.Where(o => o.Name == strKey).FirstOrDefault();
            if (methodInfo == null)
            {
                throw new Exception("Can not find the " + strKey + " method in [" + type.Name + "].");
            }
            return methodInfo;
        }

        public static object GetCustomAttributeData(IEnumerable<CustomAttributeData> attrs, string strCustomAttributeName)
        {
            CustomAttributeData attr = attrs.Where(o => o.AttributeType.Name == strCustomAttributeName)
                .FirstOrDefault();
            if (attr == null || !attr.ConstructorArguments.Any())
            {
                return null;
            }
            return attr.ConstructorArguments.First().Value;
        }

        public static object GetCustomAttributeData(this Type type, string strCustomAttributeName)
        {
            IEnumerable<CustomAttributeData> attrs = type.CustomAttributes;
            return GetCustomAttributeData(attrs, strCustomAttributeName);
        }

        public static object GetCustomAttributeData(this Type type, string strMethod, string strCustomAttributeName)
        {
            MethodInfo methodInfo = GetMethodInfo(type, strMethod);
            IEnumerable<CustomAttributeData> attrs = methodInfo.CustomAttributes;
            return GetCustomAttributeData(attrs, strCustomAttributeName);
        }

        public static PropertyInfo GetPropertyInfo(Type type, string strKey, bool bIsDefaultPrimitive = true)
        {
            return GetPropertyInfo(type, new string[] { strKey }, bIsDefaultPrimitive);
        }

        public static PropertyInfo GetPropertyInfo(PropertyInfo[] props, string strKey, bool bIsDefaultPrimitive = true)
        {
            return GetPropertyInfo(props, new string[] { strKey }, bIsDefaultPrimitive);
        }

        public static PropertyInfo GetPropertyInfo<TEntity>(string strKey, bool bIsDefaultPrimitive = true) where TEntity : class
        {
            return GetPropertyInfo<TEntity>(new string[] { strKey }, bIsDefaultPrimitive);
        }

        public static PropertyInfo GetPropertyInfo<TEntity>(string[] aryStrKey, bool bIsDefaultPrimitive = true) where TEntity : class
        {
            Type type = typeof(TEntity);
            return GetPropertyInfo(type, aryStrKey, bIsDefaultPrimitive);
        }

        public static PropertyInfo GetPropertyInfo(Type type, string[] aryStrKey, bool bIsDefaultPrimitive = true)
        {
            PropertyInfo[] props = type.GetProperties();
            return GetPropertyInfo(props, aryStrKey, bIsDefaultPrimitive);
        }

        public static PropertyInfo GetPropertyInfo(PropertyInfo[] props, string[] aryStrKey, bool bIsDefaultPrimitive = true)
        {
            PropertyInfo prop = null;
            foreach (string strKey in aryStrKey)
            {
                prop = props.FirstOrDefault(o => o.Name.ToUpper() == strKey.ToUpper());
                if (prop != null)
                {
                    return prop;
                }
            }

            if (bIsDefaultPrimitive)
            {
                foreach (var propTemp in props)
                {
                    if (propTemp.PropertyType.IsPrimitive)
                    {
                        return propTemp;
                    }
                }
            }

            return null;
        }

        public static PropertyInfo GetIdPropertyInfo<TEntity>() where TEntity : class
        {
            return GetPropertyInfo<TEntity>("Id");
        }

        public static TProperty GetPropertyValue<TEntity, TProperty>(TEntity entity, string strKey)
            where TEntity : class
        {
            if (entity == null)
            {
                return default(TProperty);
            }

            PropertyInfo propertyInfo = GetPropertyInfo<TEntity>(strKey);

            var objValue = propertyInfo.GetValue(entity);
            if (objValue == null)
            {
                return default(TProperty);
            }

            return (TProperty)propertyInfo.GetValue(entity);
        }

        public static int GetIdPropertyValue<TEntity>(TEntity entity)
            where TEntity : class
        {
            return GetPropertyValue<TEntity, int>(entity, "Id");
        }

        public static int GetUsedCompanyIdPropertyValue<TEntity>(TEntity entity)
            where TEntity : class
        {
            return GetPropertyValue<TEntity, int>(entity, "UsedCompanyId");
        }

        public static TModel InitModel<TModel>(Func<PropertyInfo, object> funcValueProvider) where TModel : class, new()
        {
            TModel model = new TModel();

            DoModel<TModel>(prop =>
            {
                object objValue = funcValueProvider(prop);
                if (objValue != null)
                {
                    prop.SetValue(model, objValue);
                }
            });

            return model;
        }

        public static TModel InitModel<TModel>(Func<string, object> funcValueProvider) where TModel : class, new()
        {
            return InitModel<TModel>(prop =>
            {
                object objValue = funcValueProvider(prop.Name);
                if (prop.PropertyType == objValue.GetType())
                {
                    return objValue;
                }
                if (prop.PropertyType == typeof(bool))
                {
                    return ToBoolen(objValue.ToString());
                }

                try
                {
                    return Convert.ChangeType(objValue, prop.PropertyType);
                }
                catch (Exception ex)
                {
                    LogHelper.Error(ex);
                    return null;
                }
            });
        }

        public static TModel InitModel<TModel>(Dictionary<string, string> keyValueList) where TModel : class, new()
        {
            return InitModel<TModel>(prop =>
            {
                if (!keyValueList.Keys.Contains(prop.Name))
                {
                    return null;
                }
                return Convert.ChangeType(keyValueList[prop.Name], prop.PropertyType);
            });
        }

        public static void DoModel<TModel>(Action<PropertyInfo> funcPropProvider) where TModel : class, new()
        {
            Type type = typeof(TModel);
            DoModel(type, funcPropProvider);
        }

        public static void DoModel(Type type, Action<PropertyInfo> funcPropProvider)
        {
            PropertyInfo[] props = type.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                funcPropProvider(prop);
            }
        }

        public static void Update<TModel>(TModel source, TModel update, List<string> ignoreProps = null)
             where TModel : class, new()
        {
            DoModel<TModel>(prop =>
            {
                if (ignoreProps != null && ignoreProps.Contains(prop.Name))
                {
                    return;
                }
                prop.SetValue(source, prop.GetValue(update));
            });
        }

        public static string Update<TModel, TUpdate>(TModel source, TUpdate update, List<string> ignoreProps = null,
                Func<TModel, TUpdate, PropertyInfo, PropertyInfo, object, object, string> updateCallback = null)
            where TModel : class, new()
            where TUpdate : class, new()

        {
            PropertyInfo[] props = typeof(TUpdate).GetProperties();
            string strUpdateRecord = string.Empty;
            DoModel<TModel>(sourceProp =>
            {
                if (ignoreProps != null && ignoreProps.Contains(sourceProp.Name))
                {
                    return;
                }
                if ((sourceProp.PropertyType.IsClass && sourceProp.PropertyType.Name != typeof(string).Name)
                                    || (sourceProp.PropertyType.IsGenericType && !sourceProp.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                                    )
                {
                    return;
                }
                var updateProp = GetPropertyInfo(props, sourceProp.Name, false);
                if (updateProp != null)
                {
                    if (updateProp.HasAttribute<OwnerAttribute>())
                    {
                        return;
                    }

                    object objSourceValue = sourceProp.GetValue(source);
                    object objUpdateValue = updateProp.GetValue(update);

                    if (!IsSameObject(objSourceValue, objUpdateValue))
                    {
                        string strRecord = updateCallback?.Invoke(source, update, sourceProp, updateProp, objSourceValue, objUpdateValue);
                        if (!string.IsNullOrEmpty(strRecord))
                        {
                            strUpdateRecord += strRecord + ";" + Environment.NewLine;
                        }

                        sourceProp.SetValue(source, updateProp.GetValue(update));
                    }
                }
            });
            return strUpdateRecord;
        }

        public static bool IsSameObject(object objSourceValue, object objUpdateValue)
        {
            if (objSourceValue == null && objUpdateValue == null)
            {
                return true;
            }

            if ((objSourceValue == null && objUpdateValue != null)
                        || (objSourceValue != null && objUpdateValue == null)
                        || !objSourceValue.Equals(objUpdateValue))
            {
                return false;
            }

            return true;
        }

        public static string GetUpdateRecord<TModel, TUpdate>(TModel source, TUpdate update,
                PropertyInfo sourceProp, PropertyInfo updateProp,
                object objSourceValue, object objUpdateValue,
                Func<string, int, string, string> foreignCallback)
            where TModel : class, new()
            where TUpdate : class, new()
        {
            string strUpdateRecord = string.Empty;
            UpdateLogAttribute updateLogAttr = sourceProp.GetCustomAttribute<UpdateLogAttribute>();
            if (updateLogAttr == null)
            {
                return strUpdateRecord;
            }

            string strSourceValue = string.Empty;
            string strUpdateValue = string.Empty;
            if (objSourceValue is DateTime)
            {
                strSourceValue = ((DateTime)objSourceValue).ToString(updateLogAttr.Formatter);
                strUpdateValue = ((DateTime)objUpdateValue).ToString(updateLogAttr.Formatter);
            }
            else if (!string.IsNullOrEmpty(updateLogAttr.ForeignTable))
            {
                strSourceValue = objSourceValue == null ? GetUpdateValue(objSourceValue) : foreignCallback(updateLogAttr.ForeignTable, (int)objSourceValue, updateLogAttr.ForeignField);
                strUpdateValue = objUpdateValue == null ? GetUpdateValue(objUpdateValue) : foreignCallback(updateLogAttr.ForeignTable, (int)objUpdateValue, updateLogAttr.ForeignField);
            }
            else
            {
                strSourceValue = GetUpdateValue(objSourceValue);
                strUpdateValue = GetUpdateValue(objUpdateValue);
            }

            strUpdateRecord += $@"【{updateLogAttr.Title}】由{strSourceValue}变更为{strUpdateValue}";
            return strUpdateRecord;
        }

        private static string GetUpdateValue(object objValue)
        {
            if (objValue == null)
            {
                return "<空>";
            }

            string strValue = objValue.ToString();

            if (objValue is decimal && strValue.EndsWith(".00"))
            {
                strValue = strValue.Substring(0, strValue.Length - 3);
            }

            return $@"{strValue}";
        }

        #endregion Reflection

        #region Fill Content

        public static string FillContent<TModel>(TModel model, string strTemplate)
        {
            return FillContent(model, strTemplate, "{{", @"\w+", "}}");
        }

        public static string FillContent<TModel>(TModel model, string strTemplate, string strStartWith, string strField, string strEndWith)
        {
            MatchCollection matches = RegexHelper.Matches(strTemplate, $@"{strStartWith}{strField}{strEndWith}");
            if (matches.Count > 0)
            {
                Type type = typeof(TModel);
                PropertyInfo[] props = type.GetProperties();
                PropertyInfo prop = null;
                string strTemp = string.Empty;

                int intStartIndex = strStartWith.Length;
                int intLength = strStartWith.Length + strEndWith.Length;

                foreach (Match match in matches)
                {
                    strTemp = match.Value.Substring(intStartIndex, match.Value.Length - intLength);
                    prop = props.FirstOrDefault(o => o.Name.ToUpper() == strTemp.ToUpper());
                    if (prop != null)
                    {
                        strTemplate = strTemplate.Replace(match.Value, Convert.ToString(prop.GetValue(model)));
                    }
                }
            }
            return strTemplate;
        }

        #endregion Fill Content

        #region Serialization

        public static string ToJson<TModel>() where TModel : class, new()
        {
            string strKey = typeof(TModel).FullName + "-Json";
            string strJson = CacheHelper.GetCache<string>(strKey);
            if (!string.IsNullOrEmpty(strJson))
            {
                return strJson;
            }

            TModel instance = new TModel();
            strJson = SerializationHelper.ToJson(instance);
            CacheHelper.SetCache(strJson, strKey);
            return strJson;
        }

        #endregion Serialization

        #region Type Convert

        public static byte[] Image2Byte(Image image)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Jpeg);
                byte[] data = new byte[stream.Length];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(data, 0, Convert.ToInt32(stream.Length));
                return data;
            }
        }

        public static Image BytesToImage(byte[] Bytes)
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(Bytes);
                return Image.FromStream(stream);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            finally
            {
                stream.Close();
            }
        }

        public static byte[] Stream2Bytes(Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        }

        public static Stream Bytes2Stream(byte[] bytes)
        {
            Stream stream = new MemoryStream(bytes);
            return stream;
        }

        #endregion Type Convert

        #region Helper

        public static List<int> GetTreeIdList<TEntity>(List<TEntity> list, List<int> parentIdList)
             where TEntity : class, ITreeDto, new()
        {
            List<int> treeIdlist = new List<int>();
            parentIdList.ForEach(parentId =>
            {
                treeIdlist.AddRange(GetTreeIdList(list, parentId));
            });
            return treeIdlist;
        }

        public static List<int> GetTreeIdList<TEntity>(List<TEntity> list, int intParentId = 0)
             where TEntity : class, ITreeDto, new()
        {
            List<int> treeIdlist = new List<int>() { intParentId };
            List<TEntity> children = list.Where(o => o.ParentId == intParentId).ToList();

            foreach (TEntity item in children)
            {
                treeIdlist.AddRange(GetTreeIdList(list, item.Id));
            }

            return treeIdlist;
        }

        public static List<TEntity> GetTreeList<TEntity>(List<TEntity> list, int intParentId = 0, int intLayerIndex = 1, bool bIsTree = true)
            where TEntity : class, ITreeDto, new()
        {
            return GetTreeList<TEntity, int>(list, intParentId, intLayerIndex, bIsTree);
        }

        public static List<TEntity> GetTreeList<TEntity, TKey>(List<TEntity> list, int intParentId = 0, int intLayerIndex = 1, bool bIsTree = true, Expression<Func<TEntity, TKey>> orderByKeySelector = null, bool bIsAscOrder = true)
            where TEntity : class, ITreeDto, new()
        {
            PropertyInfo childrenProp = bIsTree ? GetPropertyInfo<TEntity>("Children") : null;

            List<TEntity> treelist = new List<TEntity>();

            if (intLayerIndex == 1 && intParentId > 0)
            {
                var topParent = list.FirstOrDefault(x => x.Id == intParentId);
                if (topParent != null)
                {
                    treelist.Add(topParent);
                    List<TEntity> childrenTemp = GetTreeList(list, intParentId, intLayerIndex + 1, bIsTree, orderByKeySelector, bIsAscOrder);
                    if (bIsTree)
                    {
                        childrenProp.SetValue(topParent, childrenTemp);
                    }
                    else
                    {
                        treelist.AddRange(childrenTemp);
                    }

                    return treelist;
                }
            }

            var childrenEnumerable = list.Where(o => o.ParentId == intParentId).AsQueryable<TEntity>();
            if (orderByKeySelector != null)
            {
                if (bIsAscOrder)
                {
                    childrenEnumerable = childrenEnumerable.OrderBy(orderByKeySelector);
                }
                else
                {
                    childrenEnumerable = childrenEnumerable.OrderBy(orderByKeySelector);
                }
            }

            List<TEntity> children = childrenEnumerable.ToList();

            if (children is List<IOrderIndexDto>)
            {
                children = (children as List<IOrderIndexDto>)
                    .OrderBy(o => o.OrderIndex).Cast<TEntity>().ToList();
            }

            foreach (TEntity item in children)
            {
                item.LayerIndex =
                item.LayerIndex = intLayerIndex;

                List<TEntity> childrenTemp = GetTreeList(list, item.Id, intLayerIndex + 1, bIsTree, orderByKeySelector, bIsAscOrder);
                item.IsLeaf = !childrenTemp.Any();

                treelist.Add(item);
                
                if (bIsTree)
                {
                    childrenProp.SetValue(item, childrenTemp);
                }
                else
                {
                    treelist.AddRange(childrenTemp);
                }
            }
            return treelist;
        }

        public static TEntity GetById<TEntity>(List<TEntity> list, int intId = 0, bool bIsNotNull = true)
            where TEntity : IEntity
        {
            TEntity entity = list.Where(o => o.Id == intId).FirstOrDefault();
            if (bIsNotNull && entity == null)
            {
                throw new Exception("Not exist [" + typeof(TEntity).Name + "] entity with Id=" + intId);
            }
            return entity;
        }

        public static List<TEntity> GetChilrenList<TEntity>(IEnumerable<TEntity> list, int intParentId = 0, bool bIsAllLayerChildren = true)
            where TEntity : ITreeEntity
        {
            if (list == null)
            {
                return null;
            }

            List<TEntity> treeList = list.Where(o => o.ParentId == intParentId).ToList();
            if (bIsAllLayerChildren)
            {
                List<TEntity> treeListTemp = new List<TEntity>();
                foreach (TEntity item in treeList)
                {
                    treeListTemp.AddRange(GetChilrenList(list, item.Id, bIsAllLayerChildren));
                }
                treeList.AddRange(treeListTemp);
            }
            return treeList;
        }

        #endregion Helper

        #region Excel Type Convert

        public static bool ToBoolen(string strValue)
        {
            return strValue == "1"
                || strValue == "是"
                || strValue.Equals(true.ToString(), StringComparison.CurrentCultureIgnoreCase);
        }

        #endregion Excel Type Convert

        #region NPOI
        public static Dictionary<string, NPOIPropertyInfo> GetNPOIPropertyInfoDic<TModel>()
            where TModel : class, new()
        {
            return GetNPOIPropertyInfoDic(typeof(TModel));
        }

        public static Dictionary<string, NPOIPropertyInfo> GetNPOIPropertyInfoDic(Type type)
        {
            Dictionary<string, NPOIPropertyInfo> baseDic = new Dictionary<string, NPOIPropertyInfo>();
            Dictionary<string, NPOIPropertyInfo> selfDic = new Dictionary<string, NPOIPropertyInfo>();
            Type baseType = type.BaseType;
            NPOIPropertyInfo npoiPropInfo = null;
            Type childredType = null;
            DoModel(type, prop =>
            {
                ExcelAttribute excelAttr = prop.GetCustomAttribute<ExcelAttribute>();
                if (excelAttr == null)
                {
                    return;
                }

                npoiPropInfo = new NPOIPropertyInfo
                {
                    PropertyInfo = prop,
                    Excel = excelAttr
                };

                if (prop.PropertyType.IsGenericType)
                {
                    childredType = prop.PropertyType.GetGenericArguments().First();
                    npoiPropInfo.DicChildrenPropertyInfo = GetNPOIPropertyInfoDic(childredType);
                }

                if (baseType != null && prop.DeclaringType == baseType)
                {
                    baseDic.Add(excelAttr.Title, npoiPropInfo);
                }
                else
                {
                    selfDic.Add(excelAttr.Title, npoiPropInfo);
                }
            });

            foreach (string strSelfKey in selfDic.Keys)
            {
                if (baseDic.ContainsKey(strSelfKey))
                {
                    baseDic[strSelfKey] = selfDic[strSelfKey];
                }
                else
                {
                    baseDic.Add(strSelfKey, selfDic[strSelfKey]);
                }
            }

            return baseDic;
        }
        #endregion

        #region Check Type
        public static Type UnwrapNullableType(this Type type) => Nullable.GetUnderlyingType(type) ?? type;
        public static void CheckInheritOf<SubClass, ParentClass>()
            where SubClass : class, new()
        {
            if (!IsInheritOf<SubClass, ParentClass>())
            {
                Type perent = typeof(ParentClass);
                string strProfix = perent.IsInterface ? "interface" : "class";
                throw new Exception($"The class {typeof(SubClass).Name} must inherit the {strProfix} {perent.Name}.");
            }
        }

        public static bool IsInheritOf<SubClass, ParentClass>()
            where SubClass : class, new()
        {
            SubClass sub = new SubClass();
            return sub is ParentClass;
        }

        public static bool IsStringType(Type type)
        {
            return type.Name == "String";
        }

        public static bool IsInt16Type(Type type)
        {
            return type.Name == "Int16";
        }

        public static bool IsNullableInt16Type(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Int16");
        }

        public static bool IsInt32Type(Type type)
        {
            return type.Name == "Int32";
        }

        public static bool IsNullableInt32Type(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Int32");
        }

        public static bool IsInt64Type(Type type)
        {
            return type.Name == "Int64";
        }

        public static bool IsNullableInt64Type(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Int64");
        }

        public static bool IsDecimalType(Type type)
        {
            return type.Name == "Decimal";
        }

        public static bool IsNullableDecimalType(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Decimal");
        }

        public static bool IsSingleType(Type type)
        {
            return type.Name == "Single";
        }

        public static bool IsNullableSingleType(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Single");
        }

        public static bool IsDoubleType(Type type)
        {
            return type.Name == "Double";
        }

        public static bool IsNullableDoubleType(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Double");
        }

        public static bool IsBooleanType(Type type)
        {
            return type.Name == "Boolean";
        }

        public static bool IsNullableBooleanType(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Boolean");
        }

        public static bool IsDateTimeType(Type type)
        {
            return type.Name == "DateTime";
        }

        public static bool IsNullableDateTimeType(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("DateTime");
        }

        public static bool IsByteType(Type type)
        {
            return type.Name == "Byte";
        }

        public static bool IsNullableByteType(Type type)
        {
            return type.Name.Contains("Nullable")
                && type.FullName.Contains("Byte");
        }

        public static bool IsEnumType(Type type)
        {
            return type.BaseType.Name == "Enum";
        }

        #endregion Check Type
    }
}