﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;

namespace JerryPlat.Utils.Helpers
{
    /// <summary>
    /// Need to test
    /// </summary>
    public static class EnumHelper
    {
        public static object ToEnum(Type type, int intValue)
        {
            return Enum.ToObject(type, intValue);
        }
        public static object ToEnum(Type type, string strValue)
        {
            if (Regex.IsMatch(strValue, "^[1-9]\\d*$"))
            {
                return ToEnum(type, Convert.ToInt32(strValue));
            }

            return Enum.Parse(type, strValue, true);
        }
        
        public static T ToEnum<T>(int intValue)
        {
            //(T)intValue
            return (T)ToEnum(typeof(T), intValue);
        }

        public static T ToEnum<T>(string strValue)
        {
            return (T)ToEnum(typeof(T), strValue);
        }

        public static string GetName(Type enumType, int intValue)
        {
            return Enum.GetName(enumType, intValue);
        }

        public static string GetDescription<T>(T tEnum)
        {
            Type type = typeof(T);
            string strEnumName = tEnum.ToString();
            FieldInfo fieldInfo = type.GetField(strEnumName);
            return GetDescription(fieldInfo, strEnumName);
        }

        public static string GetDescription(FieldInfo fieldInfo, string strEnumName)
        {
            DescriptionAttribute descriptionAttr = fieldInfo.GetCustomAttribute<DescriptionAttribute>();
            if (descriptionAttr == null)
            {
                return strEnumName;
            }
            return descriptionAttr.Description;
        }
    }
}