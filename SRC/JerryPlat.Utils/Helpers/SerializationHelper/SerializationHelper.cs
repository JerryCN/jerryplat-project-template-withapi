﻿using Newtonsoft.Json;

namespace JerryPlat.Utils.Helpers
{
    public static class SerializationHelper
    {
        public static JsonSerializerSettings DefaultJsonSerializerSettings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            DateFormatString = "yyyy-MM-dd HH:mm:ss"
        };
        
        public static string ToJson<T>(T value)
        {
            return JsonConvert.SerializeObject(value, DefaultJsonSerializerSettings);
        }

        public static T JsonToObject<T>(string strJson)
        {
            return JsonConvert.DeserializeObject<T>(strJson);
        }
    }
}