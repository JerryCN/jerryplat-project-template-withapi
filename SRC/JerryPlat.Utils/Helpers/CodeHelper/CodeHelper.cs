﻿
using System.Drawing;
using ZXing;
using ZXing.Common;

namespace JerryPlat.Utils.Helpers
{
    public static class CodeHelper
    {
        public static Image CreateBarCode(string strContent, int intMaxWidth, int intMaxHeight)
        {
            IBarcodeWriter writer = new BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128,
                Options = new EncodingOptions
                {
                    Width = intMaxWidth,
                    Height = intMaxHeight,
                    PureBarcode = true
                }
            };
            return writer.Write(strContent);
        }

        public static Bitmap CreateQrCode(string strContent, int size)
        {
            IBarcodeWriter writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Width = size,
                    Height = size,
                    Margin = 0,
                    PureBarcode = true
                }
            };
            return writer.Write(strContent);
        }
    }
}
