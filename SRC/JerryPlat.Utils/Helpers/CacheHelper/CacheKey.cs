﻿namespace JerryPlat.Utils.Helpers
{
    public enum CacheKey
    {
        ActionRole,
        Navigation,
        NavigationDto,
    }
}