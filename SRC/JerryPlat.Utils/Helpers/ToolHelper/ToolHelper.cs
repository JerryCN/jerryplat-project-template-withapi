﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace JerryPlat.Utils.Helpers
{
    public static class ToolHelper
    {
        public static string ToSpecialString(this string strText, int intStartIndex, int intLength, string strSpecial = "*")
        {
            string strReturn = string.Empty;
            int intLen = 0;
            for (int index = 0; index < strText.Length; index++)
            {
                if (index >= intStartIndex && intLen < intLength)
                {
                    strReturn += strSpecial;
                    intLen++;
                }
                else
                {
                    strReturn += strText[index];
                }
            }
            return strReturn;
        }

        #region 检查是否为IP地址

        /// <summary>
        /// 是否为ip
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }

        /// <summary>
        /// 获得当前页面客户端的IP
        /// </summary>
        /// <returns>当前页面客户端的IP</returns>
        public static string GetIP()
        {
            string result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(result))
            {
                result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            if (string.IsNullOrEmpty(result))
            {
                result = HttpContext.Current.Request.UserHostAddress;
            }
            if (string.IsNullOrEmpty(result) || !IsIP(result))
            {
                return "127.0.0.1";
            }
            return result;
        }

        #endregion 检查是否为IP地址

        public static string GetDateCode(string strCodeRole)
        {
            RegexHelper.Matches(strCodeRole, "{Date:[^:}]+}", match =>
            {
                string strMatch = match.Value;
                string[] aryMatch = strMatch.Substring(1, strMatch.Length - 2).Split(':');
                string strValue = DateTime.Now.ToFormat(aryMatch[1]);
                strCodeRole = strCodeRole.Replace(strMatch, strValue);
            });
            return strCodeRole;
        }

        /// <summary>
        /// GetCode
        /// </summary>
        /// <param name="strCodeRole"></param>
        /// <param name="strMaxSubCode"></param>
        /// <param name="strOutOfDbCodes">Code1,Code2</param>
        /// <returns></returns>
        public static string GetCode(string strCodeRole, string strMaxSubCode = "", string strOutOfDbCodes = "")
        {
            RegexHelper.Matches(strCodeRole, "{[^}]+:?[^:}]+}", match =>
            {
                string strMatch = match.Value;
                string[] aryMatch = strMatch.Substring(1, strMatch.Length - 2).Split(':');
                int intLength = 4;
                string strValue = string.Empty;
                switch (aryMatch[0])
                {
                    case "Date":
                        if (aryMatch.Length == 1)
                        {
                            throw new Exception($"Please config the currect Code Date Role {strCodeRole}");
                        }
                        strValue = DateTime.Now.ToFormat(aryMatch[1]);
                        break;
                    case "Random":
                        if (aryMatch.Length > 1)
                        {
                            if (!int.TryParse(aryMatch[1], out intLength))
                            {
                                throw new Exception($"Please config the currect Code Random Role {strCodeRole}");
                            }
                        }
                        strValue = RandomHelper.CreateCode(intLength);
                        break;
                    case "Order":
                        if (aryMatch.Length > 1)
                        {
                            if (!int.TryParse(aryMatch[1], out intLength))
                            {
                                throw new Exception($"Please config the currect Code Order Role {strCodeRole}");
                            }
                        }

                        int intOrder = 0;

                        if (!string.IsNullOrEmpty(strMaxSubCode))
                        {
                            strMaxSubCode = strMaxSubCode.Substring(strMaxSubCode.Length - intLength);

                            if (!int.TryParse(strMaxSubCode, out intOrder))
                            {
                                throw new Exception($"Please note that the MaxSubCode is not the current format.");
                            }
                        }

                        string[] aryOutOfDbCodes = null;
                        if (!string.IsNullOrEmpty(strOutOfDbCodes))
                        {
                            aryOutOfDbCodes = strOutOfDbCodes.Split(',');
                        }
                        string strCodeRoleTemp = string.Empty;
                        do
                        {
                            intOrder = intOrder + 1;
                            strValue = $"{"0".ToRepeat(intLength)}{intOrder}";
                            strValue = strValue.Substring(strValue.Length - intLength);

                            strCodeRoleTemp = strCodeRole.Replace(strMatch, strValue);
                        } while (aryOutOfDbCodes != null && aryOutOfDbCodes.Contains(strCodeRoleTemp));
                        break;
                }

                strCodeRole = strCodeRole.Replace(strMatch, strValue);
            });

            return strCodeRole;
        }

        public static string ToRepeat(this string strText, int intLength)
        {
            string strResult = string.Empty;
            while (intLength > 0)
            {
                strResult += strText;
                intLength--;
            }
            return strResult;
        }

        public static string GetCodeStartWith(string strCode)
        {
            return strCode.Substring(0, strCode.IndexOf('{'));
        }

        /// <summary>
        /// GetCodeAsync
        /// </summary>
        /// <param name="IsExistCodeAsync"></param>
        /// <param name="strCodeRole"></param>
        /// <param name="strMaxSubCode"></param>
        /// <param name="strOutOfDbCodes">Code1,Code2</param>
        /// <returns></returns>
        public static async Task<string> GetCodeAsync(Func<string, Task<bool>> IsExistCodeAsync = null, string strCodeRole="C{Date:yyyyMMddHHmmss}{Random:4}", string strMaxSubCode = "", string strOutOfDbCodes = "")
        {
            DateTime dateTime = DateTime.Now;
            string strCode = string.Empty;
            while (string.IsNullOrEmpty(strCode) || (IsExistCodeAsync != null && await IsExistCodeAsync(strCode)))
            {
                strCode = GetCode(strCodeRole, strMaxSubCode, strOutOfDbCodes);
            }
            return strCode;
        }

        public static async Task<string> GetOrderNoAsync(Func<string, Task<bool>> IsExistOrderNo = null, string strProfix = "C", int intLength = 4)
        {
            return await GetCodeAsync(IsExistOrderNo, $"{strProfix}{{Date:yyyyMMddHHmmss}}{{Random:{intLength}}}");
        }
        
        public static decimal ToMultiply(this decimal decMultiply, string strMultiply)
        {
            decimal dec1 = 0m, dec2 = 1m;
            if (strMultiply.Contains("/") & strMultiply != "N/A")
            {
                string[] aryMultiply = strMultiply.Split(new char[] { '/' });
                if (aryMultiply.Length != 2
                    || !decimal.TryParse(aryMultiply[0], out dec1)
                    || !decimal.TryParse(aryMultiply[1], out dec2)
                    || dec2 == 0m
                    )
                {
                    throw new Exception("Illeagal Multiple:" + strMultiply);
                }
            }
            else if (strMultiply.EndsWith("%"))
            {
                if (!decimal.TryParse(strMultiply.TrimEnd('%'), out dec1))
                {
                    throw new Exception("Illeagal Multiple:" + strMultiply);
                }
                dec2 = 100m;
            }
            else
            {
                if (!string.IsNullOrEmpty(strMultiply) && strMultiply != "N/A" && !decimal.TryParse(strMultiply, out dec1))
                {
                    throw new Exception("Illeagal Multiple:" + strMultiply);
                }
            }
            return (decMultiply * dec1 / dec2);
        }

        public static DateTime GetMonthStartDate(this DateTime datetime)
        {
            return new DateTime(datetime.Year, datetime.Month, 1);
        }

        public static string ToFormat(this DateTime datetime, string format = "yyyy-MM-dd HH:mm:ss")
        {
            return datetime.ToString(format);
        }

        public static bool IsFormRequest(this HttpRequestBase request)
        {
            return request.ContentType.ToLower().Contains("form");
        }
    }
}