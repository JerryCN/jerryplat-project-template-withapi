﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace JerryPlat.Utils.Helpers
{
    public static class SingleInstanceHelper
    {
        private static readonly object _lockObj = new object();
        private const string _SelfInstanceKey = "_Self_Instance_Key_";

        //https://blog.csdn.net/qq_29227939/article/details/51713422
        public static T GetInstance<T>(bool bIsAwaysNew = false) where T : class, new()
        {
            string strInstanceName = typeof(T).FullName;

            T _SingleInstance = HttpContext.Current.Items[strInstanceName] as T;

            if (bIsAwaysNew && _SingleInstance != null)
            {
                if (_SingleInstance is IDisposable disposableInstance)
                {
                    disposableInstance.Dispose();
                    disposableInstance = null;
                }

                _SingleInstance = null;

                HttpContext.Current.Items.Remove(strInstanceName);
            }

            if (_SingleInstance == null)
            {
                lock (_lockObj)
                {
                    if (_SingleInstance == null)
                    {
                        _SingleInstance = new T();
                    }
                }

                if (!(HttpContext.Current.Items[_SelfInstanceKey] is List<string> selfInstanceKeys))
                {
                    selfInstanceKeys = new List<string>();
                    HttpContext.Current.Items[_SelfInstanceKey] = selfInstanceKeys;
                }

                if (!selfInstanceKeys.Contains(strInstanceName))
                {
                    selfInstanceKeys.Add(strInstanceName);
                }
                
                HttpContext.Current.Items.Add(strInstanceName, _SingleInstance);
            }

            return _SingleInstance;
        }

        public static void DisposeInstance()
        {
            if (!(HttpContext.Current.Items[_SelfInstanceKey] is List<string> selfInstanceKeys))
            {
                return;
            }

            selfInstanceKeys.ForEach(key => {
                if(HttpContext.Current.Items[key] is IDisposable disposableInstance)
                {
                    disposableInstance.Dispose();
                    disposableInstance = null;
                }
            });
        }
    }
}