﻿namespace JerryPlat.Utils.Helpers
{
    public static class MessageHelper
    {
        public const string NoSession = "当前登陆已过期，请重新登陆。";

        public const string NoAction = "当前记录不允许被操作。";
        public const string NoActionList = "当前所选记录中存在不允许被操作的记录。";

        public const string CascadingDelete = "当前记录存在关联记录，删除后也将删除关联记录，删除后将无法恢复，确认删除吗?";
        public const string CascadingDeleteList = "所选记录存在关联记录，删除后也将删除关联记录，删除后将无法恢复，确认删除吗?";
        public const string ImportError = "当前导入数据库失败，请确保导入的模板以及数据格式正确，再重新导入。";
        
        public const string NoPermissionToAccess = "您没有权限进行该操作，请联系系统管理人员。";

        public const string InValidEmail = "请输入正确的邮箱。";
        public const string InValidPhone = "请输入正确的手机号。";

        public const string UploadInvalidFile = "请上传合法的文件。";

        public const string NotFound = "未发现查找的记录。";
    }
}