﻿using JerryPlat.Utils.Models;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JerryPlat.Utils.Helpers
{
    public class ApiHelper : HttpHelper
    {
        public static ApiHelper Instance => SingleInstanceHelper.GetInstance<ApiHelper>();

        protected readonly string[] AryTokenName = new string[] { "client_id" };
        protected string TokenType = "Bearer";
        

        public override string GetBaseUrl()
        {
            if (string.IsNullOrEmpty(WebConfigModel.Instance.ApiBaseUrl))
            {
                WebConfigModel.Instance.ApiBaseUrl = GetLocalDomain();
            }
            return WebConfigModel.Instance.ApiBaseUrl;
        }

        private string GetTokenId()
        {
            HttpCookie cookie = null;
            foreach (string tokenName in AryTokenName)
            {
                if (!CookieHelper.IsExistCookie(tokenName))
                {
                    continue;
                }
                cookie = CookieHelper.GetCookie(tokenName);
                if (cookie != null)
                {
                    return cookie.Value;
                }
            }
            return string.Empty;
        }

        protected override AuthenticationHeaderValue GetAuthenticationHeaderValue()
        {
            string strTokenId = GetTokenId();
            if (string.IsNullOrEmpty(strTokenId))
            {
                return null;
            }

            return GetAuthenticationHeaderValue(TokenType, strTokenId);
        }

        private string GetBaseTokenId(LoginModel loginModel)
        {
            string strBaseToken = string.Empty;
            if (loginModel != null && loginModel.IsFromApi)
            {
                strBaseToken = $"{loginModel.OwinClientId}:{loginModel.OwinClientSecret}";
            }
            else
            {
                strBaseToken = $"{WebConfigModel.Instance.OwinClientId}:{WebConfigModel.Instance.OwinClientSecret}";
            }

            byte[] byteAryBaseToken = Encoding.ASCII.GetBytes(strBaseToken);
            return Convert.ToBase64String(byteAryBaseToken);
        }

        private AuthenticationHeaderValue GetBaseAuthenticationHeaderValue(LoginModel loginModel = null)
        {
            string strTokenId = GetBaseTokenId(loginModel);
            if (string.IsNullOrEmpty(strTokenId))
            {
                return null;
            }

            return GetAuthenticationHeaderValue("Basic", strTokenId);
        }

        public async Task<TokenModel> GetTokenAsync(LoginModel loginModel)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("grant_type", "password");
            parameters.Add("UserName", loginModel.Email);
            parameters.Add("PassWord", loginModel.Password);

            TokenModel model = await PostAsync<TokenModel, Dictionary<string, string>>("/Token",
                parameters,
                RequestType.Form,
                GetBaseAuthenticationHeaderValue(loginModel));
            return model;
        }

        public async Task<TokenModel> GetRefreshTokenAsync(string strRefreshToken)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("grant_type", "refresh_token");
            parameters.Add("refresh_token", strRefreshToken);

            TokenModel model = await PostAsync<TokenModel, Dictionary<string, string>>("/Token",
                parameters, RequestType.Form,
                GetBaseAuthenticationHeaderValue());
            return model;
        }
    }
}