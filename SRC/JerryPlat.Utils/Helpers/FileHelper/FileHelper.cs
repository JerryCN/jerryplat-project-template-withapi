﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace JerryPlat.Utils.Helpers
{
    public static class FileHelper
    {
        public static bool IsValid(this HttpPostedFileBase file)
        {
            return true;
        }

        //For MVC
        public static string SaveTo(this HttpPostedFileBase file, string strCategory = "")
        {
            if (file == null)
            {
                return "";
            }

            string strFilePath = file.FileName.GetUploadFilePath(strCategory);
            file.SaveAs(strFilePath.ToMapPath());
            return strFilePath;
        }

        public static void Create(this string strPath)
        {
            IOHelper.CreateDir(strPath);
        }

        public static string ToMapPath(this string strPath)
        {
            return HttpContext.Current.Server.MapPath(strPath);
        }

        public static string GetFolder(string strDefaultFolderName, string strCategory = "")
        {
            if (string.IsNullOrEmpty(strCategory))
            {
                strCategory = strDefaultFolderName + "/" + DateTime.Now.ToString("yyyy-MM-dd");
            }

            string strFilePath = "/File/" + strCategory;
            strFilePath.ToMapPath().Create();
            return strFilePath;
        }

        public static string GetUploadFolder(string strCategory = "")
        {
            return GetFolder("Upload", strCategory);
        }

        public static string GetDownloadFolder(string strCategory = "")
        {
            return GetFolder("Download", strCategory);
        }

        public static string GetUploadFilePath(this string strName, string strCategory = "", string strExt = "")
        {
            string strFileFolder = GetUploadFolder(strCategory);

            string strFullFilePath = strFileFolder + "/" + strName;

            if (!string.IsNullOrEmpty(strExt) && !strFullFilePath.Contains("."))
            {
                strFullFilePath = strFullFilePath + "." + strExt;
            }

            return strFullFilePath;
        }

        public static HttpResponseMessage ApiDownload(byte[] bytes, string contentType, string strDownloadFileName)
        {
            Stream stream = TypeHelper.Bytes2Stream(bytes);
            return ApiDownload(stream, contentType, strDownloadFileName);
        }

        public static HttpResponseMessage ApiDownload(string strFileName, string contentType, string strDownloadFileName)
        {
            Stream stream = new FileStream(strFileName, FileMode.Open);
            return ApiDownload(stream, contentType, strDownloadFileName);
        }

        public static HttpResponseMessage ApiDownload(Stream stream, string contentType, string strDownloadFileName)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = ExcelHelper.GetExcelName(strDownloadFileName);
            return result;
        }

        public static HttpResponseMessage ApiImage(Image  image)
        {
            byte[] bytes = TypeHelper.Image2Byte(image);

            return ApiImage(bytes);
        }

        public static HttpResponseMessage ApiImage(byte[] bytes)
        {
            var imgStream = new MemoryStream(bytes);
            var resp = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(imgStream)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            return resp;
        }
    }
}