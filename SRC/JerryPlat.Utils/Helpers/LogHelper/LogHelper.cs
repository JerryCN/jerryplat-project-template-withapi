﻿using log4net;
using System;
using System.Diagnostics;
using System.Reflection;

namespace JerryPlat.Utils.Helpers
{
    public static class LogHelper
    {
        public static ILog GetLogInstance()
        {
            StackTrace trace = new StackTrace();
            StackFrame traceFrame = trace.GetFrame(2);
            return  LogManager.GetLogger(traceFrame.GetMethod().DeclaringType);
        }

        public static void Debug(object message)
        {
            GetLogInstance().Debug(message);
        }

        public static void Debug(object message, Exception exception)
        {
            GetLogInstance().Debug(message, exception);
        }

        public static void DebugFormat(string format, params object[] args)
        {
            GetLogInstance().DebugFormat(format, args);
        }

        public static void DebugFormat(string format, object arg0)
        {
            GetLogInstance().DebugFormat(format, arg0);
        }

        public static void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            GetLogInstance().DebugFormat(provider, format, args);
        }

        public static void DebugFormat(string format, object arg0, object arg1)
        {
            GetLogInstance().DebugFormat(format, arg0, arg1);
        }

        public static void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            GetLogInstance().DebugFormat(format, arg0, arg1, arg2);
        }

        public static void Error(object message)
        {
            GetLogInstance().Error(message);
        }

        public static void Error(object message, Exception exception)
        {
            GetLogInstance().Error(message, exception);
        }

        public static void ErrorFormat(string format, params object[] args)
        {
            GetLogInstance().ErrorFormat(format, args);
        }

        public static void ErrorFormat(string format, object arg0)
        {
            GetLogInstance().ErrorFormat(format, arg0);
        }

        public static void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            GetLogInstance().ErrorFormat(provider, format, args);
        }

        public static void ErrorFormat(string format, object arg0, object arg1)
        {
            GetLogInstance().ErrorFormat(format, arg0, arg1);
        }

        public static void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            GetLogInstance().ErrorFormat(format, arg0, arg1, arg2);
        }

        public static void Fatal(object message)
        {
            GetLogInstance().Fatal(message);
        }

        public static void Fatal(object message, Exception exception)
        {
            GetLogInstance().Fatal(message, exception);
        }

        public static void FatalFormat(string format, params object[] args)
        {
            GetLogInstance().FatalFormat(format, args);
        }

        public static void FatalFormat(string format, object arg0)
        {
            GetLogInstance().FatalFormat(format, arg0);
        }

        public static void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            GetLogInstance().FatalFormat(provider, format, args);
        }

        public static void FatalFormat(string format, object arg0, object arg1)
        {
            GetLogInstance().FatalFormat(format, arg0, arg1);
        }

        public static void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            GetLogInstance().FatalFormat(format, arg0, arg1, arg2);
        }

        public static void Info(object message)
        {
            GetLogInstance().Info(message);
        }

        public static void Info(object message, Exception exception)
        {
            GetLogInstance().Info(message, exception);
        }

        public static void InfoFormat(string format, params object[] args)
        {
            GetLogInstance().InfoFormat(format, args);
        }

        public static void InfoFormat(string format, object arg0)
        {
            GetLogInstance().InfoFormat(format, arg0);
        }

        public static void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            GetLogInstance().InfoFormat(provider, format, args);
        }

        public static void InfoFormat(string format, object arg0, object arg1)
        {
            GetLogInstance().InfoFormat(format, arg0, arg1);
        }

        public static void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            GetLogInstance().InfoFormat(format, arg0, arg1, arg2);
        }

        public static void Warn(object message)
        {
            GetLogInstance().Warn(message);
        }

        public static void Warn(object message, Exception exception)
        {
            GetLogInstance().Warn(message, exception);
        }

        public static void WarnFormat(string format, params object[] args)
        {
            GetLogInstance().WarnFormat(format, args);
        }

        public static void WarnFormat(string format, object arg0)
        {
            GetLogInstance().WarnFormat(format, arg0);
        }

        public static void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            GetLogInstance().WarnFormat(provider, format, args);
        }

        public static void WarnFormat(string format, object arg0, object arg1)
        {
            GetLogInstance().WarnFormat(format, arg0, arg1);
        }

        public static void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            GetLogInstance().WarnFormat(format, arg0, arg1, arg2);
        }

        public static bool IsDebugEnabled { get { return GetLogInstance().IsDebugEnabled; } }

        public static bool IsErrorEnabled { get { return GetLogInstance().IsErrorEnabled; } }

        public static bool IsFatalEnabled { get { return GetLogInstance().IsFatalEnabled; } }

        public static bool IsInfoEnabled { get { return GetLogInstance().IsInfoEnabled; } }

        public static bool IsWarnEnabled { get { return GetLogInstance().IsWarnEnabled; } }
    }
}