﻿using JerryPlat.Utils.Attributes;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Helpers
{
    public static class NPOIHelper
    {
        internal static class Setting
        {
            public static string Company = "Jerry 15802775429";
            public static string Author = "Jerry 15802775429";
            public static string Subject = "数据导出";
        }

        internal enum ExcelType
        {
            XLS,
            XLSX
        }

        private static Dictionary<Type, Dictionary<string, NPOIPropertyInfo>> _DicNPOIPropertyInfoMapping
                = new Dictionary<Type, Dictionary<string, NPOIPropertyInfo>>();

        private static Dictionary<string, NPOIPropertyInfo> GetMapping<TModel>()
            where TModel : class, new()
        {
            Type type = typeof(TModel);
            if (!_DicNPOIPropertyInfoMapping.Keys.Contains(type))
            {
                _DicNPOIPropertyInfoMapping.Add(type, TypeHelper.GetNPOIPropertyInfoDic<TModel>());
            }
            return _DicNPOIPropertyInfoMapping[type];
        }

        private static List<string> GetExcelTitle(IRow row, int intTotalColumns)
        {
            List<string> excelTitleList = new List<string>();
            for (int intCellIndex = 0; intCellIndex < intTotalColumns; intCellIndex++)
            {
                excelTitleList.Add(row.GetCell(intCellIndex).StringCellValue);
            }
            return excelTitleList;
        }

        private static void CheckTemplateTitle(Dictionary<string, NPOIPropertyInfo> dicNPOI, List<string> excelTitleList)
        {
            List<string> errorList = new List<string>();
            foreach (string excelTitle in excelTitleList)
            {
                if (!dicNPOI.Keys.Contains(excelTitle))
                {
                    errorList.Add(excelTitle);
                }
            }
            if (errorList.Any())
            {
                string strError = string.Join(",", errorList);
                throw new Exception($"模板的格式不正确，不存在以下列:{strError}");
            }
        }

        private static ExcelType GetExcelType(string strExcelFullName)
        {
            string strExtension = Path.GetExtension(strExcelFullName).ToLower();
            switch (strExtension)
            {
                case ".xls":
                    return ExcelType.XLS;
                case ".xlsx":
                    return ExcelType.XLSX;
                default:
                    throw new Exception($"不支持{strExtension}类型。");
            }
        }

        private static IWorkbook GetWorkBook(string strExcelFullName)
        {
            using (FileStream excelFile = new FileStream(strExcelFullName, FileMode.Open, FileAccess.Read))
            {
                ExcelType excelType = GetExcelType(strExcelFullName);
                switch (excelType)
                {
                    case ExcelType.XLS:
                        return new HSSFWorkbook(excelFile);
                    case ExcelType.XLSX:
                    default:
                        return new XSSFWorkbook(excelFile);
                }
            }
        }

        private static IWorkbook GetWorkBook(ExcelType excelType)
        {
            switch (excelType)
            {
                case ExcelType.XLS:
                    HSSFWorkbook xlsWorkbook = new HSSFWorkbook();

                    var dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                    dsi.Company = Setting.Company;
                    xlsWorkbook.DocumentSummaryInformation = dsi;

                    var si = PropertySetFactory.CreateSummaryInformation();
                    si.Author = Setting.Author;
                    si.Subject = Setting.Subject;
                    xlsWorkbook.SummaryInformation = si;

                    return xlsWorkbook;
                case ExcelType.XLSX:
                default:
                    XSSFWorkbook xlsxWorkbook = new XSSFWorkbook();
                    return xlsxWorkbook;
            }
        }

        public static List<TModel> LoadExcel<TModel>(string strExcelFullName, Func<int, int, object, NPOIPropertyInfo, object> valueConverter = null, Func<ValueMappingAttribute, object, object> getValueMappingValue = null)
            where TModel : class, new()
        {
            IWorkbook workbook = GetWorkBook(strExcelFullName);
            ISheet sheet = workbook.GetSheetAt(0);
            Dictionary<string, NPOIPropertyInfo> dicNPOI = GetMapping<TModel>();
            List<string> excelTitleList = null;

            List<TModel> modelList = new List<TModel>();
            TModel model = null;

            IEnumerator rowEnumerator = sheet.GetRowEnumerator();
            int intRowNumber = 0;
            int intCellIndex = 0;
            while (rowEnumerator.MoveNext())
            {
                intRowNumber++;

                IRow row = rowEnumerator.Current as IRow;

                if (excelTitleList == null)
                {
                    excelTitleList= GetExcelTitle(row, dicNPOI.Keys.Count);
                    CheckTemplateTitle(dicNPOI, excelTitleList);
                    continue;
                }

                model = new TModel();
                intCellIndex = 0;

                while (intCellIndex < excelTitleList.Count)
                {
                    NPOIPropertyInfo npoiPropertyInfo = dicNPOI[excelTitleList[intCellIndex]];

                    object value = row.GetCellValue(intCellIndex);
                    if (valueConverter != null)
                    {
                        value = valueConverter(row.RowNum, intCellIndex, value, npoiPropertyInfo);
                    }

                    var propType = Nullable.GetUnderlyingType(npoiPropertyInfo.PropertyInfo.PropertyType) ?? npoiPropertyInfo.PropertyInfo.PropertyType;

                    var valueMapping = npoiPropertyInfo.PropertyInfo.GetCustomAttribute<ValueMappingAttribute>();
                    if (valueMapping != null && getValueMappingValue != null)
                    {
                        value = getValueMappingValue(valueMapping, value);
                    }

                    try
                    {
                        var safeValue = Convert.ChangeType(value, propType, CultureInfo.CurrentCulture);

                        npoiPropertyInfo.PropertyInfo.SetValue(model, safeValue);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"第{intRowNumber}行第{intCellIndex}({excelTitleList[intCellIndex]})列：{typeof(TModel).Name}.{npoiPropertyInfo.PropertyInfo.Name} = {value}", ex);
                    }

                    intCellIndex++;
                }
                modelList.Add(model);
            }
            
            return modelList;
        }

        internal static object GetCellValue(this IRow row, int index)
        {
            var cell = row.GetCell(index);
            if (cell == null)
            {
                return null;
            }
            
            switch (cell.CellType)
            {
                case CellType.Numeric:
                    return cell.ToString();
                case CellType.String:
                    return cell.StringCellValue;
                case CellType.Boolean:
                    return cell.BooleanCellValue;
                case CellType.Error:
                    return cell.ErrorCellValue;
                case CellType.Formula:
                    return cell.ToString();
                case CellType.Blank:
                case CellType.Unknown:
                default:
                    return null;
            }
        }

        public static byte[] ToExcelContent<TModel>(this IEnumerable<TModel> source, string strSheetName = "sheet0", Func<string, object, string> valueConverter = null)
            where TModel : class, new()
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            IWorkbook book = source.ToWorkbook(null, strSheetName, ExcelType.XLSX, valueConverter);

            using (var ms = new MemoryStream())
            {
                book.Write(ms);
                return ms.ToArray();
            }
        }

        public static void ToExcel<TModel>(this IEnumerable<TModel> source, string strExcelFile, string strSheetName = "sheet0", Func<string, object, string> valueConverter = null)
            where TModel : class, new()
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            ExcelType excelType = GetExcelType(strExcelFile);
            var book = source.ToWorkbook(null, strSheetName, excelType, valueConverter);

            // Write the stream data of workbook to file
            using (var stream = new FileStream(strExcelFile, FileMode.OpenOrCreate, FileAccess.Write))
            {
                book.Write(stream);
            }
        }

        private static void SetCellValue(ICell cell, NPOIPropertyInfo npoiPropertyInfo, object value, Func<string, object, string> valueConverter, string strContextName)
        {
            if (valueConverter != null)
            {
                string strValue = valueConverter($"{strContextName}{npoiPropertyInfo.PropertyInfo.Name}", value);
                if (!string.IsNullOrEmpty(strValue))
                {
                    cell.SetCellValue(strValue);
                    return;
                }
            }

            if(value is IFormattable)
            {
                IFormattable fv = value as IFormattable;
                if (string.IsNullOrEmpty(npoiPropertyInfo.Excel.Formatter))
                {
                    cell.SetCellValue(fv.ToString());
                    return;
                }
                cell.SetCellValue(fv.ToString(npoiPropertyInfo.Excel.Formatter, CultureInfo.CurrentCulture));
                return;
            }

            var unwrapType = npoiPropertyInfo.PropertyInfo.PropertyType.UnwrapNullableType();
            if (unwrapType == typeof(bool))
            {
                bool bValue = (bool)value;
                cell.SetCellValue(bValue ? "是" : "否");
            }
            else if (unwrapType == typeof(DateTime))
            {
                cell.SetCellValue(Convert.ToDateTime(value));
            }
            else if (unwrapType == typeof(double))
            {
                cell.SetCellValue(Convert.ToDouble(value));
            }
            else
            {
                cell.SetCellValue(value.ToString());
            }
        }
        
        private static ICellStyle GetHeaderCellStyle(IWorkbook workbook)
        {
            ICellStyle style = workbook.CreateCellStyle();

            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 12;
            font.Color = HSSFColor.White.Index;
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;
            style.FillPattern = FillPattern.SolidForeground;
            style.FillForegroundColor = HSSFColor.SkyBlue.Index;

            return style;
        }

        private static int GetMaxMergeRowCount<TModel>(TModel model, Dictionary<string, NPOIPropertyInfo> dicNPOI)
            where TModel : class, new()
        {
            int intMaxMergeRowCount = dicNPOI.Values.Where(o => o.DicChildrenPropertyInfo != null).Max(o => {
                object value = o.PropertyInfo.GetValue(model);
                if (value == null)
                {
                    return 0;
                }
                return (value as IList).Count;
            });

            return Math.Max(1, intMaxMergeRowCount);
        }

        private static IWorkbook ToWorkbook<TModel>(this IEnumerable<TModel> source, string excelFile, string sheetName, ExcelType excelType, Func<string, object, string> valueConverter)
            where TModel : class, new()
        {
            Dictionary<string, NPOIPropertyInfo> dicNPOI = GetMapping<TModel>();

            IWorkbook workbook = GetWorkBook(excelType);
            
            ISheet sheet = workbook.CreateSheet(sheetName);

            ICellStyle titleStyle = GetHeaderCellStyle(workbook);

            int intRowIndex = 0, intRowIndexTemp = 0, intCellIndex = 0, intCellIndexTemp = 0;

            IRow row = sheet.CreateRow(intRowIndex);
            bool bIsExistChildren = dicNPOI.Values.Any(o => o.DicChildrenPropertyInfo != null);
            IRow rowTemp = bIsExistChildren ? sheet.CreateRow(intRowIndex + 1) : null;
            ICell cell = null;
            NPOIPropertyInfo npoiPropertyInfo = null;
            NPOIPropertyInfo childrenNpoiPropertyInfo = null;

            #region Excel Title
            foreach (string strKey in dicNPOI.Keys)
            {
                cell = row.CreateCell(intCellIndex);
                cell.CellStyle = titleStyle;
                cell.SetCellValue(strKey);
                if(!bIsExistChildren)
                {
                    intCellIndex++;
                    continue;
                }

                npoiPropertyInfo = dicNPOI[strKey];
                if (npoiPropertyInfo.DicChildrenPropertyInfo == null)
                {
                    sheet.AddMergedRegion(new CellRangeAddress(intRowIndex, intRowIndex + 1, intCellIndex, intCellIndex));
                    intCellIndex++;
                }
                else
                {
                    sheet.AddMergedRegion(new CellRangeAddress(intRowIndex, intRowIndex, intCellIndex, intCellIndex + npoiPropertyInfo.DicChildrenPropertyInfo.Keys.Count - 1));
                    foreach (string strChildrenKey in npoiPropertyInfo.DicChildrenPropertyInfo.Keys)
                    {
                        cell = rowTemp.CreateCell(intCellIndex);
                        cell.CellStyle = titleStyle;
                        cell.SetCellValue(strChildrenKey);
                        intCellIndex++;
                    }
                }
            }
            #endregion

            #region Excel Content
            intRowIndex = bIsExistChildren ? 2 : 1;

            int intMaxMergeRowCount = 0;
            foreach (TModel model in source)
            {
                row = sheet.CreateRow(intRowIndex);
                intMaxMergeRowCount = bIsExistChildren ? GetMaxMergeRowCount(model, dicNPOI) : 1;
                intCellIndex = 0;
                foreach (string strKey in dicNPOI.Keys)
                {
                    npoiPropertyInfo = dicNPOI[strKey];

                    object value = npoiPropertyInfo.PropertyInfo.GetValue(model);

                    if (npoiPropertyInfo.DicChildrenPropertyInfo == null)
                    {
                        if (value != null)
                        {
                            cell = row.CreateCell(intCellIndex);
                            SetCellValue(cell, npoiPropertyInfo, value, valueConverter, "");
                        }
                        
                        if (intMaxMergeRowCount > 1)
                        {
                            sheet.AddMergedRegion(new CellRangeAddress(intRowIndex, intRowIndex + intMaxMergeRowCount - 1, intCellIndex, intCellIndex));
                        }

                        intCellIndex++;
                    }
                    else
                    {
                        if(value == null)
                        {
                            intCellIndex += npoiPropertyInfo.DicChildrenPropertyInfo.Keys.Count;
                            continue;
                        }
                        
                        IList valueList = value as IList;
                        intRowIndexTemp = intRowIndex;
                        foreach (object chilrenModel in valueList)
                        {
                            rowTemp = intRowIndexTemp == intRowIndex ? row : sheet.CreateRow(intRowIndexTemp);
                            intCellIndexTemp = intCellIndex;
                            foreach (string strChildrenKey in npoiPropertyInfo.DicChildrenPropertyInfo.Keys)
                            {
                                childrenNpoiPropertyInfo = npoiPropertyInfo.DicChildrenPropertyInfo[strChildrenKey];
                                object childrenValue = childrenNpoiPropertyInfo.PropertyInfo.GetValue(chilrenModel);
                                if (value != null)
                                {
                                    cell = rowTemp.CreateCell(intCellIndexTemp);
                                    SetCellValue(cell, childrenNpoiPropertyInfo, childrenValue, valueConverter, $"{npoiPropertyInfo.PropertyInfo.Name}.");
                                }

                                intCellIndexTemp++;
                            }
                            intRowIndexTemp++;
                        }
                    }
                }
                intRowIndex += intMaxMergeRowCount;
            }
            #endregion

            #region AutoSizeColumn
            intCellIndex = 0;
            foreach (string strKey in dicNPOI.Keys)
            {
                npoiPropertyInfo = dicNPOI[strKey];
                if (npoiPropertyInfo.DicChildrenPropertyInfo == null)
                {
                    sheet.AutoSizeColumn(intCellIndex);
                    intCellIndex++;
                }
                else
                {
                    foreach (string strChildrenKey in npoiPropertyInfo.DicChildrenPropertyInfo.Keys)
                    {
                        sheet.AutoSizeColumn(intCellIndex);
                        intCellIndex++;
                    }
                }
            }
            #endregion

            return workbook;
        }
    }
}
