﻿using JerryPlat.Utils.Attributes;
using System;
using System.Collections.Generic;

namespace JerryPlat.Utils.Helpers
{
    public static class ExcelHelper
    {
        public const string Excel_ContentType = "application/octet-stream";

        public static string SaveExcelFile<TEntity>(List<TEntity> entityList, string strPureFileName, Func<string, object, string> valueConverter = null, string strCategory = "")
            where TEntity : class, new()
        {
            string strFileFolder = FileHelper.GetDownloadFolder(strCategory);
            string strFileName = ExcelHelper.GetExcelName(strPureFileName);
            string strFilePath = $"{strFileFolder}/{strFileName}";

            SaveExcel(entityList, strFilePath.ToMapPath(), valueConverter);
            return HttpHelper.GetLocalUrl(strFilePath);
        }

        public static void SaveExcel<TEntity>(List<TEntity> list, string strExcelFullName, Func<string, object, string> valueConverter = null)
             where TEntity : class, new()
        {
            list.ToExcel(strExcelFullName, valueConverter: valueConverter);
        }

        public static byte[] SaveExcelContent<TEntity>(List<TEntity> list, Func<string, object, string> valueConverter = null)
             where TEntity : class, new()
        {
            return list.ToExcelContent(valueConverter: valueConverter);
        }

        public static IEnumerable<TEntity> LoadExcel<TEntity>(string strExcelFullName, Func<int, int, object, NPOIPropertyInfo, object> valueConverter = null, Func<ValueMappingAttribute, object, object> getValueMappingValue = null)
           where TEntity : class, new()
        {
            return NPOIHelper.LoadExcel<TEntity>(strExcelFullName, valueConverter: valueConverter, getValueMappingValue: getValueMappingValue);
        }

        public static string GetExcelName(string strName)
        {
            return $"{strName}_{DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss")}.xlsx";
        }
    }
}