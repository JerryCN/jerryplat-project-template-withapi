﻿using JerryPlat.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Helpers
{
    public class NPOIPropertyInfo
    {
        public PropertyInfo PropertyInfo { get; set; }
        public Dictionary<string, NPOIPropertyInfo> DicChildrenPropertyInfo { get; set; }
        public ExcelAttribute Excel { get; set; }
    }
}
