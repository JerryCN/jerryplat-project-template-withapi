﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Attributes
{
    public class ExcelAttribute : Attribute
    {
        public string Title { get; set; }
        public string Formatter { get; set; }
    }
}
