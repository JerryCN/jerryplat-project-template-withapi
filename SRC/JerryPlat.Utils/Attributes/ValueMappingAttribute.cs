﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Attributes
{
    public class ValueMappingAttribute : Attribute
    {
        public string Table { get; set; }
        public string From { get; set; } = "Name";
        public string To { get; set; } = "Id";

        public string GetSql()
        {
            return $"SELECT * FROM {Table}";
        }

        public string GetCacheKey(object value)
        {
            return $"{Table}-{From}-{To}-{value}";
        }
    }
}
