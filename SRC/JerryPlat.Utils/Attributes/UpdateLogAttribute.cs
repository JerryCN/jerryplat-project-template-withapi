﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Attributes
{
    public class UpdateLogAttribute : Attribute
    {
        public string Title { get; set; }
        public string ForeignField { get; set; } = "Name";
        public string ForeignTable { get; set; }
        public string Formatter { get; set; } = "yyyy-MM-dd";
    }
}
