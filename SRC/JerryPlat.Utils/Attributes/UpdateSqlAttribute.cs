﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Attributes
{
    public class UpdateSqlAttribute : Attribute
    {
        public string Sql { get; set; }
    }
}
