﻿namespace JerryPlat.Utils.Models
{
    public class ImprotModel
    {
        public string ExcelPath { get; set; }
        public bool IsPreImport { get; set; }
    }
}