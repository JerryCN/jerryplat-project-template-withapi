﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Models
{
    public class LoginTokenModel<T>
    {
        public TokenModel TokenModel { get; set; }

        public T Session { get; set; }
    }
}
