﻿using JerryPlat.Utils.Helpers;

namespace JerryPlat.Utils.Models
{
    public class WebConfigModel
    {
        public static WebConfigModel Instance = TypeHelper.InitModel<WebConfigModel>(ConfigHelper.GetConfig);

        #region For API
        public bool AllowInsecureHttp { get; set; }
        #endregion

        #region For Web
        public string OwinClientId { get; set; }
        public string OwinClientSecret { get; set; }
        public string ApiBaseUrl { get; set; }
        #endregion
    }
}