﻿using JerryPlat.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JerryPlat.Utils.Models
{
    public class SystemConfigModel
    {
        public static SystemConfigModel Instance;

        public static Dictionary<string, string> KeyValueList;

        public static void Reset(Dictionary<string, string> keyValueList)
        {
            KeyValueList = keyValueList;
            Instance = TypeHelper.InitModel<SystemConfigModel>(keyValueList);
        }

        public static string GetConfig(string strKey)
        {
            if(KeyValueList == null)
            {
                throw new Exception("Please init SystemConfigModel first.");
            }

            if (KeyValueList.Keys.Contains(strKey))
            {
                return KeyValueList[strKey];
            }

            throw new Exception($"Please config the Key = {strKey} into SystemConfig table.");
        }

        public static void Reset(SystemConfigModel model)
        {
            Instance = model;
        }

        #region Code
        public string Department_Code_Role { get; set; }
        public string Employee_Code_Role { get; set; }
        public string AssetStorage_Code_Role { get; set; }
        public string Asset_Code_Role { get; set; }
        public string AssetUsed_Code_Role { get; set; }
        public string AssetUsedBack_Code_Role { get; set; }
        public string AssetBorrow_Code_Role { get; set; }
        public string AssetBorrowReturn_Code_Role { get; set; }
        public string AssetClear_Code_Role { get; set; }
        public string AssetUpdate_Code_Role { get; set; }
        public string AssetFinancialUpdate_Code_Role { get; set; }
        #endregion

        #region 缓存机制
        public bool IsEnableLocalStorage { get; set; }
        #endregion

        #region 允许跨域访问域名

        public string AllowOriginSites { get; set; }

        #endregion 允许跨域访问域名
        
        #region 数据库管理

        public string MasterDbConnStr { get; set; }
        public string DbName { get; set; }
        public string BackUpDbName { get; set; }

        #endregion 数据库管理
    }
}