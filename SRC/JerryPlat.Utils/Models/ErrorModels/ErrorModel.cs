﻿using JerryPlat.Utils.Helpers;

namespace JerryPlat.Utils.Models
{
    public class ErrorModel
    {
        public string Title { get; set; } = "页面发生了错误";
        public string Error { get; set; } = "程序发生了错误，请先联系技术人员，或稍候再试。";
        public bool IsRestart { get; set; } = false;
    }
}