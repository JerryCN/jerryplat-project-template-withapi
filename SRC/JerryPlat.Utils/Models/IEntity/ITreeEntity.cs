﻿namespace JerryPlat.Utils.Models
{
    public interface ITreeEntity : IEntity
    {
        int ParentId { get; set; }
    }
}