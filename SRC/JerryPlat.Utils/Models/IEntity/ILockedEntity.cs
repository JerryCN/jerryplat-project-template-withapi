﻿namespace JerryPlat.Utils.Models
{
    public interface ILockedEntity : IEntity
    {
        bool IsLocked { get; set; }
    }
}