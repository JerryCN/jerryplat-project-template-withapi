﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Models
{
    public interface IOrderIndexEntity : IEntity
    {
        int OrderIndex { get; set; }
    }
}
