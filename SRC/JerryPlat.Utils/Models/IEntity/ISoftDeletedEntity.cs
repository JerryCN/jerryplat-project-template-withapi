﻿namespace JerryPlat.Utils.Models
{
    public interface ISoftDeletedEntity : IEntity
    {
        bool IsDeleted { get; set; }
    }
}