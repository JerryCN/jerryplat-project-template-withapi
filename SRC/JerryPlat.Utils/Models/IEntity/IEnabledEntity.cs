﻿namespace JerryPlat.Utils.Models
{
    public interface IEnabledEntity : IEntity
    {
        bool Enabled { get; set; }
    }
}