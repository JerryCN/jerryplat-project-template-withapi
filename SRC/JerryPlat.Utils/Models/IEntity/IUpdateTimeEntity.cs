﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Models
{
    public interface IUpdateTimeEntity : IEntity
    {
        DateTime UpdateTime { get; set; }
    }
}
