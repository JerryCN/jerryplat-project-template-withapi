﻿namespace JerryPlat.Utils.Models
{
    public interface IDefaultUsedEntity : IEntity
    {
        bool IsDefaultUsed { get; set; }
    }
}