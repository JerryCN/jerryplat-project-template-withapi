﻿namespace JerryPlat.Utils.Models
{
    public interface ISessionEntity: IEntity
    {
        int SessionId { get; set; }
    }
}