﻿namespace JerryPlat.Utils.Models
{
    public interface IValidEntity : IEntity
    {
        bool IsValid { get; set; }
    }
}