﻿using System.ComponentModel.DataAnnotations;

namespace JerryPlat.Utils.Models
{
    public class LoginModel
    {
        public bool IsFromApi { get; set; }
        public string OwinClientId { get; set; }
        public string OwinClientSecret { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}