﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Models
{
    public class ConfirmModel<TModel>
    {
        public bool IsConfirmed { get; set; }
        public TModel Data { get; set; }
    }
}
