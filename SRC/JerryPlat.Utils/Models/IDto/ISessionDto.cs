﻿namespace JerryPlat.Utils.Models
{
    public interface ISessionDto: IDto
    {
        int SessionId { get; set; }
    }
}