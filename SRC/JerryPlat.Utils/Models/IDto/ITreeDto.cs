﻿namespace JerryPlat.Utils.Models
{
    public interface ITreeDto : IDto
    {
        int ParentId { get; set; }
        int LayerIndex { get; set; }
        bool IsLeaf { get; set; }
    }
}