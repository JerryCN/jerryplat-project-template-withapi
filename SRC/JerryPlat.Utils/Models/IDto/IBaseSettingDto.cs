﻿using JerryPlat.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JerryPlat.Utils.Models
{
    public interface IBaseSettingDto : ILockedDto, IOrderIndexDto,IUsedManagementDto
    {
        string Name { get; set; }
    }

    public class BaseSettingDto : IBaseSettingDto {
        [Excel(Title = "Id")]
        public int Id { get; set; }
        [Excel(Title = "名称")]
        public string Name { get; set; }
        [Excel(Title = "排序")]
        public int OrderIndex { get; set; }
        [Excel(Title = "是否锁定")]
        public bool IsLocked { get; set; }
        [Excel(Title = "使用单位Id")]
        public int UsedCompanyId { get; set; }
    }
}
