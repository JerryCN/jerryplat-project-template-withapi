﻿namespace JerryPlat.Utils.Models
{
    public interface ILockedDto : IDto
    {
        bool IsLocked { get; set; }
    }
}