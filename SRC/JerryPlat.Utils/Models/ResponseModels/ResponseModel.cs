﻿using JerryPlat.Utils.Helpers;

namespace JerryPlat.Utils.Models
{
    public class ResponseModel<T>
    {
        public static ResponseModel<T> Ok(T data, string Message = "")
        {
            if (data == null)
            {
                return NotFound(Message);
            }

            return new ResponseModel<T>
            {
                Status = ConstantHelper.Ok,
                Message = Message,
                Data = data
            };
        }

        public static ResponseModel<T> Confirm(string Message = "")
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.Confirm,
                Message = Message
            };
        }

        public static ResponseModel<T> Error(string errorMessage)
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.Error,
                Message = errorMessage,
                Data = default(T)
            };
        }

        public static ResponseModel<T> Invalid(string errorMessage)
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.Invalid,
                Message = errorMessage,
                Data = default(T)
            };
        }

        public static ResponseModel<T> NotFound(string strMessage = "")
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.NotFound,
                Message = strMessage,
                Data = default(T)
            };
        }

        public static ResponseModel<T> Existed(string strMessage = "")
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.Existed,
                Message = strMessage,
                Data = default(T)
            };
        }

        public static ResponseModel<T> Logout(T data, string strMessage = "")
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.Logout,
                Message = strMessage,
                Data = data
            };
        }

        public static ResponseModel<T> RefreshTokenFaild(string errorMessage)
        {
            return new ResponseModel<T>
            {
                Status = ConstantHelper.RefreshTokenFaild,
                Message = errorMessage,
                Data = default(T)
            };
        }

        public string Status { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }

    public class ResponseModel : ResponseModel<string> { }
}