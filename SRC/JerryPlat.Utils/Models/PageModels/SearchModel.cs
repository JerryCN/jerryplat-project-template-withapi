﻿using JerryPlat.Utils.Attributes;
using System;

namespace JerryPlat.Utils.Models
{
    public class SearchModel : BaseSearchModel
    {
        public static SearchModel Instance = new SearchModel();

        public int Id1 { get; set; }
        public int Id2 { get; set; }
        public int Id3 { get; set; }
        public int Id4 { get; set; }
        public string SearchText { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        [IgnoreWatch]
        public string FileName { get; set; }
    }
}