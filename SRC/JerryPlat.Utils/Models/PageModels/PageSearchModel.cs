﻿namespace JerryPlat.Utils.Models
{
    public class PageSearchModel
    {
        public SearchModel SearchModel { get; set; }
        public PageParam PageParam { get; set; }

        public PageSearchModel()
        {
            SearchModel = new SearchModel();
            PageParam = new PageParam();
        }
    }
}