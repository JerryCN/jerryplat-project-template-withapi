﻿using JerryPlat.Utils.Attributes;

namespace JerryPlat.Utils.Models
{
    public class BaseSearchModel : ISearchModel
    {
        public int Id { get; set; }
        [IgnoreWatch]
        public string Sort { get; set; }
    }
}